#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 24 15:32:45 2019

@author: doctor
"""

from calculo_intersismico import cut_file

filename="./DatosBase/strike_fosa_chile.csv"
data_type = {'Latitud': float,
                     'Longitud': float,
                     'Strike': float,
                     'Dip': float,
                     'Profundidad': float}
sarna=cut_file(filename,data_type,limites=(-28,-29))
stk=[elem.get('Strike') for elem in sarna ]
stkprima=[elem.get('Strike') for elem in sarna if str(elem.get('Strike')) != 'nan']