from calculo_intersismico import Intersismico
import numpy as np
if __name__ == "__main__":
    params = {
        'zona':'Nicoya',
        'limites_zona': (10,8.9),
        'stations': [],
        'filtro':'cosismico',
        'inversion': False,
        'grid_size': (12,12),
        'plano_CD': False,
        'plano_E': False,
        'alto_placa': 12000,
        'vel_placa': 0.085,
        'rake_referencia': 123,
        'dip_inicial': np.radians(12),
        'condicion_borde_fosa':True,
        'ID': '/sali_nicoya',
        'file_fosa':'./DatosBase/Nicoya/slab_trench_Nicoya.csv',
        'file_profundidad':'./DatosBase/Nicoya/slab_depth_Nicoya.csv',
        'file_strike':'./DatosBase/Nicoya/slab_strike_Nicoya.csv',
        'file_dip':'./DatosBase/Nicoya/slab_dip_Nicoya.csv',

        'desplazamientos_obs.txt': './Fuentes_Sali/datos_Nicoya_2.txt'
             }
    intersismico = Intersismico(**params)
    map_params = {
        'latmin': 7,
        'latmax': 11,
        'lonmin':  -88,
        'lonmax': -84,
        "paralelos": [ 8, 9, 10],
        "meridianos": [-87,-86, -85],
                  }
    ## -40 -20 -80 -65
    construir_params={
            'AB': True,
            'CD': False,
            'E' : False   }
    intersismico.set_map_params(**map_params)
    #intersismico.build_map()
    print("Planos de falla data")
    intersismico.plano_falla_ab()
    #intersismico.plano_falla_cd()
    #intersismico.plano_falla_e()
    print("Construyendo planos")
    intersismico.construye_A()
    #intersismico.construye_B()
    #intersismico.construye_CD()
    #intersismico.construye_E()
    #intersismico.construye_planos()
#   
#    print('Hacer problema directo')
#    pdirecto_params={'interfases':'single','save_slips':True}
#    intersismico.make_directo(**pdirecto_params)
###    
##    print('Mapa de las fallas')
#    geom_params={'plot_fosa':True,
#                 'plot_contourf':True,
#                 'interfaz':'upper'}
#    intersismico.mapa_geometria(**geom_params)
#####
    print("Haciendo inversion")
    inversion_params={'interfases':'single'}
    intersismico.inversion_intersismico_slab(**inversion_params)
    almacenar_params={'interfases':'single'}
    intersismico.almacenar_resultados(**almacenar_params)
###
###
####    print('Hacer problema directo')
####    pdirecto_params={'interfases':'single','save_slips':True}
####    intersismico.make_directo(**pdirecto_params)
#####    
    print('Mapa de las fallas')
    geom_params={'plot_fosa':True,
                 'plot_contourf':False,
                 'interfaz':'upper',
                 'datos_and_inv':True,
                 'quiverkey':False,
                 'plot_datos':False,
                 'which_datos':'directas'
                 }
    intersismico.mapa_geometria(**geom_params)
####
#    #print('Mapa de velocidades sinteticas')
    #directo_params={'tipo':'sintetico','datos':'dummy' }
    #intersismico.mapa_velocidades_sinteticas(**directo_params)
    

   
    #print('Mapa de velocidades sinteticas')
    #intersismico.mapa_velocidades_sinteticas() 
#    print("Haciendo inversion")
#    inversion_params={'interfases':'single'}
#    intersismico.inversion_intersismico_slab(**inversion_params)
#    #intersismico.calcula_inversion()
#    print('creando outputs')

#    print('Mapa de velocidades sinteticas')
#    sint_param={'tipo': 'ambos','datos':'directo_postinverso'}
#    intersismico.mapa_velocidades_sinteticas(**sint_param)
    
#
#    
