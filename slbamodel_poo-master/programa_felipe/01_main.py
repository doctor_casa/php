# -*- coding: utf-8 -*-

#from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import subroutine as sub
import numpy as np

'''
#############################################################################
Felipe Vera S. - 2017
Consultas o preguntas a fveras@udec.cl/fvera.sanhueza@gmail.com
#############################################################################
'''

'''
###############################################################################
0. ZONA DE SUBDUCCION
###############################################################################
'''

#zona de subduccion
zona_subduccion    = 'Ecuador_Colombia'

#Sentido de la subduccion
sentido_subduccion = 'WE'      #   EW  : Sentido de subduccion East-West o Este-Oeste (Ej. Japon)
                               #   WE  : Sentido de subduccion West-East o Oeste-Este (Ej. Chile, Ecuador_Colombia) 

generar_inversion  = True     #  Activa o desactiva el procedimiento de inversion de datos.
                               #  Cuando esta desactivada, solo se genera la geometria de subduccion.

condicion_borde_fosa = True

'''
-----------------------------------------------------------------------------
#############################################################################
                         PARAMETROS DE ENTRADA
#############################################################################
-----------------------------------------------------------------------------
'''

'''
#############################################################################
1. PARAMETROS: LECTURA DE DATOS
#############################################################################
'''

print ' ---------------------------------------------------------------------'
print ' ETAPA  1 == Leer datos                                               '
print ' ---------------------------------------------------------------------'

#1. Datos asociados a la distribucion de la fosa en superficie (Slab 1.0).
fosa = np.loadtxt('1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/4_slab_model_'+zona_subduccion+'_trench_interpolation.txt')    #<-Se cargan datos de la fosa.
fosa_lon = fosa[:,0] ; fosa_lat = fosa[:,1]  

#2. Datos de velocidades intersismicas observadas desde receptores GPS.
data   = np.loadtxt('1_Datos/2_GPS_data/vel_colombia.txt');   # Velocidades_Colombia.txt');
lon    = data[:,1]            #<-Longitud de estaciones [grados]
lat    = data[:,0]            #<-Latitud de estaciones [grados]
Ue_obs = data[:,2]            #<-Componente Este de los datos [metros/agno]
Un_obs = data[:,3]            #<-Componente Norte de los datos [metros/agno]
Uz_obs = []                   #<-Componente Vertical de los datos [metros/agno]

#Numero de dimensiones referente a la cantidad de componentes de los registros 
#GPS a utilizar en la inversion (Un, Ue y Uz).
n_dim = 2       #Ue ; Un

#Directorios con datos de profundidad, dip y strike desde modelo Slab 1.0
dir_depth_slab_1_0   = '1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/1_slab_model_depth_data_'+zona_subduccion+'.txt'    #<-Profundidad
dir_dip_slab_1_0     = '1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/2_slab_model_dip_data_'+zona_subduccion+'.txt'      #<-dip
dir_strike_slab_1_0  = '1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/3_slab_model_strike_data_'+zona_subduccion+'.txt'   #<-Strike 

'''
#############################################################################
2. PARAMETROS: PLANOS DE FALLA A, B, C, D y E
#############################################################################

                    TRENCH
___OCEAN PLATE________|________CONTINENTAL PLATE______
         Vp -->        -
_______PLANO E (NORMAL)  ___ _     - <-- PLANO A (INVERSA)
                               -     -
                                 -     -
           PLANO B (NORMAL) -->    -     -
                                     ------
                                      -     -
                 PLANO D (NORMAL)->     - Vp  -  <- PLANO C (INVERSA)
                                          -     -

-------------------------------------------------------------------------------
2.1 PARAMETROS: PLANO DE FALLA A y B 
-------------------------------------------------------------------------------
A : PLANO DE FALLA CON MOVIMIENTO RELATIVO DE TIPO INVERSO
B : PLANO DE FALLA CON MOVIMIENTO RELATIVO DE TIPO NORMAL
-------------------------------------------------------------------------------
'''

print ' -----------------------------------------------------------------------'
print ' ETAPA  2.1 == Obteniendo parametros: Plano de falla A y B              '
print ' -----------------------------------------------------------------------'

V_placa           = 0.047                                     #<-Velocidad de convergencia en metros/agno
lonf              = -81.291709  + 0.2  ; latf = -1.000300    #<-Limite inferior de la fosa. 
lon1              = -79.148690 + 0.2  ; lat1 = 3.000700      #<-Limite superior de la fosa.   
W                 = 260000                                   #<-Ancho deL plano de falla A y B en sentido del downdip [metros].
rake_inv_ref_deg  = 123                                      #<-Rake de referencia para el movimiento relativo de tipo inverso [grados].
H                 = 16000                                    #<-Espesor de placa [metros].
nx                = 30                                       #<-Numero de subfalla en sentido del strike del plano de falla.
ny                = 14                                       #<-Numero de subfallas en sentido del dip del plano de falla. 
delta_lat         = 0.08                                     #<-Angulo de inclinacion de subfallas de los planos A y B con respecto a la fosa.

dip_plano_inicial = np.radians(12.)                          #<-Dip en grados base para la construcion de subfallas de los planos A y B.                                 
#Obs: Dip_plano_inicial solo para iniciar la generacion de subfallas. Posteriormente, se obtiene un dip
#independiente para cada subfalla desde el modelo Slab 1.0.

'''
-------------------------------------------------------------------------------
2.2 PARAMETROS: PLANO DE FALLA C y D 
-------------------------------------------------------------------------------
C : PLANO DE FALLA CON MOVIMIENTO RELATIVO DE TIPO INVERSO
D : PLANO DE FALLA CON MOVIMIENTO RELATIVO DE TIPO NORMAL
-------------------------------------------------------------------------------
'''

print ' -----------------------------------------------------------------------'
print ' ETAPA  2.2 == Obteniendo parametros: Falla C y Falla D                 '
print ' -----------------------------------------------------------------------'

ny_CD = 13        #<-Numero de subfallas en sentido del dip del plano de falla
#Obs: EL numero de subfalla en sentido del strike de los planos de falla C y D 
#queda determinado por el valor asignado en los planos de falla A y B.
w_CD  = W/ny     #<-Ancho subfallas plano C y D equivalente al de las subfallas de los planos C y D.
                 #  El area de subfallas de los planos  C y D es equivalente al de los planos A y B.
 
'''
-------------------------------------------------------------------------------
2.3 PARAMETROS: PLANO DE FALLA E "OUTER TRENCH" 
-------------------------------------------------------------------------------
E : PLANO DE FALLA CON MOVIMIENTO RELATIVO DE TIPO NORMAL
-------------------------------------------------------------------------------
'''

print ' -----------------------------------------------------------------------'
print ' ETAPA  2.3 == Obteniendo parametros: Falla E                          '
print ' -----------------------------------------------------------------------'
 

loninf_E      = -81.291709  - 0.3   ; latinf_E = -1.000300     #<-Limite inferior de la fosa. 
lonsup_E      = -79.148690  - 0.3   ; latsup_E = 3.000700      #<-Limite superior de la fosa.  

W_E           = 300*1000                                       #<-Ancho deL plano de falla E [metros].
H_E           = H                                              #<-Espesor de placa [metros].
nx_E          = nx                                             #<-Numero de subfalla en sentido del strike del plano de falla
ny_E          = 1                                              #<-Numero de subfallas en sentido del dip del plano de falla.
delta_lat_E   = -0.19                                          #<-Angulo de inclinacion de subfallas del plano E con respecto a la fosa.

'''
###############################################################################
3. PARAMETROS DE INVERSION: CONSTANTES DE REGULARIZACION
###############################################################################
'''


#Constantes de regularizacion para el plano de falla A.
lmbd1     = 0.02                               #<-Minimizacion.
lmbd2     = 0.01                               #<-Suavidad.

#Constantes de regularizacion para el plano de falla B.
lmbd3     = 0.02                               #<-Minimizacion.
lmbd4     = 0.01                               #<-Suavidad.
#Obs: lmbd1 = lmbd3 ; lmbd2 = lmbd4

'''
#############################################################################
4. PARAMETROS GRAFICAS
#############################################################################
'''

#1. Distribucion de coordenadas para el margen del mapa.
latmin_map = -3               #<-Latitud inferior (grados)
latmax_map = 4             #<-Latitud superior (grados)
lonmin_map = -83              #<-Longitud minima (grados)
lonmax_map = -77              #<-Longitud maxima (grados)

#2. Paralelos y meridianos para representacion grafica.
paralelos  = [3,2,1,0,-1,-2,-3,-6]  #<-Paralelos (grados) 
meridianos = [-81, -79]      #<-Meridianos (grados)

'''
-----------------------------------------------------------------------------
#############################################################################
                CONSTRUCTOR PLANOS DE FALLA A, B, C, D, E
#############################################################################
-----------------------------------------------------------------------------
'''

'''
###############################################################################
5.                CONSTRUCTOR PLANOS DE FALLA A Y B: SLAB 1.0 MODEL
###############################################################################
Se establece geometria y distribucion de subfallas para los planos de falla A 
y B con respecto a los datos de profundidad, dip y strike desde el modelo 
slab 1.0.
PROCEDIMIENTO.
   1. Se define geometria de fallamiento para el plano B con un movimiento
      relativo de tipo normal.
   2. Se establecen subfallas para un dip constante bajo una distribucion lineal
      de profundidad con respecto al angulo de buzamiento/dip constante.
   3. Se redistribuyen las subfallas con especto al modelo Slab 1.0. Se obtienen
      subfallas con variaciones en profundidad, dip y strike segun la region
      de estudio.
   4  Se establecen variaciones en el rake del plano de falla.
###############################################################################

-------------------------------------------------------------------------------
Geometria compartida entre la falla inversa y la normal
-------------------------------------------------------------------------------
'''   

print ' -----------------------------------------------------------------------'
print ' ETAPA  3 == Constructor: PLanos de falla A y B                                   '
print ' -----------------------------------------------------------------------'
 
L, strike_aux_deg, backstrike = sub.vinc_dist(  latf,  lonf,  lat1,  lon1 )     #<-Largo y strike del plano de falla
#Obs: Strike para la contruccion deL plano de falla. Posteriormente se implementan
#variaciones en strike desde el modelo slab 1.0.

if sentido_subduccion == 'EW':
    strike_rad = np.radians(strike_aux_deg+180)                                  #<-Strike o rumbo en radianes del plano de falla.
if sentido_subduccion == 'WE':
    strike_rad = np.radians(strike_aux_deg)                                      #<-Strike o rumbo en radianes del plano de falla.
  
'''
-------------------------------------------------------------------------------
1  Geometria asociada al plano de falla B con movimiento relativo de tipo normal.
-------------------------------------------------------------------------------
'''

rake_norm_ref_deg=rake_inv_ref_deg+180                 #<-Rake de referencia para los planos de falla con
                                                       #  movimiento relativo de tipo normal (en radianes)
'''
-------------------------------------------------------------------------------
2 Se obtiene distribucion de las subfallas para un dip constante y bajo
una distribucion segun la configuracion de la fosa.
-------------------------------------------------------------------------------
'''

if sentido_subduccion == 'EW':
    #Se determina el origen geometrico base del plano de falla.
    #Obs. Sistema de referencia de Okada.
    lat0_ini,  lon0_ini,  alpha21  = sub.vinc_pt( lat1, lon1, np.degrees(strike_rad+np.pi/2), W*np.cos(dip_plano_inicial) )  
    #Se establecen las coordenadas de las subfallas con respecto a la configuracion de la fosa
    aux, aux, vertices_plano_inicial_lon, vertices_plano_inicial_lat =sub.coordenadas_subfallas_EW(ny,nx,dip_plano_inicial,W,L,lon0_ini,lat0_ini,strike_rad,fosa_lon,fosa_lat,delta_lat)
if sentido_subduccion == 'WE':
    #Se determina el origen geometrico base del plano de falla.
    #Obs. Sistema de referencia de Okada.    
    lat0_ini,  lon0_ini,  alpha21  = sub.vinc_pt( latf, lonf, np.degrees(strike_rad+np.pi/2), W*np.cos(dip_plano_inicial) )  
    #Se establecen las coordenadas de las subfallas con respecto a la configuracion de la fosa
    aux, aux, vertices_plano_inicial_lon, vertices_plano_inicial_lat =sub.coordenadas_subfallas(ny,nx,dip_plano_inicial,W,L,lon0_ini,lat0_ini,strike_rad,fosa_lon,fosa_lat,delta_lat)

'''
-----------------------------------------------------------------------------
3 Se reordenan las subfallas con respecto a los datos de profundidad y dip
del modelos Slab 1.0.
-----------------------------------------------------------------------------
'''

subfallas   = sub.subfallas_SlabModel(nx,ny,W,L,strike_rad,vertices_plano_inicial_lon, vertices_plano_inicial_lat,dir_depth_slab_1_0, dir_dip_slab_1_0,dir_strike_slab_1_0)

profundidad_subfallas_inversa  = subfallas.Z                 #<-Profundidad de subfallas del plano de falla A 
dip_subfallas_radianes         = subfallas.dip_slab_rad      #<-Dip de subfallas correspondientes a los planos de falla A y B.
strike_subfallas_deg           = subfallas.strike_deg        #<-Strike de subfallas correspondientes a los planos de falla A y B.
lon_vertices_subfallas_slab    = subfallas.vert_lon_new      #<-Longitud de los vertices de subfallas segun el slab 1.0 Model
lat_vertices_subfallas_slab    = subfallas.vert_lat_new      #<-Latitud de los vertices de subfallas segun el slab 1.0 Model.
lon_central_subfallas_slab     = subfallas.lon_central_new   #<-Longitud central de las subfallas con respecto al Slab 1.0 Model.
lat_central_subfallas_slab     = subfallas.lat_central_new   #<-Latitud central de las subfallas con respecto al Slab 1.0 Model.

#Se establece profundidad de las subfallas del plano de falla B en relacion al espesor de placa.
profundidad_subfallas_normal   = profundidad_subfallas_inversa + (H / np.cos(dip_subfallas_radianes))

'''
-----------------------------------------------------------------------------
4 Se obtienen variaciones en el rake de cada subfalla con respecto al sentido 
de convergencia y el strike del plano de falla
-----------------------------------------------------------------------------
'''

#Strike del vector de convergencia de la placa                 A
phi_placa_rad = np.radians(np.degrees(strike_rad) + (360-(rake_inv_ref_deg+180)))

rake_inv_rad  = []     #<-Rake para subfallas de la interfase superior con un movimiento relativo de tipo inverso.
rake_norm_rad = []     #<-Rake para subfallas de la interfase inferior con un movimiento relativo de tipo normal.

print '  Hasta ACA llega OMAR'
print ' len(strike_subfallas_deg) '

print ' --------------------'

for subfalla in range(len(strike_subfallas_deg)):
    #Se establece rake en subfallas emplazadas en la interfase superior con un movimiento relativo de tipo inverso
    rake_inv_rad.append(sub.get_rake(phi_placa_rad, np.radians(strike_subfallas_deg[subfalla]), dip_subfallas_radianes[subfalla]))
    #Se establece rake en subfallas emplazadas en la interfase inferior con un movimiento relativo de tipo normal
    rake_norm_rad.append(rake_inv_rad[subfalla]+np.pi) 

#name='test.txt'
#file = open(name, "w")
#for i in range(len(rake_inv_rad)):
#    file.write(str(strike_subfallas_deg[i])+' '+str(np.degrees(rake_inv_rad[i])))    
#    file.write('\n')
#file.close()

'''
#############################################################################
6                CONSTRUCTOR PLANOS DE FALLA C Y D: SLAB 1.0 MODEL
#############################################################################
'''

print ' ---------------------------------------------------------------------'
print ' ETAPA  4 == Constructor: Planos de falla C y D                       '
print ' ---------------------------------------------------------------------'

#1.2 Se generan vertices de las subfallas de acuerdo a la geometria rectangular.
COL=5      #Cada subfalla tiene cinco vertices a dibujar (el ultimo coincide con el primero)

FIL=ny_CD*nx
#Se declaran variables para almacenar vertices de todas las subfallas de los
#planos de falla C y D.
lat_vert_CD_init    = []                #<-Latitud de vertices de subfallas.
lon_vert_CD_init    = []                #<-Longitud de vertices de subfallas.
lat_central_CD_init = []                #<-Latitud de la posicion central de subfallas.
lon_central_CD_init = []                #<-Longitud de la posicion central de subfallas.

#Se inicializan coordenadas de los vertices de subfallas.
for i in range(FIL): 
    lon_vert_CD_init.append([0]*COL)    
    lat_vert_CD_init.append([0]*COL)  
            
#Se establece angulo de construcion (beta) dado por el strike desde las subfallas
#adyacentes de los planos de falla A y B.
alpha = np.degrees(strike_rad) ; beta = alpha + 90

#Se constuyen vertices.
import time    
inicio = time.time()

subfalla = 0
for i_nx in range(nx): 

   for j_ny in range(ny_CD):                   
    #Se construye primera subfalla adyacente a la primera fila de los planos
    #A y B. Se mantiene como ancho, el mismo valor asignado a las subfallas de
    #las regiones A y B.
        if j_ny == 0:        
            #----------------------------------------------------------------
            #Se construyen listas para almacenar vertices constructores. Estos, 
            #se utilizaran para evaluar la diponibilidad de datos para esta 
            #subfalla desde el modelo slab 1.0.        
      
            #Vertice 2
            lat_vert_CD_init[subfalla][2] = lat_vertices_subfallas_slab[i_nx][1]
            lon_vert_CD_init[subfalla][2] = lon_vertices_subfallas_slab[i_nx][1]
            
            #Vertice 3
            lat_vert_CD_init[subfalla][3] = lat_vertices_subfallas_slab[i_nx][0]
            lon_vert_CD_init[subfalla][3] = lon_vertices_subfallas_slab[i_nx][0]
        
            #Vertice 0
            lat_vert_CD_init[subfalla][0], lon_vert_CD_init[subfalla][0], aux = sub.vinc_pt( lat_vert_CD_init[subfalla][3], lon_vert_CD_init[subfalla][3], beta , w_CD)
    
            #Vertice 1
            lat_vert_CD_init[subfalla][1], lon_vert_CD_init[subfalla][1], aux = sub.vinc_pt( lat_vert_CD_init[subfalla][2], lon_vert_CD_init[subfalla][2], beta , w_CD)

            #Veretice 4
            lat_vert_CD_init[subfalla][4] = lat_vert_CD_init[subfalla][0]
            lon_vert_CD_init[subfalla][4] = lon_vert_CD_init[subfalla][0]
            
        else:
        
            #------------------------------------------------------------------
            #Se construyen listas para almacenar vertices constructores. Estos, 
            #se utilizaran para evaluar la diponibilidad de datos para esta 
            #subfalla desde el slab 1.0 model.        
             
            #Vertice 2
            lat_vert_CD_init[subfalla][2] = lat_vert_CD_init[subfalla-1][1]
            lon_vert_CD_init[subfalla][2] = lon_vert_CD_init[subfalla-1][1]
            
            #Vertice 3
            lat_vert_CD_init[subfalla][3] = lat_vert_CD_init[subfalla-1][0]
            lon_vert_CD_init[subfalla][3] = lon_vert_CD_init[subfalla-1][0]
        
            #Vertice 0
            lat_vert_CD_init[subfalla][0], lon_vert_CD_init[subfalla][0], aux = sub.vinc_pt( lat_vert_CD_init[subfalla][3], lon_vert_CD_init[subfalla][3], beta , w_CD)
    
            #Vertice 1
            lat_vert_CD_init[subfalla][1], lon_vert_CD_init[subfalla][1], aux = sub.vinc_pt( lat_vert_CD_init[subfalla][2], lon_vert_CD_init[subfalla][2], beta , w_CD) 

            #Vertice 4
            lat_vert_CD_init[subfalla][4] = lat_vert_CD_init[subfalla][0]
            lon_vert_CD_init[subfalla][4] = lon_vert_CD_init[subfalla][0]
            
        subfalla = subfalla + 1

subfalla_CD  = sub.subfallas_SlabModel(nx,ny_CD,w_CD*ny_CD,L,strike_rad,lon_vert_CD_init, lat_vert_CD_init,dir_depth_slab_1_0, dir_dip_slab_1_0,dir_strike_slab_1_0)

Z_inversa_CD_final     = subfalla_CD.Z                 #<-Profundidad de subfallas del plano de falla C
dip_radianes_CD_final  = subfalla_CD.dip_slab_rad      #<-Dip de subfallas correspondientes a los planos de falla C y D.
strike_CD_final_deg    = subfalla_CD.strike_deg        #<-Strike de subfallas correspondientes a los planos de falla C y D.
lon_vert_CD_final      = subfalla_CD.vert_lon_new      #<-Longitud de los vertices de subfallas segun el slab 1.0 Model.
lat_vert_CD_final      = subfalla_CD.vert_lat_new      #<-Latitud de los vertices de subfallas segun el slab 1.0 Model.
lon_central_CD_final   = subfalla_CD.lon_central_new   #<-Longitud central de las subfallas con respecto al Slab 1.0 Model.
lat_central_CD_final   = subfalla_CD.lat_central_new   #<-Latitud central de las subfallas con respecto al Slab 1.0 Model.       

#Se establece profundidad de las subfallas del plano de falla D en relacion al espesor de placa.
Z_normal_CD_final      = Z_inversa_CD_final + (H / np.cos(dip_radianes_CD_final))

'''
-----------------------------------------------------------------------------
Se obtienen variaciones en el rake de cada subfalla con respecto al sentido 
de convergencia y el strike del plano de falla
-----------------------------------------------------------------------------
'''

rake_inv_CD_rad  = []      #<-Rake para subfallas de la interfase superior (C) con un movimiento relativo de tipo inverso.
rake_norm_CD_rad = []      #<-Rake para subfallas de la interfase inferior (D) con un movimiento relativo de tipo normal.

for subfalla in range(len(strike_CD_final_deg)):
    #Se establece rake en subfallas emplazadas en la interfase superior con un movimiento relativo de tipo inverso
    rake_inv_CD_rad.append(sub.get_rake(phi_placa_rad, np.radians(strike_CD_final_deg[subfalla]), dip_radianes_CD_final[subfalla]))
    #Se establece rake en subfallas emplazadas en la interfase inferior con un movimiento relativo de tipo normal
    rake_norm_CD_rad.append(rake_inv_CD_rad[subfalla]+np.pi)                
    
'''
################################################################################
7.                CONSTRUCTOR PLANO DE FALLA E: SLAB 1.0 MODEL
################################################################################
#'''

print ' -----------------------------------------------------------------------'
print ' ETAPA  5 == Constructor: Falla E              '
print ' -----------------------------------------------------------------------'

if sentido_subduccion == 'EW':
    #Se obtienen coordendas de subfallas y goemetria asociada al plano de falla E.
    lon_vertices_E, lat_vertices_E,L_E,strike_E_rad = sub.constructor_fallaE_EW(loninf_E, latinf_E, lonsup_E, latsup_E,V_placa,W_E,H_E,nx_E,ny_E,delta_lat_E,fosa_lon,fosa_lat)
if sentido_subduccion == 'WE':
    #Se obtienen coordendas de subfallas y goemetria asociada al plano de falla E.
    lon_vertices_E, lat_vertices_E,L_E,strike_E_rad = sub.constructor_fallaE_WE(loninf_E, latinf_E, lonsup_E, latsup_E,V_placa,W_E,H_E,nx_E,ny_E,delta_lat_E,fosa_lon,fosa_lat)

if generar_inversion == True:
    
    '''
    ###############################################################################
    8.           PROCESO DE INVERSION: PLANOS DE FALLA A Y B
    ###############################################################################
    '''

    print ' -----------------------------------------------------------------------'
    print ' ETAPA  6.1 == Caracterizacion geometrica: A y B                        '
    print ' -----------------------------------------------------------------------'

    #2. Caracterizacion geometrica para el plano de falla A
    print '6.1.1 Calculando matriz A...'
    A = sub.model_matrix_slab(n_dim,dip_subfallas_radianes,profundidad_subfallas_inversa,W,L,rake_inv_rad,np.radians(strike_subfallas_deg),nx,ny,lon_vertices_subfallas_slab,lat_vertices_subfallas_slab,lon,lat)

    #3. Caracterizacion geometrica para el plano de falla B
    print '6.1.2 Calculando matriz B...'
    B = sub.model_matrix_slab(n_dim,dip_subfallas_radianes,profundidad_subfallas_normal,W,L,rake_norm_rad,np.radians(strike_subfallas_deg),nx,ny,lon_vertices_subfallas_slab,lat_vertices_subfallas_slab,lon,lat)
    
    print ' -----------------------------------------------------------------------'
    print ' ETAPA  6.2 == Caracterizacion geometrica: C y D                        '
    print ' -----------------------------------------------------------------------'    
   
    l_CD             = L/nx                         #<-Largo de cada subfalla en sentido del strike.
    num_subfallas_CD = len(dip_radianes_CD_final)   #<-Numero de subfallas del plano de falla C y D.

    #1. Caracterizacion geometrica para el plano de falla C
    print '6.2.1 Calculando matriz C...'
    C = sub.model_matrix_slab_CD(n_dim,dip_radianes_CD_final,Z_inversa_CD_final,w_CD,l_CD,rake_inv_CD_rad,np.radians(strike_CD_final_deg),num_subfallas_CD,lon_vert_CD_final,lat_vert_CD_final,lon,lat)

    #2. Caracterizacion geometrica para el plano de falla D
    print '6.2.2 Calculando matriz D...'
    D = sub.model_matrix_slab_CD(n_dim,dip_radianes_CD_final,Z_normal_CD_final,w_CD,l_CD,rake_norm_CD_rad,np.radians(strike_CD_final_deg),num_subfallas_CD,lon_vert_CD_final,lat_vert_CD_final,lon,lat)
    
    print ' -----------------------------------------------------------------------'
    print ' ETAPA  6.3 == Caracterizacion geometrica: E                            '
    print ' -----------------------------------------------------------------------'       
    
    #1. Caracterizacion geometrica para el plano de falla E
    print '6.3.1 Calculando matriz E...'    
    E = sub.model_matrix_slab_E(n_dim,0,H_E,W_E,L_E,np.radians(rake_norm_ref_deg),strike_E_rad,nx_E,ny_E,lon_vertices_E,lon_vertices_E,lon,lat)
    
    '''
    ################################################################################
    7.                         PROCESO DE INVERSION
    ################################################################################
    '''    

    print ' -----------------------------------------------------------------------'
    print ' ETAPA  7 == Proceso de inversion                                       '
    print ' -----------------------------------------------------------------------'

    #Se establecen velocidades libres en los planos de falla C, D y E.
    print '7.1 Incluyendo velocidad de placa     ...'        

    #Velocidades libres plano de falla C y D.
    for subfalla in range(num_subfallas_CD):  
        if subfalla == 0:
          Vp_CD = V_placa
        else:
          Vp_CD = np.hstack((Vp_CD,V_placa)) 
    
    #Velocidades libres plano de falla E.    
    for subfalla in range(nx_E*ny_E):
        if subfalla == 0:
          Vp_E = V_placa
        else:
          Vp_E = np.hstack((Vp_E,V_placa))  
      
    '''
    #############################################################################
    SE REALIZA INVERSION
    #############################################################################
    '''

    #Se realiza inversion de datos
    print '7.2 Realizando inversion              ...'
    slip_rate_aux,horror, horror_slip, horror_suave, U_corregido = sub.inversion_intersismico_slab(n_dim,Ue_obs,Un_obs,Uz_obs,A,B,C,D,E,Vp_CD,Vp_E,lmbd1,lmbd2,lmbd3,lmbd4,dip_subfallas_radianes ,W,L,nx,ny,V_placa,condicion_borde_fosa)

    '''
    #############################################################################
    ERROR CUADRATICO MEDIO
    #############################################################################
    '''
    print '7.3 Obteniendo Error Cuadratico Medio ... '
    
    Ue_aux = []
    Un_aux = []

    k = len(Ue_obs)
    for i in range(len(Ue_obs)):
       Ue_aux.append(U_corregido[i])
       Un_aux.append(U_corregido[i+k])
    
    #Desplazamientos observados finales
    Ue_obs_final = []
    Un_obs_final = []
    for i in range(len(Ue_obs)):
        if i == 0:
            Ue_obs_final = Ue_aux[i]
            Un_obs_final = Un_aux[i]
        else:
            Ue_obs_final = np.vstack( (Ue_obs_final, Ue_aux[i] ))
            Un_obs_final = np.vstack( (Un_obs_final, Un_aux[i] ))

    #Se establece ECM para el conjunto de observaciones.
    ECM_Ue, ECM_Un =sub.ECM_inversion_slab_intersismico(dip_subfallas_radianes,profundidad_subfallas_inversa,profundidad_subfallas_normal,W,L,rake_inv_rad,rake_norm_rad,np.radians(strike_subfallas_deg),nx,ny,slip_rate_aux,Ue_obs_final,Un_obs_final,Uz_obs,lon_vertices_subfallas_slab,lat_vertices_subfallas_slab,lon,lat)

    #Residual total.
    horror_total=horror+lmbd1*horror_slip+lmbd2*horror_suave

    print ' -----------------------------------------------------------------------'
    print 'Resumen de datos: '
    print '1. lambda1      : ',lmbd1
    print '2. lambda2      : ',lmbd2
    print '3. lambda3      : ',lmbd3
    print '4. lambda4      : ',lmbd4
    print '5. Inv. ECM_Ue  : ',ECM_Ue
    print '6. Inv. ECM_Un  : ',ECM_Un
    print '7. Res. ||U-AS||: ',horror
    print '8. Res. ||S||   : ',horror_slip
    print '9. Res. ||FS||  : ', horror_suave
    print '10. Res. Total  : ',horror_total
    print ' -----------------------------------------------------------------------'
    
    '''
    #############################################################################
    VELOCIDADES MODELADAS DESDE PROBLEMA DIRECTO
    #############################################################################
    '''

    print '7.4 Realizando problema directo ...'

    Ue_teo=[]    #<-Datos modelados en componente este.
    Un_teo=[]    #<-Datos modelados en componente norte.
    
    #Se obtienen velocidades modeladas en superficie desde los resultados de la
    #inversion. Se establece un problema directo.
    Ue_teo,Un_teo,Ue_teo_inv,Un_teo_inv,Ue_teo_norm,Un_teo_norm=sub.Pdirecto_intersismico(dip_subfallas_radianes,profundidad_subfallas_inversa,profundidad_subfallas_normal,W,L,rake_inv_rad,rake_norm_rad,np.radians(strike_subfallas_deg),nx,ny,slip_rate_aux,lon_vertices_subfallas_slab,lat_vertices_subfallas_slab,lon,lat)




    '''
    ###############################################################################
    8.                           ALMACENANDO RESULTADOS 
    ###############################################################################
    '''

    print ' -----------------------------------------------------------------------'
    print ' ETAPA 8 == Almacenando resultados                                      '
    print ' -----------------------------------------------------------------------'

    #1 Se almacenan componentes de velocidades modeladas.
    name='2_Output/1_Inversion/synthetic_component.txt'
    file = open(name, "w")
    for i in range(len(Ue_obs_final)):
        file.write(str(Ue_obs_final[i][0])+' '+str(Un_obs_final[i][0])+' '+str(Ue_teo[i])+' '+str(Un_teo[i])+' '+str(lon[i])+' '+str(lat[i]))    
        file.write('\n')
    file.close()     

    #Se obtienen velocidades invertidas para los planos de falla A y B.
    slip_rate_upper_interface =[]    #Velocidades para plano de falla A
    slip_rate_lower_interface =[]    #Velocidades para plano de falla B
    
    k=nx*ny;
    for i in range(nx*ny):
       slip_rate_upper_interface.append(slip_rate_aux[i])
       slip_rate_lower_interface.append(slip_rate_aux[i+k]);

    #Se almacenan resultados para la interfase superior A con un movimiento relativo de tipo inverso.
    name='2_Output/1_Inversion/interseismic_slip_rate_upper_interface.txt'
    file = open(name, "w")
    for i in range(len(slip_rate_upper_interface)):
        file.write(str(lon_central_subfallas_slab[i])+' '+str(lat_central_subfallas_slab[i])+' '+str(slip_rate_upper_interface[i]))  
        file.write('\n')
    file.close()

    #Se almacenan resultados para la interfase inferior B con un movimiento relativo de tipo normal.
    name='2_Output/1_Inversion/interseismic_slip_rate_lower_interface.txt'
    file = open(name, "w")
    for i in range(len(slip_rate_lower_interface)):
        file.write(str(lon_central_subfallas_slab[i])+' '+str(lat_central_subfallas_slab[i])+' '+str(slip_rate_lower_interface[i]))  
        file.write('\n')
    file.close()


    '''
    #########################################################################
    VELOCIDADES OBSERVADAS Y TEORICAS CORREGIDAS
    #########################################################################
    '''
    
    #observaciones modeladas producto de velocidades en las interfases A y B
    U_teo = np.dot(A,slip_rate_upper_interface) + np.dot(B,slip_rate_lower_interface)
    
    #Observaciones modeladas incluyendo el aporte de las interfases C, D y E
    U_teo_corregido = U_teo + (np.dot(C,Vp_CD) + np.dot(D,Vp_CD) + np.dot(E,Vp_E))

    #Se obtiene componente este y norte.
    Ue_teo_corregido = []
    Un_teo_corregido = []

    k = len(Ue_obs)
    for i in range(len(Ue_obs)):
       #velocidades teoricas incorporando als contribuciones de las interfases C,D y E 
       Ue_teo_corregido.append(U_teo_corregido[i])
       Un_teo_corregido.append(U_teo_corregido[i+k])
      
    name='2_Output/1_Inversion/synthetic_components_corregidas.txt'
    file = open(name, "w")
    for i in range(len(Ue_obs_final)):
        #              Observado  (sin correcciond de interfase C,D y E)  |  Modelado (sumando contribuciones de C, D y E)
        file.write(str(Ue_obs[i])+' '+str(Un_obs[i])+' '+str(Ue_teo_corregido[i])+' '+str(Un_teo_corregido[i])+' '+str(lon[i])+' '+str(lat[i]))    
        file.write('\n')
    file.close()  

'''
###############################################################################
9. GRAFICA CON DISTRIBUCION DE SUBFALLAS
###############################################################################
'''

fig = plt.figure(20)
plt.clf()
ax = fig.add_axes([0.1,0.1,0.8,0.8])
map = Basemap(projection='merc', resolution="h", llcrnrlon=lonmin_map, llcrnrlat=latmin_map, urcrnrlon=lonmax_map, urcrnrlat=latmax_map)                        
map.drawcoastlines(zorder=20)
map.drawcountries(zorder=20)
#Se trazan los paralelos.
parallels = map.drawparallels(paralelos, labels=[1, 1, 0, 0], fmt="%.0f", #4
                dashes=[2, 2],linewidth=0.0,zorder=24)
for m in parallels:
    try:
        parallels[m][1][0].set_rotation(90)     
        parallels[m][1][1].set_rotation(90)  #borrar este       
        
    except:
        pass                 
#Se trazan los meridianos.                
map.drawmeridians(meridianos, labels=[0, 0, 1, 1], fmt="%.0f",
               dashes=[2, 2],linewidth=0.0,zorder=24)
#Se dibuja al fosa.
map.plot(fosa_lon,fosa_lat,'r-',linewidth = 1.5, latlon=True,zorder=22)
#Se grafica la poscicion de estaciones
map.scatter(lon,lat, 18, color="r", marker="o", edgecolor="k",linewidth = 0.4, latlon=True,zorder=22) 

#Distribucion de subfallas para los planos de falla C y D
for i_nx in range(len(lon_vert_CD_final)):
    re = [] ; rn = []
    re.append(lon_vert_CD_final[i_nx][0]) ; rn.append(lat_vert_CD_final[i_nx][0])
    re.append(lon_vert_CD_final[i_nx][1]) ; rn.append(lat_vert_CD_final[i_nx][1])
    re.append(lon_vert_CD_final[i_nx][2]) ; rn.append(lat_vert_CD_final[i_nx][2])
    re.append(lon_vert_CD_final[i_nx][3]) ; rn.append(lat_vert_CD_final[i_nx][3])
    re.append(lon_vert_CD_final[i_nx][4]) ; rn.append(lat_vert_CD_final[i_nx][4])
    map.plot(re,rn,color='r',  linestyle = '-', linewidth = 0.15, latlon = True,zorder=21)

#Distribucion de subfallas para los planos de falla A y B
for i_nx in range(nx*ny):
    re = [] ; rn = []
    re.append(lon_vertices_subfallas_slab[i_nx][0]) ; rn.append(lat_vertices_subfallas_slab[i_nx][0])
    re.append(lon_vertices_subfallas_slab[i_nx][1]) ; rn.append(lat_vertices_subfallas_slab[i_nx][1])
    re.append(lon_vertices_subfallas_slab[i_nx][2]) ; rn.append(lat_vertices_subfallas_slab[i_nx][2])
    re.append(lon_vertices_subfallas_slab[i_nx][3]) ; rn.append(lat_vertices_subfallas_slab[i_nx][3])
    re.append(lon_vertices_subfallas_slab[i_nx][4]) ; rn.append(lat_vertices_subfallas_slab[i_nx][4])
    map.plot(re,rn,color='k',  linestyle = '-', linewidth = 0.25, latlon = True,zorder=21)

#Distribucion de subfallas para el plano de falla E           
for i_nx in range(nx_E*ny_E):
    re = [] ; rn = []
    re.append(lon_vertices_E[i_nx][0]) ; rn.append(lat_vertices_E[i_nx][0])
    re.append(lon_vertices_E[i_nx][1]) ; rn.append(lat_vertices_E[i_nx][1])
    re.append(lon_vertices_E[i_nx][2]) ; rn.append(lat_vertices_E[i_nx][2])
    re.append(lon_vertices_E[i_nx][3]) ; rn.append(lat_vertices_E[i_nx][3])
    re.append(lon_vertices_E[i_nx][4]) ; rn.append(lat_vertices_E[i_nx][4])
    map.plot(re,rn,color='r',  linestyle = '-', linewidth = 0.15, latlon = True,zorder=21)       

if sentido_subduccion == 'EW':
    #Vector de convergencia de placas.                
    theta = (rake_norm_ref_deg - np.radians(180)) - np.radians(np.degrees(strike_rad)-180)
if sentido_subduccion == 'WE':
    #Vector de convergencia de placas.                
    theta = np.radians(rake_norm_ref_deg)  - strike_rad

rotMatrix = np.array([[np.cos(theta), -np.sin(theta)], 
                         [np.sin(theta),  np.cos(theta)]])
c=[0, V_placa]                         
u  = np.dot(rotMatrix, c)
Ux_v = u[0]
Uy_v = u[1]
#print Ux_v, Uy_v
xx,yy=map(-84.5,4)
quiver_obs=map.quiver(xx,yy,Ux_v,Uy_v,color=(0.8,0,0),width=0.0028, linewidth = 0,headwidth = 7.,scale=0.55, zorder=26) #0.5
xx,yy=map(-84.5+0.25 , 4+0.35)                    
plt.text(xx,yy,str(int(V_placa*100))+' cm/yr',fontsize=8,fontweight='bold', ha='center',va='center',color='black',zorder=26,bbox = dict(boxstyle="square",ec='None',fc=(1,1,1,0.5)))

plt.show()    
plt.savefig('FIG.png', format='png', dpi=200,bbox_inches='tight') 
  
print ' -------------------------------------------------------'
print ' ETAPA  9 == RESUMEN PARAMETROS GEOMETRICOS Y RESULTADOS'
print ' -------------------------------------------------------'
print '                                                        '
print ' -------------------------------------------------------'
print 'INTERFASE SUPERIOR (A):                                 '
print ' -------------------------------------------------------'

print 'Min Depth: '+str(min(profundidad_subfallas_inversa)/1000)+' km'
print 'Max Depth: '+str(max(profundidad_subfallas_inversa)/1000)+' km'
print '                                                              '
print 'Min Dip  : '+str(min(np.degrees(dip_subfallas_radianes)))+' degrees'
print 'Max Dip  : '+str(max(np.degrees(dip_subfallas_radianes)))+' degrees'
print '                                                              '
print 'Min Strike : '+str(min(strike_subfallas_deg))+' degrees'
print 'Max Strike : '+str(max(strike_subfallas_deg))+' degrees'
print '                                                              '
print 'Min Rake  : '+str(min(np.degrees(rake_inv_rad)))+' degrees'
print 'Max Rake  : '+str(max(np.degrees(rake_inv_rad)))+' degrees'
print '                                                        '
print ' -------------------------------------------------------'
print 'INTERFASE INFERIOR (B):'
print ' -------------------------------------------------------'
print 'Min Depth: '+str(min(profundidad_subfallas_normal)/1000)+' km'
print 'Max Depth: '+str(max(profundidad_subfallas_normal)/1000)+' km'
print '                                                              '
print 'Min Dip  : '+str(min(np.degrees(dip_subfallas_radianes)))+' degrees'
print 'Max Dip  : '+str(max(np.degrees(dip_subfallas_radianes)))+' degrees'
print '                                                              '
print 'Min Strike : '+str(min(strike_subfallas_deg))+' degrees'
print 'Max Strike : '+str(max(strike_subfallas_deg))+' degrees'
print '                                                              '
print 'Min Rake  : '+str(min(np.degrees(rake_norm_rad)))+' degrees'
print 'Max Rake  : '+str(max(np.degrees(rake_norm_rad)))+' degrees'

if generar_inversion == True:
    print ' -------------------------------------------------------'
    print '          VELOCIDADES INVERTIDAS                         '
    print ' -------------------------------------------------------'
    print ' -------------------------------------------------------'
    print 'INTERFASE SUPERIOR (A):                                 '
    print ' -------------------------------------------------------'
    print 'Max. Vel: ',str(max(slip_rate_upper_interface)),'m/yr'    
    print 'Min. Vel: ',str(min(slip_rate_upper_interface)),'m/yr'    
    print ' -------------------------------------------------------'
    print 'INTERFASE INFERIOR (B):                                 '
    print ' -------------------------------------------------------'
    print 'Max. Vel: ',str(max(slip_rate_lower_interface)),'m/yr'    
    print 'Min. Vel: ',str(min(slip_rate_lower_interface)),'m/yr'   
