from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import subroutine as sub
from matplotlib.mlab import griddata
import numpy as np
from matplotlib.patches import Polygon
from matplotlib import cm
import matplotlib as mpl

'''
#############################################################################
Felipe Vera S. - 2017
Consultas o preguntas a fveras@udec.cl/fvera.sanhueza@gmail.com
#############################################################################
'''

'''
###############################################################################
0. ZONA DE SUBDUCCION
###############################################################################
'''

#zona de subduccion
zona_subduccion    = 'Ecuador_Colombia'

#Sentido de la subduccion
sentido_subduccion = 'WE'      #   EW  : Sentido de subduccion East-West o Este-Oeste (Ej. Japon)
                               #   WE  : Sentido de subduccion West-East o Oeste-Este (Ej. Chile) 
'''
###############################################################################
1. DATOS
###############################################################################
'''

#1. Datos asociados a la distribucion de la fosa en superficie (Slab 1.0).
fosa = np.loadtxt('1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/4_slab_model_'+zona_subduccion+'_trench_interpolation.txt')    #<-Se cargan datos de la fosa.
fosa_lon = fosa[:,0] ; fosa_lat = fosa[:,1]                                                     #<-Latitud y longitud de la configuracion la fosa.

data = np.loadtxt('2_Output/1_Inversion/interseismic_slip_rate_lower_interface.txt');
lon_slip = data[:,0]            #<-Longitud de los datos cargados.
lat_slip = data[:,1]            #<-Latitud de los datos cargados.
slip     = data[:,2]*100        #<-Latitud de los datos cargados.

#Directorios con datos de profundidad, dip y strike desde modelo Slab 1.0
dir_depth_slab_1_0   = '1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/1_slab_model_depth_data_'+zona_subduccion+'.txt'    #<-Profundidad
dir_dip_slab_1_0     = '1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/2_slab_model_dip_data_'+zona_subduccion+'.txt'      #<-dip
dir_strike_slab_1_0  = '1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/3_slab_model_strike_data_'+zona_subduccion+'.txt'   #<-Strike 

#Coordenadas de estaciones
GPS = np.loadtxt('2_Output/1_Inversion/synthetic_component.txt');
lon = GPS[:,4]                 #<-Longitud de los datos cargados.
lat = GPS[:,5]                 #<-Longitud de los datos cargados.
Ue_obs = GPS[:,0]              #<-Componente Este de los datos [en metros]
Un_obs = GPS[:,1]              #<-componente Norte de los datos [en metros]
Ue_teo = GPS[:,2]              #<-Componente Este de los datos [en metros]
Un_teo = GPS[:,3]              #<-componente Norte de los datos [en metros]

#Residuales definidos desde desplazamientos teoricos y observados.
res_Ue = []
res_Un = []
for i in range(len(Ue_obs)):
    res_Ue.append(Ue_obs[i]-Ue_teo[i])
    res_Un.append(Un_obs[i]-Un_teo[i])

'''
###############################################################################
1. PARAMETROS MAPA
###############################################################################
'''

poligono         = True                   #<-Activa poligono latarales al plano de falla    
paleta           = plt.cm.hot_r           #<-Paleta de colores para graficar el deslizamiento
 
#Slip Contourf
min_slip         = 0                      #<-Minimo slip a considerar en el contourf
max_slip         = 20                     #<-Maximo slip a considerar en el contourf

#Discretizacion en niveles para contourf:
#cada par de numeros consecutivos define un nivel de color distinto.
niveles_contourf = np.linspace(min_slip, max_slip, 250)

#tick para colorbar
tick_colorbar    = [0,5,10,15,20]

#Titulo para colorbar
colorbar_title   = 'Interseismic slip-rate on the lower interface'

#Epicentro
lon_epicentro          = 142.373        #<-Longitud en grados del epicentro.
lat_epicentro          = 38.297         #<-Latitud en grados del epicentro.
#http://earthquake.usgs.gov/earthquakes/eventpage/usp000hvnu#executive

#Distribucion de coordenadas para el margen del mapa.
latmin_map = -7                         #<-Latitud inferior (grados)
latmax_map = 6.5                        #<-Latitud superior (grados)
lonmin_map = -86                        #<-Longitud minima (grados)
lonmax_map = -74                        #<-Longitud maxima (grados)

#Paralelos y meridianos para representacion grafica.
paralelos  = [6, 3,0,-3,-6]     #<-Paralelos (grados) 
meridianos = [-83, -76]         #<-Meridianos (grados)

#Nombre de la figura de salida.
output_name = '2_Output/2_Figuras/B.png'

'''
###############################################################################
2. PARAMETROS: FALLA A, B, C, D y E
###############################################################################


                    TRENCH
___OCEAN PLATE________|________CONTINENTAL PLATE______
               -
____________________     - <-- FALLA A (INVERSA)
                     -     -
                       -     -
  FALLA B (NORMAL) -->   -     -

                            -     -
                              -  H  -  
                                -     -
                                
-------------------------------------------------------------------------------
2.1 PARAMETROS: FALLA A (INVERSA), FALLA B (NORMAL)
-------------------------------------------------------------------------------
'''


print ' -----------------------------------------------------------------------'
print ' ETAPA  2.1 == Obteniendo parametros: Falla A y Falla B                   '
print ' -----------------------------------------------------------------------'
           
lonf              = -81.874813 + 0.2  ; latf = -5.437300     #<-Limite inferior de la fosa. 
lon1              = -77.824402 + 0.2   ; lat1 = 5.301700     #<-Limite superior de la fosa.   
W                 = 260000                                   #<-Ancho deL plano de falla A y B en sentido del downdip [metros].
rake_inv_ref_deg  = 113                                      #<-Rake de referencia para el movimiento relativo de tipo inverso [grados].
H                 = 30000                                    #<-Espesor de placa [metros].
nx                = 30                                       #<-Numero de subfalla en sentido del strike del plano de falla.
ny                = 6                                        #<-Numero de subfallas en sentido del dip del plano de falla. 
delta_lat         = 0.15                                     #<-Angulo de inclinacion de subfallas de los planos A y B con respecto a la fosa.

dip_plano_inicial = np.radians(12.)                          #<-Dip en grados base para la construcion de subfallas de los planos A y B.                                 
#Obs: Dip_plano_inicial solo para iniciar la generacion de subfallas. Posteriormente, se obtiene un dip
#independiente para cada subfalla desde el modelo Slab 1.0.


'''
###############################################################################
3.                CONSTRUCTOR FALLA A Y B: SLAB 1.0 MODEL
###############################################################################
3. Se establece geometria y distribucion de subfallas para las interfases A (inversa)
y B (normal) con respecto a los datos de profundidad, dip y strike desde el modelo slab 1.0.
PROCEDIMIENTO.
   3.1. Se define geometria de fallamiento normal.
   3.2. Se establecen subfallas para un dip constante bajo una distribucion lineal
        de profundidad con respecto al angulo de buzamiento/dip constante.
   3.3. Se redistribuyen las subfallas con especto al Slab 1.0 Model. Se obtienen
        subfallas con variaciones en profundidad, dip y strike segun la region de estudio
   3.4  Se establecen variacion en el rake del plano de falla.
###############################################################################

-------------------------------------------------------------------------------
Geometria compartida entre la falla inversa y la normal
-------------------------------------------------------------------------------
'''   

print ' -------------------------------------------------------'
print ' ETAPA  3 == Constructor: Falla A y B                   '
print ' -------------------------------------------------------'
 
L, strike_aux, backstrike = sub.vinc_dist(  latf,  lonf,  lat1,  lon1 )     #<-Largo y strike del plano de falla.

if sentido_subduccion == 'EW':
    strike_rad = np.radians(strike_aux+180)                                     #<-Strike o rumbo en radianes del plano de falla.
if sentido_subduccion == 'WE':
    strike_rad = np.radians(strike_aux)                                         #<-Strike o rumbo en radianes del plano de falla.
  

'''
-------------------------------------------------------------------------------
3.1 Geometria asociada a la falla normal.
-------------------------------------------------------------------------------
'''

rake_norm_ref_deg=rake_inv_ref_deg+180                 #<-Rake de referencia para la falla normal en radianes.

'''
-------------------------------------------------------------------------------
3.2 Se obtiene distribucion de las subfallas para un dip constante y bajo
una distribucion segun la configuracion de la fosa.
-------------------------------------------------------------------------------
'''

if sentido_subduccion == 'EW':
    #3.2.1 Se determina el origen geometrico base de la falla.
    lat0_ini,  lon0_ini,  alpha21  = sub.vinc_pt( lat1, lon1, np.degrees(strike_rad+np.pi/2), W*np.cos(dip_plano_inicial) )  
    #3.2.2 Se establece las coordenadas de subfallas con respecto a la configuracion de la fosa
    aux, aux, vertices_plano_inicial_lon, vertices_plano_inicial_lat =sub.coordenadas_subfallas_EW(ny,nx,dip_plano_inicial,W,L,lon0_ini,lat0_ini,strike_rad,fosa_lon,fosa_lat,delta_lat)
if sentido_subduccion == 'WE':
    #3.2.1 Se determina el origen geometrico base de la falla.
    lat0_ini,  lon0_ini,  alpha21  = sub.vinc_pt( latf, lonf, np.degrees(strike_rad+np.pi/2), W*np.cos(dip_plano_inicial) )  
    #3.2.2 Se establece las coordenadas de subfallas con respecto a la configuracion de la fosa
    aux, aux, vertices_plano_inicial_lon, vertices_plano_inicial_lat =sub.coordenadas_subfallas(ny,nx,dip_plano_inicial,W,L,lon0_ini,lat0_ini,strike_rad,fosa_lon,fosa_lat,delta_lat)

'''
-------------------------------------------------------------------------------
3.3 Se reordenan las subfallas con respecto a los datos de profundidad y dip
del modelos Slab 1.0.
-------------------------------------------------------------------------------
'''

subfallas   = sub.subfallas_SlabModel(nx,ny,W,L,strike_rad,vertices_plano_inicial_lon, vertices_plano_inicial_lat,dir_depth_slab_1_0, dir_dip_slab_1_0,dir_strike_slab_1_0)

profundidad_subfallas_inversa  = subfallas.Z                 #<-Profundidad de subfallas en la interfase superior (inversa)
dip_subfallas_radianes         = subfallas.dip_slab_rad      #<-Dip de subfallas para la interfase superior (inversa)
strike_subfallas_deg           = subfallas.strike_deg        #<-Strike de subfalla para la interfase superior (inversa)
lon_vertices_subfallas_slab    = subfallas.vert_lon_new      #<-Longitud de los vertices de subfallas segun el slab 1.0 Model
lat_vertices_subfallas_slab    = subfallas.vert_lat_new      #<-Latitud de los vertices de subfallas segun el slab 1.0 Model.
lon_central_subfallas_slab     = subfallas.lon_central_new   #<-Longitud central de las subfallas con respecto al Slab 1.0 Model.
lat_central_subfallas_slab     = subfallas.lat_central_new   #<-Latitud central de las subfallas con respecto al Slab 1.0 Model.

#Se establece profundidad de las subfallas de la interfase inferior (normal) en relacion al espesor de placa.
profundidad_subfallas_normal   = profundidad_subfallas_inversa + (H / np.cos(dip_subfallas_radianes))

'''
###############################################################################
MAPA: DISTRIBUCION DE DESLIZAMIENTO
###############################################################################
'''


slip_mask         = []
lat_central_mask  = []
lon_central_mask  = []


subfalla=0
#Mascara para el dibujo. Evita que contourf pinte/grafique fuera de las subfallas.
for j_ny in range(ny):
    for j_nx in range(nx):
        slip_mask.append(slip[subfalla])      #<----------------
        lat_central_mask.append(lat_central_subfallas_slab[subfalla])
        lon_central_mask.append(lon_central_subfallas_slab[subfalla])
        if (j_ny==0):  
            slip_mask.append(0)
            alpha = np.degrees(strike_rad) ; beta = alpha + 90
            lataux , lonaux, aux = sub.vinc_pt( lat_central_subfallas_slab[subfalla], lon_central_subfallas_slab[subfalla],  beta , W/ny )
            lat_central_mask.append(lataux)
            lon_central_mask.append(lonaux)
        if (j_ny==(ny-1)):
            slip_mask.append(slip[subfalla])   #<----------------
            alpha = np.degrees(strike_rad) ; beta = alpha + 270
            lataux , lonaux, aux = sub.vinc_pt( lat_central_subfallas_slab[subfalla], lon_central_subfallas_slab[subfalla],  beta , (W/ny)/2 )
            lat_central_mask.append(lataux)
            lon_central_mask.append(lonaux)
            #borde final
            slip_mask.append(0)
            alpha = np.degrees(strike_rad) ; beta = alpha + 270
            lataux , lonaux, aux = sub.vinc_pt( lat_central_subfallas_slab[subfalla], lon_central_subfallas_slab[subfalla],  beta , (W/ny)/2+(W/ny)/4)
            lat_central_mask.append(lataux)
            lon_central_mask.append(lonaux)            
        if (j_nx == 0):
            slip_mask.append(slip[subfalla])   #<----------------
            alpha = np.degrees(strike_rad) ; beta = alpha + 180
            lataux , lonaux, aux = sub.vinc_pt( lat_central_subfallas_slab[subfalla], lon_central_subfallas_slab[subfalla],  beta , (W/ny)/2 )
            lat_central_mask.append(lataux)
            lon_central_mask.append(lonaux)   
        if (j_nx == (nx-1)):
            slip_mask.append(slip[subfalla])   #<----------------
            alpha = np.degrees(strike_rad) ; beta = alpha 
            lataux , lonaux, aux = sub.vinc_pt( lat_central_subfallas_slab[subfalla], lon_central_subfallas_slab[subfalla],  beta , (W/ny)/2 )
            lat_central_mask.append(lataux)
            lon_central_mask.append(lonaux)    
        subfalla=subfalla+1     


fig = plt.figure(1)
plt.clf()
ax = fig.add_axes([0.1,0.1,0.8,0.8])
map = Basemap(projection='merc', resolution="h", llcrnrlon=lonmin_map, llcrnrlat=latmin_map, urcrnrlon=lonmax_map, urcrnrlat=latmax_map)                        

map.drawcoastlines(zorder=23)
map.drawcountries(zorder=23)
               
parallels = map.drawparallels(paralelos, labels=[1, 1, 0, 0], fmt="%.0f", #4
                dashes=[2, 2],linewidth=0.0, zorder=24)
for m in parallels:
    try:
        parallels[m][1][0].set_rotation(90)     
        parallels[m][1][1].set_rotation(90)     
        
    except:
        pass 
#Se trazan los meridianos.                
map.drawmeridians(meridianos, labels=[0, 0, 1, 0], fmt="%.0f",
               dashes=[2, 2],linewidth=0.0,zorder=24)
               
#Se dibuja al fosa.
map.plot(fosa_lon,fosa_lat,'-k',linewidth = 1.5, latlon=True,zorder=21)

#Se interpola el numero de datos de deslizamamiento
numcols, numrows = 500, 500

xi = np.linspace(min(lon_central_mask), max(lon_central_mask), numcols)
yi = np.linspace(min(lat_central_mask), max(lat_central_mask), numrows)

xi, yi = np.meshgrid(xi, yi)
zi = griddata(lon_central_mask, lat_central_mask, slip_mask, xi, yi, interp='nn')

im=map.contourf(xi,yi,zi,vmin=min_slip,vmax=max_slip,levels=niveles_contourf,cmap=paleta,zorder=18,latlon = True) #,extend='max'

                
cb = map.colorbar(im ,ticks=tick_colorbar, location = 'bottom' , size = '3%', pad = '2%' , extend = 'neither' , ax = ax)
cb.set_label(colorbar_title,fontsize=10.3) 


for i_nx in range(nx*ny):
    re = [] ; rn = []
    re.append(lon_vertices_subfallas_slab[i_nx][0]) ; rn.append(lat_vertices_subfallas_slab[i_nx][0])
    re.append(lon_vertices_subfallas_slab[i_nx][1]) ; rn.append(lat_vertices_subfallas_slab[i_nx][1])
    re.append(lon_vertices_subfallas_slab[i_nx][2]) ; rn.append(lat_vertices_subfallas_slab[i_nx][2])
    re.append(lon_vertices_subfallas_slab[i_nx][3]) ; rn.append(lat_vertices_subfallas_slab[i_nx][3])
    re.append(lon_vertices_subfallas_slab[i_nx][4]) ; rn.append(lat_vertices_subfallas_slab[i_nx][4])
    map.plot(re,rn,color='k',  linestyle = '-', linewidth = 0.05, latlon = True,zorder=19)


map.scatter(lon,lat, 13, color="w", marker="o", edgecolor="k",linewidth = 0.4, latlon=True,zorder=25,alpha=0.9) 

#Residuales entre datos observados y modelados.
quiver_obs=map.quiver(lon,lat,res_Ue,res_Un,color='black',width=0.0024, linewidth = 0,headwidth = 4.,scale=0.25, latlon=True, zorder=26) #0.5

plt.quiverkey(quiver_obs,0.75,0.08,0.01,r'Residual: 1 cm/yr',labelpos='N', 
                labelcolor= (0,0,0),zorder=26, 
                fontproperties={'size': 9, 'weight': 'bold'}) 

#Epicentro 
tx,ty=map(lon_epicentro, lat_epicentro)    
map.scatter(tx, ty, 150, color="w", marker="*", edgecolor="k", zorder=27)

if poligono == True:
    lat_mask_poly = []
    lon_mask_poly = []
    for i_ny in range(ny):
        for i_nx in range(nx):
            if i_ny == 1:
            #Vertice okada
                lat_mask_poly.append(lat_vertices_subfallas_slab[i_nx][0])  
                lon_mask_poly.append(lon_vertices_subfallas_slab[i_nx][0])

                lat_mask_poly.append(lat_vertices_subfallas_slab[i_nx][1])
                lon_mask_poly.append(lon_vertices_subfallas_slab[i_nx][1])         
  
    #La lectura en latitud se realiza de inferior a superior
    alpha = np.degrees(strike_rad); 
    
    #2. vertice superior derecho
    if sentido_subduccion == 'EW':
        lat_aux, lon_aux, aux = sub.vinc_pt( lat_vertices_subfallas_slab[(nx*2)-1][2], lon_vertices_subfallas_slab[(nx*2)-1][2], 270 , W/2) 
    if sentido_subduccion == 'WE':
        lat_aux, lon_aux, aux = sub.vinc_pt( lat_vertices_subfallas_slab[(nx*2)-1][2], lon_vertices_subfallas_slab[(nx*2)-1][2], 90  , W/2) 
    lat_mask_poly.append(lat_aux) ; lon_mask_poly.append(lon_aux)
   
    #3. vertice inferior derecho
    lat_aux, lon_aux, aux = sub.vinc_pt( lat_aux, lon_aux, alpha+180 , L)
    lat_mask_poly.append(lat_aux) ; lon_mask_poly.append(lon_aux)
    
    #4. Se agrega vertice inferior de subfallas
    lat_mask_poly.append(lat_vertices_subfallas_slab[0][0]); lon_mask_poly.append(lon_vertices_subfallas_slab[0][0])
    
    #Se construye poligono        
    tem_la = lat_mask_poly
    tem_lo = lon_mask_poly
    
    norm = mpl.colors.Normalize(vmin=0,vmax=1) #<-----------------------
    xtem_lo , xtem_la = map(tem_lo, tem_la)
    xy = zip( xtem_lo , xtem_la )
    scale_mask = 0
    cmap = cm.hot_r
    mc = cm.ScalarMappable(norm=norm, cmap=cmap)
    poly = Polygon( xy, fc=mc.to_rgba(scale_mask),ec='none',alpha = 1,zorder=20)
    plt.gca().add_patch(poly)


    #####################FOSA######################################################
    
    lat_mask_poly = []
    lon_mask_poly = []


    if sentido_subduccion == 'EW':    
        for i in range(len(fosa_lat)):
            #                   superior                                        inferior
            if fosa_lat[i]<=lat_vertices_subfallas_slab[(nx*ny)-nx][3] and fosa_lat[i]>=lat_vertices_subfallas_slab[(nx*ny)-1][3]: #Limite superior de la fosa
                lat_mask_poly.append(fosa_lat[i])
                lon_mask_poly.append(fosa_lon[i])

    if sentido_subduccion == 'WE':
        for i in range(len(fosa_lat)):  
            #                   superior                                        inferior
            if fosa_lat[i]>=lat_vertices_subfallas_slab[(nx*ny)-nx][3] and fosa_lat[i]<=lat_vertices_subfallas_slab[(nx*ny)-1][2]: #Limite superior de la fosa
                lat_mask_poly.append(fosa_lat[i])
                lon_mask_poly.append(fosa_lon[i])
    
    #La lectura en latitud se realiza de inferior a superior
    alpha = np.degrees(strike_rad); 
    
    #1. Se agrega vertice superior de subfallas
    if sentido_subduccion == 'EW':        
        lat_mask_poly.append(lat_vertices_subfallas_slab[(nx*ny)-nx][3]) ; lon_mask_poly.append(lon_vertices_subfallas_slab[(nx*ny)-nx][3])
    if sentido_subduccion == 'WE':        
        lat_mask_poly.append(lat_vertices_subfallas_slab[(nx*ny)-1][2]) ; lon_mask_poly.append(lon_vertices_subfallas_slab[(nx*ny)-1][2])


    #2. vertice superior derecho
    if sentido_subduccion == 'EW':        
        lat_aux, lon_aux, aux = sub.vinc_pt( lat_vertices_subfallas_slab[(nx*ny)-nx][3], lon_vertices_subfallas_slab[(nx*ny)-nx][3], alpha+270 , W)
    if sentido_subduccion == 'WE':        
        lat_aux, lon_aux, aux = sub.vinc_pt( lat_vertices_subfallas_slab[(nx*ny)-1][2], lon_vertices_subfallas_slab[(nx*ny)-1][2], alpha+270 , W) #<-
    lat_mask_poly.append(lat_aux) ; lon_mask_poly.append(lon_aux)
    
    #3. vertice inferior derecho
    if sentido_subduccion == 'EW':        
        lat_aux, lon_aux, aux = sub.vinc_pt( lat_aux, lon_aux, alpha , L)
    if sentido_subduccion == 'WE':        
        lat_aux, lon_aux, aux = sub.vinc_pt( lat_aux, lon_aux, alpha+180 , L)      
    lat_mask_poly.append(lat_aux) ; lon_mask_poly.append(lon_aux)
    
    #4. Se agrega vertice inferior de subfallas
    if sentido_subduccion == 'EW':        
        lat_mask_poly.append(lat_vertices_subfallas_slab[(nx*ny)-1][2]) ; lon_mask_poly.append(lon_vertices_subfallas_slab[(nx*ny)-1][2])
    if sentido_subduccion == 'WE':        
        lat_mask_poly.append(lat_vertices_subfallas_slab[(nx*ny)-nx][3]) ; lon_mask_poly.append(lon_vertices_subfallas_slab[(nx*ny)-nx][3])
  
    #5. Se repite el primer punto de la fosa
    lat_mask_poly.append(lat_mask_poly[0]) ; lon_mask_poly.append(lon_mask_poly[0])
    
    #Se construye poligono        
    tem_la = lat_mask_poly
    tem_lo = lon_mask_poly

    norm = mpl.colors.Normalize(vmin=0,vmax=1) #<-----------------------
    xtem_lo , xtem_la = map(tem_lo, tem_la)
    xy = zip( xtem_lo , xtem_la )
    scale_mask = 0
    cmap = cm.hot_r
    mc = cm.ScalarMappable(norm=norm, cmap=cmap)
    poly = Polygon( xy, fc=mc.to_rgba(scale_mask),ec='none',alpha = 1,zorder=20)
    plt.gca().add_patch(poly)

plt.show()
plt.savefig(output_name, format='png', dpi=200,bbox_inches='tight') 

