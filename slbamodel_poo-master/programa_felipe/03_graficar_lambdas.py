# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 15:35:47 2016

@author: felipe
"""

import matplotlib.pyplot as plt
import numpy as np

'''
#############################################################################
Felipe Vera S. - 2017
Consultas o preguntas a fveras@udec.cl/fvera.sanhueza@gmail.com
#############################################################################
'''

'''
###############################################################################
Variables GLobales
###############################################################################
'''

lmbd1_target = []
lmbd2_target = []
lmbd3_target = []
lmbd4_target = []

print ' --------------------- '
print ' ETAPA 1 == Leer datos '
print ' --------------------- '

#Se obtienen curvas de residual
data      = np.loadtxt('2_Output/4_Residuales/residuales_lambda1.txt');
lmbd1     = data[:,0]        #Longitud de los datos cargados.
residual1 = data[:,4]        #componente Norte de los datos [en metros]

data      = np.loadtxt('2_Output/4_Residuales/residuales_lambda2.txt');
lmbd2     = data[:,1]        #Longitud de los datos cargados.
residual2 = data[:,4]        #Componente Norte de los datos [en metros]

class graficar():
    def __init__(self,lambda_xaxis,residual, ID_figure, ID_lambda,output):
        self.lambda_xaxis = lambda_xaxis
        self.residual     = residual
        self.ID_figure    = ID_figure
        self.ID_lambda    = ID_lambda
        self.output = output
        
        self.ix          = []
        self.iy          = []
        self.line        = []
        self.pass_remove = False
        self.fig         = []
        
        self.generar_plot()
        
    def generar_plot(self):
        self.fig=plt.figure(self.ID_figure,facecolor='white')  
        plt.clf()
        self.ax = self.fig.add_subplot(1,1,1)
        for i in range(len(self.lambda_xaxis)):
            plt.plot(self.lambda_xaxis[i],self.residual[i],'-b', linewidth=0.8)
            plt.plot(self.lambda_xaxis[i],self.residual[i],'sk', linewidth=0.8)

            plt.hold(True)
        self.ax.set_xscale('log')
        self.ax.set_xlabel(r'$\lambda_'+str(self.ID_lambda)+'$')
        self.ax.set_ylabel('Residual [m]')
        plt.grid(True)        
        self.cid = self.fig.canvas.mpl_connect('button_press_event', self) 
        plt.savefig(self.output, format='png', dpi=200) 


    def __call__(self,event):
               
        try:     
            self.ix, self.iy = event.xdata, event.ydata
            #print 'x = %f, y = %f'%(self.ix, self.iy)
        
            if self.pass_remove == True:
                self.line.remove()   
            
            plt.figure(self.ID_figure,facecolor='white')    
            self.line = plt.axvline(x=self.ix,color='k')
            if self.pass_remove == False:
                self.pass_remove = True
                
            if self.ID_lambda == 1:
                global lmbd1_target 
                lmbd1_target = self.ix
                print 'lmbd1: ',self.ix
            elif self.ID_lambda == 2:
                global lmbd2_target 
                lmbd2_target = self.ix
                print 'lmbd2: ',self.ix
            elif self.ID_lambda == 3:
                global lmbd3_target 
                lmbd3_target = self.ix
                print 'lmbd3: ',self.ix                
            elif self.ID_lambda == 4:
                global lmbd4_target 
                lmbd4_target = self.ix 
                print 'lmbd4: ',self.ix                
            else:
                print 'ERROR: ID Lambda...'

        except (TypeError, ValueError):
            print 'Opps! try again...'
            pass

'''
###############################################################################
lmbd2 (lmb1 constante)   [lmbd2_1, lmbd2_2,....,lmbd2_n]  [lmb1, lmbd1 ..... cte]
###############################################################################
'''

num_lmbd1 = 9     #<-Numero de constantes lmbd1 utilizadas en la inversion
num_lmbd2 = 9     #<-Numero de constantes lmbd2 utilizadas en la inversion

lmbd1_aux    = []
r_lmbd1_aux = []
for i in range(0,num_lmbd1*num_lmbd2-num_lmbd1+1,num_lmbd1):
    lmbd1_aux.append(lmbd1[i:i+num_lmbd1])
    r_lmbd1_aux.append(residual1[i:i+num_lmbd1])

'''
###############################################################################
lmbd1 (lmb2 constante)   [lmbd1_1, lmbd1_2,....,lmbd1_n]  [lmb2, lmbd2 ..... cte]
###############################################################################
'''

lmbd2_aux    = []
r_lmbd2_aux = []
for i in range(0,num_lmbd1,num_lmbd1-1):
    lmbd2_aux.append(lmbd2[i:i+num_lmbd1])
    r_lmbd2_aux.append(residual2[i:i+num_lmbd1])

graficar(lmbd1_aux,r_lmbd1_aux,4,1,'2_Output/4_Residuales/lambda1.png')
graficar(lmbd2_aux,r_lmbd2_aux,5,2,'2_Output/4_Residuales/lambda2.png')
