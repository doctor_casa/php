# -*- coding: utf-8 -*-

import subroutine as sub
import numpy as np
import pp
import sys
import time 

'''
#############################################################################
Felipe Vera S. - 2017
Consultas o preguntas a fveras@udec.cl/fvera.sanhueza@gmail.com
#############################################################################
'''

#Modulo pp (Parallel Python) descargar desde http://www.parallelpython.com/ 
#instalar como ----> $python setup.py install 

def get_lambdas(n_dim,Ue_obs,Un_obs,Uz_obs,A,B,C,D,E,Vp_CD,Vp_E,input,dip_subfallas_radianes ,W,L,nx,ny,V_placa,):
    #Establece el residual respecto a una combinacion de constantes de regularizacion
    import subroutine as sub   #<-Para procesos en paralelo se requiere importar la subrutina en paralelo.

    lmbd1 = input[0]     #<-Minimizacion solucion plano de falla A.
    lmbd2 = input[1]     #<-Suavidad solucion plano de falla A.
    lmbd3 = input[2]     #<-Minimizacion solucion plano de falla B.
    lmbd4 = input[3]     #<-Suavidad solucion plano de falla B.

    #Se realiza inversion de datos
    slip_rate_aux,horror, horror_slip, horror_suave, U_corregido = sub.inversion_intersismico_slab(n_dim,Ue_obs,Un_obs,Uz_obs,A,B,C,D,E,Vp_CD,Vp_E,lmbd1,lmbd2,lmbd3,lmbd4,dip_subfallas_radianes ,W,L,nx,ny,V_placa)

    return lmbd1, lmbd2, lmbd3, lmbd4, horror 

'''
###############################################################################
0. ZONA DE SUBDUCCION
###############################################################################
'''

#zona de subduccion
zona_subduccion    = 'Ecuador_Colombia'

#Sentido de la subduccion
sentido_subduccion = 'WE'      #   EW  : Sentido de subduccion East-West o Este-Oeste (Ej. Japon)
                               #   WE  : Sentido de subduccion West-East o Oeste-Este (Ej. Chile, Ecuador_Colombia) 
'''
-----------------------------------------------------------------------------
#############################################################################
                         PARAMETROS DE ENTRADA
#############################################################################
-----------------------------------------------------------------------------
'''

'''
#############################################################################
1. PARAMETROS: LECTURA DE DATOS
#############################################################################
'''

print ' ---------------------------------------------------------------------'
print ' ETAPA  1 == Leer datos                                               '
print ' ---------------------------------------------------------------------'

#1. Datos asociados a la distribucion de la fosa en superficie (Slab 1.0).
fosa = np.loadtxt('1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/4_slab_model_'+zona_subduccion+'_trench_interpolation.txt')    #<-Se cargan datos de la fosa.
fosa_lon = fosa[:,0] ; fosa_lat = fosa[:,1]  

#2. Datos de velocidades intersismicas observadas desde receptores GPS.
data   = np.loadtxt('1_Datos/2_GPS_data/synthetic_data.txt');
lon    = data[:,1]            #<-Longitud de estaciones [grados]
lat    = data[:,0]            #<-Latitud de estaciones [grados]
Ue_obs = data[:,2]/1000       #<-Componente Este de los datos [metros/agno]
Un_obs = data[:,3]/1000       #<-Componente Norte de los datos [metros/agno]
Uz_obs = []                   #<-Componente Vertical de los datos [metros/agno]

#Numero de dimensiones referente a la cantidad de componentes de los registros 
#GPS a utilizar en la inversion (Un, Ue y Uz).
n_dim = 2       #Ue ; Un

#Directorios con datos de profundidad, dip y strike desde modelo Slab 1.0
dir_depth_slab_1_0   = '1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/1_slab_model_depth_data_'+zona_subduccion+'.txt'    #<-Profundidad
dir_dip_slab_1_0     = '1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/2_slab_model_dip_data_'+zona_subduccion+'.txt'      #<-dip
dir_strike_slab_1_0  = '1_Datos/1_Slab_1_0_Model/'+zona_subduccion+'/3_slab_model_strike_data_'+zona_subduccion+'.txt'   #<-Strike 

'''
#############################################################################
2. PARAMETROS: PLANOS DE FALLA A, B, C, D y E
#############################################################################

                    TRENCH
___OCEAN PLATE________|________CONTINENTAL PLATE______
         Vp -->        -
_______PLANO E (NORMAL)  ___ _     - <-- PLANO A (INVERSA)
                               -     -
                                 -     -
           PLANO B (NORMAL) -->    -     -
                                     ------
                                      -     -
                 PLANO D (NORMAL)->     - Vp  -  <- PLANO C (INVERSA)
                                          -     -

-------------------------------------------------------------------------------
2.1 PARAMETROS: PLANO DE FALLA A y B 
-------------------------------------------------------------------------------
A : PLANO DE FALLA CON MOVIMIENTO RELATIVO DE TIPO INVERSO
B : PLANO DE FALLA CON MOVIMIENTO RELATIVO DE TIPO NORMAL
-------------------------------------------------------------------------------
'''

print ' -----------------------------------------------------------------------'
print ' ETAPA  2.1 == Obteniendo parametros: Plano de falla A y B              '
print ' -----------------------------------------------------------------------'

V_placa           = 0.08                                     #<-Velocidad de convergencia en metros/agno
lonf              = -81.874813 + 0.2  ; latf = -5.437300     #<-Limite inferior de la fosa. 
lon1              = -77.824402 + 0.2  ; lat1 = 5.301700      #<-Limite superior de la fosa.   
W                 = 260000                                   #<-Ancho deL plano de falla A y B en sentido del downdip [metros].
rake_inv_ref_deg  = 113                                      #<-Rake de referencia para el movimiento relativo de tipo inverso [grados].
H                 = 30000                                    #<-Espesor de placa [metros].
nx                = 30                                       #<-Numero de subfalla en sentido del strike del plano de falla.
ny                = 6                                        #<-Numero de subfallas en sentido del dip del plano de falla. 
delta_lat         = 0.15                                     #<-Angulo de inclinacion de subfallas de los planos A y B con respecto a la fosa.

dip_plano_inicial = np.radians(12.)                          #<-Dip en grados base para la construcion de subfallas de los planos A y B.                                 
#Obs: Dip_plano_inicial solo para iniciar la generacion de subfallas. Posteriormente, se obtiene un dip
#independiente para cada subfalla desde el modelo Slab 1.0.

'''
-------------------------------------------------------------------------------
2.2 PARAMETROS: PLANO DE FALLA C y D 
-------------------------------------------------------------------------------
C : PLANO DE FALLA CON MOVIMIENTO RELATIVO DE TIPO INVERSO
D : PLANO DE FALLA CON MOVIMIENTO RELATIVO DE TIPO NORMAL
-------------------------------------------------------------------------------
'''

print ' -----------------------------------------------------------------------'
print ' ETAPA  2.2 == Obteniendo parametros: Falla C y Falla D                 '
print ' -----------------------------------------------------------------------'

ny_CD = 4        #<-Numero de subfallas en sentido del dip del plano de falla
#Obs: EL numero de subfalla en sentido del strike de los planos de falla C y D 
#queda determinado por el valor asignado en los planos de falla A y B.
w_CD  = W/ny     #<-Ancho subfallas plano C y D equivalente al de las subfallas de los planos C y D.
                 #  El area de subfallas de los planos  C y D es equivalente al de los planos A y B.
 
'''
-------------------------------------------------------------------------------
2.3 PARAMETROS: PLANO DE FALLA E "OUTER TRENCH" 
-------------------------------------------------------------------------------
E : PLANO DE FALLA CON MOVIMIENTO RELATIVO DE TIPO NORMAL
-------------------------------------------------------------------------------
'''

print ' -----------------------------------------------------------------------'
print ' ETAPA  2.3 == Obteniendo parametros: Falla E                          '
print ' -----------------------------------------------------------------------'
 
loninf_E      = -81.874813  - 0.3   ; latinf_E = -5.437300     #<-Limite inferior de la fosa. 
lonsup_E      = -77.824402  - 0.3   ; latsup_E = 5.301700      #<-Limite superior de la fosa.  
W_E           = 300*1000                                       #<-Ancho deL plano de falla E [metros].
H_E           = H                                              #<-Espesor de placa [metros].
nx_E          = nx                                             #<-Numero de subfalla en sentido del strike del plano de falla
ny_E          = 1                                              #<-Numero de subfallas en sentido del dip del plano de falla.
delta_lat_E   = -0.19                                          #<-Angulo de inclinacion de subfallas del plano E con respecto a la fosa.

'''
-----------------------------------------------------------------------------
#############################################################################
                CONSTRUCTOR PLANOS DE FALLA A, B, C, D, E
#############################################################################
-----------------------------------------------------------------------------
'''

'''
###############################################################################
3.                CONSTRUCTOR PLANOS DE FALLA A Y B: SLAB 1.0 MODEL
###############################################################################
Se establece geometria y distribucion de subfallas para los planos de falla A 
y B con respecto a los datos de profundidad, dip y strike desde el modelo 
slab 1.0.
PROCEDIMIENTO.
   1. Se define geometria de fallamiento para el plano B con un movimiento
      relativo de tipo normal.
   2. Se establecen subfallas para un dip constante bajo una distribucion lineal
      de profundidad con respecto al angulo de buzamiento/dip constante.
   3. Se redistribuyen las subfallas con especto al modelo Slab 1.0. Se obtienen
      subfallas con variaciones en profundidad, dip y strike segun la region
      de estudio.
   4  Se establecen variacion en el rake del plano de falla.
###############################################################################

-------------------------------------------------------------------------------
Geometria compartida entre la falla inversa y la normal
-------------------------------------------------------------------------------
'''   

print ' -----------------------------------------------------------------------'
print ' ETAPA  3 == Constructor: PLanos de falla A y B                                   '
print ' -----------------------------------------------------------------------'
 
L, strike_aux_deg, backstrike = sub.vinc_dist(  latf,  lonf,  lat1,  lon1 )     #<-Largo y strike del plano de falla
#Obs: Strike para la contruccion deL plano de falla. Posteriormente se implementan
#variaciones en strike desde el modelo slab 1.0.

if sentido_subduccion == 'EW':
    strike_rad = np.radians(strike_aux_deg+180)                                     #<-Strike o rumbo en radianes del plano de falla.
if sentido_subduccion == 'WE':
    strike_rad = np.radians(strike_aux_deg)                                         #<-Strike o rumbo en radianes del plano de falla.
  
'''
-------------------------------------------------------------------------------
1  Geometria asociada al plano de falla B con movimiento relativo de tipo normal.
-------------------------------------------------------------------------------
'''

rake_norm_ref_deg=rake_inv_ref_deg+180                 #<-Rake de referencia para los planos de falla con
                                                       #  movimiento relativo de tipo normal (en radianes)
'''
-------------------------------------------------------------------------------
2 Se obtiene distribucion de las subfallas para un dip constante y bajo
una distribucion segun la configuracion de la fosa.
-------------------------------------------------------------------------------
'''

if sentido_subduccion == 'EW':
    #Se determina el origen geometrico base del plano de falla.
    #Obs. Sistema de referencia de Okada.
    lat0_ini,  lon0_ini,  alpha21  = sub.vinc_pt( lat1, lon1, np.degrees(strike_rad+np.pi/2), W*np.cos(dip_plano_inicial) )  
    #Se establecen las coordenadas de las subfallas con respecto a la configuracion de la fosa
    aux, aux, vertices_plano_inicial_lon, vertices_plano_inicial_lat =sub.coordenadas_subfallas_EW(ny,nx,dip_plano_inicial,W,L,lon0_ini,lat0_ini,strike_rad,fosa_lon,fosa_lat,delta_lat)
if sentido_subduccion == 'WE':
    #Se determina el origen geometrico base del plano de falla.
    #Obs. Sistema de referencia de Okada.    
    lat0_ini,  lon0_ini,  alpha21  = sub.vinc_pt( latf, lonf, np.degrees(strike_rad+np.pi/2), W*np.cos(dip_plano_inicial) )  
    #Se establecen las coordenadas de las subfallas con respecto a la configuracion de la fosa
    aux, aux, vertices_plano_inicial_lon, vertices_plano_inicial_lat =sub.coordenadas_subfallas(ny,nx,dip_plano_inicial,W,L,lon0_ini,lat0_ini,strike_rad,fosa_lon,fosa_lat,delta_lat)

'''
-----------------------------------------------------------------------------
3 Se reordenan las subfallas con respecto a los datos de profundidad y dip
del modelos Slab 1.0.
-----------------------------------------------------------------------------
'''

subfallas   = sub.subfallas_SlabModel(nx,ny,W,L,strike_rad,vertices_plano_inicial_lon, vertices_plano_inicial_lat,dir_depth_slab_1_0, dir_dip_slab_1_0,dir_strike_slab_1_0)

profundidad_subfallas_inversa  = subfallas.Z                 #<-Profundidad de subfallas del plano de falla A 
dip_subfallas_radianes         = subfallas.dip_slab_rad      #<-Dip de subfallas correspondientes a los planos de falla A y B.
strike_subfallas_deg           = subfallas.strike_deg        #<-Strike de subfallas correspondientes a los planos de falla A y B.
lon_vertices_subfallas_slab    = subfallas.vert_lon_new      #<-Longitud de los vertices de subfallas segun el slab 1.0 Model
lat_vertices_subfallas_slab    = subfallas.vert_lat_new      #<-Latitud de los vertices de subfallas segun el slab 1.0 Model.
lon_central_subfallas_slab     = subfallas.lon_central_new   #<-Longitud central de las subfallas con respecto al Slab 1.0 Model.
lat_central_subfallas_slab     = subfallas.lat_central_new   #<-Latitud central de las subfallas con respecto al Slab 1.0 Model.

#Se establece profundidad de las subfallas del plano de falla B en relacion al espesor de placa.
profundidad_subfallas_normal   = profundidad_subfallas_inversa + (H / np.cos(dip_subfallas_radianes))

'''
-----------------------------------------------------------------------------
4 Se obtienen variaciones en el rake de cada subfalla con respecto al sentido 
de convergencia y el strike del plano de falla
-----------------------------------------------------------------------------
'''

#Strike del vector de convergencia de la placa
phi_placa_rad = np.radians(np.degrees(strike_rad) + (360-(rake_inv_ref_deg+180)))

rake_inv_rad  = []     #<-Rake para subfallas de la interfase superior con un movimiento relativo de tipo inverso.
rake_norm_rad = []     #<-Rake para subfallas de la interfase inferior con un movimiento relativo de tipo normal.

for subfalla in range(len(strike_subfallas_deg)):
    #Se establece rake en subfallas emplazadas en la interfase superior con un movimiento relativo de tipo inverso
    rake_inv_rad.append(sub.get_rake(phi_placa_rad, np.radians(strike_subfallas_deg[subfalla]), dip_subfallas_radianes[subfalla]))
    #Se establece rake en subfallas emplazadas en la interfase inferior con un movimiento relativo de tipo normal
    rake_norm_rad.append(rake_inv_rad[subfalla]+np.pi) 

'''
#############################################################################
4                CONSTRUCTOR PLANOS DE FALLA C Y D: SLAB 1.0 MODEL
#############################################################################
'''

print ' ---------------------------------------------------------------------'
print ' ETAPA  4 == Constructor: Planos de falla C y D                       '
print ' ---------------------------------------------------------------------'

#1.2 Se generan vertices de las subfallas de acuerdo a la geometria rectangular.
COL=5      #Cada subfalla tiene cinco vertices a dibujar (el ultimo coincide con el primero)

FIL=ny_CD*nx
#Se declaran variables para almacenar vertices de todas las subfallas de los
#planos de falla C y D.
lat_vert_CD_init    = []                #<-Latitud de vertices de subfallas.
lon_vert_CD_init    = []                #<-Longitud de vertices de subfallas.
lat_central_CD_init = []                #<-Latitud de la posicion central de subfallas.
lon_central_CD_init = []                #<-Longitud de la posicion central de subfallas.

#Se inicializan coordenadas de los vertices de subfallas.
for i in range(FIL): 
    lon_vert_CD_init.append([0]*COL)    
    lat_vert_CD_init.append([0]*COL)  
            
#Se establece angulo de construcion (beta) dado por el strike desde las subfallas
#adyacentes de los planos de falla A y B.
alpha = np.degrees(strike_rad) ; beta = alpha + 90

subfalla = 0
for i_nx in range(nx): 

   for j_ny in range(ny_CD):                   
    #Se construye primera subfalla adyacente a la primera fila de los planos
    #A y B. Se mantiene como ancho, el mismo valor asignado a las subfallas de
    #las regiones A y B.
        if j_ny == 0:        
            #----------------------------------------------------------------
            #Se construyen listas para almacenar vertices constructores. Estos, 
            #se utilizaran para evaluar la diponibilidad de datos para esta 
            #subfalla desde el modelo slab 1.0.        
      
            #Vertice 2
            lat_vert_CD_init[subfalla][2] = lat_vertices_subfallas_slab[i_nx][1]
            lon_vert_CD_init[subfalla][2] = lon_vertices_subfallas_slab[i_nx][1]
            
            #Vertice 3
            lat_vert_CD_init[subfalla][3] = lat_vertices_subfallas_slab[i_nx][0]
            lon_vert_CD_init[subfalla][3] = lon_vertices_subfallas_slab[i_nx][0]
        
            #Vertice 0
            lat_vert_CD_init[subfalla][0], lon_vert_CD_init[subfalla][0], aux = sub.vinc_pt( lat_vert_CD_init[subfalla][3], lon_vert_CD_init[subfalla][3], beta , w_CD)
    
            #Vertice 1
            lat_vert_CD_init[subfalla][1], lon_vert_CD_init[subfalla][1], aux = sub.vinc_pt( lat_vert_CD_init[subfalla][2], lon_vert_CD_init[subfalla][2], beta , w_CD)

            #Veretice 4
            lat_vert_CD_init[subfalla][4] = lat_vert_CD_init[subfalla][0]
            lon_vert_CD_init[subfalla][4] = lon_vert_CD_init[subfalla][0]
            
        else:
        
            #------------------------------------------------------------------
            #Se construyen listas para almacenar vertices constructores. Estos, 
            #se utilizaran para evaluar la diponibilidad de datos para esta 
            #subfalla desde el slab 1.0 model.        
             
            #Vertice 2
            lat_vert_CD_init[subfalla][2] = lat_vert_CD_init[subfalla-1][1]
            lon_vert_CD_init[subfalla][2] = lon_vert_CD_init[subfalla-1][1]
            
            #Vertice 3
            lat_vert_CD_init[subfalla][3] = lat_vert_CD_init[subfalla-1][0]
            lon_vert_CD_init[subfalla][3] = lon_vert_CD_init[subfalla-1][0]
        
            #Vertice 0
            lat_vert_CD_init[subfalla][0], lon_vert_CD_init[subfalla][0], aux = sub.vinc_pt( lat_vert_CD_init[subfalla][3], lon_vert_CD_init[subfalla][3], beta , w_CD)
    
            #Vertice 1
            lat_vert_CD_init[subfalla][1], lon_vert_CD_init[subfalla][1], aux = sub.vinc_pt( lat_vert_CD_init[subfalla][2], lon_vert_CD_init[subfalla][2], beta , w_CD) 

            #Vertice 4
            lat_vert_CD_init[subfalla][4] = lat_vert_CD_init[subfalla][0]
            lon_vert_CD_init[subfalla][4] = lon_vert_CD_init[subfalla][0]
            
        subfalla = subfalla + 1

subfalla_CD  = sub.subfallas_SlabModel(nx,ny_CD,w_CD*ny_CD,L,strike_rad,lon_vert_CD_init, lat_vert_CD_init,dir_depth_slab_1_0, dir_dip_slab_1_0,dir_strike_slab_1_0)

Z_inversa_CD_final     = subfalla_CD.Z                 #<-Profundidad de subfallas del plano de falla C
dip_radianes_CD_final  = subfalla_CD.dip_slab_rad      #<-Dip de subfallas correspondientes a los planos de falla C y D.
strike_CD_final_deg    = subfalla_CD.strike_deg        #<-Strike de subfallas correspondientes a los planos de falla C y D.
lon_vert_CD_final      = subfalla_CD.vert_lon_new      #<-Longitud de los vertices de subfallas segun el slab 1.0 Model.
lat_vert_CD_final      = subfalla_CD.vert_lat_new      #<-Latitud de los vertices de subfallas segun el slab 1.0 Model.
lon_central_CD_final   = subfalla_CD.lon_central_new   #<-Longitud central de las subfallas con respecto al Slab 1.0 Model.
lat_central_CD_final   = subfalla_CD.lat_central_new   #<-Latitud central de las subfallas con respecto al Slab 1.0 Model.       

#Se establece profundidad de las subfallas del plano de falla D en relacion al espesor de placa.
Z_normal_CD_final      = Z_inversa_CD_final + (H / np.cos(dip_radianes_CD_final))

'''
-----------------------------------------------------------------------------
Se obtienen variaciones en el rake de cada subfalla con respecto al sentido 
de convergencia y el strike del plano de falla
-----------------------------------------------------------------------------
'''

rake_inv_CD_rad  = []      #<-Rake para subfallas de la interfase superior (C) con un movimiento relativo de tipo inverso.
rake_norm_CD_rad = []      #<-Rake para subfallas de la interfase inferior (D) con un movimiento relativo de tipo normal.

for subfalla in range(len(strike_CD_final_deg)):
    #Se establece rake en subfallas emplazadas en la interfase superior con un movimiento relativo de tipo inverso
    rake_inv_CD_rad.append(sub.get_rake(phi_placa_rad, np.radians(strike_CD_final_deg[subfalla]), dip_radianes_CD_final[subfalla]))
    #Se establece rake en subfallas emplazadas en la interfase inferior con un movimiento relativo de tipo normal
    rake_norm_CD_rad.append(rake_inv_CD_rad[subfalla]+np.pi)                
    
'''
################################################################################
5                CONSTRUCTOR PLANO DE FALLA E: SLAB 1.0 MODEL
################################################################################
#'''

print ' -----------------------------------------------------------------------'
print ' ETAPA  5 == Constructor: Falla E              '
print ' -----------------------------------------------------------------------'

if sentido_subduccion == 'EW':
    #Se obtienen coordendas de subfallas y goemetria asociada al plano de falla E.
    lon_vertices_E, lat_vertices_E,L_E,strike_E_rad = sub.constructor_fallaE_EW(loninf_E, latinf_E, lonsup_E, latsup_E,V_placa,W_E,H_E,nx_E,ny_E,delta_lat_E,fosa_lon,fosa_lat)
if sentido_subduccion == 'WE':
    #Se obtienen coordendas de subfallas y goemetria asociada al plano de falla E.
    lon_vertices_E, lat_vertices_E,L_E,strike_E_rad = sub.constructor_fallaE_WE(loninf_E, latinf_E, lonsup_E, latsup_E,V_placa,W_E,H_E,nx_E,ny_E,delta_lat_E,fosa_lon,fosa_lat)

    
'''
###############################################################################
6               PROCESO DE INVERSION: PLANOS DE FALLA A Y B
###############################################################################
'''

print ' -----------------------------------------------------------------------'
print ' ETAPA  6.1 == Caracterizacion geometrica: A y B                        '
print ' -----------------------------------------------------------------------'

#2. Caracterizacion geometrica para el plano de falla A
print '6.1.1 Calculando matriz A...'
A = sub.model_matrix_slab(n_dim,dip_subfallas_radianes,profundidad_subfallas_inversa,W,L,rake_inv_rad,np.radians(strike_subfallas_deg),nx,ny,lon_vertices_subfallas_slab,lat_vertices_subfallas_slab,lon,lat)

#3. Caracterizacion geometrica para el plano de falla B
print '6.1.2 Calculando matriz B...'
B = sub.model_matrix_slab(n_dim,dip_subfallas_radianes,profundidad_subfallas_normal,W,L,rake_norm_rad,np.radians(strike_subfallas_deg),nx,ny,lon_vertices_subfallas_slab,lat_vertices_subfallas_slab,lon,lat)
    
print ' -----------------------------------------------------------------------'
print ' ETAPA  6.2 == Caracterizacion geometrica: C y D                        '
print ' -----------------------------------------------------------------------'    
   
l_CD             = L/nx                         #<-Largo de cada subfalla en sentido del strike.
num_subfallas_CD = len(dip_radianes_CD_final)   #<-Numero de subfallas del plano de falla C y D.

#1. Caracterizacion geometrica para el plano de falla C
print '6.2.1 Calculando matriz C...'
C = sub.model_matrix_slab_CD(n_dim,dip_radianes_CD_final,Z_inversa_CD_final,w_CD,l_CD,rake_inv_CD_rad,np.radians(strike_CD_final_deg),num_subfallas_CD,lon_vert_CD_final,lat_vert_CD_final,lon,lat)

#2. Caracterizacion geometrica para el plano de falla D
print '6.2.2 Calculando matriz D...'
D = sub.model_matrix_slab_CD(n_dim,dip_radianes_CD_final,Z_normal_CD_final,w_CD,l_CD,rake_norm_CD_rad,np.radians(strike_CD_final_deg),num_subfallas_CD,lon_vert_CD_final,lat_vert_CD_final,lon,lat)
    
print ' -----------------------------------------------------------------------'
print ' ETAPA  6.3 == Caracterizacion geometrica: E                            '
print ' -----------------------------------------------------------------------'       
    
#1. Caracterizacion geometrica para el plano de falla E
print '6.3.1 Calculando matriz E...'    
E = sub.model_matrix_slab_E(n_dim,0,H_E,W_E,L_E,np.radians(rake_norm_ref_deg),strike_E_rad,nx_E,ny_E,lon_vertices_E,lon_vertices_E,lon,lat)
    
'''
################################################################################
7.                         PROCESO DE INVERSION
################################################################################
'''    

print ' -----------------------------------------------------------------------'
print ' ETAPA  7 == Proceso de inversion                                       '
print ' -----------------------------------------------------------------------'

#Se establecen velocidades libres en los planos de falla C, D y E.
print '7.1 Incluyendo velocidad de placa     ...'        

#Velocidades libres plano de falla C y D
for subfalla in range(num_subfallas_CD):  
    if subfalla == 0:
        Vp_CD = V_placa
    else:
        Vp_CD = np.hstack((Vp_CD,V_placa)) 
    
#Velocidades libres plano de falla E.    
for subfalla in range(nx_E*ny_E):
    if subfalla == 0:
        Vp_E = V_placa
    else:
        Vp_E = np.hstack((Vp_E,V_placa))  
      
'''
###############################################################################
Multiprocesadores
###############################################################################
'''

ppservers = ()

if len(sys.argv) > 1:
    ncpus = int(sys.argv[1])
    # Creates jobserver with ncpus workers
    job_server = pp.Server(ncpus, ppservers=ppservers)
else:
    # Creates jobserver with automatically detected number of workers
    job_server = pp.Server(ppservers=ppservers)

 
'''
#############################################################################
                          LAMBDA 1, LAMBDA 3
#############################################################################
'''
#Se obtiene curva de residual asumiendo lmbd1=lmbd3 y lmbd2 = lmbd4.

#lmbd1 = [0.0000001, 0.000001, 0.00001, 0.0001, 0.001,0.005, 0.01, 0.05, 0.1]
#lmbd2 = [0.0000001, 0.000001, 0.00001, 0.0001, 0.001,0.005, 0.01, 0.05, 0.1]
#
#for ID_lambda in range(1):
#    inicio = time.time()
#
#    '''
#    ###############################################################################
#    VARIABLES 
#    ###############################################################################
#    '''
#
#    lmbd1_list    = []
#    lmbd2_list    = []
#    lmbd3_list    = []
#    lmbd4_list    = []
#    residual_list = []
#
#    '''
#    ############################################################################
#    SE CONSTRUYE ARGUMENTO      
#    ############################################################################
#    '''
#
#    inputs = []
#    
#    if ID_lambda == 0:
#        print 'Determinando lmbd1 ...'
#        for i in range(len(lmbd2)):   
#            for j in range(len(lmbd1)):
#                inputs.append([lmbd1[j],lmbd2[i],lmbd1[j],lmbd2[i]])
#  
#    print 'Combinaciones: ',str(len(inputs))
#
#    '''
#    ############################################################################
#    PROCESO EN PARALELO   
#    ############################################################################
#    '''       
#
#    job2 = [(input, job_server.submit(get_lambdas, (n_dim,Ue_obs,Un_obs,Uz_obs,A,B,C,D,E,Vp_CD,Vp_E,input,dip_subfallas_radianes ,W,L,nx,ny,V_placa,))) for input in inputs]
#
#    '''
#    ############################################################################
#    SE OBTIENEN RESULTADOS
#    ############################################################################
#    '''
#
#    cont = 1
#    for input, job in job2:
#        print cont,". lmbd1: ", job()[0], " lmbd2: ", job()[1], " lmbd3: ",job()[2], " lmbd4: ",job()[3], " Res: ",job()[4]
#        lmbd1_list.append(job()[0])
#        lmbd2_list.append(job()[1])
#        lmbd3_list.append(job()[2])
#        lmbd4_list.append(job()[3])
#        residual_list.append(job()[4])
#        cont = cont +1
#           
#    '''
#    ###############################################################################
#    TIEMPO DE EJECUCION
#    ###############################################################################
#    '''               
#    print "Tiempo de Ejecucion: ", time.time() - inicio, "Segundos"
#
#    '''
#    ###############################################################################
#    SE ALMACENAN DATOS 
#    ###############################################################################
#    '''
#
#    name='2_Output/4_Residuales/residuales_lambda'+str(ID_lambda+1)+'.txt'     
#    print 'almacenando: '+name    
#    #Se almacenan los datos en un fichero de texto.
#    file = open(name, "w")
#    for i in range(len(lmbd1_list)):
#        file.write(str(lmbd1_list[i])+' '+str(lmbd2_list[i])+' '+str(lmbd3_list[i])+' '+str(lmbd4_list[i])+' '+str(residual_list[i]))    
#        file.write('\n')
#    file.close()    
    
'''
################################################################################
#                                 lmbd2,lmbd4
################################################################################
'''
#Seleccionado el valor lmbd1=lmbd3, se procede a obtener lmbd2=lmbd4

lmbd2 = [0.00000001, 0.0000001, 0.000001, 0.00001, 0.0001, 0.001,0.005, 0.01, 0.05, 0.1]

lmbd1_aux = 0.002     #<-Seleccion de lmbd1=lmbd2

for ID_lambda in range(1):
    inicio = time.time()

    '''
    ###############################################################################
    VARIABLES
    ###############################################################################
    '''

    lmbd1_list    = []
    lmbd2_list    = []
    lmbd3_list    = []
    lmbd4_list    = []
    residual_list = []

    '''
    ############################################################################
    SE CONSTRUYE ARGUMENTO      
    ############################################################################
    '''

    inputs = []
    
    if ID_lambda == 0:
        print 'Determinando lmbd2 ...'
        for i in range(len(lmbd2)):
            inputs.append([lmbd1_aux,lmbd2[i],lmbd1_aux,lmbd2[i]])
  
    print 'Combinaciones: ',str(len(inputs))

    '''
    ############################################################################
    PROCESO EN PARALELO   
    ############################################################################
    '''       

    job2 = [(input, job_server.submit(get_lambdas, (n_dim,Ue_obs,Un_obs,Uz_obs,A,B,C,D,E,Vp_CD,Vp_E,input,dip_subfallas_radianes ,W,L,nx,ny,V_placa, ))) for input in inputs]

    '''
    ############################################################################
    SE OBTIENEN RESULTADOS
    ############################################################################
    '''

    cont = 1
    for input, job in job2:
        print cont,". lmbd1: ", job()[0], " lmbd2: ", job()[1], " lmbd3: ",job()[2], " lmbd4: ",job()[3], " Res: ",job()[4]
        lmbd1_list.append(job()[0])
        lmbd2_list.append(job()[1])
        lmbd3_list.append(job()[2])
        lmbd4_list.append(job()[3])
        residual_list.append(job()[4])
        cont = cont +1
           
    '''
    ###############################################################################
    TIEMPO DE EJECUCION
    ###############################################################################
    '''               
    print "Tiempo de Ejecucion: ", time.time() - inicio, "Segundos"

    '''
    ###############################################################################
    SE ALMACENAN DATOS
    ###############################################################################
    '''

    name='2_Output/4_Residuales/residuales_lambda2.txt'     
    print 'almacenando: '+name    
    #Se almacenan los datos en un fichero de texto.
    file = open(name, "w")
    for i in range(len(lmbd2_list)):
        file.write(str(lmbd1_list[i])+' '+str(lmbd2_list[i])+' '+str(lmbd3_list[i])+' '+str(lmbd4_list[i])+' '+str(residual_list[i]))    
        file.write('\n')
    file.close() 