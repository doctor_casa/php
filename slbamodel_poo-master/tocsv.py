#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 18:10:23 2019

@author: doctor
"""
import csv
rows=[]
with open('Fuentes/estaciones_cosismico.txt','r') as fileobject:
    for row in fileobject:
        row=row.strip().split(' ')
        name=row[0]
        lat="{:7.04f}".format(float(row[1])).strip()
        lon="{:7.04f}".format(float(row[2])).strip()
        ue="{:7.04f}".format(float(row[6])).strip()
        un="{:7.04f}".format(float(row[7])).strip()
        uz="{:7.04f}".format(float(row[8])).strip()
        #lat="{:10.4f}".format(float(row[1]).strip())

        rows.append([name,lat,lon,ue,un,uz])
        
        
print(rows)
with open('Fuentes/estaciones_cosismico.csv','w') as writeobject:
        writer = csv.writer(writeobject,delimiter=';',)
        for row in rows:
            writer.writerow(row)