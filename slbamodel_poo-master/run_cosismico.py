from cosismico import Cosismico

if __name__ == "__main__":
#    params = {
#        'zona':'Chile',
#        'limites_zona': (-18,-45),
#        'stations': []
#    }
    cosismico = Cosismico()
    map_params = {
        'latmin': -37,
        'latmax': -25,
        'lonmin': -79,
        'lonmax': -65,
        "paralelos": [-67, -70, -73, -76, -79],
        "meridianos": [-34,-31,-28],
    }
    cosismico.set_map_params(**map_params)
    #intersismico.build_map()
    print("Planos de falla data")
    cosismico.plano_falla_ab()
#    intersismico.plano_falla_cd()
#    intersismico.plano_falla_e()
    print("Construyendo planos")
    cosismico.construye_planos()
    print('Mapa de las fallas')
    cosismico.mapa_geometria()
    print("Haciendo inversion")
    #intersismico.calcula_inversion()
    print('Mapa de velocidades sinteticas')
    #intersismico.mapa_velocidades_sinteticas()
    
