import numpy as np

## number of grid points in x direction
nx=3
## number of grid points in y direction
ny=3
## size of the grid
n=nx*ny
## build the F matrix
F = np.zeros(n*n).reshape(n,n)
## largo 100 km 
L=1000000
## ancho  placa 200 km
ancho_placa=200000
## create pos list, this list contain which points are used to calculate the laplacian
## on a given point (x,y)
pos = []
## create coef list, this list contain the coefficients that go along with post items
coef = []
## create the dip vector which is used in the loop
dip_subf_rad=np.zeros((nx*ny))
## the number of subfault (aka grid point) we will be working in the loop
subfault = 0

for j in range(ny):
   ## first row: we fix j so we can characterize the laplacian points in the lower edge
    if j == 0: 
            for i in range(nx):
                ## length of the gridpoint along strike
                dx=L / nx
                ## length of the gridpoint along dip
                dy =ancho_placa*np.cos(dip_subf_rad[subfault]) / ny
                dxi = 1.0 / (dx*dx) 
                dyi = 1.0 / (dy*dy)
                ## por qué necesitamos dr?
                dr = dyi/dxi
                ## vertice inferior izquierdo
                if i == 0:
                    pos.append(np.array([ (i+1,j), (i+2,j), (i,j+1), (i,j+2), (i,j) ]))
                    coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
                 ## vertice inferior derecho
                elif i == nx-1 :
                    pos.append(np.array([ (i-1,j), (i-2,j), (i,j+1), (i,j+2), (i,j) ]))
                    coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
                ## arista inferior
                else :
                    pos.append(np.array([ (i+1,j), (i-1,j), (i,j+1), (i,j+2), (i,j) ]))
                    coef.append(np.array([ 1. , 1. , -2.*dr , dr , -2. + dr ]))
                subfault = subfault+ 1
            ## ultima fila
    elif j == ny-1 : 
            for i in range(nx):
             ##################################################################
                dx = L / nx
                dy = ancho_placa * np.cos(dip_subf_rad[subfault]) / ny  
                dxi = 1.0 / (dx*dx)
                dyi = 1.0 / (dy*dy)
                dr = dyi / dxi 
             ###################################################################                
                ## vertice superior izquierdo
                if i == 0 :
                    pos.append(np.array([ (i+1,j), (i+2,j), (i,j-1), (i,j-2), (i,j) ]))
                    coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
                 ## vertice superior derecho
                elif i == nx-1 :
                     pos.append(np.array([ (i-1,j), (i-2,j), (i,j-1), (i,j-2), (i,j) ]))
                     coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))             
                   ## arista superior
                else :
                    pos.append(np.array([ (i+1,j), (i-1,j), (i,j-1), (i,j-2), (i,j) ]))
                    coef.append(np.array([ 1. , 1. , -2.*dr , dr , -2. + dr ]))
                subfault = subfault +1
        
    else :             
            for i in range(nx):
             ##################################################################
                dx = L / nx
                dy = ancho_placa * np.cos(dip_subf_rad[subfault]) / ny  
                dxi = 1.0 / (dx*dx)
                dyi = 1.0 / (dy*dy)
                dr = dyi / dxi 
                ###################################################################
                    ## arista izquierda
                if i == 0:
                    pos.append(np.array([ (i+1,j), (i+2,j), (i,j+1), (i,j-1), (i,j) ]))
                    coef.append(np.array([ -2. , 1. , dr , dr , 1. - 2.*dr ]))
                  ## arista derecha
                elif i == nx-1 :
                    pos.append(np.array([ (i-1,j), (i-2,j), (i,j+1), (i,j-1), (i,j) ]))
                    coef.append(np.array([ -2. , 1. , dr , dr , 1. - 2.*dr ]))             
                 ## punto central en general
                else :
                    pos.append(np.array([ (i+1,j), (i-1,j), (i,j+1), (i,j-1), (i,j) ]))
                    coef.append(np.array([ 1. , 1. , dr , dr , -2. - 2.*dr ]))
                subfault = subfault + 1

for k in range(nx*ny):
   index = pos[k]
   m = 0
   print(index)
   for i,j in index:
       F[k][j*nx+i] = coef[k][m]
       m += 1