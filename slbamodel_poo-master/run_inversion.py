#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 21 00:09:42 2019

@author: doctor
"""



from calculo_intersismico import Intersismico

if __name__ == "__main__":
    params = {
        'zona':'Illapel',
        'smooth_matrix': False,
        'limites_zona': (-27,-36),
        'stations': [],
        'filtro':'cosismico',
        'inversion': False,
        'grid_size': (2,1),
        'plano_CD': False,
        'plano_E': False,
        'ID': '/solo_una',
        'desplazamientos_obs.txt': './Fuentes/single_fault_prueba.txt'
             }
#    intersismico = Intersismico(**params)
#    map_params = {
#        'latmin': -40,
#        'latmax': -10,
#        'lonmin': -90,
#        'lonmax': -65,
#        "paralelos": [-36, -32, -28, -24, -20],
#        "meridianos": [-75, -70],
#                  }
#    ## -40 -20 -80 -65
#    construir_params={
#            'AB': True,
#            'CD': False,
#            'E' : False   }
#    intersismico.set_map_params(**map_params)
#    #intersismico.build_map()
#    print("Planos de falla data")
#    intersismico.plano_falla_ab()
#    #intersismico.plano_falla_cd()
#    #intersismico.plano_falla_e()
#    print("Construyendo planos")
#    intersismico.construye_A()
    #intersismico.construye_B()
    #intersismico.construye_CD()
    #intersismico.construye_E()
    #intersismico.construye_planos()
#    print('Hacer problema directo')
#    pdirecto_params={'interfases':'single','save_slips':True}
#    intersismico.make_directo(**pdirecto_params)
#    print('Mapa de las fallas')
#    geom_params={'plot_fosa':True,
#                 'plot_contourf':True,
#                 'interfaz':'upper',
#                 'plot_datos':True,
#                 'which_datos':'directos',
#                 'quiverkey':False}
#    
#    intersismico.mapa_geometria(**geom_params)
#    print('Hacer problema directo')
#    pdirecto_params={'interfases':'single'}
#    intersismico.make_directo(**pdirecto_params)
    #print('Mapa de velocidades sinteticas')
    #directo_params={'tipo':'sintetico','datos':'dummy' }
    #intersismico.mapa_velocidades_sinteticas(**directo_params)
   
    #print('Mapa de velocidades sinteticas')
#    #intersismico.mapa_velocidades_sinteticas() 
    print("Haciendo inversion")
    inversion_params={'interfases':'single'}
    intersismico.inversion_intersismico_slab(**inversion_params)
    intersismico.calcula_inversion()
    #print('mapa velocidades invertidas y data')
    
#    mapa_todo_params={'plot_fosa':True,
#                     'plot_contourf':False,
#                     'interfaz':'upper',
#                     'plot_datos':False,
#                     'which_datos':'invertidas',
#                     'datos_and_inv':True,
#                     'quiverkey':False}
#    #intersismico.mapa_geometria(**mapa_todo_params)
#    
#    slip_upper_params={'plot_fosa':True,
#                     'plot_contourf':True,
#                     'interfaz':'lower',
#                     'plot_datos':False,
#                     'which_datos':'invertidas',
#                     'datos_and_inv':True,
#                     'quiverkey':False}
#    intersismico.mapa_geometria(**slip_upper_params)
#    almacenar_params={'interfases':'double'}
#    intersismico.almacenar_resultados(**almacenar_params)

#    print('Mapa de velocidades sinteticas')
#    sint_param={'tipo': 'ambos','datos':'directo_postinverso'}
#    intersismico.mapa_velocidades_sinteticas(**sint_param)
#    
#
#    