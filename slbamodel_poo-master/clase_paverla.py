# -*- coding: utf-8 -*-

from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import subroutine as sub
import numpy as np
import math
from datetime import datetime
import time    
from functools import reduce
"""
Args

zona_subduccion
zona(seccion)
filtros = {
intersmimico: Boolean,
cosismico: Boolean,
postsismico: Boolean
}

fecha_sismo

Boleanos:

datos_moreno
mapa_previo
mapa_vectores
nombre_estacion
sentido_subduccion = ['EW','WE'] --> {0,1}
generar_inversion
condicion_borde_fosa


Files String:

input_file_fosa="./DatosBase/perimetro_fosa_chile.txt" 
limites_zona = (lat_norte, lat_sur)


funcion -> archivo, limites.
-> corte

"""

import csv
def cut_file(filename,
             dict_types,
             limites=(0, -90),
             colname='Latitud'):
    norte = limites[0]
    sur = limites[1]
    data = []
    with open(filename, 'r') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=';')
        # extract headers
        for row in reader:
            final_types = {key:dict_types.get(key, str) for key in row.keys()}
            new_row = {key: final_types.get(key)(value) for key, value in row.items()}
            value = new_row.get(colname, 0)
            if sur <= value and value <= norte :
                data.append(new_row)
    return data

def find_real(lista, value, esp=0.01):
    for index, elem in enumerate(lista):
        if elem-esp<=value and value<=elem+esp:
            return index, elem


class Intersismico:
#class Seismic_Cycle:
    # # velocidad de placa en metros por año
    # v_placa = 0.068
    # # grosor de la placa en metros
    # W = 200000
    # # un rake de referencia
    # rake_inv_ref_deg = 120
    # # alto de la placa
    # H = 11000
    #delta_lat = 0.08
    #dip_plano_inicial = np.radians(14.)
    ## constantes de regularización
    #delta_lat = 0.08
    lambdas = dict.fromkeys(['A_min',
                             'A_suav',
                             'B_min',
                             'B_suav'], 0.02)

    def __init__(self, *args, **kwargs):
        self.nombre_zona = kwargs.get('zona', 'Chile')
        self.limites_zona = kwargs.get('limites_zona',
                                       (-18, -50))
        self.stations = kwargs.get('stations', [])
        self.filtro = kwargs.get('filtro', 'intersismico')
        self.actions = dict(
            intersismico=self.load_intersismico,
            cosismico=self.load_cosismico,
            postsismico=self.load_postsismico
        self.n_dim = kwargs.get('dimensiones', 2)
        self.dip_plano_inicial = kwargs.get('dip_inicial', np.radians(14.))
        self.delta_lat = kwargs.get('delta_lat', 0.08)
        ## velocidad de la placa en metros por año
        self.velocidad_placa = kwargs.get('vel_placa', 0.068)
        # grosor de la placa en metros
        self.groso_placa = kwargs.get('grosor_placa', 200000)
        # rake de referencia
        self.rake_referencia = kwargs.get('rake_referencia', 120)
        # alto de la placa en metros
        self.alto_placa = kwargs.get('alto_placa', 11000)
   
        self.action = self.actions.get(self.filtro)
        self.fecha = kwargs.get('fecha', datetime.utcnow().isoformat())
        self.mapa_previo = kwargs.get('mapa_previo', True)
        self.mapa_vectores = kwargs.get('mapa_vectores', True)
        ## para el caso de estudio de chile siempre es WE
        self.sentido_subduccion = kwargs.get('sentido', 'WE')
        # de no requerir inversión dejar Falso
        self.inversion = kwargs.get('inversion', True)
        self.condicion_borde_fosa = kwargs.get('condicion_borde_fosa', True)
        self.file_fosa = kwargs.get(
            'file_fosa',
            "./DatosBase/perimetro_fosa_chile.csv")
        self.file_profundidad = kwargs.get(
            'file_profundidad',
            "./DatosBase/profundidad_xyz_fosa_chile.csv")
        self.file_strike = kwargs.get(
            'file_strike',
            "./DatosBase/strike_fosa_chile.csv")
        self.file_dip = kwargs.get(
            'file_dip',
            "./DatosBase/dip_fosa_chile.csv")
        self.data_falla = self.load_data_falla()
        # datos de fuentes de estaciones
        ## cuales fallas construir, por default solo construye plano AB
        self.falla_AB=kwargs.get('plano_AB', True)
        self.falla_CD=kwargs.get('plano_CD', False)
        self.falla_E=kwargs.get('plano_E',  False)

        
        
        ## grillado de las subfallas
        self.grid_size = kwargs.get('grid_size', (75,5))
        ## planos de falla
        self.planos_falla = {}
        self.planos_falla_obj = {}
        ## distintos inputs y outputs dependiendo de la parte del ciclo que se quiera modelar
        if self.filtro == 'intersismico':
            ## fuente estaciones puede ser usada independientemente si se hace inversion o no
            self.fuente_estaciones = kwargs.get('velocidades_obs.txt','./Fuentes/estaciones_intersismico.txt')
            self.data_estaciones = self.action(stations=self.stations)
            self.velocidades_output=kwargs.get('velocidades_output','Outputs/Intersismico/synthetic_component.txt')
            self.lower_interface_output=kwargs.get('velocidades_output','Outputs/Intersismico/interseismic_slip_rate_lower_interface.txt')
            self.upper_interface_output=kwargs.get('velocidades_output','Outputs/Intersismico/interseismic_slip_rate_upper_interface.txt')
            # Debo generar esto
            self.velocidades_sinteticas=None
            
        if self.filtro == 'cosismico':
            ## fuente estaciones puede ser usada independientemente si se hace inversion o no
            self.fuente_estaciones = kwargs.get('desplazamientos_obs.txt','./Fuentes/estaciones_cosismico.csv')
            self.data_estaciones = self.action(stations=self.stations)
            self.lower_interface_output=kwargs.get('slips_output','Outputs/Cosismico/coseismic_slip__lower_interface.txt')
            self.upper_interface_output=kwargs.get('slips_output','Outputs/Cosismico/coseismic_slip__upper_interface.txt')
            self.desplazamientos_sinteticos=None
            
        # PROGRAMAR POSTSÍSMICO
        #if self.filtro = 'cosismico':
        #    self.data_desplaz = self.action(stations=self.stations)
        #    self.velocidades_sinteticos={}
        #    self.slip_sinteticos={}
        
        

        

    def plano_falla_ab(self, **kwargs):
        fosa = self.data_falla.get('fosa')
        fosa_lat = [elem.get('Latitud') for elem in fosa]
        fosa_lon = [elem.get('Longitud') for elem in fosa]
        # <-Limite superior de la fosa.
        #if self.filtro=="intersismico":
        latf = min(fosa_lat)
        index, latf = find_real(fosa_lat, latf)
        lonf = fosa_lon[index]
        lat1 = max(fosa_lat)
        index, lat1 = find_real(fosa_lat, lat1)
        lon1 = fosa_lon[index]
        # min(lat) fosa_lat[6]<-Limite inferior de la fosa.
        latf = min(fosa_lat)
        PF_AB = dict(
                superior=(lat1, lon1),
                inferior=(latf, lonf)
                    )
        self.planos_falla.update({'AB': PF_AB})

    def plano_falla_cd(self, **kwargs):
        ny_cd = kwargs.get('ny_cd', 8)
        w_cd = self.W/self.grid_size[1]
        PF_CD = dict(
                ny_cd=ny_cd,
                w_cd=w_cd
                    )
        self.planos_falla.update({'CD': PF_CD})

    def plano_falla_e(self, **kwargs):
        km = 1000
        ancho = kwargs.get('ancho_e', 300)
        E = dict(
            W_E=ancho*km,
            H_E=self.H,
            nx_E=self.grid_size[0],
            ny_E=1,
            delta_lat_E=-0.19
                )
        self.planos_falla.update({"E": E})

    def load_data_falla(self):
        data_type = {'Latitud': float,
                     'Longitud': float,
                     'Strike': float,
                     'Dip': float,
                     'Profundidad': float}
        data_profundidad = cut_file(
            self.file_profundidad,
            data_type,
            self.limites_zona,
            'Latitud')
        data_fosa = cut_file(
            self.file_fosa,
            data_type,
            self.limites_zona,
            'Latitud')
        data_strike = cut_file(
            self.file_profundidad,
            data_type,
            self.limites_zona,
            'Latitud')
        data_dip = cut_file(
            self.file_profundidad,
            data_type,
            self.limites_zona,
            'Latitud')
        data_falla = dict(
            fosa=data_fosa,
            profundidad=data_profundidad,
            strike=data_strike,
            dip=data_dip)
        return data_falla

    def load_intersismico(self, limites=(0, -90), stations=[]):
        data_type = {
                     'Latitud': float,
                     'Longitud': float,
                     'UE': lambda x: float(x)/1000,
                     'UN': lambda x: float(x)/1000,
                     'UU': lambda x: float(x)/1000
                    }
        archivo = self.fuente_estaciones
        data_velocidad = cut_file(archivo, data_type, limites)
        data_velocidad_set = data_velocidad
        if stations:
            data_velocidad_set = [data for data in data_velocidad
                                  if data.get('Station') in stations]

        return data_velocidad_set

    def load_cosismico(self, limites=(0,-90), stations=[]):
        data_type = { 
                     'Latitud': float,
                     'Longitud': float,
                     'UE': lambda x: float(x)/1000,
                     'UN': lambda x: float(x)/1000,
                     'UU': lambda x: float(x)/1000}
        archivo = self.fuente_estaciones
        data_disp = cut_file(archivo, data_type, limites)
        data_disp_set = data_disp
        if stations:
            data_disp_set = [data for data in data_disp
                                  if data.get('Station') in stations]

        return data_disp_set

    def load_postsismico(self):
        pass

    def set_map_params(self, **kwargs):
        self.map_params = dict(
            projection='merc',
            llcrnrlat=kwargs.get('latmin'),
            urcrnrlat=kwargs.get('latmax'),
            llcrnrlon=kwargs.get('lonmin'),
            urcrnrlon=kwargs.get('lonmax'),
            lat_ts=(kwargs.get('latmin')+kwargs.get('latmax'))/2,
            resolution='h',
            paralelos=kwargs.get('paralelos'),
            meridianos=kwargs.get('meridianos'))

    def build_map(self):
        del self.map_params['paralelos']
        del self.map_params['meridianos']
        plt.figure()
        m = Basemap(**self.map_params)
        m.drawcoastlines()
        m.drawparallels(
            np.arange(-90, 90, 5),
            labels=[1, 1, 1, 1])
        m.drawmeridians(
            np.arange(-90, 90, 5),
            labels=[0, 0, 0, 1])
        m.drawcountries()
        m.drawmapscale(-75, -41, 0, 0, 100)
        self.show_arrows(m)
        self.plot_fosa(m)
        plt.show()
        plt.savefig('Outputs/Figuras/mapa_previo.png',format='png',dpi='figure')

    

    
    
    def plot_fosa(self,m):
        lat_fosa=[elem.get('Latitud') for elem in self.data_falla.get('fosa')]
        lon_fosa=[elem.get('Longitud') for elem in self.data_falla.get('fosa')]
        x,y=m(lon_fosa,lat_fosa)
        m.plot(x,y,'-r',linewidth=2)
        #print('aaaa',lat_fosa,lon_fosa)
        
        
   
    def mapa_geometria(self):
        del self.map_params['paralelos']
        del self.map_params['meridianos']
        plt.figure()
        m = Basemap(**self.map_params)               
        m.drawcoastlines()
        m.drawcountries()
        #Se trazan los paralelos.
        m.drawparallels(
            np.arange(-90, 90, 5),
            labels=[1, 1, 1, 1])
        m.drawmeridians(
            np.arange(-90, 90, 5),
            labels=[0, 0, 0, 1])      

        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        x,y = m(lon,lat)
        m.plot(x,y,'go',markersize=10)
        self.plot_fosa(m)
        plt.savefig('Outputs/Figuras/geometria_modelo.png',format='png',dpi='figure')

        ##Se dibuja la fosa.
        #map.plot(mfosa_lon,mfosa_lat,'r-',linewidth = 1.5,zorder=22)
        ##Se grafica la poscicion de estaciones
        #map.scatter(mlon,mlat, 18, color="r", marker="o", edgecolor="k",linewidth = 0.4,zorder=22) 

        #Distribucion de subfallas para los planos de falla C y D
        if self.falla_CD:
            lon_fallas_cd=self.planos_falla_obj.get('CD')[0].vert_lon_new
            lat_fallas_cd=self.planos_falla_obj.get('CD')[0].vert_lat_new
            #print('lon_cd',lon_fallas_cd)
            #print('lat_cd',lat_fallas_cd)
            for lonx,laty in zip(lon_fallas_cd,lat_fallas_cd):
                x,y=m(lonx,laty)
                m.plot(x,y,color='r',  linestyle = '-', linewidth = 0.15,zorder=21)
           

        #Distribucion de subfallas para los planos de falla A y B
        if self.falla_AB:
            lon_fallas_ab=self.planos_falla_obj.get('AB')[0].vert_lon_new
            lat_fallas_ab=self.planos_falla_obj.get('AB')[0].vert_lat_new
            #print('lon_ab',lon_fallas_ab)
            #print('lat_ab',lat_fallas_ab)
            for lonx,laty in zip(lon_fallas_ab,lat_fallas_ab):
                x,y=m(lonx,laty)
                m.plot(x,y,color='b',  linestyle = '-', linewidth = 0.15,zorder=21)

 
            #Distribucion de subfallas para el plano de falla E     
        if self.falla_E:
            lon_fallas_e=self.planos_falla_obj.get('E')[0]
            lat_fallas_e=self.planos_falla_obj.get('E')[1]
            #print('lon_e',lon_fallas_e)
            #print('lat_e',lat_fallas_e)
            for lonx,laty in zip(lon_fallas_e,lat_fallas_e):
                x,y=m(lonx,laty)
                m.plot(x,y,color='r',  linestyle = '-', linewidth = 0.15,zorder=21)
        plt.show()
        
#

    def show_arrows(self, m):
        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        Ue_obs = [elem.get('UE') for elem in self.data_estaciones]
        Un_obs = [elem.get('UN') for elem in self.data_estaciones]
        V = m.quiver(
                     lon,
                     lat,
                     Ue_obs,
                     Un_obs,
                     color='r',
                     scale=0.25,
                     width=0.0050,
                     linewidth=0.5,
                     headwidth=4.,
                     zorder=26
                     )
            
        plt.quiverkey(
                      V,
                      0.75,
                      0.18,
                      0.01,
                      r'GPS obs: 1 cm/yr',
                      labelpos='N',
                      labelcolor= (0,0,0),zorder=26,
                      )
       
            
## ver si se puede arreglar esto         
#    def which_planos(self, **kwargs):
#        self.planos_params=dict(
#                                'AB'=True,
#                                'CD'=True,
#                                'E'=True
#                                )
#        
#    def construye_planos(self, **kwargs):
#        self.construye_AB()
#        self.construye_CD()
#        self.construye_E()

    def construye_AB(self):
        AB=self.planos_falla.get('AB')
        latf = AB.get('inferior')[0]
        lonf = AB.get('inferior')[1]
        lat1 = AB.get('superior')[0]
        lon1 = AB.get('superior')[1]
        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        print('data',self.data_estaciones)
        print('lon',lon,'lat',lat)
        L, strike_aux_deg, backstrike = sub.vinc_dist(
            latf,
            lonf,
            lat1,
            lon1 )
        strike_rad = None
        if self.sentido_subduccion == 'EW':
            strike_rad = np.radians(strike_aux_deg+180)
        if self.sentido_subduccion == 'WE':
            strike_rad = np.radians(strike_aux_deg)
        rake_norm_ref_deg = self.rake_inv_ref_deg+180
        coord_subfallas_opt = {
            'EW': sub.coordenadas_subfallas_EW,
            'WE': sub.coordenadas_subfallas}
        coord_subfallas = coord_subfallas_opt.get(self.sentido_subduccion, print)
                                                  
        lat0_ini, lon0_ini, alpha21  = sub.vinc_pt(
            latf,
            lonf,
            np.degrees(strike_rad+np.pi/2),
            self.W*np.cos(self.dip_plano_inicial) )
        
        #print('lat0', lat0_ini,'lon0',lon0_ini)
        
        nx = self.grid_size[0]
        ny = self.grid_size[1]
        fosa_lon = [elem.get('Longitud') for elem in self.data_falla.get('fosa')]
        fosa_lat = [elem.get('Latitud') for elem in self.data_falla.get('fosa')]       
        #print('fosa_lon',fosa_lon)
        #print('fosa_lat',fosa_lat)
        aux, aux, vertices_plano_inicial_lon, vertices_plano_inicial_lat = coord_subfallas(
            ny,
            nx,
            self.dip_plano_inicial,
            self.W,
            L,
            lon0_ini,
            lat0_ini,
            strike_rad,
            fosa_lon,
            fosa_lat,
            self.delta_lat)
        subfallas = sub.subfallas_SlabModel(
            nx,
            ny,
            self.W,
            L,
            strike_rad,
            vertices_plano_inicial_lon,
            vertices_plano_inicial_lat,
            self.file_profundidad,
            self.file_dip,
            self.file_strike)
        profundidad_subfallas_inversa = subfallas.Z
#        print('prof',profundidad_subfallas_inversa)
#        print('inicial_lon',vertices_plano_inicial_lon)
#        print('inicial_lat',vertices_plano_inicial_lat)
        #<-Profundidad de subfallas del plano de falla A
        dip_subfallas_radianes  = subfallas.dip_slab_rad
        #<-Dip de subfallas correspondientes a los planos de falla A y B.
        strike_subfallas_deg = subfallas.strike_deg
        #<-Strike de subfallas correspondientes a los planos de falla A y B.
        lon_vertices_subfallas_slab = subfallas.vert_lon_new
        #<-Longitud de los vertices de subfallas segun el slab 1.0 Model
        lat_vertices_subfallas_slab = subfallas.vert_lat_new
        #<-Latitud de los vertices de subfallas segun el slab 1.0 Model.
        lon_central_subfallas_slab = subfallas.lon_central_new
        #<-Longitud central de las subfallas con respecto al Slab 1.0 Model.
        lat_central_subfallas_slab = subfallas.lat_central_new
        #<-Latitud central de las subfallas con respecto al Slab 1.0 Model.
        #Se establece profundidad de las subfallas del plano de falla B en relacion al espesor de placa.
        profundidad_subfallas_normal = profundidad_subfallas_inversa + (
            self.H / np.cos(dip_subfallas_radianes))
        phi_placa_rad = np.radians(np.degrees(strike_rad) + (360-(self.rake_inv_ref_deg+180)))
        rake_inv_rad  = []
        #<-Rake para subfallas de la interfase superior con un movimiento relativo de tipo inverso.
        rake_norm_rad = []
        #<-Rake para subfallas de la interfase inferior con un movimiento relativo de tipo normal.
        for index, elem in enumerate(strike_subfallas_deg):
            # Se establece rake en subfallas emplazadas en la
            # interfase superior con un movimiento relativo de tipo inverso
            rake = sub.get_rake(
                    phi_placa_rad,
                    np.radians(elem),
                    dip_subfallas_radianes[index])
            rake_inv_rad.append(rake)
            # Se establece rake en subfallas emplazadas en la interfase
            # inferior con un movimiento relativo de tipo normal
            rake_norm_rad.append(rake_inv_rad[index]+np.pi)

        """
        OUT:
        subfallas,
        profundidad_subfallas_normal
        phi_placa_rad
        rake_inv_rad
        rake_norm_rad
        """
        n_dim = 2

        A = sub.model_matrix_slab(
            n_dim,
            dip_subfallas_radianes,
            profundidad_subfallas_inversa,
            self.W,
            L,
            rake_inv_rad,
            np.radians(strike_subfallas_deg),
            nx,
            ny,
            lon_vertices_subfallas_slab,
            lat_vertices_subfallas_slab,
            lon,
            lat)

        B = sub.model_matrix_slab(
                         n_dim,
                         dip_subfallas_radianes,
                         profundidad_subfallas_normal,
                         self.W,
                         L,
                         rake_norm_rad,
                         np.radians(strike_subfallas_deg),
                         nx,
                         ny,
                         lon_vertices_subfallas_slab,
                         lat_vertices_subfallas_slab,
                         lon,
                         lat)

        self.planos_falla_obj.update({'AB':
                                      (subfallas,
                                       phi_placa_rad,
                                       profundidad_subfallas_normal,
                                       rake_norm_ref_deg,
                                       rake_inv_rad,
                                       rake_norm_rad,
                                       L,
                                       A, B)})
        return (subfallas,
                phi_placa_rad,
                profundidad_subfallas_normal,
                rake_inv_rad,
                rake_norm_rad,
                A)

    def construye_CD(self):
        subfallas = self.planos_falla_obj.get('AB')[0]
        phi_placa_rad = self.planos_falla_obj.get('AB')[1]
        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]

        AB=self.planos_falla.get('AB')
        latf = AB.get('inferior')[0]
        lonf = AB.get('inferior')[1]
        lat1 = AB.get('superior')[0]
        lon1 = AB.get('superior')[1]
        L, strike_aux_deg, backstrike = sub.vinc_dist(
            latf,
            lonf,
            lat1,
            lon1 )
        strike_rad = None
        if self.sentido_subduccion == 'EW':
            strike_rad = np.radians(strike_aux_deg+180)
        if self.sentido_subduccion == 'WE':
            strike_rad = np.radians(strike_aux_deg)
        lat_vertices_subfallas_slab = subfallas.vert_lat_new
        lon_vertices_subfallas_slab = subfallas.vert_lon_new
        CD = self.planos_falla.get('CD')
        ny_cd = CD.get('ny_cd')
        w_cd = CD.get('w_cd')
        nx = self.grid_size[0]
        ny = self.grid_size[1]
        FIL = ny_cd*nx
        COL = 5
        lat_vert_CD_init    = []
        #<-Latitud de vertices de subfallas.
        lon_vert_CD_init    = []
        #<-Longitud de vertices de subfallas.
        lat_central_CD_init = []
        #<-Latitud de la posicion central de subfallas.
        lon_central_CD_init = []
        #<-Longitud de la posicion central de subfallas.
        for i in range(FIL): 
            lon_vert_CD_init.append([0]*COL)    
            lat_vert_CD_init.append([0]*COL)  
        alpha = np.degrees(strike_rad) ; beta = alpha + 90

        #Se constuyen vertices.
        inicio = time.time()

        subfalla = 0
        for i_nx in range(nx): 
           for j_ny in range(ny_cd):                   
            #Se construye primera subfalla adyacente a la primera fila de los planos
            #A y B. Se mantiene como ancho, el mismo valor asignado a las subfallas de
            #las regiones A y B.
                if j_ny == 0:        
                    #----------------------------------------------------------------
                    #Se construyen listas para almacenar vertices constructores. Estos, 
                    #se utilizaran para evaluar la diponibilidad de datos para esta 
                    #subfalla desde el modelo slab 1.0.        

                    #Vertice 2
                    lat_vert_CD_init[subfalla][2] = lat_vertices_subfallas_slab[i_nx][1]
                    lon_vert_CD_init[subfalla][2] = lon_vertices_subfallas_slab[i_nx][1]

                    #Vertice 3
                    lat_vert_CD_init[subfalla][3] = lat_vertices_subfallas_slab[i_nx][0]
                    lon_vert_CD_init[subfalla][3] = lon_vertices_subfallas_slab[i_nx][0]

                    #Vertice 0
                    lat_vert_CD_init[subfalla][0], lon_vert_CD_init[subfalla][0], aux = sub.vinc_pt(
                        lat_vert_CD_init[subfalla][3],
                        lon_vert_CD_init[subfalla][3],
                        beta ,
                        w_cd)

                    #Vertice 1
                    lat_vert_CD_init[subfalla][1], lon_vert_CD_init[subfalla][1], aux = sub.vinc_pt(
                        lat_vert_CD_init[subfalla][2],
                        lon_vert_CD_init[subfalla][2],
                        beta,
                        w_cd)

                    #Veretice 4
                    lat_vert_CD_init[subfalla][4] = lat_vert_CD_init[subfalla][0]
                    lon_vert_CD_init[subfalla][4] = lon_vert_CD_init[subfalla][0]

                else:

                    #------------------------------------------------------------------
                    #Se construyen listas para almacenar vertices constructores. Estos, 
                    #se utilizaran para evaluar la diponibilidad de datos para esta 
                    #subfalla desde el slab 1.0 model.        

                    #Vertice 2
                    lat_vert_CD_init[subfalla][2] = lat_vert_CD_init[subfalla-1][1]
                    lon_vert_CD_init[subfalla][2] = lon_vert_CD_init[subfalla-1][1]

                    #Vertice 3
                    lat_vert_CD_init[subfalla][3] = lat_vert_CD_init[subfalla-1][0]
                    lon_vert_CD_init[subfalla][3] = lon_vert_CD_init[subfalla-1][0]

                    #Vertice 0
                    lat_vert_CD_init[subfalla][0], lon_vert_CD_init[subfalla][0], aux = sub.vinc_pt(
                        lat_vert_CD_init[subfalla][3],
                        lon_vert_CD_init[subfalla][3],
                        beta,
                        w_cd)

                    #Vertice 1
                    lat_vert_CD_init[subfalla][1], lon_vert_CD_init[subfalla][1], aux = sub.vinc_pt(
                        lat_vert_CD_init[subfalla][2],
                        lon_vert_CD_init[subfalla][2],
                        beta,
                        w_cd)

                    #Vertice 4
                    lat_vert_CD_init[subfalla][4] = lat_vert_CD_init[subfalla][0]
                    lon_vert_CD_init[subfalla][4] = lon_vert_CD_init[subfalla][0]

                subfalla = subfalla + 1
#        print(nx,
#            ny_cd,
#            w_cd*ny_cd,
#            L,
#            strike_rad,
#            lon_vert_CD_init,
#            lat_vert_CD_init,
#            self.file_profundidad,
#            self.file_dip,
#            self.file_strike)
                    
        subfalla_CD  = sub.subfallas_SlabModel(
            nx,
            ny_cd,
            w_cd*ny_cd,
            L,
            strike_rad,
            lon_vert_CD_init,
            lat_vert_CD_init,
            self.file_profundidad,
            self.file_dip,
            self.file_strike)
        Z_inversa_CD_final = subfalla_CD.Z
        #<-Profundidad de subfallas del plano de falla C
        dip_radianes_CD_final = subfalla_CD.dip_slab_rad
        #<-Dip de subfallas correspondientes a los planos de falla C y D.
        strike_CD_final_deg = subfalla_CD.strike_deg
        #<-Strike de subfallas correspondientes a los planos de falla C y D.
        lon_vert_CD_final = subfalla_CD.vert_lon_new
        #<-Longitud de los vertices de subfallas segun el slab 1.0 Model.
        lat_vert_CD_final = subfalla_CD.vert_lat_new
        #<-Latitud de los vertices de subfallas segun el slab 1.0 Model.
        lon_central_CD_final = subfalla_CD.lon_central_new
        #<-Longitud central de las subfallas con respecto al Slab 1.0 Model.
        lat_central_CD_final = subfalla_CD.lat_central_new
        #<-Latitud central de las subfallas con respecto al Slab 1.0 Model.

        #Se establece profundidad de las subfallas del plano de falla D en relacion al espesor de placa.
        Z_normal_CD_final = Z_inversa_CD_final + (self.H / np.cos(dip_radianes_CD_final))

        '''
        -----------------------------------------------------------------------------
        Se obtienen variaciones en el rake de cada subfalla con respecto al sentido 
        de convergencia y el strike del plano de falla
        -----------------------------------------------------------------------------
        '''
        rake_inv_CD_rad  = []
        #<-Rake para subfallas de la interfase superior (C) con un movimiento relativo de tipo inverso.
        rake_norm_CD_rad = []
        #<-Rake para subfallas de la interfase inferior (D) con un movimiento relativo de tipo normal.

        for index, elem in enumerate(strike_CD_final_deg):
            #Se establece rake en subfallas emplazadas en la interfase superior con un movimiento relativo de tipo inverso
            rake = sub.get_rake(
                phi_placa_rad,
                np.radians(elem),
                dip_radianes_CD_final[index])
            rake_inv_CD_rad.append(rake)
            #Se establece rake en subfallas emplazadas en la interfase inferior con un movimiento relativo de tipo normal
            rake_norm_CD_rad.append(rake_inv_CD_rad[index]+np.pi)
        l_CD             = L/nx                         #<-Largo de cada subfalla en sentido del strike.
        num_subfallas_CD = len(dip_radianes_CD_final)   #<-Numero de subfallas del plano de falla C y D.

        #1. Caracterizacion geometrica para el plano de falla C
        print('6.2.1 Calculando matriz C...')
        n_dim = 2
       
        C = sub.model_matrix_slab_CD(
            n_dim,
            dip_radianes_CD_final,
            Z_inversa_CD_final,
            w_cd,
            l_CD,
            rake_inv_CD_rad,
            np.radians(strike_CD_final_deg),
            num_subfallas_CD,
            lon_vert_CD_final,
            lat_vert_CD_final,
            lon,
            lat)

        #2. Caracterizacion geometrica para el plano de falla D
        print('6.2.2 Calculando matriz D...')
        D = sub.model_matrix_slab_CD(
            n_dim,
            dip_radianes_CD_final,
            Z_normal_CD_final,
            w_cd,
            l_CD,
            rake_norm_CD_rad,
            np.radians(strike_CD_final_deg),
            num_subfallas_CD,
            lon_vert_CD_final,
            lat_vert_CD_final,
            lon,
            lat)
        self.planos_falla_obj.update({'CD':
                                      (subfalla_CD,
                                       Z_normal_CD_final,
                                       rake_inv_CD_rad,
                                       rake_norm_CD_rad,
                                       l_CD,
                                       C,
                                       D)})
        return (subfalla_CD,
                Z_normal_CD_final,
                rake_inv_CD_rad,
                rake_norm_CD_rad)

    def construye_E(self):
        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        AB = self.planos_falla.get('AB')
        latf = AB.get('inferior')[0]
        lonf = AB.get('inferior')[1]
        lat1 = AB.get('superior')[0]
        lon1 = AB.get('superior')[1]
        E = self.data_falla.get('E')
        constructor_E_opt = {
            'EW': sub.constructor_fallaE_EW,
            'WE': sub.constructor_fallaE_WE}
        constructor_E = constructor_E_opt.get(
            self.sentido_subduccion, print)
        fosa_lon = [elem.get('Longitud')
                    for elem in self.data_falla.get('fosa')]
        fosa_lat = [elem.get('Latitud')
                    for elem in self.data_falla.get('fosa')]

        PF_E = self.planos_falla.get('E')
        rake_norm_ref_deg = self.planos_falla_obj.get('AB')[3]
        lon_vertices_E, lat_vertices_E, L_E, strike_E_rad = constructor_E(
            lonf,
            latf,
            lon1,
            lat1,
            self.v_placa,
            PF_E.get("W_E"),
            PF_E.get("H_E"),
            PF_E.get("nx_E"),
            PF_E.get("ny_E"),
            PF_E.get("delta_lat_E"),
            fosa_lon,
            fosa_lat
        )
        # componentes de la velocidad
        n_dim = 2
        E = sub.model_matrix_slab_E(
            n_dim,
            0,
            PF_E.get("H_E"),
            PF_E.get("W_E"),
            L_E,
            np.radians(rake_norm_ref_deg),
            strike_E_rad,
            PF_E.get("nx_E"),
            PF_E.get("ny_E"),
            lon_vertices_E,
            lon_vertices_E,
            lon,
            lat)
        self.planos_falla_obj.update({'E':
                                      (lon_vertices_E,
                                       lat_vertices_E, L_E,
                                       strike_E_rad, E)})
        return lon_vertices_E, lat_vertices_E, L_E, strike_E_rad


    def calcula_inversion(self):
        print(' -----------------------------------------------------------------------')
        print(' ETAPA  6.1 == Caracterizacion geometrica: A y B                        ')
        print(' -----------------------------------------------------------------------')

        #2. Caracterizacion geometrica para el plano de falla A
        print('6.1.1 Calculando matriz A...')
        A = self.planos_falla_obj.get('AB')[-2]
#        print('matriz A')
#        print(A)
        #3. Caracterizacion geometrica para el plano de falla B
        print('6.1.2 Calculando matriz B...')
        B = self.planos_falla_obj.get('AB')[-1]
#        print('matriz B')
#        print(B)
        L = self.planos_falla_obj.get('AB')[-3]
        print('L')
        print(L)
        print(' -----------------------------------------------------------------------')
        print(' ETAPA  6.2 == Caracterizacion geometrica: C y D                        ')
        print(' -----------------------------------------------------------------------')    
        C = self.planos_falla_obj.get('CD')[-2]
        D = self.planos_falla_obj.get('CD')[-1]
#        print('Matriz C')
#        print(C)
#        print('Matriz D')
#        print(D)
        print(' -----------------------------------------------------------------------')
        print(' ETAPA  6.3 == Caracterizacion geometrica: E                            ')
        print(' -----------------------------------------------------------------------')       

        #1. Caracterizacion geometrica para el plano de falla E
        print('6.3.1 Calculando matriz E...')    
        E = self.planos_falla_obj.get('E')[-1]
#        print('Matriz E')
#        print(E)
        print(' -----------------------------------------------------------------------')
        print(' ETAPA  7 == Proceso de inversion                                       ')
        print(' -----------------------------------------------------------------------')

        #Se establecen velocidades libres en los planos de falla C, D y E.
        print('7.1 Incluyendo velocidad de placa     ...')
        l_CD  = self.planos_falla_obj.get('CD')[-3]
        dip_radianes_CD_final = self.planos_falla_obj.get('CD')[0].dip_slab_rad
        num_subfallas_CD = len(dip_radianes_CD_final)
        #Velocidades libres plano de falla C y D.
        for subfalla in range(num_subfallas_CD):  
            if subfalla == 0:
              Vp_CD = self.v_placa
            else:
              Vp_CD = np.hstack((Vp_CD, self.v_placa)) 
        E = self.planos_falla_obj.get('E')[-1]
        PF_E = self.planos_falla.get('E')
        for subfalla in range(PF_E.get("nx_E")*PF_E.get("ny_E")):
            if subfalla == 0:
              Vp_E = self.v_placa
            else:
              Vp_E = np.hstack((Vp_E, self.v_placa))  
        '''
        #############################################################################
        SE REALIZA INVERSION
        #############################################################################
        '''

        #Se realiza inversion de datos
        print('7.2 Realizando inversion              ...')
        n_dim = 2
        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        Ue_obs = [elem.get('VE') for elem in self.data_estaciones]
        Un_obs = [elem.get('VN') for elem in self.data_estaciones]
        Uz_obs = [elem.get('VU') for elem in self.data_estaciones]
        lmbd1, lmbd2, lmbd3, lmbd4 = (self.lambdas.get('A_min'),
                                      self.lambdas.get('A_suav'),
                                      self.lambdas.get('B_min'),
                                      self.lambdas.get('B_suav'))
        dip_subfallas_radianes  = self.planos_falla_obj.get('AB')[0].dip_slab_rad
        (slip_rate_aux,
         horror,
         horror_slip,
         horror_suave,
         U_corregido) = sub.inversion_intersismico_slab(
            n_dim,
            Ue_obs,
            Un_obs,
            Uz_obs,
            A,
            B,
            C,
            D,
            E,
            Vp_CD,
            Vp_E,
            lmbd1,
            lmbd2,
            lmbd3,
            lmbd4,
            dip_subfallas_radianes,
            self.W,
            L,
            self.grid_size[0],
            self.grid_size[1],
            self.v_placa,
            self.condicion_borde_fosa)
        print('slip_rate_aux',slip_rate_aux,len(slip_rate_aux))
        '''
        #############################################################################
        ERROR CUADRATICO MEDIO
        #############################################################################
        '''
        print('7.3 Obteniendo Error Cuadratico Medio ... ')

        Ue_aux = []
        Un_aux = []

        k = len(Ue_obs)
        for i in range(len(Ue_obs)):
           Ue_aux.append(U_corregido[i])
           Un_aux.append(U_corregido[i+k])

        #Desplazamientos observados finales
        Ue_obs_final = []
        Un_obs_final = []
        for i in range(len(Ue_obs)):
            if i == 0:
                Ue_obs_final = Ue_aux[i]
                Un_obs_final = Un_aux[i]
            else:
                Ue_obs_final = np.vstack( (Ue_obs_final, Ue_aux[i] ))
                Un_obs_final = np.vstack( (Un_obs_final, Un_aux[i] ))
        print('Ue_obs:',Ue_obs_final)
        Ue_obs_final=[x[0] for x in Ue_obs_final]
        Un_obs_final=[x[0] for x in Un_obs_final]
        #Se establece ECM para el conjunto de observaciones.
        profundidad_subfallas_inversa=self.planos_falla_obj.get('AB')[0].Z
        profundidad_subfallas_normal=self.planos_falla_obj.get('AB')[2]
#        print('prof inv:',profundidad_subfallas_inversa)
#        print('prof normal:',profundidad_subfallas_normal)
        #rake_inv_rad=self.planos_falla_obj.get('AB')[-3]
        #rake_norm_rad=self.planos_falla_obj.get('AB')[-2]
        rake_inv_rad=self.planos_falla_obj.get('AB')[-5]
        rake_norm_rad=self.planos_falla_obj.get('AB')[-4]
#        print('rake inv:',rake_inv_rad,'rake norm:',rake_norm_rad)
        strike_subfallas_deg=self.planos_falla_obj.get('AB')[0].strike_deg
        lon_vertices_subfallas_slab=self.planos_falla_obj.get('AB')[0].vert_lon_new
        lat_vertices_subfallas_slab=self.planos_falla_obj.get('AB')[0].vert_lat_new
        ECM_Ue, ECM_Un =sub.ECM_inversion_slab_intersismico(
            dip_subfallas_radianes,
            profundidad_subfallas_inversa,
            profundidad_subfallas_normal,
            self.W,
            L,
            rake_inv_rad,
            rake_norm_rad,
            np.radians(strike_subfallas_deg),
            self.grid_size[0],
            self.grid_size[1],
            slip_rate_aux,
            Ue_obs_final,
            Un_obs_final,
            Uz_obs,
            lon_vertices_subfallas_slab,
            lat_vertices_subfallas_slab,
            lon,
            lat)

        #Residual total.
        horror_total=horror+lmbd1*horror_slip+lmbd2*horror_suave

        print(' -----------------------------------------------------------------------')
        print('Resumen de datos: ')
        print(('1. lambda1      : ', lmbd1))
        print(('2. lambda2      : ', lmbd2))
        print(('3. lambda3      : ', lmbd3))
        print(('4. lambda4      : ', lmbd4))
        print(('5. Inv. ECM_Ue  : ', ECM_Ue))
        print(('6. Inv. ECM_Un  : ', ECM_Un))
        print(('7. Res. ||U-AS||: ', horror))
        print(('8. Res. ||S||   : ', horror_slip))
        print(('9. Res. ||FS||  : ', horror_suave))
        print(('10. Res. Total  : ', horror_total))
        print(' -----------------------------------------------------------------------')
        '''
        #############################################################################
        VELOCIDADES MODELADAS DESDE PROBLEMA DIRECTO
        #############################################################################
        '''

        print('7.4 Realizando problema directo ...')

        Ue_teo=[]    #<-Datos modelados en componente este.
        Un_teo=[]    #<-Datos modelados en componente norte.

        #Se obtienen velocidades modeladas en superficie desde los resultados de la
        #inversion. Se establece un problema directo.
        (Ue_teo,
         Un_teo,
         Ue_teo_inv,
         Un_teo_inv,
         Ue_teo_norm,
         Un_teo_norm) = sub.Pdirecto_intersismico(
            dip_subfallas_radianes,
            profundidad_subfallas_inversa,
            profundidad_subfallas_normal,
            self.W,
            L,
            rake_inv_rad,
            rake_norm_rad,
            np.radians(strike_subfallas_deg),
            self.grid_size[0],
            self.grid_size[1],
            slip_rate_aux,
            lon_vertices_subfallas_slab,
            lat_vertices_subfallas_slab,
            lon,
            lat)

        print(' -----------------------------------------------------------------------')
        print(' ETAPA 8 == Almacenando resultados                                      ')
        print(' -----------------------------------------------------------------------')

        #1 Se almacenan componentes de velocidades modeladas.
        #name='Outputs/Intersismico/synthetic_component.txt'
        name=self.velocidades_output
        with open(name, "w") as csvfile:
            writer = csv.writer(csvfile,
                                delimiter=';')
            writer.writerow(['Ue_obs', 'Un_obs', 'Ue_teo', 'Un_teo', 'lon', 'lat'])
            for row in zip(Ue_obs_final,
                         Un_obs_final,
                         Ue_teo,
                         Un_teo,
                         lon,
                         lat):
                writer.writerow(row)
            #writer.close() 
        self.velocidades_sinteticos.update({'Ve_teo':Ue_teo,'Vn_teo':Un_teo})
        #Se obtienen velocidades invertidas para los planos de falla A y B.
        slip_rate_upper_interface = []    #Velocidades para plano de falla A
        slip_rate_lower_interface = []    #Velocidades para plano de falla B

        k = reduce(lambda x, y: x*y, self.grid_size)
        print(k)

        for i in range(k):
           slip_rate_upper_interface.append(slip_rate_aux[i])
           slip_rate_lower_interface.append(slip_rate_aux[i+k])

        #Se almacenan resultados para la interfase superior A con un movimiento relativo de tipo inverso.
        #name='Outputs/Intersismico/interseismic_slip_rate_upper_interface.txt'
        name=self.upper_interface_output
        lon_central_subfallas_slab=self.planos_falla_obj.get('AB')[0].lon_central_new
        lat_central_subfallas_slab=self.planos_falla_obj.get('AB')[0].lat_central_new
        with open(name, "w") as csvfile:
            writer = csv.writer(csvfile, delimiter=';')
            writer.writerow(['Lon_central','Lat_central', 'Slip_rate_upper'])
            for row in zip(lon_central_subfallas_slab,
                         lat_central_subfallas_slab,
                         slip_rate_upper_interface):
                writer.writerow(row)
        
    
            #writer.close()

        #Se almacenan resultados para la interfase inferior B con un movimiento relativo de tipo normal.
        #name='Outputs/Intersismico/interseismic_slip_rate_lower_interface.txt'
        name=self.lower_interface_output
        with open(name, "w") as csvfile:
            writer = csv.writer(csvfile, delimiter=';')
            writer.writerow(['Lon_central', 'Lat_central', 'Slip_rate', 'Slip_rate_lower'])
            for row in zip(lon_central_subfallas_slab,
                           lat_central_subfallas_slab,
                           slip_rate_lower_interface):
                writer.writerow(row)
        self.slip_sinteticos.update({'Lon_central':lon_central_subfallas_slab,
                                           'Lat_central':lat_central_subfallas_slab,
                                           'Slip_upper':slip_rate_upper_interface,
                                           'Slip_lower':slip_rate_lower_interface})         
            #writer.close()
        
        #Se almacenan datos de la generación de la inversión:
        name='Outputs/Intersismico/Parametros_Inversion.txt'
        file = open(name, "w")
        file.write( ' -----------------------------------------------------------------------')
        file.write('\n')
        file.write( 'Resumen de datos: ')
        file.write('\n')
        file.write( '1. lambda1      : '+str(lmbd1))
        file.write('\n')
        file.write( '2. lambda2      : '+str(lmbd2))
        file.write('\n')
        file.write( '3. lambda3      : '+str(lmbd3))
        file.write('\n')
        file.write( '4. lambda4      : '+str(lmbd4))
        file.write('\n')
        file.write( '5. Inv. ECM_Ue  : '+str(ECM_Ue))
        file.write('\n')
        file.write( '6. Inv. ECM_Un  : '+str(ECM_Un))
        file.write('\n')
        file.write( '7. Res. ||U-AS||: '+str(horror))
        file.write('\n')
        file.write( '8. Res. ||S||   : '+str(horror_slip))
        file.write('\n')
        file.write( '9. Res. ||FS||  : '+str(horror_suave))
        file.write('\n')
        file.write( '10. Res. Total  : '+str(horror_total))
        file.write('\n')
        file.write( ' -----------------------------------------------------------------------')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( ' ETAPA  9 == RESUMEN PARAMETROS GEOMETRICOS Y RESULTADOS')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( '                                                        ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE SUPERIOR (A):                                 ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'Min Depth: '+str(min(profundidad_subfallas_inversa)/1000)+' km')
        file.write('\n')
        file.write( 'Max Depth: '+str(max(profundidad_subfallas_inversa)/1000)+' km')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Dip  : '+str(min(np.degrees(dip_subfallas_radianes)))+' degrees')
        file.write('\n')
        file.write( 'Max Dip  : '+str(max(np.degrees(dip_subfallas_radianes)))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Strike : '+str(min(strike_subfallas_deg))+' degrees')
        file.write('\n')
        file.write( 'Max Strike : '+str(max(strike_subfallas_deg))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Rake  : '+str(min(np.degrees(rake_inv_rad)))+' degrees')
        file.write('\n')
        file.write( 'Max Rake  : '+str(max(np.degrees(rake_inv_rad)))+' degrees')
        file.write('\n')
        file.write( '                                                        ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE INFERIOR (B):')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'Min Depth: '+str(min(profundidad_subfallas_normal)/1000)+' km')
        file.write('\n')
        file.write( 'Max Depth: '+str(max(profundidad_subfallas_normal)/1000)+' km')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Dip  : '+str(min(np.degrees(dip_subfallas_radianes)))+' degrees')
        file.write('\n')
        file.write( 'Max Dip  : '+str(max(np.degrees(dip_subfallas_radianes)))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Strike : '+str(min(strike_subfallas_deg))+' degrees')
        file.write('\n')
        file.write( 'Max Strike : '+str(max(strike_subfallas_deg))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Rake  : '+str(min(np.degrees(rake_norm_rad)))+' degrees')
        file.write('\n')
        file.write( 'Max Rake  : '+str(max(np.degrees(rake_norm_rad)))+' degrees')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( '          VELOCIDADES INVERTIDAS                         ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE SUPERIOR (A):                                 ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'Max. Vel: '+str(max(slip_rate_upper_interface))+'m/yr')
        file.write('\n')
        file.write( 'Min. Vel: '+str(min(slip_rate_upper_interface))+'m/yr')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE INFERIOR (B):                                 ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'Max. Vel: '+str(max(slip_rate_lower_interface))+'m/yr')
        file.write('\n')
        file.write( 'Min. Vel: '+str(min(slip_rate_lower_interface))+'m/yr')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'nx x ny: '+str(self.grid_size[0])+str('X')+str(self.grid_size[1]))
        file.write('\n')
        file.write( 'rake_inv_ref_deg: '+str(self.rake_inv_ref_deg))
        #file.write('\n')
        #file.write( 'Angulo convergencia: '+str(math.degrees(math.atan(Uy_v/Ux_v)))+str('°'))
        file.write('\n')
        file.write( 'V_placa: '+str(self.v_placa))
        file.write('\n')
        file.write( 'W :'+str(self.W))
        file.write('\n')
        file.write( 'H :'+str(self.H))
        file.close()
    def show_arrows_post(self, m):
        #print(self.data_velocidad)
        #print(self.velocidades_sinteticos)
        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        Ue_obs = [elem.get('VE') for elem in self.data_estaciones]
        Un_obs = [elem.get('VN') for elem in self.data_estaciones]
        Ve_teo =self.velocidades_sinteticos.get('Ve_teo')
        Vn_teo =self.velocidades_sinteticos.get('Vn_teo')
        x, y = m(lon, lat)
#        print('x',x,'y',y)
        x1,y1=m(-77,-19)
        x2,y2=m(-77,-23)
        #print('xx',xx,'yy',yy)
        #print(len(lon), len(lat), len(Ue_obs))
        V = m.quiver(
            x,y,
            Ue_obs,
            Un_obs,
            color='r',
            scale=0.25,
            width=0.0050,
            linewidth=0.5,
            headwidth=4.,
            zorder=26,
            label='V_obs')
        W =  m.quiver(
            x,y,
            Ve_teo,
            Vn_teo,
            color='k',
            scale=0.25,
            width=0.0050,
            linewidth=0.5,
            headwidth=4.,
            zorder=26,
            label='V_teo')
        plt.quiverkey(
               V,
               x1,
               y1,
               0.01,
               label='V_teo: 1cm/yr',
               labelpos='N',
               coordinates='data',
               labelcolor= (0,0,0),
               zorder=26, 
               fontproperties={'size': 8, 'weight': 'bold'})
        plt.quiverkey(
            W,
            x2,
            y2,
            0.01,
            'V obs: 1 cm/yr',
            labelpos='N',
            coordinates='data',
            labelcolor= (0,0,0),
            zorder=26, 
            fontproperties={'size': 8, 'weight': 'bold'})
#        plt.quiverkey(
#            V,
#            0.69,
#            0.9,
#            0.00001,
#            str(str('2007/01/01 - 2010/02/26')),
#            labelpos='N', #0.09
#            labelcolor=(0,0,0), 
#            fontproperties={'size': 10, 'weight': 'bold'})    
       
    def mapa_velocidades_sinteticas(self):
        plt.figure()
        m = Basemap(**self.map_params)
        m.drawcoastlines()
        m.drawparallels(
            np.arange(-90, 90, 5),
            labels=[1, 1, 1, 1])
        m.drawmeridians(
            np.arange(-90, 90, 5),
            labels=[0, 0, 0, 1])
        m.drawcountries()
        m.drawmapscale(-77, -38, 0, 0, 300)
        self.plot_fosa(m)
        self.show_arrows_post(m)
        #self.plot_fosa(m)
        plt.title('Velocidades sinteticas y observadas para la zona de subduccion de Chile.  01/01/2007-26/02/2007')
        plt.savefig('Outputs/Figuras/velocidades_sinteticas.png',format='png',dpi='figure')
        plt.show()
    ################################################################################
    # OUTPUT: coordenadas xi,yi proyectadas en los ejes de Okada
    #OBS FELIPE: La funcion determina las coordenadas okada (X Y) de un un punto
    #de observacion a partir de sus coordenadas geograficas.
    def proj_mesh2okada(self,lat_falla,lon_falla,strike): 
        lon_estaciones = [elem.get('Longitud') for elem in self.data_estaciones]
        lat_estaciones = [elem.get('Latitud') for elem in self.data_estaciones]
        dist_lat=[]
        dist_lon=[]
        for (lat_sta,lon_sta) in zip(lat_estaciones,lon_estaciones):
            l,az,baz=vinc_dist(lat_falla,lon_falla,lat_sta,lon_sta)
            dist_lat.append(l*np.cos(np.radians(az)))
            dist_lon.append(l*np.sin(np.radians(az)))
        ## transformar a array
        dist_lat_arr=np.array(dist_lat)
        dist_lon_arr=np.array(dist_lon)
        
        xi = dist_lat_arr*np.cos(strike) + dist_lon_arr*np.sin(strike)  
        yi = dist_lat_arr*np.sin(strike) - dist_lon_arr*np.cos(strike)

        return xi,yi

    def model_matrix_slab(self):
        AB   = self.planos_falla.get('AB')
        latf = AB.get('inferior')[0]
        lonf = AB.get('inferior')[1]
        lat1 = AB.get('superior')[0]
        lon1 = AB.get('superior')[1]
        largo_falla, strike_aux_deg, bastrike = sub.vinc_dist(latf,lonf,lat1,lon1)
        # largo de las subfallas a lo largo del strike
        largo_subf_strike=L/self.grid_size[0]
        # largo de las subfallas a lo largo del dip
        largo_subf_dip=L(self.grid_size[1]

   def model_matrix_slab(n_dim,dip_slab,depth_slab,W,L,rake,strike,nx,ny,lon_vert,lat_vert,lonGPS,latGPS):
    #Obtiene la caracterizacion geometrica de todas las subfallas de acuerdo al modelo slab 1.0.
    #Esto se realiza en terminos de la configuracion de la fosa, del dip y profunfidad
    #estimada en el modelo slab 1.0. 
    #Funcion valida para fallas rectagulares nx x ny.
    l = L/nx; w = W/ny
    subfalla=0
    for iy in range(ny):
        for ix in range(nx):
            if ix == 0 and iy == 0:
                ddx, ddy = proj_mesh2okada(latGPS,lonGPS,lat_vert[subfalla][0],lon_vert[subfalla][0],strike[subfalla])
                #Se determina caracterizacion geometrica de la primera subfalla.
                ve,vn,uz = desplaz_okada(ddx,ddy,dip_slab[subfalla],depth_slab[subfalla],w,l,rake[subfalla],strike[subfalla])
                if n_dim == 1 :
                    M = ve
                elif n_dim == 2 :
                    M = hstack((ve,vn))     #genera matriz dimensiones 1x2 [ve, vn]                
                elif n_dim == 3 :
                    M = hstack((ve,vn,uz))  #genera matriz dimensiones 1x2 [ve, vn]                   
            else:
                #Se determinan las coordendas Okada de las estaciones asumiento que la
                #nueva subfalla contiene el vertice de okada.
                '''
                print('latGPS',len(latGPS))
                print('lonGPS',len(lonGPS))
                print('lat_vert[subfalla][0]',lat_vert[subfalla][0])
                print('lon_vert[subfalla][0]',lon_vert[subfalla][0])
                print('strike[subfalla]',strike[subfalla])
                '''
                print('subfalla: ',subfalla)
                ddx, ddy = proj_mesh2okada(latGPS,lonGPS,lat_vert[subfalla][0],lon_vert[subfalla][0],strike[subfalla])
                #Se determina caracterizacion geometrica  
                ve,vn,uz = desplaz_okada(ddx,ddy,dip_slab[subfalla],depth_slab[subfalla],w,l,rake[subfalla],strike[subfalla])
                if n_dim == 1 :                
                    tempstack = ve
                elif n_dim == 2 :
                    tempstack = hstack((ve,vn))
                elif n_dim == 3 :
                    tempstack = hstack((ve,vn,uz))
                M = vstack((M,tempstack))
            subfalla=subfalla+1
    #obtiene la caracterizacion geometrica de todas las subfalla     
    A = M.T 
    return A    
    
def desplaz_okada(xi,yi,dip,d,w,l,rake,strike):
    #La funcion determina la caracterizacion geometrica para una determinada falla.
    # notacion  de Chinnery: f(e,eta)||= f(x,p)-f(x,p-W)-f(x-L,p)+f(x-L,W-p)
    p = yi*np.cos(dip) + d*np.sin(dip)                                       
    q = yi*np.sin(dip) - d*cos(dip)                                       
    e = np.array([xi,xi,xi-l,xi-l]).T; eta = np.array([p,p-w,p,p-w]).T       
    #qq array de q, hay cuatro valores porque se opera con los cuatro valores de eta.               
    qq = np.array([q,q,q,q]).T                    
    ytg = eta*np.cos(dip) + qq*np.sin(dip)           
    dtg = eta*np.sin(dip) - qq*np.cos(dip)           
    R = np.power(e**2 + eta**2 + qq**2, 0.5)     
    X = np.power(e**2 + qq**2, 0.5)               

    I5 = (1/np.cos(dip))*scp.np.arctan((eta*(X+qq*np.cos(dip))+X*(R+X)*np.sin(dip))/(e*(R+X)*np.cos(dip))) 
    I4 = 0.5/np.cos(dip)*(scp.log(R+dtg)-np.sin(dip)*scp.log(R+eta)) 
    # Solido de Poisson mu=lambda
    I1 = 0.5*((-1./np.cos(dip))*(e/(R+dtg)))-(np.sin(dip)*I5/np.cos(dip)) 
    I3 = 0.5*(1/np.cos(dip)*(ytg/(R+(dtg)))-scp.log(R+eta))+(np.sin(dip)*I4/np.cos(dip))  
    I2 = 0.5*(-scp.log(R+eta))-I3   #ok

    # dip-slip  (en direccion del manteo)
    ux_ds = -np.sin(rake)/(2*np.pi)*(qq/R-I3*np.sin(dip)*np.cos(dip))  #o
    uy_ds = -np.sin(rake)/(2*np.pi)*((ytg*qq/R/(R+e))+(np.cos(dip)*scp.arctan(e*eta/qq/R))
                -(I1*sin(dip)*cos(dip))) 
    uz_ds = -np.sin(rake)/(2*np.pi)*((dtg*qq/R/(R+e))+(np.sin(dip)*scp.arctan(e*eta/qq/R))
                -(I5*np.sin(dip)*cos(dip))) 

    # strike-slip  (en direccion del strike)
    ux_ss = -np.cos(rake)/(2*np.pi)*((e*qq/R/(R+eta))+(scp.arctan(e*eta/(qq*R)))+I1*np.sin(dip))  
    uy_ss = -np.cos(rake)/(2*np.pi)*((ytg*qq/R/(R+eta))+qq*np.cos(dip)/(R+eta)+I2*np.sin(dip))       
    uz_ss = -np.cos(rake)/(2*np.pi)*((dtg*qq/R/(R+eta))+qq*np.sin(dip)/(R+eta)+I4*np.sin(dip))
        
    # representacion chinnery dip-slip
    uxd = ux_ds.T[0]-ux_ds.T[1]-ux_ds.T[2]+ux_ds.T[3]   
    uyd = uy_ds.T[0]-uy_ds.T[1]-uy_ds.T[2]+uy_ds.T[3]   
    uzd = uz_ds.T[0]-uz_ds.T[1]-uz_ds.T[2]+uz_ds.T[3]   

    # representacion chinnery strike-slip
    uxs = ux_ss.T[0]-ux_ss.T[1]-ux_ss.T[2]+ux_ss.T[3]  
    uys = uy_ss.T[0]-uy_ss.T[1]-uy_ss.T[2]+uy_ss.T[3]  
    uzs = uz_ss.T[0]-uz_ss.T[1]-uz_ss.T[2]+uz_ss.T[3]  

    # solucion 
    ux = uxd+uxs  
    uy = uyd+uys  
    uz = uzd+uzs 
    # proyeccion a las componentes geograficas
    # rotacion de sistema de coordenadas 
    Ue = ux*np.sin(strike) - uy*np.cos(strike) 
    Un = ux*np.cos(strike) + uy*np.sin(strike) 
    
    #El gran problema es que falta el vector de deslizamiento, pero se podria agregar despues (multiplicando)

    return Ue,Un,uz
