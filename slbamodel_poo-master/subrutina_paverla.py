

from math import *
from numpy import *
import scipy as scp
from scipy import optimize
#from geopy import distance.vincenty
import matplotlib.pyplot as plt
#from matplotlib.mlab import griddata
from scipy.interpolate import griddata
from fractions import Fraction
import numpy as np

'''
#############################################################################
Felipe Vera S. - 2017
Consultas preguntas a fveras@udec.cl/fvera.sanhueza@gmail.com

MODIFICADO POR:
Leonardo Aguirre Sandoval. - 2018
Consultas o preguntas a leoaguirre@udec.cl/leaguirre1992@gmail.com
#############################################################################
'''

################################################################################
# OUTPUT: ux,uy,uz en coordenadas de okada
def desplaz_okada(xi,yi,dip,d,w,l,rake,strike):
    #La funcion determina la caracterizacion geometrica para una determinada falla.
    # notacion  de Chinnery: f(e,eta)||= f(x,p)-f(x,p-W)-f(x-L,p)+f(x-L,W-p)
    p = yi*cos(dip) + d*sin(dip)                                       #ok
    q = yi*sin(dip) - d*cos(dip)                                       #ok
    e = array([xi,xi,xi-l,xi-l]).T; eta = array([p,p-w,p,p-w]).T       #ok
    qq = array([q,q,q,q]).T    # b = 4  ok, qq array de q, hay cuatro valores porque se opera con los cuatro valores de eta.                              

    ytg = eta*cos(dip) + qq*sin(dip)           #ok
    dtg = eta*sin(dip) - qq*cos(dip)           #ok
    R = power(e**2 + eta**2 + qq**2, 0.5)      #ok,  is equivalent to using the power operator: x**y.
    X = power(e**2 + qq**2, 0.5)               #ok

    I5 = (1/cos(dip))*scp.arctan((eta*(X+qq*cos(dip))+X*(R+X)*sin(dip))/(e*(R+X)*cos(dip))) #ok
    I4 = 0.5/cos(dip)*(scp.log(R+dtg)-sin(dip)*scp.log(R+eta)) #ok
    I1 = 0.5*((-1./cos(dip))*(e/(R+dtg)))-(sin(dip)*I5/cos(dip)) #ok Solido de Poisson mu=lambda
    I3 = 0.5*(1/cos(dip)*(ytg/(R+(dtg)))-scp.log(R+eta))+(sin(dip)*I4/cos(dip))  #ok
    I2 = 0.5*(-scp.log(R+eta))-I3   #ok

    # dip-slip  (en direccion del manteo)
    ux_ds = -sin(rake)/(2*pi)*(qq/R-I3*sin(dip)*cos(dip))  #ok
    uy_ds = -sin(rake)/(2*pi)*((ytg*qq/R/(R+e))+(cos(dip)*scp.arctan(e*eta/qq/R))-(I1*sin(dip)*cos(dip))) #ok
    uz_ds = -sin(rake)/(2*pi)*((dtg*qq/R/(R+e))+(sin(dip)*scp.arctan(e*eta/qq/R))-(I5*sin(dip)*cos(dip))) #ok

    # strike-slipe  (en direccion del strike)
    ux_ss = -cos(rake)/(2*pi)*((e*qq/R/(R+eta))+(scp.arctan(e*eta/(qq*R)))+I1*sin(dip))  #ok
    uy_ss = -cos(rake)/(2*pi)*((ytg*qq/R/(R+eta))+qq*cos(dip)/(R+eta)+I2*sin(dip))       #ok
    uz_ss = -cos(rake)/(2*pi)*((dtg*qq/R/(R+eta))+qq*sin(dip)/(R+eta)+I4*sin(dip))
        
    # representacion chinnery dip-slip
    uxd = ux_ds.T[0]-ux_ds.T[1]-ux_ds.T[2]+ux_ds.T[3]   #ok
    uyd = uy_ds.T[0]-uy_ds.T[1]-uy_ds.T[2]+uy_ds.T[3]   #ok
    uzd = uz_ds.T[0]-uz_ds.T[1]-uz_ds.T[2]+uz_ds.T[3]   #ok

    # representacion chinnery strike-slip
    uxs = ux_ss.T[0]-ux_ss.T[1]-ux_ss.T[2]+ux_ss.T[3]  #ok
    uys = uy_ss.T[0]-uy_ss.T[1]-uy_ss.T[2]+uy_ss.T[3]  #ok
    uzs = uz_ss.T[0]-uz_ss.T[1]-uz_ss.T[2]+uz_ss.T[3]  #ok

    # solucion 
    ux = uxd+uxs  #ok
    uy = uyd+uys  #ok
    uz = uzd+uzs #ok

    # proyeccion a las componentes geograficas 
    Ue = ux*sin(strike) - uy*cos(strike) #ok rotacio de sistema de coordenadas
    Un = ux*cos(strike) + uy*sin(strike) #ok
    
    #El gran problema es que falta el vetor de deslizamiento, pero se podria agregar despues (multiplicando)

    return Ue,Un,uz

################################################################################
# OUTPUT: coordenadas xi,yi proyectadas en los ejes de Okada
#OBS FELIPE: La funcion determina las coordenadas okada (X Y) de un un punto
#de observacion a partir de sus coordenadas geograficas.
def proj_mesh2okada(lat,lon,lat0,lon0,strike):

    dlat = list(range(len(lat))) #Se crea un arreglo con dimensiones igual a la cantidad de observaciones de lat.
    dlon = list(range(len(lon))) # lo mismo para la latitud
    for i in range(len(lat)):
        llat = lat[i]; llon = lon[i]
        #print('lat',lat,'lon',lon,'lat0',lat0,'lon0',lon0,'strike',strike)
        l , az, baz = vinc_dist( lat0 , lon0 , llat , llon) #Problema geodesico, distancia l en metros y azimut en grados
        dlat[i] = l * cos( radians(az) )   #se obtiene distancia en latitud entre las coordenadas geograficas de okada y la observacion
        dlon[i] = l * sin( radians(az) )   #Lo mismo pero con la longitud

    Dla = array(dlat)  #se crea arreglo con las distancias en latitud obtenida
    Dlo = array(dlon) # se crea arreglo con las distanciass en longitud obtenida

   #Se rota el sistema de coordenadas dlat y dlon (metros) al sistema de okada. Rotacion antohoraria, pero hay que invertir el eje Y de okada (por eso un -cos)
    xi = Dla*cos(strike) + Dlo*sin(strike)  
    yi = Dla*sin(strike) - Dlo*cos(strike)

    #se retorana las coordenadas okada.
    return xi,yi




    
def model_matrix_slab(n_dim,dip_slab,depth_slab,W,L,rake,strike,nx,ny,lon_vert,lat_vert,lonGPS,latGPS):
    #Obtiene la caracterizacion geometrica de todas las subfallas de acuerdo al modelo slab 1.0.
    #Esto se realiza en terminos de la configuracion de la fosa, del dip y profunfidad
    #estimada en el modelo slab 1.0. 
    #Funcion valida para fallas rectagulares nx x ny.
    l = L/nx; w = W/ny
    subfalla=0
    for iy in range(ny):
        for ix in range(nx):
            if ix == 0 and iy == 0:
                ddx, ddy = proj_mesh2okada(latGPS,lonGPS,lat_vert[subfalla][0],lon_vert[subfalla][0],strike[subfalla])
                #Se determina caracterizacion geometrica de la primera subfalla.
                ve,vn,uz = desplaz_okada(ddx,ddy,dip_slab[subfalla],depth_slab[subfalla],w,l,rake[subfalla],strike[subfalla])
                if n_dim == 1 :
                    M = ve
                elif n_dim == 2 :
                    M = hstack((ve,vn))     #genera matriz dimensiones 1x2 [ve, vn]                
                elif n_dim == 3 :
                    M = hstack((ve,vn,uz))  #genera matriz dimensiones 1x2 [ve, vn]                   
            else:
                #Se determinan las coordendas Okada de las estaciones asumiento que la
                #nueva subfalla contiene el vertice de okada.
                '''
                print('latGPS',len(latGPS))
                print('lonGPS',len(lonGPS))
                print('lat_vert[subfalla][0]',lat_vert[subfalla][0])
                print('lon_vert[subfalla][0]',lon_vert[subfalla][0])
                print('strike[subfalla]',strike[subfalla])
                '''
                print('subfalla: ',subfalla)
                ddx, ddy = proj_mesh2okada(latGPS,lonGPS,lat_vert[subfalla][0],lon_vert[subfalla][0],strike[subfalla])
                #Se determina caracterizacion geometrica  
                ve,vn,uz = desplaz_okada(ddx,ddy,dip_slab[subfalla],depth_slab[subfalla],w,l,rake[subfalla],strike[subfalla])
                if n_dim == 1 :                
                    tempstack = ve
                elif n_dim == 2 :
                    tempstack = hstack((ve,vn))
                elif n_dim == 3 :
                    tempstack = hstack((ve,vn,uz))
                M = vstack((M,tempstack))
            subfalla=subfalla+1
    #obtiene la caracterizacion geometrica de todas las subfalla     
    A = M.T 
    return A    
    
    
def model_matrix_slab_CD(n_dim,dip_slab,depth_slab,w,l,rake,strike,num_subfallas,lon_vert,lat_vert,lonGPS,latGPS):
    #Obtiene la caracterizacion geometrica de todas las subfallas de acuerdo al modelo slab 1.0.
    #Esto se realiza en terminos de la configuracion de la fosa, del dip y profunfidad
    #estimada en el modelo slab 1.0. 
    #Funcion valida para fallas rectagulares nx x ny.
    
    for subfalla in range(num_subfallas):
        if subfalla == 0:
            ddx, ddy = proj_mesh2okada(latGPS,lonGPS,lat_vert[subfalla][0],lon_vert[subfalla][0],strike[subfalla])
            #Se determina caracterizacion geometrica de la primera subfalla.
            ve,vn,uz = desplaz_okada(ddx,ddy,dip_slab[subfalla],depth_slab[subfalla],w,l,rake[subfalla],strike[subfalla])
            if n_dim == 1 :
                M = ve
            elif n_dim == 2 :
                M = hstack((ve,vn))     #genera matriz dimensiones 1x2 [ve, vn]                
            elif n_dim == 3 :
                M = hstack((ve,vn,uz))  #genera matriz dimensiones 1x2 [ve, vn]                   
        else:
            #Se determinan las coordendas Okada de las estaciones asumiento que la
            #nueva subfalla contiene el vertice de okada.
            ddx, ddy = proj_mesh2okada(latGPS,lonGPS,lat_vert[subfalla][0],lon_vert[subfalla][0],strike[subfalla])
            #Se determina caracterizacion geometrica  
            ve,vn,uz = desplaz_okada(ddx,ddy,dip_slab[subfalla],depth_slab[subfalla],w,l,rake[subfalla],strike[subfalla])
            if n_dim == 1 :                
                tempstack = ve
            elif n_dim == 2 :
                tempstack = hstack((ve,vn))
            elif n_dim == 3 :
                tempstack = hstack((ve,vn,uz))
            M = vstack((M,tempstack))

    #obtiene la caracterizacion geometrica de todas las subfalla     
    A = M.T 
    return A        
    
def model_matrix_slab_E(n_dim,dip,D,W,L,rake,strike,nx,ny,lon_vert,lat_vert,lonGPS,latGPS):
#Obtiene la caracterizacion geometrica de todas las subfallas de acuerdo al modelo slab1.0.
    l = L/nx; w = W/ny
    subfalla=0
    for iy in range(ny):
        dy = (iy)*w*cos(dip); dz = dy*tan(dip)
        for ix in range(nx):
            if ix == 0 and iy == 0:
                ddx, ddy = proj_mesh2okada(latGPS,lonGPS,lat_vert[subfalla][0],lon_vert[subfalla][0],strike)                                  
                ve,vn,uz = desplaz_okada(ddx,ddy,dip,D,w,l,rake,strike)
                if n_dim == 1 :
                    M = ve
                elif n_dim == 2 :
                    M = hstack((ve,vn))     #genera matriz dimensiones 1x2 [ve, vn]                
                elif n_dim == 3 :
                    M = hstack((ve,vn,uz))  #genera matriz dimensiones 1x2 [ve, vn]                   
            else:
                ddx, ddy = proj_mesh2okada(latGPS,lonGPS,lat_vert[subfalla][0],lon_vert[subfalla][0],strike)                  
                ve,vn,uz = desplaz_okada(ddx,ddy,dip,D-dz,w,l,rake,strike)
                if n_dim == 1 :                
                    tempstack = ve
                elif n_dim == 2 :
                    tempstack = hstack((ve,vn))
                elif n_dim == 3 :
                    tempstack = hstack((ve,vn,uz))
                M = vstack((M,tempstack))
            subfalla=subfalla+1
    A = M.T #obtiene la caracterizacion geometrica de todas las subfalla A=[Ve ..... Ve ;Vn...Vn] dim 2xn 
    return A        
      

def subfalla(dip,W,L,nx,ny,lon0,lat0,strike):
    #Se obtienen las coordenadas centrales asumuendo una geometrica de falla rectangular.
    dx = L / nx; dy = W * cos(dip) / ny   #largo y ancho de las subfallas
     
    xs = []; ys = []

    #Se obtiene las coordenadas del centro de cada subfalla (latitud y longitud)
    #Esto permmite asociarlo con el deslizamiento obtenido y graficarlo en un mapa.
    alpha = degrees(strike) ; beta = alpha + 270.0
    
    lat1,  lon1, alpha21 = vinc_pt( lat0, lon0, alpha , dx/2 )
    lat2,  lon2, alpha21 = vinc_pt( lat1, lon1,  beta , dy/2 )  #Coordenadas centrales de la primera subfalla.
    for j in range(ny):
        for i in range(nx):
            lat3 = lat2 ; lon3 = lon2
            if i > 0:
             lat3,  lon3, alpha21 = vinc_pt( lat2, lon2, alpha , i*dx )
            lat4 = lat3 ; lon4 = lon3
            if j > 0 :
             lat4 , lon4, alpha21 = vinc_pt( lat3, lon3,  beta , j*dy )
            xs.append(lon4)
            ys.append(lat4)

    lon_slip = array([xs])[0]
    lat_slip = array([ys])[0]
    print('komodoro')
    aa=[(x,y) for x,y in zip(lon_slip,lat_slip) ]
    print('aa')
    return  lon_slip, lat_slip

def coordenadas_subfallas(ny,nx,dip,W,L,lon0,lat0,strike,fosa_lon,fosa_lat,delta_lat):

    #La funcion obtiene las coordenadas centrales de las subfallas de acuerdo a la
    #configuracion del slab model, junto con las coordendas de los vertices de esta.    
    
    #El problema comienza generando una falla rectangular dividida en ny*nx subfallas,
    #para posteriormente desplazarlas en direccion hacia la fosa y obtener asi una configuracion
    #de subfallas de acuerdo al slab model.
    #Felipe Vera S.
    #fveras@udec.cl    
    
    #1. Se determina latitud y longitud de cada subfalla asumiendo una destribucion rectangular.
    lon_sl, lat_sl = subfalla(dip,W,L,nx,ny,lon0,lat0,strike) 
    dx = L / nx; dy = W * cos(dip) / ny
    
    #Variables para almacenar la posicion de central de las subfallas de acuerso al Slab Model..
    lon_central=[]
    lat_central=[]

    #1.1 Se obtiene posicion central de las subfallas de acuerdo a la geometria rectangular.
    k=0
    for i_nx in range(nx):
        for i_ny in range(ny):
            lon_central.append(lon_sl[k])
            lat_central.append(lat_sl[k])               
            k=k+1; 
        
    #1.2 Se generan vertices de las subfallas de acuerdo a la geometria rectangular.
    FIL=nx*ny  #Se evaluaran todas las subfallas.
    COL=5      #Cada subfalla tiene cinco vertices a dibujar (el ultimo coincide con el primero)

    #Se decalran variables para almacenar vertices de todas las subfallas.
    vertices_lon = [] 
    vertices_lat = []
    
    #Se crean matrices de ceros para rellenar con los vertices de todas las subfallas.
    for i in range(FIL):                #Numero de filas de la matriz
        vertices_lon.append([0]*COL)    #Numero de columnas de la matriz, se inicializa con ceros
        vertices_lat.append([0]*COL)
    
    #1.3 Se declaran vertices claves: Son los vertices superior e inferior de las subfallas que estan
    #Junto a la fosa. Sirven para buscar los valores de referencia con el modelo slab1.0    
    vertice_claveinf_lon=[]
    vertice_claveinf_lat=[]
    vertice_clavesup_lon=[]
    vertice_clavesup_lat=[]
    
    for i in range(nx):                       #Numero de filas de la matriz
        vertice_claveinf_lon.append([0]*1)    #Numero de columnas de la matriz, se inicializa con ceros
        vertice_claveinf_lat.append([0]*1)
        vertice_clavesup_lon.append([0]*1)    
        vertice_clavesup_lat.append([0]*1)

    #1.4 Se generan los vertices de todas las subfallas y se almacenan los vertices claves (todavia una geometria rectangular).
    k=0
    for i_ny in range(ny):
        fila=0
        for j_nx in range(nx):
                frac=Fraction((ny-i_ny),ny)        

                #Latitud,longitud inicial
                lataux,lonaux,n = vinc_pt(lat_central[k], lon_central[k], np.degrees(strike)+90, dy/2 )
                lat_ini,lon_ini,n = vinc_pt(lataux, lonaux, np.degrees(strike)+180, dx/2 )
        
                #Segundo vertice
                lataux,lonaux,n = vinc_pt(lat_central[k], lon_central[k], np.degrees(strike)+90, dy/2 )
                lat1,lon1,n = vinc_pt(lataux, lonaux, np.degrees(strike), dx/2 )
            
                #Tercer vertice
                lat2,lon2,aux2 = vinc_pt(lat1, lon1, np.degrees(strike)+270, frac*W*np.cos(dip) )
              
                #Cuarto vertice
                lat3,lon3,aux2 = vinc_pt(lat2, lon2, np.degrees(strike)+180, dx)
        
                #Quinto Vertice
                lat4 = lat_ini  
                lon4 = lon_ini

                vertices_lon[k][0]=lon_ini
                vertices_lon[k][1]=lon1
                vertices_lon[k][2]=lon2
                vertices_lon[k][3]=lon3
                vertices_lon[k][4]=lon4

                vertices_lat[k][0]=lat_ini
                vertices_lat[k][1]=lat1
                vertices_lat[k][2]=lat2
                vertices_lat[k][3]=lat3
                vertices_lat[k][4]=lat4
            
               #Vertices claves.
                if (i_ny==ny-1):  #si se esta en la ultima fila (junto a la fosa)
                    vertice_claveinf_lon[j_nx][0]=lon3
                    vertice_claveinf_lat[j_nx][0]=lat3
                    vertice_clavesup_lon[j_nx][0]=lon2
                    vertice_clavesup_lat[j_nx][0]=lat2

                k=k+1
        fila=fila+1;
        
        


    #1.5 A partir de los vertices claves se obtiene la distancia de esto con la fosa (modelo slab 1.0)
    dl=[]
 
    for i_nx in range(nx):
        #Se ingresa a una subfalla y se busca la maxima longitud proyectrada a la fosa
        minlon= vertice_claveinf_lon[i_nx][0]
        for j in range(len(fosa_lat)):
            if (fosa_lat[j]>=vertice_claveinf_lat[i_nx][0]+delta_lat and fosa_lat[j]<=vertice_clavesup_lat[i_nx][0]+delta_lat):
                if fosa_lon[j]<minlon:
                    minlon=fosa_lon[j]
        #Se calcula distancia a desplazar para todos los puntos centrales.
        #Se esta probando delta LAT, TEST
        L, aux, aux = vinc_dist(  vertice_claveinf_lat[i_nx][0],  vertice_claveinf_lon[i_nx][0],   vertice_claveinf_lat[i_nx][0],  minlon ) 
        dl.append(L)


    #1.6 Se obtienen posiciones finales de las subfallas asociadas a una geometria del slab model.                
            
    lon_central_new=[]
    lat_central_new=[]

    k=0
    for i_ny in range(ny):
        ll=0
        for i_nx in range(nx):
            dist=dl[ll]
            lat_aux,  lon_aux,  alpha21  = vinc_pt( lat_central[k], lon_central[k], np.degrees(strike)+270,dist) 
            lon_central_new.append(lon_aux)
            lat_central_new.append(lat_aux)               
            k=k+1; 
            ll=ll+1
    
    #1.7 Se reesciben los vertices desde una distribucion rectangular a una adaptada al slab        
    k=0
    for i_ny in range(ny):
        fila=0
        for j_nx in range(nx):
                #frac=Fraction((ny-i_ny),ny)   
                frac_W=Fraction(W,ny)

                #Latitud,longitud inicial
                lataux,lonaux,n = vinc_pt(lat_central_new[k], lon_central_new[k], np.degrees(strike)+90, dy/2 )
                lat_ini,lon_ini,n = vinc_pt(lataux, lonaux, np.degrees(strike)+180, dx/2 )
        
                #Segundo vertice
                lataux,lonaux,n = vinc_pt(lat_central_new[k], lon_central_new[k], np.degrees(strike)+90, dy/2 )
                lat1,lon1,n = vinc_pt(lataux, lonaux, np.degrees(strike), dx/2 )
            
                #Tercer vertice
                lat2,lon2,aux2 = vinc_pt(lat1, lon1, np.degrees(strike)+270, frac_W*np.cos(dip) )
              
                #Cuarto vertice
                lat3,lon3,aux2 = vinc_pt(lat2, lon2, np.degrees(strike)+180, dx)
        
                #Quinto Vertice
                lat4 = lat_ini  
                lon4 = lon_ini

                vertices_lon[k][0]=lon_ini
                vertices_lon[k][1]=lon1
                vertices_lon[k][2]=lon2
                vertices_lon[k][3]=lon3
                vertices_lon[k][4]=lon4

                vertices_lat[k][0]=lat_ini
                vertices_lat[k][1]=lat1
                vertices_lat[k][2]=lat2
                vertices_lat[k][3]=lat3
                vertices_lat[k][4]=lat4
        
                k=k+1
        fila=fila+1;    
        
    return lon_central_new, lat_central_new,vertices_lon, vertices_lat

###############################################################################

def coordenadas_subfallas_EW(ny,nx,dip,W,L,lon0,lat0,strike,fosa_lon,fosa_lat,delta_lat):
    #La funcion obtiene las coordenadas centrales de las subfallas de acuerdo a la
    #configuracion del slab model, junto con las coordendas de los vertices de esta.    
    
    #El problema comienza generando una falla rectangular dividida en ny*nx subfallas,
    #para posteriormente desplazarlas en direccion hacia la fosa y obtener asi una configuracion
    #de subfallas de acuerdo al slab model.    
    
    #1. Se determina latitud y longitud de cada subfalla asumiendo una destribucion rectangular.
    lon_sl, lat_sl = subfalla(dip,W,L,nx,ny,lon0,lat0,strike) 
    dx = L / nx
    dy = W * cos(dip) / ny
    
    #Variables para almacenar la posicion de central de las subfallas de acuerso al Slab Model..
    lon_central=[]
    lat_central=[]

    #1.1 Se obtiene posicion central de las subfallas de acuerdo a la geometria rectangular.
    k=0
    for i_nx in range(nx):
        for i_ny in range(ny):
            lon_central.append(lon_sl[k])
            lat_central.append(lat_sl[k])               
            k=k+1; 
        
    #1.2 Se generan vertices de las subfallas de acuerdo a la geometria rectangular.
    FIL=nx*ny  #Se evaluaran todas las subfallas.
    COL=5      #Cada subfalla tiene cinco vertices a dibujar (el ultimo coincide con el primero)

    #Se decalran variables para almacenar vertices de todas las subfallas.
    vertices_lon = [] 
    vertices_lat = []
    
    #Se crean matrices de ceros para rellenar con los vertices de todas las subfallas.
    for i in range(FIL):                #Numero de filas de la matriz
        vertices_lon.append([0]*COL)    #Numero de columnas de la matriz, se inicializa con ceros
        vertices_lat.append([0]*COL)
    
    #1.3 Se declaran vertices claves: Son los vertices superior e inferior de las subfallas que estan
    #Junto a la fosa. Sirven para buscar los valores de referencia con el modelo slab1.0    
    vertice_claveinf_lon=[]
    vertice_claveinf_lat=[]
    vertice_clavesup_lon=[]
    vertice_clavesup_lat=[]
    
    for i in range(nx):                       #Numero de filas de la matriz
        vertice_claveinf_lon.append([0]*1)    #Numero de columnas de la matriz, se inicializa con ceros
        vertice_claveinf_lat.append([0]*1)
        vertice_clavesup_lon.append([0]*1)    
        vertice_clavesup_lat.append([0]*1)

    #1.4 Se generan los vertices de todas las subfallas y se almacenan los vertices claves (todavia una geometria rectangular).
    k=0
    for i_ny in range(ny):
        fila=0
        for j_nx in range(nx):
                frac=Fraction((ny-i_ny),ny)        

                #Latitud,longitud inicial
                lataux,lonaux,n = vinc_pt(lat_central[k], lon_central[k], np.degrees(strike)+90, dy/2 )
                lat_ini,lon_ini,n = vinc_pt(lataux, lonaux, np.degrees(strike)+180, dx/2 )
        
                #Segundo vertice
                lataux,lonaux,n = vinc_pt(lat_central[k], lon_central[k], np.degrees(strike)+90, dy/2 )
                lat1,lon1,n = vinc_pt(lataux, lonaux, np.degrees(strike), dx/2 )
            
                #Tercer vertice
                lat2,lon2,aux2 = vinc_pt(lat1, lon1, np.degrees(strike)+270, frac*W*np.cos(dip) )
              
                #Cuarto vertice
                lat3,lon3,aux2 = vinc_pt(lat2, lon2, np.degrees(strike)+180, dx)
        
                #Quinto Vertice
                lat4 = lat_ini  
                lon4 = lon_ini

                vertices_lon[k][0]=lon_ini
                vertices_lon[k][1]=lon1
                vertices_lon[k][2]=lon2
                vertices_lon[k][3]=lon3
                vertices_lon[k][4]=lon4

                vertices_lat[k][0]=lat_ini
                vertices_lat[k][1]=lat1
                vertices_lat[k][2]=lat2
                vertices_lat[k][3]=lat3
                vertices_lat[k][4]=lat4
            
               #Vertices claves.
                if (i_ny==ny-1):  #si se esta en la ultima fila (junto a la fosa)
                    vertice_claveinf_lon[j_nx][0]=lon3
                    vertice_claveinf_lat[j_nx][0]=lat3
                    vertice_clavesup_lon[j_nx][0]=lon2
                    vertice_clavesup_lat[j_nx][0]=lat2

                k=k+1
        fila=fila+1;

    #1.5 A partir de los vertices claves se obtiene la distancia de esto con la fosa (modelo slab 1.0)
    dl=[]
 
    for i_nx in range(nx):
        #Se ingresa a una subfalla y se busca la maxima longitud proyectada a la fosa
        minlon= vertice_claveinf_lon[i_nx][0]
        for j in range(len(fosa_lat)):
            if (fosa_lat[j]<=vertice_claveinf_lat[i_nx][0]+delta_lat and fosa_lat[j]>=vertice_clavesup_lat[i_nx][0]+delta_lat):
                if fosa_lon[j]>minlon:
                    minlon=fosa_lon[j]
        #Se calcula distancia a desplazar para todos los puntos centrales.
        L, aux, aux = vinc_dist(  vertice_claveinf_lat[i_nx][0],  vertice_claveinf_lon[i_nx][0],   vertice_claveinf_lat[i_nx][0],  minlon ) 
        dl.append(L)


    #1.6 Se obtienen posiciones finales de las subfallas asociadas a una geometria del slab model.                
            
    lon_central_new=[]
    lat_central_new=[]

    k=0
    for i_ny in range(ny):
        ll=0
        for i_nx in range(nx):
            dist=dl[ll]
            lat_aux,  lon_aux,  alpha21  = vinc_pt( lat_central[k], lon_central[k], np.degrees(strike)-90,dist) 
            lon_central_new.append(lon_aux)
            lat_central_new.append(lat_aux)               
            k=k+1; 
            ll=ll+1
    
    #1.7 Se reesciben los vertices desde una distribucion rectangular a una adaptada al slab        
    k=0
    for i_ny in range(ny):
        fila=0
        for j_nx in range(nx):
                #frac=Fraction((ny-i_ny),ny)   
                frac_W=Fraction(W,ny)

                #Latitud,longitud inicial
                lataux,lonaux,n = vinc_pt(lat_central_new[k], lon_central_new[k], np.degrees(strike)+90, dy/2 )
                lat_ini,lon_ini,n = vinc_pt(lataux, lonaux, np.degrees(strike)+180, dx/2 )
        
                #Segundo vertice
                lataux,lonaux,n = vinc_pt(lat_central_new[k], lon_central_new[k], np.degrees(strike)+90, dy/2 )
                lat1,lon1,n = vinc_pt(lataux, lonaux, np.degrees(strike), dx/2 )
            
                #Tercer vertice
                lat2,lon2,aux2 = vinc_pt(lat1, lon1, np.degrees(strike)+270, frac_W*np.cos(dip) )
              
                #Cuarto vertice
                lat3,lon3,aux2 = vinc_pt(lat2, lon2, np.degrees(strike)+180, dx)
        
                #Quinto Vertice
                lat4 = lat_ini  
                lon4 = lon_ini

                vertices_lon[k][0]=lon_ini
                vertices_lon[k][1]=lon1
                vertices_lon[k][2]=lon2
                vertices_lon[k][3]=lon3
                vertices_lon[k][4]=lon4

                vertices_lat[k][0]=lat_ini
                vertices_lat[k][1]=lat1
                vertices_lat[k][2]=lat2
                vertices_lat[k][3]=lat3
                vertices_lat[k][4]=lat4
        
                k=k+1
        fila=fila+1;    

    return lon_central_new, lat_central_new,vertices_lon, vertices_lat


################################################################################

     

#def inversion_intersismico_slab(n_dim,ue_obs,un_obs,uz_obs,A,B,C,D,E,Vp_CD,Vp_E,lmbd1,lmbd2,lmbd3, lmbd4,dip,W,L,nx,ny,V_placa):
#
#    '''
#    ###########################################################################
#    #1. Se genera matriz F para suavidad.
#    ###########################################################################
#    '''
#    
#    n = nx*ny                             #<-numero de subfallas.
#    F = zeros(n*n).reshape(n,n)           #<-matriz de ceros de nxn
#    pos = [] ; coef = []
#    
#    subfalla = 0
#    for j in range(ny):
#        if j == 0:       #<---------- primera fila ...
#            for i in range(nx) :
#                ##################################################################
#                dx = L / nx; dy = W * cos(dip[subfalla]) / ny  
#                dxi = 1.0 / (dx*dx) ; dyi = 1.0 / (dy*dy)
#                dr = dyi / dxi 
#                ###################################################################
#                if i == 0 :       # primer vertice 
#                    pos.append(array([ (i+1,j), (i+2,j), (i,j+1), (i,j+2), (i,j) ]))
#                    coef.append(array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
#                elif i == nx-1 :  # segundo vertice
#                    pos.append(array([ (i-1,j), (i-2,j), (i,j+1), (i,j+2), (i,j) ]))
#                    coef.append(array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))             
#                else :
#                    pos.append(array([ (i+1,j), (i-1,j), (i,j+1), (i,j+2), (i,j) ]))
#                    coef.append(array([ 1. , 1. , -2.*dr , dr , -2. + dr ]))
#                subfalla = subfalla + 1
#        elif j == ny-1 : #<---------- ultima fila ...
#            for i in range(nx):
#                ##################################################################
#                dx = L / nx; dy = W * cos(dip[subfalla]) / ny  
#                dxi = 1.0 / (dx*dx) ; dyi = 1.0 / (dy*dy)
#                dr = dyi / dxi 
#                ###################################################################                
#                if i == 0 :
#                    pos.append(array([ (i+1,j), (i+2,j), (i,j-1), (i,j-2), (i,j) ]))
#                    coef.append(array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
#                elif i == nx-1 :
#                    pos.append(array([ (i-1,j), (i-2,j), (i,j-1), (i,j-2), (i,j) ]))
#                    coef.append(array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))             
#                else :
#                    pos.append(array([ (i+1,j), (i-1,j), (i,j-1), (i,j-2), (i,j) ]))
#                    coef.append(array([ 1. , 1. , -2.*dr , dr , -2. + dr ]))
#                subfalla = subfalla +1
#        else :         #<------------ filas restantes        
#            for i in range(nx):
#                ##################################################################
#                dx = L / nx; dy = W * cos(dip[subfalla]) / ny  
#                dxi = 1.0 / (dx*dx) ; dyi = 1.0 / (dy*dy)
#                dr = dyi / dxi 
#                ###################################################################
#                if i == 0:
#                    pos.append(array([ (i+1,j), (i+2,j), (i,j+1), (i,j-1), (i,j) ]))
#                    coef.append(array([ -2. , 1. , dr , dr , 1. - 2.*dr ]))
#                elif i == nx-1 :
#                    pos.append(array([ (i-1,j), (i-2,j), (i,j+1), (i,j-1), (i,j) ]))
#                    coef.append(array([ -2. , 1. , dr , dr , 1. - 2.*dr ]))             
#                else :
#                    pos.append(array([ (i+1,j), (i-1,j), (i,j+1), (i,j-1), (i,j) ]))
#                    coef.append(array([ 1. , 1. , dr , dr , -2. - 2.*dr ]))            
#                subfalla = subfalla +1
#            
#    for k in range(nx*ny):
#        index = pos[k] ; m = 0
#        for i,j in index:
#            F[k][j*nx+i] = coef[k][m]
#            m += 1
#
#
#    #Se construye matriz identidad  
#
#    I = identity(nx*ny)
#    
#    
#    '''   
#    ###########################################################################
#    Se especifican dimensiones a utilizar   
#    ###########################################################################
#    '''
#    
#    if n_dim == 1:
#      U_obs = ue_obs
#    if n_dim == 2:
#      U_obs = hstack((ue_obs,un_obs))
#    if n_dim == 3:
#      U_obs = hstack((ue_obs,un_obs,uz_obs))     
#
#    '''
#    ###########################################################################
#    Se constuye igualdad del sistema de ecuaciones: Termino derecho
#    ###########################################################################
#    '''
#    ceros   = zeros(nx*ny)
#    delta   = U_obs - (np.dot(C,Vp_CD) + np.dot(D,Vp_CD) + np.dot(E,Vp_E))  
#    #Uobs_m  = hstack((delta,ceros,ceros,ceros,ceros))  
#    Uobs_m  = hstack((delta,ceros,ceros,ceros))  
#   
#    '''
#    ###########################################################################
#    Se construye termino izquierdo del sistema de ecuaciones
#    ###########################################################################
#    '''
#    
##    A_m = vstack((A,lmbd1*I,lmbd2*F,I*0   ,F*0))  
##    B_m = vstack((B,    I*0,    F*0,lmbd3*I,lmbd4*F))  
#
#    A_m = vstack((A, lmbd1*I,lmbd2*F, F*0))  
#    B_m = vstack((B, lmbd3*I,    F*0, lmbd4*F))  
#  
#    #Se agrupa A_m y Bm para generar la matriz de dos columnas 
#    AA=hstack((A_m,B_m))
#    
# 
#    '''
#    ###########################################################################
#    Se realiza la inversion
#    ###########################################################################
#    '''
# 
#    xx = optimize.nnls(AA,Uobs_m)
#    slip = array([xx[0]])[0]        #<-velocidad resultante para interfaces A y B
#     
#    AB=hstack((A,B))
#    horror = linalg.norm( dot(AB,slip) - U_obs) 
#    horror_slip = linalg.norm( slip )
#    FF=hstack((F,F))
#    horror_suave = dxi*linalg.norm( dot(FF,slip) )
#
#    return  slip, horror, horror_slip, horror_suave, delta 
  
  
def inversion_intersismico_slab(n_dim,ue_obs,un_obs,uz_obs,A,B,C,D,E,Vp_CD,Vp_E,lmbd1,lmbd2,lmbd3, lmbd4,dip,W,L,nx,ny,V_placa,condicion_borde_fosa):

    '''
    ###########################################################################
    #1. Se genera matriz F para suavidad.
    ###########################################################################
    '''
    
    n = nx*ny                             #<-numero de subfallas.
    F = zeros(n*n).reshape(n,n)           #<-matriz de ceros de nxn
    pos = [] ; coef = []
    
    subfalla = 0
    for j in range(ny):
        if j == 0:       #<---------- primera fila ...
            for i in range(nx) :
                ##################################################################
                dx = L / nx; dy = W * cos(dip[subfalla]) / ny  
                dxi = 1.0 / (dx*dx) ; dyi = 1.0 / (dy*dy)
                dr = dyi / dxi 
                ###################################################################
                if i == 0 :       # primer vertice 
                    pos.append(array([ (i+1,j), (i+2,j), (i,j+1), (i,j+2), (i,j) ]))
                    coef.append(array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
                elif i == nx-1 :  # segundo vertice
                    pos.append(array([ (i-1,j), (i-2,j), (i,j+1), (i,j+2), (i,j) ]))
                    coef.append(array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))             
                else :
                    pos.append(array([ (i+1,j), (i-1,j), (i,j+1), (i,j+2), (i,j) ]))
                    coef.append(array([ 1. , 1. , -2.*dr , dr , -2. + dr ]))
                subfalla = subfalla + 1
        elif j == ny-1 : #<---------- ultima fila ...
            for i in range(nx):
                ##################################################################
                dx = L / nx; dy = W * cos(dip[subfalla]) / ny  
                dxi = 1.0 / (dx*dx) ; dyi = 1.0 / (dy*dy)
                dr = dyi / dxi 
                ###################################################################                
                if i == 0 :
                    pos.append(array([ (i+1,j), (i+2,j), (i,j-1), (i,j-2), (i,j) ]))
                    coef.append(array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
                elif i == nx-1 :
                    pos.append(array([ (i-1,j), (i-2,j), (i,j-1), (i,j-2), (i,j) ]))
                    coef.append(array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))             
                else :
                    pos.append(array([ (i+1,j), (i-1,j), (i,j-1), (i,j-2), (i,j) ]))
                    coef.append(array([ 1. , 1. , -2.*dr , dr , -2. + dr ]))
                subfalla = subfalla +1
        else :         #<------------ filas restantes        
            for i in range(nx):
                ##################################################################
                dx = L / nx; dy = W * cos(dip[subfalla]) / ny  
                dxi = 1.0 / (dx*dx) ; dyi = 1.0 / (dy*dy)
                dr = dyi / dxi 
                ###################################################################
                if i == 0:
                    pos.append(array([ (i+1,j), (i+2,j), (i,j+1), (i,j-1), (i,j) ]))
                    coef.append(array([ -2. , 1. , dr , dr , 1. - 2.*dr ]))
                elif i == nx-1 :
                    pos.append(array([ (i-1,j), (i-2,j), (i,j+1), (i,j-1), (i,j) ]))
                    coef.append(array([ -2. , 1. , dr , dr , 1. - 2.*dr ]))             
                else :
                    pos.append(array([ (i+1,j), (i-1,j), (i,j+1), (i,j-1), (i,j) ]))
                    coef.append(array([ 1. , 1. , dr , dr , -2. - 2.*dr ]))            
                subfalla = subfalla +1
            
    for k in range(nx*ny):
        index = pos[k] ; m = 0
        for i,j in index:
            F[k][j*nx+i] = coef[k][m]
            m += 1


    #Se construye matriz identidad  

    I = identity(nx*ny)
    
    
    '''   
    ###########################################################################
    Se especifican dimensiones a utilizar   
    ###########################################################################
    '''
    
    if n_dim == 1:
      U_obs = ue_obs
    if n_dim == 2:
      U_obs = hstack((ue_obs,un_obs))
    if n_dim == 3:
      U_obs = hstack((ue_obs,un_obs,uz_obs))     



    if condicion_borde_fosa == False:

        #cosismico
        '''
        ###########################################################################
        Se constuye igualdad del sistema de ecuaciones: Termino derecho
        ###########################################################################
        '''
        ceros   = zeros(nx*ny)
        #solo contribuye las falas A y B
        delta   = U_obs - (np.dot(C,Vp_CD) + np.dot(D,Vp_CD) + np.dot(E,Vp_E))  
        #Uobs_m  = hstack((delta,ceros,ceros,ceros,ceros))  
        Uobs_m  = hstack((delta,ceros,ceros,ceros))  
   
        '''
        ###########################################################################
        Se construye termino izquierdo del sistema de ecuaciones
        ###########################################################################
        '''
     

        A_m = vstack((A, lmbd1*I,lmbd2*F, F*0))  
        B_m = vstack((B, lmbd3*I,    F*0, lmbd4*F))  
 
    else:
       #intersismico
       print('Ajustando condicion de borde en la fosa ... ')
       #Condicion de borde lateral 
       
       g = []
       for jy in range(ny):
           for ix in range(nx):
               if jy == (ny-1) or jy==0:
                   g.append(1)
               else:
                   g.append(0)
                   
       g = np.array(g)
       G = zeros(n*n).reshape(n,n) 
       fill_diagonal(G,g) 
       
       
       V = np.ones(nx*ny)
       for i in range(nx*ny):
           V[i] = V_placa

       '''
       ###########################################################################
       Se constuye igualdad del sistema de ecuaciones: Termino derecho
       ###########################################################################
       '''
       ceros   = zeros(nx*ny)
       print("C:",C,
             "Vp:",Vp_CD,
             "D:",D,
             "E:",E,
             "Vp_E:",Vp_E)
       delta   = U_obs - (np.dot(C,Vp_CD) + np.dot(D,Vp_CD) + np.dot(E,Vp_E))  
       #Uobs_m  = hstack((delta,ceros,ceros,ceros,ceros))  
       Uobs_m  = hstack((delta,ceros,ceros,ceros,V,V))  
   
       '''
       ###########################################################################
       Se construye termino izquierdo del sistema de ecuaciones
       ###########################################################################
       '''
     

       A_m = vstack((A, lmbd1*I,lmbd2*F, F*0,G,G*0))  
       B_m = vstack((B, lmbd3*I,    F*0, lmbd4*F,G*0,G)) 


 
    #Se agrupa A_m y Bm para generar la matriz de dos columnas 
    AA=hstack((A_m,B_m))
    
 
    '''
    ###########################################################################
    Se realiza la inversion
    ###########################################################################
    '''

    print("AA:", AA)
    print("Uobs_m", Uobs_m)
    xx = optimize.nnls(AA,Uobs_m)
    slip = array([xx[0]])[0]        #<-velocidad resultante para interfaces A y B
     
    AB=hstack((A,B))
    horror = linalg.norm( dot(AB,slip) - U_obs) 
    horror_slip = linalg.norm( slip )
    FF=hstack((F,F))
    horror_suave = dxi*linalg.norm( dot(FF,slip) )

    return  slip, horror, horror_slip, horror_suave, delta 
  
  
  
def Pdirecto_subfault_slab_dip_depth(dip,depth,W,L,rake,strike,nx,ny,slip,lon_vert,lat_vert,lonGPS,latGPS):
#A partir del deslizamiento de cada subfalla (dado por un problema inverso o por una distribucion arbitraria)
#se determina la caracterizacion geometrica de cada subfalla para luego obtener
#el desplazamiento en superficie (para cada observacion GPS) respecto al deslizamiento de todas
#las subfallas (constribucion de cada una al GPS).


#OBS: en realidad xi e yi se ocupan solo para la primera subfalla, pero podrian reemplazarse con
#ddx, ddy = proj_mesh2okada(latGPS,lonGPS,lat_vert[kk][0],lon_vert[kk][0],strike) para kk=0
    l = L/nx; w = W/ny

    subfalla = 0
    for iy in range(ny):
        for ix in range(nx):
            tap = slip[subfalla]
            if ix == 0 and iy == 0:  #Para la primera subfalla.
                ddx, ddy = proj_mesh2okada(latGPS,lonGPS,lat_vert[subfalla][0],lon_vert[subfalla][0],strike[subfalla])
                #print('congrio',ddx,ddy,dip,depth,w,l,rake,strike)
                ue,un,uz = desplaz_okada(ddx,ddy,dip[subfalla],depth[subfalla],w,l,rake[subfalla],strike[subfalla]) #Caracterizacion geometrica de la primera subfalla.
                ve = tap*ue; vn = tap*un; vz = tap*uz                 #desplazamiento en superficie dado el deslizamiento de la primera subfalla (se calcula para el GPS de entrada).
            else:   #Para las subfallas restantes
            
                ddx, ddy = proj_mesh2okada(latGPS,lonGPS,lat_vert[subfalla][0],lon_vert[subfalla][0],strike[subfalla])
                  
                ue,un,uz = desplaz_okada(ddx,ddy,dip[subfalla],depth[subfalla],w,l,rake[subfalla],strike[subfalla])
                ve += tap*ue; vn += tap*un; vz += tap*uz                       #desplazamiento en superficie como suma de todos los deslizamientos en las subfallas (el desplazamiento resultante en superficie se acumula).
            subfalla = subfalla + 1
    return ve,vn,vz
  
def ECM_inversion_slab_intersismico(dip,D_inv,D_norm,W,L,rake_inv,rake_norm,strike,nx,ny,sl_oka,Ue_obs,Un_obs,Uz_obs,lon_vert,lat_vert,lonGPS,latGPS):
    #La funcion determina el Error Cuadratico Medio a traves de la aplicacion de
    #un problema directo. Se comparan Uobs y Uteo en todas las componentes.    
    sl_oka_inv=[]
    sl_oka_norm=[]
    k=nx*ny;
    for i in range(nx*ny):
        sl_oka_inv.append(sl_oka[i])
        sl_oka_norm.append(sl_oka[i+k]);
    
    #Para falla inversa.
    Ue_teo_inv,Un_teo_inv,Uz_teo_inv=Pdirecto_subfault_slab_dip_depth(dip,D_inv,W,L,rake_inv,strike,nx,ny,sl_oka_inv,lon_vert,lat_vert,lonGPS,latGPS)

    #Para falla norm.
   # print('lol')
    print(dip,D_norm,W,L,rake_norm,strike,nx,ny,sl_oka_norm,lon_vert,lat_vert,lonGPS,latGPS)
    Ue_teo_norm,Un_teo_norm,Uz_teo_norm=Pdirecto_subfault_slab_dip_depth(dip,D_norm,W,L,rake_norm,strike,nx,ny,sl_oka_norm,lon_vert,lat_vert,lonGPS,latGPS)
    print('Ue_teo_norm',Ue_teo_norm)
    print('Ue_teo_inv',Ue_teo_inv)
    #Se determina desplazamiento total
    Ue_teo=[]
    Un_teo=[]
    Uz_teo=[]
    for i in range(len(latGPS)):
        Ue_teo.append(Ue_teo_inv[i]+Ue_teo_norm[i])
        Un_teo.append(Un_teo_inv[i]+Un_teo_norm[i])
        Uz_teo.append(Uz_teo_inv[i]+Uz_teo_norm[i])
    print('Ue_teo: ', Ue_teo)
    print('type(Ue_teo)', type(Ue_teo))
    print('type(Ue_obs)', type(Ue_obs))
    Ue_teo=np.array(Ue_teo)
    Un_teo=np.array(Un_teo)
    Uz_teo=np.array(Uz_teo)

    print('-------------')
    print('Solucion ridicula: for')
    print('-------------')
    Ue_obs_teo = []
    Un_obs_teo = []
    Uz_obs_teo = []
    for i in range(len(Ue_teo)):
        Ue_obs_teo.append(Ue_obs[i]-Ue_teo[i])
        Un_obs_teo.append(Un_obs[i]-Un_teo[i])
        
    print('Ue_obs_teo',Ue_obs_teo)
    
    Ue_obs_teo= np.array(Ue_obs_teo)
    Un_obs_teo= np.array(Un_obs_teo)
    ECM_Ue=sum((Ue_obs_teo)**2)/len(Ue_obs)
    ECM_Un=sum((Un_obs_teo)**2)/len(Un_obs)
    print('ECM_Ue: ', ECM_Ue)
    print('ECM_Un: ', ECM_Un)
    print('-------------')
    '''
    print'type(Ue_teo)', type(Ue_teo)
    print'Ue_obs-Ue_teo',Ue_obs-Ue_teo
    print'type(Ue_obs-Ue_teo)', type(Ue_obs-Ue_teo)
    print'(Ue_obs-Ue_teo)**2',(Ue_obs-Ue_teo)**2
    print'sum((Ue_obs-Ue_teo)**2)',sum((Ue_obs-Ue_teo)**2)
    print'(sum((Ue_obs-Ue_teo)**2))/len(Ue_obs)',(sum((Ue_obs-Ue_teo)**2))/(len(Ue_obs))
    #Se determina el Error Cuadratico Medio    
    ECM_Ue=sum((Ue_obs-Ue_teo)**2)/len(Ue_obs)
    ECM_Un=sum((Un_obs-Un_teo)**2)/len(Un_obs)
    print 'rutina: Ue_obs: ',Ue_obs
    print 'rutina: Un_obs: ',Un_obs
    print 'ECM_Ue: ', ECM_Ue
    print 'ECM_Un: ', ECM_Un
    print 'Ue_teo: ', Ue_teo
    #ECM_Uz=sum((Uz_obs-Uz_teo)**2)/len(Uz_obs)
    '''
    return ECM_Ue,ECM_Un 
   
   
    
def Pdirecto_intersismico(dip,D_inv,D_norm,W,L,rake_inv,rake_norm,strike,nx,ny,sl_oka,lon_vert,lat_vert,lonGPS,latGPS):
    #La funcion determina el Error Cuadratico Medio a traves de la aplicacion de
    #un problema directo. Se comparan Uobs y Uteo en todas las componentes.    
    
    sl_oka_inv=[]
    sl_oka_norm=[]
    k=nx*ny;
    for i in range(nx*ny):
        sl_oka_inv.append(sl_oka[i])
        sl_oka_norm.append(sl_oka[i+k]);
    

    #Para falla inversa.
    Ue_teo_inv,Un_teo_inv,Uz_teo_inv=Pdirecto_subfault_slab_dip_depth(dip,D_inv,W,L,rake_inv,strike,nx,ny,sl_oka_inv,lon_vert,lat_vert,lonGPS,latGPS)
       
    #Para falla normal
    Ue_teo_norm,Un_teo_norm,Uz_teo_norm=Pdirecto_subfault_slab_dip_depth(dip,D_norm,W,L,rake_norm,strike,nx,ny,sl_oka_norm,lon_vert,lat_vert,lonGPS,latGPS)

    #Se determina desplazamiento total
    Ue_teo=[]
    Un_teo=[]
    
    for i in range(len(latGPS)):
        Ue_teo.append(Ue_teo_inv[i]+Ue_teo_norm[i])
        Un_teo.append(Un_teo_inv[i]+Un_teo_norm[i])
    
    return Ue_teo,Un_teo,Ue_teo_inv,Un_teo_inv,Ue_teo_norm,Un_teo_norm


#
# --------------------------------------------------------------------- 
# |                                                                     |
# |	geodetic.cc -  a collection of geodetic functions                   |
# |	Jim Leven  - Dec 99                                                 |
# |                                                                     |
# | originally from:                                                    |
# | http://wegener.mechanik.tu-darmstadt.de/GMT-Help/Archiv/att-8710/Geodetic_py |                                                                   |
# |                                                                     |
# --------------------------------------------------------------------- 
# 
# 
# ----------------------------------------------------------------------
# | Algrothims from Geocentric Datum of Australia Technical Manual	    |
# | 								                                    |
# | http://www.anzlic.org.au/icsm/gdatum/chapter4.html	        		|
# | 								                                    |
# | This page last updated 11 May 1999 	                				|
# | 								                                    |
# | Computations on the Ellipsoid	                    				|
# | 								                                    |
# | There are a number of formulae that are available           		|
# | to calculate accurate geodetic positions, 		            		|
# | azimuths and distances on the ellipsoid.			                |
# | 								                                    |
# | Vincenty's formulae (Vincenty, 1975) may be used 		            |
# | for lines ranging from a few cm to nearly 20,000 km, 	            |
# | with millimetre accuracy. 					                        |
# | The formulae have been extensively tested 		                    |
# | for the Australian region, by comparison with results       		|
# | from other formulae (Rainsford, 1955 & Sodano, 1965). 	            |
# |								                                        |
# | * Inverse problem: azimuth and distance from known 	        		|
# |			latitudes and longitudes 			                        |
# | * Direct problem: Latitude and longitude from known 	            |
# |			position, azimuth and distance. 		                    |
# | * Sample data 						                                |
# | * Excel spreadsheet 			                            		|
# | 								                                    |
# | Vincenty's Inverse formulae				                    		|
# | Given: latitude and longitude of two points                 		|
# |			(phi1, lembda1 and phi2, lembda2), 	                        |
# | Calculate: the ellipsoidal distance (s) and 	            		|
# | forward and reverse azimuths between the points (alpha12, alpha21).	|
# |									                                    |
# ---------------------------------------------------------------------- 

def vinc_dist(  phi1,  lembda1,  phi2,  lembda2 ) :
        """ 
        Returns the distance between two geographic points on the ellipsoid
        and the forward and reverse azimuths between these points.
        lats, longs and azimuths are in decimal degrees, distance in metres 

        Returns ( s, alpha12,  alpha21 ) as a tuple
        """

        f = 1.0 / 298.257223563		# WGS84
        a = 6378137.0 			# metres
        if (abs( phi2 - phi1 ) < 1e-8) and ( abs( lembda2 - lembda1) < 1e-8 ) :
                return 0.0, 0.0, 0.0

        piD4   = math.atan( 1.0 )
        two_pi = piD4 * 8.0

        phi1    = phi1 * piD4 / 45.0
        lembda1 = lembda1 * piD4 / 45.0		# unfortunately lambda is a key word!
        phi2    = phi2 * piD4 / 45.0
        lembda2 = lembda2 * piD4 / 45.0

        b = a * (1.0 - f)

        TanU1 = (1-f) * tan( phi1 )
        TanU2 = (1-f) * tan( phi2 )

        U1 = atan(TanU1)
        U2 = atan(TanU2)

        lembda = lembda2 - lembda1
        last_lembda = -4000000.0		# an impossibe value
        omega = lembda

        # Iterate the following equations, 
        #  until there is no significant change in lembda 

        while ( last_lembda < -3000000.0 or lembda != 0 and abs( (last_lembda - lembda)/lembda) > 1.0e-9 ) :

                sqr_sin_sigma = pow( cos(U2) * sin(lembda), 2) + \
                        pow( (cos(U1) * sin(U2) - \
                        sin(U1) *  cos(U2) * cos(lembda) ), 2 )

                Sin_sigma = sqrt( sqr_sin_sigma )

                Cos_sigma = sin(U1) * sin(U2) + cos(U1) * cos(U2) * cos(lembda)
        
                sigma = atan2( Sin_sigma, Cos_sigma )

                Sin_alpha = cos(U1) * cos(U2) * sin(lembda) / sin(sigma)
                alpha = asin( Sin_alpha )

                Cos2sigma_m = cos(sigma) - (2 * sin(U1) * sin(U2) / pow(cos(alpha), 2) )

                C = (f/16) * pow(cos(alpha), 2) * (4 + f * (4 - 3 * pow(cos(alpha), 2)))

                last_lembda = lembda

                lembda = omega + (1-C) * f * sin(alpha) * (sigma + C * sin(sigma) * \
                        (Cos2sigma_m + C * cos(sigma) * (-1 + 2 * pow(Cos2sigma_m, 2) )))

        u2 = pow(cos(alpha),2) * (a*a-b*b) / (b*b)

        A = 1 + (u2/16384) * (4096 + u2 * (-768 + u2 * (320 - 175 * u2)))

        B = (u2/1024) * (256 + u2 * (-128+ u2 * (74 - 47 * u2)))

        delta_sigma = B * Sin_sigma * (Cos2sigma_m + (B/4) * \
                (Cos_sigma * (-1 + 2 * pow(Cos2sigma_m, 2) ) - \
                (B/6) * Cos2sigma_m * (-3 + 4 * sqr_sin_sigma) * \
                (-3 + 4 * pow(Cos2sigma_m,2 ) )))

        s = b * A * (sigma - delta_sigma)

        alpha12 = atan2( (cos(U2) * sin(lembda)), \
                (cos(U1) * sin(U2) - sin(U1) * cos(U2) * cos(lembda)))

        alpha21 = atan2( (cos(U1) * sin(lembda)), \
                (-sin(U1) * cos(U2) + cos(U1) * sin(U2) * cos(lembda)))

        if ( alpha12 < 0.0 ) : 
                alpha12 =  alpha12 + two_pi
        if ( alpha12 > two_pi ) : 
                alpha12 = alpha12 - two_pi

        alpha21 = alpha21 + two_pi / 2.0
        if ( alpha21 < 0.0 ) : 
                alpha21 = alpha21 + two_pi
        if ( alpha21 > two_pi ) : 
                alpha21 = alpha21 - two_pi

        alpha12    = alpha12    * 45.0 / piD4
        alpha21    = alpha21    * 45.0 / piD4
        return s, alpha12,  alpha21 

   # END of Vincenty's Inverse formulae 


#-------------------------------------------------------------------------------
# Vincenty's Direct formulae							|
# Given: latitude and longitude of a point (phi1, lembda1) and 			|
# the geodetic azimuth (alpha12) 						|
# and ellipsoidal distance in metres (s) to a second point,			|
# 										|
# Calculate: the latitude and longitude of the second point (phi2, lembda2) 	|
# and the reverse azimuth (alpha21).						|
# 										|
#-------------------------------------------------------------------------------

def  vinc_pt( phi1, lembda1, alpha12, s ) :
        """

        Returns the lat and long of projected point and reverse azimuth
        given a reference point and a distance and azimuth to project.
        lats, longs and azimuths are passed in decimal degrees

        Returns ( phi2,  lambda2,  alpha21 ) as a tuple 

        """
 
        f = 1.0 / 298.257223563		# WGS84
        a = 6378137.0 			# metres
        piD4 = atan( 1.0 )
        two_pi = piD4 * 8.0

        phi1    = phi1    * piD4 / 45.0
        lembda1 = lembda1 * piD4 / 45.0
        alpha12 = alpha12 * piD4 / 45.0
        if ( alpha12 < 0.0 ) : 
                alpha12 = alpha12 + two_pi
        if ( alpha12 > two_pi ) : 
                alpha12 = alpha12 - two_pi

        b = a * (1.0 - f)

        TanU1 = (1-f) * tan(phi1)
        U1 = atan( TanU1 )
        sigma1 = atan2( TanU1, cos(alpha12) )
        Sinalpha = cos(U1) * sin(alpha12)
        cosalpha_sq = 1.0 - Sinalpha * Sinalpha

        u2 = cosalpha_sq * (a * a - b * b ) / (b * b)
        A = 1.0 + (u2 / 16384) * (4096 + u2 * (-768 + u2 * \
                (320 - 175 * u2) ) )
        B = (u2 / 1024) * (256 + u2 * (-128 + u2 * (74 - 47 * u2) ) )

        # Starting with the approximation
        sigma = (s / (b * A))

        last_sigma = 2.0 * sigma + 2.0	# something impossible

        # Iterate the following three equations 
        #  until there is no significant change in sigma 

        # two_sigma_m , delta_sigma
        two_sigma_m = 0
        while ( abs( (last_sigma - sigma) / sigma) > 1.0e-9 ) :
                two_sigma_m = 2 * sigma1 + sigma

                delta_sigma = B * sin(sigma) * ( cos(two_sigma_m) \
                        + (B/4) * (cos(sigma) * \
                        (-1 + 2 * pow( cos(two_sigma_m), 2 ) -  \
                        (B/6) * cos(two_sigma_m) * \
                        (-3 + 4 * pow(sin(sigma), 2 )) *  \
                        (-3 + 4 * pow( cos (two_sigma_m), 2 ))))) \

                last_sigma = sigma
                sigma = (s / (b * A)) + delta_sigma

        phi2 = atan2 ( (sin(U1) * cos(sigma) + cos(U1) * sin(sigma) * cos(alpha12) ), \
                ((1-f) * sqrt( pow(Sinalpha, 2) +  \
                pow(sin(U1) * sin(sigma) - cos(U1) * cos(sigma) * cos(alpha12), 2))))

        lembda = atan2( (sin(sigma) * sin(alpha12 )), (cos(U1) * cos(sigma) -  \
                sin(U1) *  sin(sigma) * cos(alpha12)))

        C = (f/16) * cosalpha_sq * (4 + f * (4 - 3 * cosalpha_sq ))

        omega = lembda - (1-C) * f * Sinalpha *  \
                (sigma + C * sin(sigma) * (cos(two_sigma_m) + \
                C * cos(sigma) * (-1 + 2 * pow(cos(two_sigma_m),2) )))

        lembda2 = lembda1 + omega

        alpha21 = atan2 ( Sinalpha, (-sin(U1) * sin(sigma) +  \
                cos(U1) * cos(sigma) * cos(alpha12)))

        alpha21 = alpha21 + two_pi / 2.0
        if ( alpha21 < 0.0 ) :
                alpha21 = alpha21 + two_pi
        if ( alpha21 > two_pi ) :
                alpha21 = alpha21 - two_pi

        phi2       = phi2       * 45.0 / piD4
        lembda2    = lembda2    * 45.0 / piD4
        alpha21    = alpha21    * 45.0 / piD4

        return phi2,  lembda2,  alpha21 

  # END of Vincenty's Direct formulae

#--------------------------------------------------------------------------
# Notes: 
# 
# * "The inverse formulae may give no solution over a line 
# 	between two nearly antipodal points. This will occur when 
# 	lembda ... is greater than pi in absolute value". (Vincenty, 1975)
#  
# * In Vincenty (1975) L is used for the difference in longitude, 
# 	however for consistency with other formulae in this Manual, 
# 	omega is used here. 
# 
# * Variables specific to Vincenty's formulae are shown below, 
# 	others common throughout the manual are shown in the Glossary. 
# 
# 
# alpha = Azimuth of the geodesic at the equator
# U = Reduced latitude
# lembda = Difference in longitude on an auxiliary sphere (lembda1 & lembda2 
# 		are the geodetic longitudes of points 1 & 2)
# sigma = Angular distance on a sphere, from point 1 to point 2
# sigma1 = Angular distance on a sphere, from the equator to point 1
# sigma2 = Angular distance on a sphere, from the equator to point 2
# sigma_m = Angular distance on a sphere, from the equator to the 
# 		midpoint of the line from point 1 to point 2
# u, A, B, C = Internal variables
# 
# 

#*******************************************************************

def est_dist( phi1,  lembda1,  phi2,  lembda2 ) :
        """ 

        Returns an estimate of the distance between two geographic points
        This is a quick and dirty vinc_dist 
        which will generally estimate the distance to within 1%
        Returns distance in metres

        """

        f = 1.0 / 298.257223563		# WGS84
        a = 6378137.0 			# metres
        piD4   = 0.785398163397 

        phi1    = phi1 * piD4 / 45.0
        lembda1 = lembda1 * piD4 / 45.0
        phi2    = phi2 * piD4 / 45.0
        lembda2 = lembda2 * piD4 / 45.0

        c = cos((phi2+phi1)/2.0)

        return sqrt( pow(fabs(phi2-phi1), 2) + \
                pow(fabs(lembda2-lembda1)*c, 2) ) * a * ( 1.0 - f + f * c )

   # END of rough estimate of the distance.

 

class subfallas_SlabModel():
    #Felipe Vera S.
    #fveras@udec.cl
    #03.02.16
    def __init__(self,nx,ny,W,L,strike,lon_vertices, lat_vertices,name_file_depth,name_file_dip, name_file_strike):
        
        '''
        #######################################################################
        PARAMETROS PLANO DE FALLA
        #######################################################################
        '''
        
        self.nx              = nx                  #<-Numero de subfallas en sentido del rumbo.
        self.ny              = ny                  #<-Numero de subfallas en sentido del dip.
        self.W               = W                   #<-Ancho del plano de falla en metros.
        self.L               = L                   #<-Largo del plano de falla en metros.
        self.strike          = strike              #<-Strike del plano de falla en radianes.
        self.lon_vertices    = lon_vertices        #<-Longitud de los vertices de las subfallas.
        self.lat_vertices    = lat_vertices        #<-Latitud de los vertices de las subfallas.

        '''
        #######################################################################
        DATOS MODELO SLAB 1.0
        #######################################################################
        '''
        
        self.name_file_depth = name_file_depth     #<-Nombre del fichero con la informacion de profundidad (en kilometros negativos).
        self.name_file_dip   = name_file_dip       #<-Nombre del fichero con la informacion del dip (en grados negativos).    
        self.name_file_strike   = name_file_strike #<-Nombre del fichero con la informacion del strike (en grados positivos).    
        
        '''
        #######################################################################
        OPERACIONES        
        #######################################################################        
        '''
        
        self.numcols = 50   #Numero de columnas para la interpolacion de datos.
        self.numrows = 150   #Numero de filas para la interpolacion de datos.
               
        #Se inician los procedimientos.
        self.operaciones()        
        
    def operaciones(self):
        
         '''
         ######################################################################
         1. Se establece longitud y latitud de los vertices de okada de cada 
         subfalla.
         ######################################################################
         '''
         self.LON_POINTS = []
         self.LAT_POINTS = []
         for subfalla in range(self.nx*self.ny):
             self.LON_POINTS.append(self.lon_vertices[subfalla][0])
             self.LAT_POINTS.append(self.lat_vertices[subfalla][0])
############################################################
#BORRAR
         #print ('self.LON_POINTS: ',self.LON_POINTS)
         print('len(self.LON_POINTS)',len(self.LON_POINTS))
         #print ('self.LAT_POINTS: ',self.LAT_POINTS)
         print('len(self.LAT_POINTS)',len(self.LAT_POINTS))
############################################################
         self.num_subfallas = self.nx*self.ny
         print('self.num_subfallas: ',self.num_subfallas)

         
         '''
         ######################################################################
         2. Se establece profundidad, dip y strike de cada subfalla desde el 
         modelo SLab 1.0
         ######################################################################
         '''
         self.Z            = self.slab_points_depth()       #<-Se obtiene profundidad en metros de cada subfalla.
         self.dip_slab_deg = self.slab_points_dip()         #<-Se obtiene dip en grados de cada subfalla.
         self.dip_slab_rad = np.radians(self.dip_slab_deg)  #<-Se obtiene dip en radianes de cada subfalla.
         self.strike_deg   = self.slab_points_strike()      #<-Se obtiene strike en grados de cada subfalla.
         
         '''
         ######################################################################
         3. Se reescriben los vertices de cada subfalla y las coordenadas centrales
         en relacion al nuevo dip
         ######################################################################
         '''
         try:         
             self.vert_lon_new, self.vert_lat_new, self.lon_central_new, self.lat_central_new = self.reescribir_subfallas()
         except:
             #Cuando el plano de falla se emplaza fuera de la disponibilidad de datos del modelo slab 1.0
             print('Error:')
             print('Se han encontrado coordenadas fuera de la grilla de datos.')
             print('Reorganizando/eliminado subfallas erroneas...')

             #Se reescriben los vertices para datos validos.
             self.lon_vertices_aux = []
             self.lat_vertices_aux = []
             
             for subfalla in range(len(self.Z)):
                 if self.dip_slab_rad[subfalla]!='nan' and self.Z[subfalla]!='masked':
                     self.lat_vertices_aux.append(self.lat_vertices[subfalla])
                     self.lon_vertices_aux.append(self.lon_vertices[subfalla])
              
             self.lon_vertices = []
             self.lat_vertices = []
             
             self.lat_vertices = self.lat_vertices_aux
             self.lon_vertices = self.lon_vertices_aux
             
             #Se obtienen vertices de okada desde los vertices validos. 
             self.LON_POINTS = []
             self.LAT_POINTS = []
             
             for subfalla in range(len(self.lat_vertices)):
                 self.LON_POINTS.append(self.lon_vertices[subfalla][0])
                 self.LAT_POINTS.append(self.lat_vertices[subfalla][0])                     
             
             #Se obtiene profundidad,dip y strike desde los vertices validos.
             self.num_subfallas = len(self.LON_POINTS)       
             self.Z             = self.slab_points_depth()         #<-Se obtiene profundidad en metros de cada subfalla.
             self.dip_slab_deg  = self.slab_points_dip()           #<-Se obtiene dip en grados de cada subfalla.
             self.dip_slab_rad  = np.radians(self.dip_slab_deg)    #<-Se obtiene dip en radianes de cada subfalla.
             
             self.strike_deg = self.slab_points_strike()
              
             #Se reescriben las subfallas para representar adecuadamente el dip.  
             self.vert_lon_new, self.vert_lat_new, self.lon_central_new, self.lat_central_new = self.reescribir_subfallas()
           
                        
        
    def slab_points_depth(self):
        #La funcion determina la profunidad asociada a una cierta ubicacion 
        #geografica (latitud/longitud) del slab 1.0 model.
        #INPUT:
        #      LON_POINTS: Longitud en grados (-180 a 180) de los puntos de interes. 
        #      LAT_POINTS: Latitud en grados (-90 a 90) de los puntos de interes.
        #      FILE_NAME : Nombre del fichero txt contenedor de los datos de profundidad
        #                  o dip.
        #OUTPUT:
        #      Z         : Profundidad en metros de cada vertice de okada.

        '''
        #######################################################################
        1. Se cargan datos de profunidad o dip.
        #######################################################################
        '''
        
        DATA = np.loadtxt(self.name_file_depth, delimiter=';',skiprows=1)

        LON   = DATA[:,1]    #<- Longitud en grados de los datos.
        LAT   = DATA[:,0]    #<- Latitud en grados de los datos.
        DEPTH = DATA[:,2]    #<- Profundidad en metros de los datos.
        print('lon',LON,'lat',LAT,'dep',DEPTH)
        LON_NEW=[]
        LAT_NEW=[]
        DEP_NEW=[]
        for lon,lat,dep in zip(LON,LAT,DEPTH):
            if not np.isnan(dep):
                LON_NEW.append(lon)
                LAT_NEW.append(lat)
                DEP_NEW.append(dep)
        #print('lon',LON_NEW,'lat',LAT_NEW,'dep',DEP_NEW)
        '''
        #######################################################################
        2. Listas contenedoras de los puntos a interpolar
        #######################################################################
        '''
        
        #Puntos a interpolar del area de estudio.
        xi_start = np.linspace(min(LON_NEW), max(LON_NEW), self.numcols)
        yi_start = np.linspace(min(LAT_NEW), max(LAT_NEW), self.numrows)



        #Se inicializan listas para unificar los puntos de interes y los puntos
        #de definicion del area de estudio.
        xi_new = []
        yi_new = []
    
        #Se agrupa todo el conjunto de datos.
        for i in range(len(self.LON_POINTS)):
            xi_new.append(self.LON_POINTS[i])
            yi_new.append(self.LAT_POINTS[i])
    
        for i in range(len(xi_start)):
            xi_new.append(xi_start[i]) 
            yi_new.append(yi_start[i])
            

        '''
        #######################################################################
        3. Interpolacion de datos.
        #######################################################################
        '''
        xi, yi = np.meshgrid(xi_new, yi_new)
        #mlab
        #zi = griddata(LON, LAT, DEPTH, xi, yi, interp='nn')
        #scipy
        zi = griddata((np.array(LON_NEW), np.array(LAT_NEW)), np.array(DEP_NEW), (xi, yi), method='nearest')
        
   
        '''
        #######################################################################
        4. Se busca la ubicacion de los datos de interes.
        #######################################################################
        '''

        POS_xi = []
        POS_yi = []

        #Ubicacion de las coordenadas de latitud.
        for i in range(len(self.LAT_POINTS)):
            flag = True
            for j in range(len(yi)):
                if yi[j][0] == self.LAT_POINTS[i] and flag == True:
                    POS_yi.append(j)
                    flag = False
    
        #Ubicacion de las coordenadas de longitud.        
        for i in range(len(self.LON_POINTS)):
            flag = True
            for j in range(len(xi)):
                if xi[0][j] == self.LON_POINTS[i] and flag == True:
                    POS_xi.append(j)    
                    flag = False
        
        
        '''
        #######################################################################
        5. Se extraen los datos de profundidad desde el modelo slab 1.0
        #######################################################################
        '''
    
        #Se obtiene profundidad
        Z = []
    
        for i in range(len(POS_yi)):
            #Los resultados se expresan en metros.
            Z.append(-zi[POS_yi[i]][POS_xi[i]]*1000)
            
        
        return Z         

    def slab_points_dip(self):
        #La funcion determina el dip asociada a una cierta ubicacion 
        #geografica (latitud/longitud) del slab 1.0 model.
        #INPUT:
        #      LON_POINTS: Longitud en grados (-180 a 180) de los puntos de interes. 
        #      LAT_POINTS: Latitud en grados (-90 a 90) de los puntos de interes.
        #      FILE_NAME : Nombre del fichero txt contenedor de los datos de dip
        #                  o dip.
        #OUTPUT:
        #      dip         : dip en grados de cada subfalla

        '''
        #######################################################################
        1. Se cargan datos de dip desde modelo SLab 1.0.
        #######################################################################
        '''
        DATA = np.loadtxt(self.name_file_dip, delimiter=';', skiprows=1)

        LON      = DATA[:,1]    #<- Longitud en grados de los datos.
        LAT      = DATA[:,0]    #<- Latitud en grados de los datos.
        DATA_DIP = DATA[:,2]    #<- Dip en grados de los datos desde modelo Slab 1.0.
        
        LON_NEW=[]
        LAT_NEW=[]
        DATA_DIP_NEW=[]
        for lon,lat,dip in zip(LON,LAT,DATA_DIP):
            if not  np.isnan(dip):
                LON_NEW.append(lon)
                LAT_NEW.append(lat)
                DATA_DIP_NEW.append(dip)
        '''
        #######################################################################
        2. Listas contenedoras de los puntos a interpolar
        #######################################################################
        '''
        #Puntos a interpolar del area de estudio.
        xi_start = np.linspace(min(LON_NEW), max(LON_NEW), self.numcols)
        yi_start = np.linspace(min(LAT_NEW), max(LAT_NEW), self.numrows)

        #Se inicializan listas para unificar los puntos de interes y los puntos
        #de definicio del area de estudio.
        xi_new = []
        yi_new = []
    
        #Se agrupa todo el conjunto de datos.
        for i in range(len(self.LON_POINTS)):
            xi_new.append(self.LON_POINTS[i])
            yi_new.append(self.LAT_POINTS[i])
    
        for i in range(len(xi_start)):
            xi_new.append(xi_start[i]) 
            yi_new.append(yi_start[i])

        xi_new = sorted(xi_new)
        yi_new = sorted(yi_new)  

        '''
        #######################################################################
        3. Interpolacion de datos.
        #######################################################################
        '''
        xi, yi = np.meshgrid(xi_new, yi_new)
        #zi = griddata(LON, LAT, DATA_DIP, xi, yi, interp='nn')    
        zi = griddata((np.array(LON_NEW), np.array(LAT_NEW)), np.array(DATA_DIP_NEW), (xi, yi), method='nearest')
   
        '''
        #######################################################################
        4. Se busca la ubicacion de los datos de interes.
        #######################################################################
        '''

        POS_xi = []
        POS_yi = []

        #Ubicacion de las coordenadas de latitud.
        for i in range(len(self.LAT_POINTS)):
            flag = True
            for j in range(len(yi)):
                if yi[j][0] == self.LAT_POINTS[i] and flag == True:
                    POS_yi.append(j)
                    flag = False
    
        #Ubicacion de las coordenadas de longitud.        
        for i in range(len(self.LON_POINTS)):
            flag = True
            for j in range(len(xi)):
                if xi[0][j] == self.LON_POINTS[i] and flag == True:
                    POS_xi.append(j)    
                    flag = False
    
        '''
        #######################################################################
        5. Se extraen los datos de dip desde modelo slab 1.0
        #######################################################################
        '''
    
        #Se obtiene profundidad/dip
        dip = []
    
        for i in range(len(POS_yi)):
            dip.append(-zi[POS_yi[i]][POS_xi[i]])
        
        return dip         

    def slab_points_strike(self):
        #La funcion determina el strike asociado a una cierta ubicacion 
        #geografica (latitud/longitud) del slab 1.0 model.
        #INPUT:
        #      LON_POINTS: Longitud en grados (-180 a 180) de los puntos de interes. 
        #      LAT_POINTS: Latitud en grados (-90 a 90) de los puntos de interes.
        #      FILE_NAME : Nombre del fichero txt contenedor de los datos de strike.
        #OUTPUT:
        #      strike         : strike en grados de cada subfalla

        '''
        #######################################################################
        1. Se cargan datos de strike desde modelo slab 1.0.
        #######################################################################
        '''
        DATA = np.loadtxt(self.name_file_strike, delimiter=';',skiprows=1)

        LON         = DATA[:,1]    #<- Longitud en grados de los datos.
        LAT         = DATA[:,0]    #<- Latitud en grados de los datos.
        DATA_STRIKE = DATA[:,2]    #<- strike en grados
        
        LON_NEW=[]
        LAT_NEW=[]
        DATA_STRIKE_NEW=[]
        for lon,lat,strike in zip(LON,LAT,DATA_STRIKE):
            if not np.isnan(strike):
                LON_NEW.append(lon)
                LAT_NEW.append(lat)
                DATA_STRIKE_NEW.append(strike)
        
        '''
        #######################################################################
        2. Listas contenedoras de los puntos a interpolar
        #######################################################################
        '''
        #Puntos a interpolar del area de estudio.
        xi_start = np.linspace(min(LON_NEW), max(LON_NEW), self.numcols)
        yi_start = np.linspace(min(LAT_NEW), max(LAT_NEW), self.numrows)

        #Se inicializan listas para unificar los puntos de interes y los puntos
        #de definicio del area de estudio.
        xi_new = []
        yi_new = []
    
        #Se agrupa todo el conjunto de datos.
        for i in range(len(self.LON_POINTS)):
            xi_new.append(self.LON_POINTS[i])
            yi_new.append(self.LAT_POINTS[i])
    
        for i in range(len(xi_start)):
            xi_new.append(xi_start[i]) 
            yi_new.append(yi_start[i])

        xi_new = sorted(xi_new)
        yi_new = sorted(yi_new)  

        '''
        #######################################################################
        3. Interpolacion de datos.
        #######################################################################
        '''
        xi, yi = np.meshgrid(xi_new, yi_new)
        zi = griddata((np.array(LON_NEW), np.array(LAT_NEW)), np.array(DATA_STRIKE_NEW), (xi, yi), method='nearest')
        #zi = griddata(LON, LAT, DATA_STRIKE, xi, yi, interp='nn')    
   
        '''
        #######################################################################
        4. Se busca la ubicacion de los datos de interes.
        #######################################################################
        '''

        POS_xi = []
        POS_yi = []

        #Ubicacion de las coordenadas de latitud.
        for i in range(len(self.LAT_POINTS)):
            flag = True
            for j in range(len(yi)):
                if yi[j][0] == self.LAT_POINTS[i] and flag == True:
                    POS_yi.append(j)
                    flag = False
    
        #Ubicacion de las coordenadas de longitud.        
        for i in range(len(self.LON_POINTS)):
            flag = True
            for j in range(len(xi)):
                if xi[0][j] == self.LON_POINTS[i] and flag == True:
                    POS_xi.append(j)    
                    flag = False
    
        '''
        #######################################################################
        5. Se extraen los datos de strike
        #######################################################################
        '''
    
        #Se obtiene el strike en grados.
        strike_deg = []
    
        for i in range(len(POS_yi)):
            strike_deg.append(zi[POS_yi[i]][POS_xi[i]])
        
        return strike_deg         
        
    def reescribir_subfallas(self):
            
        #1 Se generan vertices de las subfallas de acuerdo a la geometria rectangular.
        FIL=self.num_subfallas  #Se evaluaran todas las subfallas.
        COL=5                   #Cada subfalla tiene cinco vertices a dibujar (el ultimo coincide con el primero)

        #Se decalran variables para almacenar vertices de todas las subfallas.
        vert_lat_new    = []
        vert_lon_new    = []
        lat_central_new = []    
        lon_central_new = []

        #Se crean matrices de ceros para rellenar con los vertices de todas las subfallas.
        for i in range(FIL):                #Numero de filas de la matriz
            vert_lon_new.append([0]*COL)    #Numero de columnas de la matriz, se inicializa con ceros
            vert_lat_new.append([0]*COL)

        alpha = np.degrees(self.strike) 
        beta = alpha + 270.0


        for subfalla in range(self.num_subfallas):
                w = self.W / self.ny; dy = w * np.sin((np.pi/2)-self.dip_slab_rad[subfalla])

                #Vertice 1
                vert_lon_new[subfalla][0] = self.lon_vertices[subfalla][0]
                vert_lat_new[subfalla][0] = self.lat_vertices[subfalla][0]
        
                #Vertice 2
                vert_lon_new[subfalla][1] = self.lon_vertices[subfalla][1]
                vert_lat_new[subfalla][1] = self.lat_vertices[subfalla][1]

                #Vertice 3.
                vert_lat_new[subfalla][2], vert_lon_new[subfalla][2], aux = vinc_pt( vert_lat_new[subfalla][1], vert_lon_new[subfalla][1], beta , dy )
                vert_lat_new[subfalla][3], vert_lon_new[subfalla][3], aux = vinc_pt( vert_lat_new[subfalla][0], vert_lon_new[subfalla][0], beta , dy )
 
                #Vertice 4.
                vert_lon_new[subfalla][4] = vert_lon_new[subfalla][0]
                vert_lat_new[subfalla][4] = vert_lat_new[subfalla][0]
 

                lat1, lon1, aux = vinc_pt( vert_lat_new[subfalla][0], vert_lon_new[subfalla][0], alpha , (self.L/self.nx)/2 )
                lat2, lon2, aux = vinc_pt( lat1, lon1, beta , dy/2 )
       
                lat_central_new.append(lat2)
                lon_central_new.append(lon2)
 
        return vert_lon_new, vert_lat_new, lon_central_new, lat_central_new

           
#def constructor_fallaE(loninf_ini, latinf_ini, lonsup_ini, latsup_ini,V_placa,W,H,rake_inv,nx,ny,delta_lat,lon_GPS,lat_GPS,fosa_lon,fosa_lat):
def constructor_fallaE_WE(loninf_ini, latinf_ini, lonsup_ini, latsup_ini,V_placa,W,H,nx,ny,delta_lat,fosa_lon,fosa_lat):

    '''
    -------------------------------------------------------------------------------   
    Parametros constructores del la gran falla
    -------------------------------------------------------------------------------
    '''
    #1. Se determina strike de la gran falla.
    aux, strike_delta, aux =  vinc_dist(  latinf_ini,  loninf_ini,   latsup_ini,  lonsup_ini) 

    #2. Se establecen puntos a distantes a la fosa.
    latf, lonf, aux = vinc_pt( latinf_ini, loninf_ini, strike_delta + 270 , 100000 )
    lat1, lon1, aux = vinc_pt( latsup_ini, lonsup_ini, strike_delta + 270 , 100000 )
    
    #3. Se definen variables de la falla: Largo (L) y strike.
    L, strike, backstrike = vinc_dist(  latf,  lonf,  lat1,  lon1 )  

    #4. dip y strike de la falla inversa definidos desde:
 
    dip = np.radians(0.)        #Dip en radianes                   
    strike_fjapan = np.radians(strike+180)    #Strike o rumbo en radianes <-Se construye con el costructor de la fosa de japon  
  
    '''
    -------------------------------------------------------------------------------
    Geometria asociada a la falla inversa.
    -------------------------------------------------------------------------------
    '''

    #5. geometria esperable de la falla Inversa.
#    rake_inv = np.radians(rake_inv)      #Rake de la falla  [grados] 116

    '''
    -------------------------------------------------------------------------------
    Geometria asociada a la falla normal.
    -------------------------------------------------------------------------------
    '''

    #7. Determina el origen del sistema de Okada a partir de la geometria base de la falla.)  
    lat0_ini,  lon0_ini,  alpha21  = vinc_pt( lat1, lon1, np.degrees(strike_fjapan+np.pi/2), W*np.cos(dip) )

    '''
    Distribucion de subfallas adaptadas a la fosa
    '''

    #8. Vertices de subfallas
    aux, aux, lon_vertices_plano_inicial, lat_vertices_plano_inicial = coordenadas_subfallas_EW(ny,nx,dip,W,L,lon0_ini,lat0_ini,strike_fjapan,fosa_lon,fosa_lat,delta_lat)

    #9. Strike representativo para Nazca/SudAmerica
    strike = np.radians(strike)

    #10. Los vertices se reordenan con respecto al formato desde el vertice de okada.
    lon_vertices_plano_final  = []
    lat_vertices_plano_final  = []
    
    for i in reversed(lon_vertices_plano_inicial):
        lon_vertices_plano_final.append(i)         
    
    for i in reversed(lat_vertices_plano_inicial):
        lat_vertices_plano_final.append(i)    
    
    return lon_vertices_plano_final, lat_vertices_plano_final,L,strike        
    
    
def constructor_fallaE_EW(loninf_ini, latinf_ini, lonsup_ini, latsup_ini,V_placa,W,H,nx,ny,delta_lat,fosa_lon,fosa_lat):

    '''
    -------------------------------------------------------------------------------   
    Parametros constructores del la gran falla
    -------------------------------------------------------------------------------
    '''
    #1. Se determina strike de la gran falla.
    aux, strike_delta, aux =  vinc_dist(  latinf_ini,  loninf_ini,   latsup_ini,  lonsup_ini) 
    strike_delta = np.radians(strike_delta+180)                                               #<-Strike o rumbo en radianes.

    #2. Se establecen puntos a distantes a la fosa.
    latf, lonf, aux = vinc_pt( latinf_ini, loninf_ini, strike_delta + 270 , 10000 )
    lat1, lon1, aux = vinc_pt( latsup_ini, lonsup_ini, strike_delta + 270 , 10000 )
    
    #3. Se definen variables de la falla: Largo (L) y strike.
    L, strike, backstrike = vinc_dist(  latf,  lonf,  lat1,  lon1 )  
    strike = np.radians(strike)                                               #<-Strike o rumbo en radianes.

    #4. dip y strike de la falla inversa definidos desde:
 
    dip = np.radians(0.)        #Dip en radianes                   
#    strike_fjapan = np.radians(strike+180)    #Strike o rumbo en radianes <-Se construye con el costructor de la fosa de japon  
  

    '''
    -------------------------------------------------------------------------------
    Geometria asociada a la falla normal.
    -------------------------------------------------------------------------------
    '''

    #7. Determina el origen del sistema de Okada a partir de la geometria base de la falla.)  
    lat0_ini,  lon0_ini,  alpha21  = vinc_pt( latf, lonf, np.degrees(strike+np.pi/2), W*np.cos(dip) )

    '''
    Distribucion de subfallas adaptadas a la fosa
    '''

    #8. Vertices de subfallas
    aux, aux, lon_vertices_plano_inicial, lat_vertices_plano_inicial = coordenadas_subfallas(ny,nx,dip,W,L,lon0_ini,lat0_ini,strike,fosa_lon,fosa_lat,delta_lat)

    #9. Strike representativo para Japon
    strike = np.radians(strike+180)

    #10. Los vertices se reordenan con respecto al formato desde el vertice de okada.
    lon_vertices_plano_final  = []
    lat_vertices_plano_final  = []
    
    for i in reversed(lon_vertices_plano_inicial):
        lon_vertices_plano_final.append(i)         
    
    for i in reversed(lat_vertices_plano_inicial):
        lat_vertices_plano_final.append(i)    
    
    return lon_vertices_plano_final, lat_vertices_plano_final,L,strike
    
        
def get_rake(p_placa, p_falla, d_falla):
 #INPUT
 #  p_placa = strike asociado al vector de convergencia de la placa en radianes
 #  p_falla = strike de la subfalla en radianes
 #  d_falla = dip de la subfalla en radianes
 #OUTPUT    
 #  rake    = rake de la subfalla en radianes para la interface superior con un
 #            movimiento relativo de tipo inverso.  
 a = np.sin(p_falla) - np.tan(p_placa) * np.cos(p_falla)
 b = np.tan(p_placa) * np.cos(d_falla) * np.sin(p_falla) + np.cos(d_falla) * np.cos(p_falla)  

 rake = np.arctan2(a,b) + np.pi
 
 return rake         
 

 
        
 
