from calculo_intersismico import Intersismico

if __name__ == "__main__":
    bandera = True
    params = {    
        'zona':'Illapel',
        'limites_zona': (-29,-33),
        'stations': [],
        'filtro':'cosismico',
        'inversion': False,
        'parte_proceso':1,
        'grid_size': (10,5),
        'plano_CD': False,
        'plano_E': False,
        'alto_placa': 12000,
        'ID': '/DELTA_10X5',
        'desplazamientos_obs.txt': './Fuentes/delta10x5.txt',
        'ancho_placa':200000,
        'rake_referencia': 110
             }
    
    intersismico = Intersismico(**params)
    map_params = {
        'latmin': -40,
        'latmax': -10,
        'lonmin': -90,
        'lonmax': -65,
        "paralelos": [-36, -32, -28, -24, -20],
        "meridianos": [-75, -70],
                  }
    ## -40 -20 -80 -65
    construir_params={
            'AB': True,
            'CD': False,
            'E' : False   }
    intersismico.set_map_params(**map_params)
    #intersismico.build_map()
    print("Planos de falla data")
    intersismico.plano_falla_ab()
    #intersismico.plano_falla_cd()
    #intersismico.plano_falla_e()
    print("Construyendo planos")
    intersismico.construye_A()
    #intersismico.construye_B()
    #intersismico.construye_CD()
    #intersismico.construye_E()
#    #intersismico.construye_planos()

    if bandera:
        print('Hacer problema directo')
        pdirecto_params={'interfases':'single','save_slips':True}
        intersismico.make_directo(**pdirecto_params)    
        print('Mapa de las fallas')
        geom_params={'plot_fosa':True,
                     'plot_contourf':False,
                     'interfaz':'upper',
                     'plot_datos':True,
                     'which_datos':'directas'}
        intersismico.mapa_geometria(**geom_params)
########
    if not bandera:
        print("Haciendo inversion")
        inversion_params={'interfases':'single','how2regularize':'none'}
        intersismico.inversion_intersismico_slab(**inversion_params)
        almacenar_params={'interfases':'single'}
        intersismico.almacenar_resultados(**almacenar_params)
#
        print('Mapa de las fallas')
        geom_params={'plot_fosa':True,
                     'plot_contourf':True,
                     'interfaz':'upper',
                     'datos_and_inv':True,
                     'quiverkey':False,
                     'plot_datos':False,
                     'which_datos':'directas'
                     }
        intersismico.mapa_geometria(**geom_params)
        
    intersismico.plot_slips(interfaz='single')
    ####
#        print('Mapa de velocidades sinteticas')
#        directo_params={'tipo':'sintetico','datos':'dummy' }
#    

   
    #print('Mapa de velocidades sinteticas')
    #intersismico.mapa_velocidades_sinteticas() 
#    print("Haciendo inversion")
#    inversion_params={'interfases':'single'}
#    intersismico.inversion_intersismico_slab(**inversion_params)
#    #intersismico.calcula_inversion()
#    print('creando outputs')

#    print('Mapa de velocidades sinteticas')
#    sint_param={'tipo': 'ambos','datos':'directo_postinverso'}
#    intersismico.mapa_velocidades_sinteticas(**sint_param)
    
#
#    
