import numpy as np 
import matplotlib.pyplot as plt 
from scipy import integrate
sigmaz=0.02
k=73
v0=4.12
g_vec=np.linspace(9,11,10)
v_vec=np.linspace(3.8,4.5,10)
sigmaz=0.02
sigmat=0.01
t1=0.2
t2=0.4
t3=0.6
t4=0.8
zobs=[0.62,0.88,0.70,0.15]

## El orden de parámetros de la funcion debe ser consistente con el orden de los limites de integración que se elijen
def func1(t4,t3,t2,t1):
	return k*np.exp(-(abs((v0*t1-0.5*g*t1**2)-zobs[0])/sigmaz + \
           abs((v0*t2-0.5*g*t2**2)-zobs[1])/sigmaz + \
           abs((v0*t3-0.5*g*t3**2)-zobs[2])/sigmaz + \
           abs((v0*t4-0.5*g*t4**2)-zobs[3])/sigmaz))

rho_vg=np.zeros((len(g_vec),len(v_vec)))
err_vg=np.zeros((len(g_vec),len(v_vec)))


for i in range(len(g_vec)):
    for j  in range(len(v_vec)):
        #print([i,j])
        g=g_vec[i]
        v0=v_vec[j]
        intr=integrate.nquad(func1,[[t4-sigmat,t4+sigmat],
                       [t3-sigmat,t3+sigmat],
                       [t2-sigmat,t2+sigmat],
                       [t1-sigmat,t1+sigmat]])
        rho_vg[i,j]=intr[0]
        err_vg[i,j]=intr[1]

gg,vv=np.meshgrid
fig,ax1 = plt.subplots(nrows=1)
ctrf=ax1.contourf(g_vec,v_vec,rho_vg,cmap=plt.cm.bone)
fig.colorbar(ctrf)
#ax1.plot(vx,vy,'^m',linewidth=4)       
#plt.contourf(g_vec,v_vec,rho_vg)
    #M=func1(v0,g,t1,t2,t3,t4,zobs)      
            
           
           