#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 18:00:01 2019

@author: doctor
"""
from calculo_cosismico import Seismic_Cycle
import numpy as np
import os
import shutil
if __name__ == "__main__":
    bandera = False
    #ids=['/BAYES_TEST_1X1_SINGLE','/BAYES_TEST_1X1_DOUBLE','/BAYES_TEST_1X1_DOUBLE_TRAMPA','/BAYES_TEST_1X1_DOUBLE_TRAMPA_DOUBLE']   
    #ids_tesis=['/BAYES_HP_1x1_SINGLE_UCSC','/BAYES_HP_1x1_DOUBLE_UCSC','/BAYES_HP_5X4_SINGLE_UCSC']
    ids_2020=['/3X3_TEST_SING',
              '/3X3_TEST_DOUB']
    
    datos_2020=['./Fuentes'+x+'_generated.txt' for x in ids_2020]
 
    
    #data_tesis=['./Fuentes/bayeshp1x1single.txt',
    #            './Fuentes/bayeshp1x1double.txt',
    #            './Fuentes/bayeshp5x4single.txt',
    #            './Fuentes/bayeshp5x4double.txt']



### CORRER PROBLEMA DIRECTO CON INTERFAZ SUPERIOR
    cosismico_params = {    
        'zona':'Illapel',
        'limites_zona': (-29,-33),
        'stations': [],
        'proceso':'coseismic',
        'interface':'single',
        'output':'direct',
        'grid_size': (3,3),
        'alto_placa': 12000,
        'ancho_placa':200000,
        'data_observada': './Fuentes/estaciones.txt',
        'rake_referencia': 110,
        'plano_CD': False,
        'plano_E': False,
        'ID': ids_2020[0]}

        #'ID': '/DIRECTO_GOOD_AGOSTO',
             
    
    directo_single = Seismic_Cycle(**cosismico_params)
    map_params = {
        'latmin': -40,
        'latmax': -10,
        'lonmin': -90,
        'lonmax': -65,
        "paralelos": [-36, -32, -28, -24, -20],
        "meridianos": [-75, -70],
                  }
    construir_params={
            'AB': True,
            'CD': False,
            'E' : False   }
    directo_single.set_map_params(**map_params)
    #intersismico.build_map()
    print("Planos de falla data")
    directo_single.plano_falla_ab()
    #intersismico.plano_falla_cd()
    #intersismico.plano_falla_e()
    print("Construyendo planos")
    directo_single.construye_A()
    print('Hacer problema directo')
    pdirecto_params={'interfases':'single','save_slips':True}
    #cosismico.make_directo_(**pdirecto_params)    
    #slip_A=3*np.ones(cosismico_params['grid_size'][0]*cosismico_params['grid_size'][1])
    #slip_A=np.array([3,1,7,2,3,4])
    #slip_A=np.array([0.7])
    slip_A=np.array([1,0,1,0,1,0,1,0,1])
    pdirecto_params={'slipA':slip_A}
    dsing,Gsing=directo_single.make_directo_new(**pdirecto_params)
    print('Mapa de las fallas')
    geom_params={'plot_fosa':True,
                 'plot_contourf':False,
                 'interfaz':'upper',
                 'plot_datos':True,
                 'which_datos':'directas'}
    directo_single.mapa_geometria(**geom_params)
    
    
#### CORRER PROBLEMA DIRECTO CON DOS INTERFACES
    cosismico_params.update({'interface':'double','ID':ids_2020[1]})
    directo_double = Seismic_Cycle(**cosismico_params)
    map_params = {
        'latmin': -40,
        'latmax': -10,
        'lonmin': -90,
        'lonmax': -65,
        "paralelos": [-36, -32, -28, -24, -20],
        "meridianos": [-75, -70]
                  }
    construir_params={
            'AB': True,
            'CD': False,
            'E' : False   }
    directo_double.set_map_params(**map_params)
    #intersismico.build_map()
    print("Planos de falla data")
    directo_double.plano_falla_ab()
    #intersismico.plano_falla_cd()
    #intersismico.plano_falla_e()
    print("Construyendo planos")
    directo_double.construye_A()
    print('Hacer problema directo')
    pdirecto_params={'interfases':'single','save_slips':True}
    #cosismico.make_directo_(**pdirecto_params)    
    #slip_A=3*np.ones(cosismico_params['grid_size'][0]*cosismico_params['grid_size'][1])
    #slip_B=1*np.ones(cosismico_params['grid_size'][0]*cosismico_params['grid_size'][1])

    #slip_A=np.array([3,1,7,2,3,4])
    #slip_A=np.array([1])
    #slip_B=np.array(3*[0,1,0])
    #slip_A=np.array(3*[1,0,1])
    slip_A=np.array([1,0,1,0,1,0,1,0,1])
    slip_B=np.array([0,1,0,1,0,1,0,1,0])

    pdirecto_params={'slipA':slip_A,'slipB':slip_B}
    ddoub,Gdoub=directo_double.make_directo_new(**pdirecto_params)
#    print('Mapa de las fallas')
#    geom_params={'plot_fosa':True,
#                 'plot_contourf':False,
#                 'interfaz':'upper',
#                 'plot_datos':True,
#                 'which_datos':'directas'}
#    cosismico.mapa_geometria(**geom_params)
#    
#    
#        
### INVERTIR EL DIRECTO DE UNA INTERFAZ CON UNA INTERFAZ:
    
    cosismico_params.update({'output':'inversion','ID':ids_2020[0],'bypass':True,'data_observada':datos_2020[0],'interface': 'single','synthetic_test':True})
    inverso_singsing = Seismic_Cycle(**cosismico_params)
    bayesian_params = {'how2regularize': 'minimize','add_noise': True}
    Gsingsing,dobs,dsingsing=inverso_singsing.bayesian_inversion(**bayesian_params)

# INVERTIR EL DIRECTO DE UNA INTERFAZ CON DOS INTERFACES
    cosismico_params.update({'output':'inversion','ID':ids_2020[0],'bypass':True,'data_observada':datos_2020[0],'interface': 'double','synthetic_test':False})
    ## Necesitamos mover las funciones de Green
    path_gf_fuente='Outputs'+ids_2020[1]+'/coseismic/'+cosismico_params.get('interface')+'/direct/gf'
    path_gf_dest=path_gf_fuente.replace('direct','inversion').replace(ids_2020[1],ids_2020[0])
    try:
        shutil.copytree(path_gf_fuente,path_gf_dest)
    except FileExistsError:
        pass
    inverso_singdoub = Seismic_Cycle(**cosismico_params)
    bayesian_params = {'how2regularize': 'minimize','add_noise': True}
    Gsingdoub,dobs,dsingdoub=inverso_singdoub.bayesian_inversion(**bayesian_params)
    ## cargar los graficos
    map_params = {
        'latmin': -40,
        'latmax': -10,
        'lonmin': -90,
        'lonmax': -65,
        "paralelos": [-36, -32, -28, -24, -20],
        "meridianos": [-75, -70]}
    
    inverso_singdoub.set_map_params(**map_params)
    php_params={'load_syntethic_slips': True}
    inverso_singdoub.plot_slips_arrows_fuentesingle(**php_params)


# INVERTIR EL DIRECTO DE DOS INTERFACES CON DOS INTERFAZ:
    cosismico_params.update({'output':'inversion','ID':ids_2020[1],'bypass':True,'data_observada':datos_2020[1],'interface': 'double','synthetic_test':True})
    inverso_doubdoub = Seismic_Cycle(**cosismico_params)
    bayesian_params = {'how2regularize': 'minimize','add_noise': True}
    Gdoubdoub,dobs,ddoubdoub=inverso_doubdoub.bayesian_inversion(**bayesian_params)

    
## INVERTIR EL DIRECTO DE DOS INTERFACES CON UNA INTERFAZ:
    cosismico_params.update({'output':'inversion','ID':ids_2020[1],'bypass':True,'data_observada':datos_2020[1],'interface': 'single','synthetic_test':False})

    ## Necesitamos mover las funciones de Green
    path_gf_fuente2='Outputs'+ids_2020[0]+'/coseismic/single/direct/gf'
    path_gf_dest2=path_gf_fuente2.replace('direct','inversion').replace(ids_2020[0],ids_2020[1])
    try:
        shutil.copytree(path_gf_fuente2,path_gf_dest2)
    except FileExistsError:
        pass
    inverso_doubsing = Seismic_Cycle(**cosismico_params)
    bayesian_params = {'how2regularize': 'minimize','add_noise': True}
    Gdoubsing,dobs,ddoubsing=inverso_doubsing.bayesian_inversion(**bayesian_params)
    
    map_params = {
        'latmin': -40,
        'latmax': -10,
        'lonmin': -90,
        'lonmax': -65,
        "paralelos": [-36, -32, -28, -24, -20],
        "meridianos": [-75, -70]}
    
    inverso_doubsing.set_map_params(**map_params)
    php_params={'load_syntethic_slips': True}
    inverso_doubsing.plot_slips_arrows_fuentedoble(**php_params)
#    
#


