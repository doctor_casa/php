from calculo_cosismico import Cosismico

if __name__ == "__main__":
    bandera = False
    params = {    
        'zona':'Illapel',
        'limites_zona': (-29,-33),
        'stations': [],
        'filtro':'cosismico',
        'grid_size': (10,5),
        'alto_placa': 12000,
        'ancho_placa':200000,
        'desplazamientos_obs.txt': './Fuentes/estaciones.txt',
        'parte_proceso':2,
        'rake_referencia': 110,
        'plano_CD': False,
        'plano_E': False,
        'ID': '/DELTA_10X5',
             }
    
    cosismico = Cosismico(**params)
    map_params = {
        'latmin': -40,
        'latmax': -10,
        'lonmin': -90,
        'lonmax': -65,
        "paralelos": [-36, -32, -28, -24, -20],
        "meridianos": [-75, -70],
                  }
    construir_params={
            'AB': True,
            'CD': False,
            'E' : False   }
    cosismico.set_map_params(**map_params)
    #intersismico.build_map()
    print("Planos de falla data")
    cosismico.plano_falla_ab()
    #intersismico.plano_falla_cd()
    #intersismico.plano_falla_e()
    print("Construyendo planos")
    cosismico.construye_A()
   

    if bandera:
        print('Hacer problema directo')
        pdirecto_params={'interfases':'single','save_slips':True}
        cosismico.make_directo(**pdirecto_params)    
        print('Mapa de las fallas')
        geom_params={'plot_fosa':True,
                     'plot_contourf':False,
                     'interfaz':'upper',
                     'plot_datos':True,
                     'which_datos':'directas'}
        cosismico.mapa_geometria(**geom_params)

    if not bandera:
        print("Haciendo inversion")
        inversion_params={'interfases':'single','how2regularize':'none'}
        cosismico.inversion_intersismico_slab(**inversion_params)
        almacenar_params={'interfases':'single'}
        cosismico.almacenar_resultados(**almacenar_params)

        print('Mapa de las fallas')
        geom_params={'plot_fosa':True,
                     'plot_contourf':True,
                     'interfaz':'upper',
                     'datos_and_inv':True,
                     'quiverkey':False,
                     'plot_datos':False,
                     'which_datos':'directas'
                     }
        cosismico.mapa_geometria(**geom_params)
        
    cosismico.plot_slips(interfaz='single')
    
    
#
#    
