#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 18:00:01 2019

@author: doctor
"""
from calculo_cosismico import Seismic_Cycle
import numpy as np
if __name__ == "__main__":
    bandera = False
    #ids=['/BAYES_TEST_1X1_SINGLE','/BAYES_TEST_1X1_DOUBLE','/BAYES_TEST_1X1_DOUBLE_TRAMPA','/BAYES_TEST_1X1_DOUBLE_TRAMPA_DOUBLE']
    #ids_tesis=['/BAYES_HP_1x1_SINGLE','/BAYES_HP_1x1_DOUBLE','/BAYES_HP_5X4_SINGLE']
    ids_tesis=['/BAYES_HP_1X1_SSYDS','/BAYES_HP_6X4_SSYDS']
    params = {    
        'zona':'Illapel',
        'limites_zona': (-29,-33),
        'stations': [],
        'proceso':'coseismic',
        'interface':'single',
        'output':'direct',
        'grid_size': (6,4),
        'alto_placa': 12000,
        'ancho_placa':200000,
        'data_observada': './Fuentes/bayesian_dummy.txt',
        'rake_referencia': 110,
        'plano_CD': False,
        'plano_E': False,
        'ID': ids_tesis[1]}

        #'ID': '/DIRECTO_GOOD_AGOSTO',
             
    
    cosismico = Seismic_Cycle(**params)
    map_params = {
        'latmin': -40,
        'latmax': -10,
        'lonmin': -90,
        'lonmax': -65,
        "paralelos": [-36, -32, -28, -24, -20],
        "meridianos": [-75, -70],
                  }
    construir_params={
            'AB': True,
            'CD': False,
            'E' : False   }
    cosismico.set_map_params(**map_params)
    #intersismico.build_map()
    print("Planos de falla data")
    cosismico.plano_falla_ab()
    #intersismico.plano_falla_cd()
    #intersismico.plano_falla_e()
    print("Construyendo planos")
    cosismico.construye_A()
    print('Hacer problema directo')
    pdirecto_params={'interfases':'single','save_slips':True}
    #cosismico.make_directo_(**pdirecto_params)    
    #slip_A=3*np.ones(params['grid_size'][0]*params['grid_size'][1])
    #slip_A=np.array([3,1,7,2,3,4])
    #slip_A=np.array([1])
    slip_Ass=np.array([0.5])
    slip_Ads=np.array([0.1])
    slip_Ass = np.array(12*[0,0.2])
    slip_Ads = np.array(12*[0.5,0])

    pdirecto_params={'slip_Ass': slip_Ass, 'slip_Ads': slip_Ads}
    d1,G2=cosismico.make_directo_new_dividido(**pdirecto_params)
    print('Mapa de las fallas')
    geom_params={'plot_fosa':True,
                 'plot_contourf':False,
                 'interfaz':'upper',
                 'plot_datos':True,
                 'which_datos':'directas'}
    cosismico.mapa_geometria(**geom_params)
        
    
    
#
#    

