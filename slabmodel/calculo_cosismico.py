# -*- coding: utf-8 -*-

from mpl_toolkits.basemap import Basemap, cm
import matplotlib.pyplot as plt
import numpy as np
import math
import os
import scipy as scp
from scipy.interpolate import griddata
from scipy.optimize import nnls,minimize
from datetime import datetime
from matplotlib import colors
import time    
from fractions import Fraction
from functools import reduce
import csv
import shutil


def cut_file(filename,
             dict_types,
             limites=(0, -90),
             colname='Latitud'):
## Get all columns in file that coincide with dict_types. limites
## are the intervals in which we get data accordint to colname
    norte = limites[0]
    sur = limites[1]
    data = []
    with open(filename, 'r') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=';')
        # extract headers
        for row in reader:
            final_types = {key:dict_types.get(key, str) for key in row.keys()}
            new_row = {key: final_types.get(key)(value) for key, value in row.items()}
            value = new_row.get(colname, 0)
            if sur <= value and value <= norte :
                data.append(new_row)
    return data

def find_real(lista, value, esp=0.01):
    ## find respective longitude values that corresponds to 
    ## latitude limit
    for index, elem in enumerate(lista):
        if elem-esp<=value and value<=elem+esp:
            return index, elem


class Seismic_Cycle:
#class Seismic_Cycle:
    # # velocidad de placa en metros por año
    # v_placa = 0.068
    # # grosor de la placa en metros
    # W = 200000
    # # un rake de referencia
    # rake_inv_ref_deg = 120
    # # alto de la placa
    # H = 11000
    #delta_lat = 0.08
    #dip_plano_inicial = np.radians(14.)
    ## constantes de regularización
    #delta_lat = 0.08
    # lambdas = dict.fromkeys(['A_min',
    #                          'A_suav',
    #                          'B_min',
    #                          'B_suav'], 0.02)

    def __init__(self, *args, **kwargs):
        
        ## hiperparametros que usamos para la inversion por optimizacion 
        self.lmbd1=kwargs.get('lambda1',0.02)
        self.lmbd2=kwargs.get('lambda2',0.02)
        self.lmbd3=kwargs.get('lambda3',0.02)
        self.lmbd4=kwargs.get('lambda4',0.02)

        ## una ID del proceso para diferenciarla de las demas.
        self.id_proceso = kwargs.get('ID', '/Output_1')
        ## elegir entre coseismic, interseismic o postseismic
        self.proceso = kwargs.get('proceso','coseismic')
        ## elegir entre Single o Double
        self.interface = kwargs.get('interface','double')
        # Elegir entre Directo o Inversion o Green_Functions
        self.output = kwargs.get('output','direct')


       ## create folder hierarchy
        self.crear_arquitectura=kwargs.get('create_folders',True)
        if self.crear_arquitectura:
            try:
                os.mkdir('Outputs'+ self.id_proceso)
            except FileExistsError:
                    print('la carpeta ya existe')

            try:
                os.mkdir('Outputs'+ self.id_proceso+'/'+self.proceso)
            except FileExistsError:
                print('la carpeta ya existe')

            try:
                os.mkdir('Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface)
            except FileExistsError:
                print('la carpeta ya existe')

            try:
                os.mkdir('Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output)
            except FileExistsError:
                print('la carpeta ya existe')


            try:
                os.mkdir('Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/gf')
            except FileExistsError:
                print('la carpeta ya existe')

            try:
                os.mkdir('Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/figs')
            except FileExistsError:
                print('la carpeta ya existe')

            try:
                os.mkdir('Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/txt')
            except FileExistsError:
                print('la carpeta ya existe')

    
        # I dont use this variable on script    
        self.path_slips=kwargs.get('path_slips','Outputs')
        ##  Whether we create smooth matrix or not
        self.makeF = kwargs.get('smooth_matrix',False)
        ## Whether we create minima matriz or not
        self.makeM = kwargs.get('minima_matrix',True)
        ## Unused variable
        self.nombre_zona = kwargs.get('zona', 'Chile')
        ## define the limits in which data will be collected for creating slab geometry
        self.limites_zona = kwargs.get('limites_zona',(-18, -50))


        ## this should be always 3
        self.n_dim = kwargs.get('dimensiones', 3)
        ## the initial dip angle assumed for the fault plane
        self.dip_plano_inicial = kwargs.get('dip_inicial', np.radians(14.))
        ## this delta is used for building subfaults coordinates as needed in okada formulation
        self.delta_lat = kwargs.get('delta_lat', 0.08)
        ## input plate velocity in meters per year
        self.velocidad_placa = kwargs.get('vel_placa', 0.068)
        ### width of the fault along dip (a.k.a W in okada formulation)
        self.ancho_placa = kwargs.get('ancho_placa',260000)
        ## reference rake for fault slip assuming thrust faulting
        self.rake_referencia = kwargs.get('rake_referencia', 120)
        # this is plate thickness,
        self.plate_thickness = kwargs.get('slab_thickness', 11000)
        ## length of the'fault along strike. Must intialize those variables
        self.L = None
        self.strike_aux_deg = None
        self.backstrike = None
        ## parameters for creating subfaults and interpolating
        self.numcols=50
        self.numrows=150
        ## date
        self.date = kwargs.get('date', datetime.utcnow().isoformat())
        ## Chile  case is always WE
        self.sentido_subduccion = kwargs.get('sentido', 'WE')
        ## whether we generate a boundary condition on trench
        ## I think this is not needed in the new way to code this
        self.condicion_borde_fosa = kwargs.get('condicion_borde_fosa', False)
        ## FILES WHERE WE GET SLAB PARAMETERS ACCORDING TO SLAB 1.0 - 2.0
        self.file_fosa = kwargs.get(
            'file_fosa',
            "./DatosBase/perimetro_fosa_chile.csv")
        self.file_profundidad = kwargs.get(
            'file_profundidad',
            "./DatosBase/profundidad_xyz_fosa_chile.csv")
        self.file_strike = kwargs.get(
            'file_strike',
            "./DatosBase/strike_fosa_chile.csv")
        self.file_dip = kwargs.get(
            'file_dip',
            "./DatosBase/dip_fosa_chile.csv")
        ## griding of faults (# along strike, # along dip)
        self.grid_size = kwargs.get('grid_size', (75,5))
        
        self.bypass = kwargs.get('bypass',False)
        if not self.bypass:
            ## if bypass is false, then we do long process and 
            ##  fault data is loaded 
            self.data_falla = self.load_data_falla()
        self.synthetic_test=kwargs.get('synthetic_test', True)
        self.load_incertezas=kwargs.get('load_incertezas',False)
        if self.bypass:
            ## if bypass is true, then a bypass is performed and 
            ## fault geometry and green functions are loaded (geometry is not generated),
            ## and process takes less time

            ## whether we splitted G which are being loaded 
            self.split_G = kwargs.get('split_G', False)
            self.green_functions,self.coord_fallas=self.load_greenfunctions()

        ## which fault planes will be constructed?
        self.falla_AB=kwargs.get('plano_AB', True)
        self.falla_CD=kwargs.get('plano_CD', False)
        self.falla_E=kwargs.get('plano_E',  False)

        
        
        
        ## Fault planes information (endpoints, number of faults, etc)
        self.planos_falla = {}
        ## In depth info of fault plane
        self.planos_falla_obj = {}
        ## Information of fault griding 
        self.subfallas_model_dict={}
        ## name of GNSS data used 
        self.name_file=kwargs.get('data_observada','./Fuentes/estaciones_cosismico.csv')
        ## load GNSS data
        self.data_estaciones=self.load_GNSS()

        ## name of output folders and files
        self.desplaz_output='Outputs'+self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/txt'+self.id_proceso+'_generated.txt'
        self.lower_interface_output='Outputs'+self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/txt/'+self.proceso.lower()+'_slip_lower_interface.txt'
        self.upper_interface_output='Outputs'+self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/txt/'+self.proceso.lower()+'_slip_upper_interface.txt'
        self.figuras_output='Outputs'+self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/figs/'
        self.inversion_params = 'Outputs'+self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/txt/inv_params.txt'
        ## Green Functions Output. Unused
        self.gf_upper_output = 'Outputs'+self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/txt/'

        ## generated synthetic data by a DIRECT PROBLEM
        self.data_sintetica={}
        ## outputs of inversion. Apparently unused
        self.outputs_inv={}
            

        

    def plano_falla_ab(self, **kwargs):
        ### this function gets the data of the lower and upper points fo the AB fault planes. It also 
        ### gets the value f the the initialized variables L, strike_aux_deg and backstrike
        fosa = self.data_falla.get('fosa')
        fosa_lat = [elem.get('Latitud') for elem in fosa]
        fosa_lon = [elem.get('Longitud') for elem in fosa]
        # <-Limite superior de la fosa.
        #if self.filtro=="intersismico":
        latf = min(fosa_lat)
        index, latf = find_real(fosa_lat, latf)
        lonf = fosa_lon[index]
        lat1 = max(fosa_lat)
        index, lat1 = find_real(fosa_lat, lat1)
        lon1 = fosa_lon[index]
        # min(lat) fosa_lat[6]<-Limite inferior de la fosa.
        latf = min(fosa_lat)
        PF_AB = dict(
                superior=(lat1, lon1),
                inferior=(latf, lonf)
                    )
        print('latflati')
        print(latf,lat1)
        print('lonflonfi')
        print(lonf,lon1)
        self.L, self.strike_aux_deg, self.backstrike = vinc_dist(latf,lonf,lat1,lon1)
    
        print('selfL',self.L,self.strike_aux_deg)
        self.planos_falla.update({'AB': PF_AB})
        #self.claves_fallo.update({'claves':(latf,lonf),(lat1,lon1)})

    def plano_falla_cd(self, **kwargs):
        """ this function saves the number of faults along dip and their respective width of the
        cd fault planes  """
        ny_cd = kwargs.get('ny_cd', 8)
        w_cd = self.ancho_placa/self.grid_size[1]
        PF_CD = dict(
                ny_cd=ny_cd,
                w_cd=w_cd
                    )
        self.planos_falla.update({'CD': PF_CD})

    def plano_falla_e(self, **kwargs):
        """ this function gets the data of fault E given an length and width for this plane.
            the number of faults along strike is the same of AB, and only one fault is used 
            dip  """
        km = 1000
        ancho = kwargs.get('ancho_e', 300)
        E = dict(
            W_E=ancho*km,
            H_E=self.H,
            nx_E=self.grid_size[0],
            ny_E=1,
            delta_lat_E=-0.19
                )
        self.planos_falla.update({"E": E})

    def load_data_falla(self):
        """ this function cuts the main files of dip, strike, depth and the trench rake
         in the interval given by the zone limits in __init__ """
        data_type = {'Latitud': float,
                     'Longitud': float,
                     'Strike': float,
                     'Dip': float,
                     'Profundidad': float}
        data_profundidad = cut_file(
            self.file_profundidad,
            data_type,
            self.limites_zona,
            'Latitud')
        data_fosa = cut_file(
            self.file_fosa,
            data_type,
            self.limites_zona,
            'Latitud')
        data_strike = cut_file(
            self.file_strike,
            data_type,
            self.limites_zona,
            'Latitud')
        data_dip = cut_file(
            self.file_dip,
            data_type,
            self.limites_zona,
            'Latitud')
        data_falla = dict(
            fosa=data_fosa,
            profundidad=data_profundidad,
            strike=data_strike,
            dip=data_dip)
        return(data_falla)
        
    #def load_intersismico(self, limites=(0, -90), stations=[]):
    def load_intersismico(self, stations=[]):

        """ this function load the observated data related to the interseismic period. the limits 
            are required only if we have data in a greater space than we want to use. This data
            will be used in the inversion process.
        """
        if not self.load_incertezas:
            data_type = {
                         'Latitud': float,
                         'Longitud': float,
                         'UE': lambda x: float(x)/1000,
                         'UN': lambda x: float(x)/1000,
                         'UU': lambda x: float(x)/1000
                        }
        if self.load_incertezas:
            data_type = {
                         'Latitud': float,
                         'Longitud': float,
                         'UE': lambda x: float(x)/1000,
                         'UN': lambda x: float(x)/1000,
                         'UU': lambda x: float(x)/1000,
                         'SE': lambda x: float(x)/1000,
                         'SN': lambda x: float(x)/1000,
                         'SU': lambda x: float(x)/1000,
                        }
        #archivo = self.fuente_estaciones
        data=self.data_estaciones
        #data_velocidad = cut_file(archivo, data_type, limites)
        data_velocidad = cut_file(archivo, data_type, self.limites_zona)

        data_velocidad_set = data_velocidad
        if stations:
            data_velocidad_set = [data for data in data_velocidad
                                  if data.get('Station') in stations]

        return data_velocidad_set





    def load_GNSS(self,  stations=[], load_incertezas=False):
        """ this function load the observated data related to the coseismic period. the limits 
            are required only if we have data in a greater space than we want to use. This data will be
            used in the inversion process
        """
        if not self.load_incertezas:
            data_type = {
                         'Latitud': float,
                         'Longitud': float,
                         'UE': lambda x: float(x)/1000,
                         'UN': lambda x: float(x)/1000,
                         'UU': lambda x: float(x)/1000
                        }
        if self.load_incertezas:
            data_type = {
                         'Latitud': float,
                         'Longitud': float,
                         'UE': lambda x: float(x)/1000,
                         'UN': lambda x: float(x)/1000,
                         'UU': lambda x: float(x)/1000,
                         'SE': lambda x: float(x)/1000,
                         'SN': lambda x: float(x)/1000,
                         'SU': lambda x: float(x)/1000,
                        }
        archivo = self.name_file
        #data_disp = cut_file(archivo, data_type, self.limites_zona)
        data_disp = cut_file(archivo, data_type) 
        data_disp_set = data_disp
        if stations:
            data_disp_set = [data for data in data_disp
                                  if data.get('Station') in stations]

        return data_disp_set

    def load_postsismico(self):
        """ this function load the observated data related to the poseismic period. the limits 
            are required only if we have data in a greater space than we want to use. This data
            will be used in the inversion process
        """
        pass


    def load_data_estaciones(self,archivo):
        """ this function loads and specific  geodetic file that  wont  be used in the inversion process.
            the purpose of this loading is mostly for comparing data
        """


        data_type = { 
                     'Latitud': float,
                     'Longitud': float,
                     'UE': lambda x: float(x)/1000,
                     'UN': lambda x: float(x)/1000,
                     'UU': lambda x: float(x)/1000}
        
        data_disp = cut_file(archivo, data_type, limites)
        data_disp_set = data_disp
        return data_disp_set



    def load_data_slips(self,archivo,interfaz='upper'):
        """ this function loads and specific  slip  file. the purpose of this loading is mostly for
         comparing data. It must be defined whether the interphase wil be the superior or the inferior
         fault. 
        """

        if interfaz=='upper':
            slip='slip_upper'
        if interfaz=='lower':
            slip='slip_lower'

        data_type = { 
                     'Longitud': float,
                     'Latitud': float,
                     slip: float,
                     }
        
        data_slip = cut_file(archivo, data_type)
        data_slip_set = data_slip
        return data_slip_set



   


    def set_map_params(self, **kwargs):
        """ this function sets the Basemap params of build_map function"""
         ## escribir info. adicional
        self.map_params = dict(
            projection='merc',
            llcrnrlat=kwargs.get('latmin'),
            urcrnrlat=kwargs.get('latmax'),
            llcrnrlon=kwargs.get('lonmin'),
            urcrnrlon=kwargs.get('lonmax'),
            lat_ts=(kwargs.get('latmin')+kwargs.get('latmax'))/2,
            resolution='h',)
            # paralelos=kwargs.get('paralelos'),
            # meridianos=kwargs.get('meridianos'))
        print(self.map_params)

    def build_map(self):
        """  this function builds a map. it is possible deprecated lol"""
        del self.map_params['paralelos']
        del self.map_params['meridianos']
        plt.figure()
        m = Basemap(**self.map_params)
        m.drawcoastlines()
        m.drawparallels(
            np.arange(-90, 90, 5),
            labels=[1, 1, 1, 1])
        m.drawmeridians(
            np.arange(-90, 90, 5),
            labels=[0, 0, 0, 1])
        m.drawcountries()
        m.drawmapscale(-75, -41, 0, 0, 100)
        self.show_arrows(m)
        self.plot_fosa(m)
        plt.show()
        plt.savefig('/Outputs' + self.id_proceso + '/Figuras/mapa_previo.png',format='png',dpi='figure')

    

    
    
    def plot_fosa(self,m):
        """ this function plot the trench data in the m instance of the map building """ 
        lat_fosa=[elem.get('Latitud') for elem in self.data_falla.get('fosa')]
        lon_fosa=[elem.get('Longitud') for elem in self.data_falla.get('fosa')]
        x,y=m(lon_fosa,lat_fosa)
        m.plot(x,y,'-r',linewidth=2)
        
        
    def show_arrows(self, m):
    ###  this function plot the vector data in the m instance. it is probably deprecated lol"""
            lon = [elem.get('Longitud') for elem in self.data_estaciones]
            lat = [elem.get('Latitud') for elem in self.data_estaciones]
            Ue_obs = [elem.get('UE') for elem in self.data_estaciones]
            Un_obs = [elem.get('UN') for elem in self.data_estaciones]
            V = m.quiver(
                         lon,
                         lat,
                         Ue_obs,
                         Un_obs,
                         color='r',
                         scale=0.25,
                         width=0.0050,
                         linewidth=0.5,
                         headwidth=4.,
                         zorder=26
                         )
                
            plt.quiverkey(
                          V,
                          0.75,
                          0.18,
                          0.01,
                          r'GPS obs: 1 cm/yr',
                          labelpos='N',
                          labelcolor= (0,0,0),zorder=26,
                          )
                   

    def contour_maps(self,m,interfaz='upper'):
        ### this function plot the slip data in the m instance """
            # subfallas_methods=self.planos_falla_obj.get('subfallas_methods')
            # prof_subf_inv=subfallas_methods[0]
            # dip_subf_rad=subfallas_methods[1]
            # strike_subf_deg=subfallas_methods[2]
            # lonv_vert_subf_slab=subfallas_methods[3]
            # lat_vert_subf_slab=subfallas_methods[4]
            # lon_cent_subf_slab=subfallas_methods[5]
            # lat_cent_subf_slab=subfallas_methods[6]
            # prof_subf_norm=subfallas_methods[7]
            # phi_placa_rad=subfallas_methods[8]
            # rake_inv_rad=subfallas_methods[9]
            # rake_norm_rad=subfallas_methods[10]

            prof_subf_inv=self.subfallas_model_dict.get('depth_model')
            strike_subf_deg=self.subfallas_model_dict.get('strike_model')
            dip_subf_rad=np.radians(self.subfallas_model_dict.get('dip_model'))
            lonv_vert_subf_slab=self.subfallas_model_dict.get('lon_vertices')
            lat_vert_subf_slab=self.subfallas_model_dict.get('lat_vertices')
            lon_cent_subf_slab=self.subfallas_model_dict.get('lon_central')
            lat_cent_subf_slab=self.subfallas_model_dict.get('lat_central')
            strike_rad=self.subfallas_model_dict.get('strike_rad')
            prof_subf_norm=prof_subf_inv+self.plate_thickness/np.cos(dip_subf_rad)
            rake_inv_rad=self.subfallas_model_dict.get('rake_inv_rad')
            rake_norm_rad=self.subfallas_model_dict.get('rake_norm_rad')

            strike_rad = np.radians(self.strike_aux_deg)


            #which_data=kwargs.get('which_data','desplaz')


            
            #if show_vectores:
                #data_disp_set=self.load_data_estaciones(which_data)
                #lat=[elem.get('Latitud') for elem in data_disp_set]
                #lon=[elem.get('Longtitud') for elem in data_disp_set]
                #slip=[elem.get('') for elem in data_disp_set]
            
            if interfaz == 'upper':
                data = self.load_data_slips(self.upper_interface_output,interfaz='upper')
                key='slip_upper'
            if interfaz == 'lower':
                data = self.load_data_slips(self.lower_interface_output,interfaz='lower')
                key='slip_lower'


                
            #lat=[float(elem.get('lat_central')) for elem in data]
            #lon=[float(elem.get('lon_central')) for elem in data]
            #print(data)
            slip=[float(elem.get(key)) for elem in data]
            ## se crea una mascara para evitar que el contourf grafique fuera de las subfallas

            slip_mask         = []
            lat_central_mask  = []
            lon_central_mask  = []


            subfalla=0
            #Mascara para el dibujo. Evita que contourf pinte/grafique fuera de las subfallas.
            for j_ny in range(self.grid_size[1]):
                for j_nx in range(self.grid_size[0]):
                    slip_mask.append(slip[subfalla])      #<----------------
                    lat_central_mask.append(lat_cent_subf_slab[subfalla])
                    lon_central_mask.append(lon_cent_subf_slab[subfalla])
                    if (j_ny==0):  
                        slip_mask.append(0)
                        ## QUEDE EN STRIKE_RAD, probar despues con strike variable 
                        alpha = np.degrees(strike_rad)
                        beta = alpha + 90
                        lataux , lonaux, aux = vinc_pt( lat_cent_subf_slab[subfalla],
                                                            lon_cent_subf_slab[subfalla],
                                                            beta ,
                                                             self.ancho_placa/self.grid_size[1] )
                        lat_central_mask.append(lataux)
                        lon_central_mask.append(lonaux)

                    if (j_ny==(self.grid_size[1]-1)):
                        slip_mask.append(slip[subfalla])
                        alpha = np.degrees(strike_rad)
                        beta = alpha + 270
                        lataux , lonaux, aux = vinc_pt(lat_cent_subf_slab[subfalla],
                                                           lon_cent_subf_slab[subfalla],
                                                           beta , 
                                                           (self.ancho_placa/self.grid_size[1])/2 )
                        lat_central_mask.append(lataux)
                        lon_central_mask.append(lonaux)
                        #borde final
                        slip_mask.append(0)
                        alpha = np.degrees(strike_rad)
                        beta = alpha + 270
                        lataux , lonaux, aux = vinc_pt( lat_cent_subf_slab[subfalla],
                                                            lon_cent_subf_slab[subfalla],
                                                            beta ,
                                                            (self.ancho_placa/self.grid_size[1])/2+(self.ancho_placa/self.grid_size[1])/4 )
                        lat_central_mask.append(lataux)
                        lon_central_mask.append(lonaux)            
                    if (j_nx == 0):
                        slip_mask.append(slip[subfalla])   #<----------------
                        alpha = np.degrees(strike_rad)
                        beta = alpha + 180
                        lataux , lonaux, aux = vinc_pt( lat_cent_subf_slab[subfalla],
                                                            lon_cent_subf_slab[subfalla],
                                                             beta , (self.ancho_placa/self.grid_size[1])/2 )
                        lat_central_mask.append(lataux)
                        lon_central_mask.append(lonaux)   
                    if (j_nx == (self.grid_size[0]-1)):
                        slip_mask.append(slip[subfalla])   #<----------------
                        alpha = np.degrees(strike_rad)
                        beta = alpha 
                        lataux , lonaux, aux = vinc_pt( lat_cent_subf_slab[subfalla],
                                                            lon_cent_subf_slab[subfalla],
                                                            beta , (self.ancho_placa/self.grid_size[1])/2 )
                        lat_central_mask.append(lataux)
                        lon_central_mask.append(lonaux)    
                    subfalla=subfalla+1
            
           
            numcols=500
            numrows=500
            xi = np.linspace(min(lon_central_mask),max(lon_central_mask),numcols)
            yi = np.linspace(min(lat_central_mask),max(lat_central_mask),numrows)
            xi,yi= np.meshgrid(xi,yi)
            zi=griddata((np.array(lon_central_mask),
                                        np.array(lat_central_mask)),
                                        np.array(slip_mask),
                                        (xi,yi),
                                        method='nearest')
            ## esto es perfectible          
            tick_colorbar=list(np.linspace(min(slip_mask),max(slip_mask),8))
            #print('t',tick_colorbar)
            #tick_colorbar[float("{0:.1f}".format(x)) for  x in tick_colorbar] 
            # tick_colorbar    = [0,
            #                     float("{0:.1f}".format(max(slip_mask)*(0.25))),
            #                     float("{0:.1f}".format(max(slip_mask)*(0.5))),
            #                     float("{0:.1f}".format(max(slip_mask)*(0.75))),
            #                     float("{0:.1f}".format(max(slip_mask)))]

            paleta=cm.GMT_polar_r
            niveles_contourf=np.linspace(min(slip_mask),max(slip_mask),50)
            xi,yi=m(xi,yi)
            im=m.contourf(xi,yi,zi,
                          vmin=min(slip_mask),
                          vmax=max(slip_mask),
                          levels=niveles_contourf,cmap=paleta,zorder=18,extend='max') #,extend='max'
            cb=m.colorbar(im,ticks=tick_colorbar,
                          location='bottom',size='3%',
                          pad='8%',
                          extend='max')   
    def mapa_geometria(self,**kwargs):
        """  this function builds a map of the studied zone. Several parameters can be plotted
             assigning  different values in the **kwargs """
        # del self.map_params['paralelos']
        # del self.map_params['meridianos']
        plot_fosa=kwargs.get('plot_fosa',True)

        plot_contourf=kwargs.get('plot_contourf',False)
        interfaz=kwargs.get('interfaz','upper')


        plot_datos=kwargs.get('plot_datos',False)
        which_datos=kwargs.get('which_datos','data')
        quiverkey=kwargs.get('quiverkey',False)
        plot_datos_and_inversion=kwargs.get('datos_and_inv',False)

        #show_vectores=kwargs.get('show_vectores',False)


        plt.figure()
        m = Basemap(**self.map_params)               
        m.drawcoastlines()
        m.drawcountries()
        #Se trazan los paralelos.
        m.drawparallels(
            np.arange(-90, 90, 5),
            labels=[1, 1, 1, 1])
        m.drawmeridians(
            np.arange(-90, 90, 5),
            labels=[0, 0, 0, 1])      

        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        x,y = m(lon,lat)
        m.plot(x,y,'go',markersize=10)

        if plot_fosa:
            self.plot_fosa(m)

        if plot_contourf:
            self.contour_maps(m,interfaz=interfaz)

        if plot_datos:
            #print(which_datos)
            self.choose_arrows(m,arrows=which_datos,quiverkey=quiverkey)

        if plot_datos_and_inversion:
                self.choose_arrows(m,arrows='data',quiverkey=quiverkey)
                self.choose_arrows(m,arrows='invertidas',quiverkey=quiverkey)


        
        if self.falla_AB:
            lon_fallas_ab=self.subfallas_model_dict.get('lon_vertices')
            lat_fallas_ab=self.subfallas_model_dict.get('lat_vertices')
            #print(lat_fallas_ab)
            #print('lon_ab',lon_fallas_ab)
            #print('lat_ab',lat_fallas_ab)
            for lonx,laty in zip(lon_fallas_ab,lat_fallas_ab):
                x,y=m(lonx,laty)
                m.plot(x,y,color='b',  linestyle = '-', linewidth = 0.15,zorder=21)

        # if self.falla_AB:
        #     lon_fallas_ab=self.planos_falla_obj.get('subfallas_methods')[3]
        #     lat_fallas_ab=self.planos_falla_obj.get('subfallas_methods')[4]
        #     #print(lat_fallas_ab)
        #     #print('lon_ab',lon_fallas_ab)
        #     #print('lat_ab',lat_fallas_ab)
        #     for lonx,laty in zip(lon_fallas_ab,lat_fallas_ab):
        #         x,y=m(lonx,laty)
        #         m.plot(x,y,color='b',  linestyle = '-', linewidth = 0.15,zorder=21)



       
        #Distribucion de subfallas para los planos de falla C y D
        if self.falla_CD:
            lon_fallas_cd=self.planos_falla_obj.get('CD')[0].vert_lon_new
            lat_fallas_cd=self.planos_falla_obj.get('CD')[0].vert_lat_new
            #print('lon_cd',lon_fallas_cd)
            #print('lat_cd',lat_fallas_cd)
            for lonx,laty in zip(lon_fallas_cd,lat_fallas_cd):
                x,y=m(lonx,laty)
                m.plot(x,y,color='r',  linestyle = '-', linewidth = 0.15,zorder=21)
           

        

 
            #Distribucion de subfallas para el plano de falla E     
        if self.falla_E:
            lon_fallas_e=self.planos_falla_obj.get('E')[0]
            lat_fallas_e=self.planos_falla_obj.get('E')[1]
            #print('lon_e',lon_fallas_e)
            #print('lat_e',lat_fallas_e)
            for lonx,laty in zip(lon_fallas_e,lat_fallas_e):
                x,y=m(lonx,laty)
                m.plot(x,y,color='r',  linestyle = '-', linewidth = 0.15,zorder=21)


      
        plt.show()
        if not interfaz:
            plt.savefig(self.figuras_output+'/geometria_modelo.png',format='png',dpi='figure')
        if interfaz:
            plt.savefig(self.figuras_output+'/geometria_slip_'+interfaz+'_.png',format='png',dpi='figure')

        
#####

    def choose_arrows(self,m,arrows='data',quiverkey=False):
        if arrows=='data':
            self.show_arrows_data(m,quiverkey)
        if arrows=='invertidas':
            self.show_arrows_invertidas(m,quiverkey)
        if arrows=='directas':
            self.show_arrows_directas(m,quiverkey)


    def show_arrows_data(self,m,quiverkey):
        """ this function plots te vector data of observations. The data is plotted
        in the m instance"""
        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        Ue_obs = [elem.get('UE') for elem in self.data_estaciones]
        Un_obs = [elem.get('UN') for elem in self.data_estaciones]
        x,y=m(lon,lat)
        V = m.quiver(
                         x,y,
                         Ue_obs,
                         Un_obs,
                         color='m',
                         scale=10,
                         width=0.0050,
                         linewidth=0.5,
                         headwidth=4.,
                         zorder=26,
                         label='V_obs')
        if quiverkey:

            plt.quiverkey(
                                 V,
                                 x1,
                                 y1,
                                 0.01,
                                 label='V_teo: 1cm/yr',
                                 labelpos='N',
                                 coordinates='data',
                                 labelcolor= (0,0,0),
                                 zorder=26, 
                                 fontproperties={'size': 8, 'weight': 'bold'})


    def show_arrows_invertidas(self,m,quiverkey):
        """ this function plots the vector data obtained in a inversion process. The data is plotted
            in the m instance """
        if self.proceso=='coseismic':
            arrows=self.outputs_inv
        if self.filtro=='coseismic':
            lon = [elem.get('Longitud') for elem in self.data_estaciones]
            lat = [elem.get('Latitud') for elem in self.data_estaciones]
            Ue_inv=self.outputs_inv.get('UE')
            #Ue_inv=[1000*x for x in Ue_inv]
            Un_inv=self.outputs_inv.get('UN')
            #Un_inv=[1000*x for x in Ue_inv]

        print('vel',Ue_inv,Un_inv)
        x,y=m(lon,lat)

        #m.plot(x,y,'go',markersize=1)
        
        W =  m.quiver(
                      x,y,
                      Ue_inv,
                      Un_inv,
                      color='k',
                      scale=10,
                      width=0.0050,
                      linewidth=0.5,
                      headwidth=4.,
                      zorder=26,
                      label='V_teo')
        if quiverkey:

            plt.quiverkey(
                          W,
                          x,
                          y,
                          0.01,
                          'V obs: 1 cm/yr',
                           labelpos='N',
                           coordinates='data',
                           labelcolor= (0,0,0),
                           zorder=26, 
                           fontproperties={'size': 8, 'weight': 'bold'}
                            )          


    def show_arrows_directas(self,m,quiverkey):
    ### this function plots the vector data obtained making a direct problem (synthetic data for given a
    ###        slip configuration). The data is plotted in the m instance 
        if self.proceso=='interseismic':
            arrows=None

        if self.proceso=='coseismic':
            lon = [elem.get('Longitud') for elem in self.data_estaciones]
            lat = [elem.get('Latitud') for elem in self.data_estaciones]
            Ue_teo=self.data_sintetica.get('UE')
            Un_teo=self.data_sintetica.get('UN')
            #print(Ue_teo)
            #print(Un_teo)
        x,y=m(lon,lat)
        W =  m.quiver(
                      x,y,
                      np.array(Ue_teo),
                      np.array(Un_teo),
                      color='g',
                      scale=1000,
                      width=0.0050,
                      linewidth=0.5,
                      headwidth=4.,
                      zorder=26,
                      label='V_teo')
        if quiverkey:

            plt.quiverkey(
                          W,
                          x,
                          y,
                          0.01,
                          'V obs: 1 cm/yr',
                          labelpos='N',
                          coordinates='data',
                          labelcolor= (0,0,0),
                          zorder=26, 
                          fontproperties={'size': 8, 'weight': 'bold'}
                           )      


  
    def construye_A(self):
        """ this function builds the A Green Function Matrix """
        #self.generar_subfallas()
        A = self.model_matrix_slab(falla='inversa',
                                   save_subfallas_methods = True,
                                   generar_subfallas = True,
                                   save_GF = True)

        
        print('A')
        #print(A[1,:])
        print('ASHAP')
        #print(np.shape(A[0,:]))
        self.planos_falla_obj.update({'A':A})

    def construye_B(self):
        """ this function builds the B Green Function Matrix """
        B = self.model_matrix_slab(falla='normal',
                                   save_subfallas_methods = True ,
                                   generar_subfallas = False,
                                   save_GF = True)
        self.planos_falla_obj.update({'B':B})
                  
                                   
    def construye_C(self):      
        """ this function builds the C Green Function Matrix """
        pass

    def construye_D(self):
        """ this function builds the D Green Function Matrix """

        pass

    def construye_CD(self):
        pass


    def construye_E(self):
        """ this function builds the E Greens Function Matrix """

        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        AB = self.planos_falla.get('AB')
        latf = AB.get('inferior')[0]
        lonf = AB.get('inferior')[1]
        lat1 = AB.get('superior')[0]
        lon1 = AB.get('superior')[1]
        E = self.data_falla.get('E')
        constructor_E_opt = {
            'EW': sub.constructor_fallaE_EW,
            'WE': sub.constructor_fallaE_WE}
        constructor_E = constructor_E_opt.get(
            self.sentido_subduccion, print)
        fosa_lon = [elem.get('Longitud')
                    for elem in self.data_falla.get('fosa')]
        fosa_lat = [elem.get('Latitud')
                    for elem in self.data_falla.get('fosa')]

        PF_E = self.planos_falla.get('E')
        rake_norm_ref_deg = self.planos_falla_obj.get('AB')[3]
        lon_vertices_E, lat_vertices_E, L_E, strike_E_rad = constructor_E(
            lonf,
            latf,
            lon1,
            lat1,
            self.v_placa,
            PF_E.get("W_E"),
            PF_E.get("H_E"),
            PF_E.get("nx_E"),
            PF_E.get("ny_E"),
            PF_E.get("delta_lat_E"),
            fosa_lon,
            fosa_lat
        )
        # componentes de la velocidad
        n_dim = 2
        E = sub.model_matrix_slab_E(
            n_dim,
            0,
            PF_E.get("H_E"),
            PF_E.get("W_E"),
            L_E,
            np.radians(rake_norm_ref_deg),
            strike_E_rad,
            PF_E.get("nx_E"),
            PF_E.get("ny_E"),
            lon_vertices_E,
            lon_vertices_E,
            lon,
            lat)
        self.planos_falla_obj.update({'E':
                                      (lon_vertices_E,
                                       lat_vertices_E, L_E,
                                       strike_E_rad, E)})
        return lon_vertices_E, lat_vertices_E, L_E, strike_E_rad


 
    # def calcula_inversion(self,**kwargs):
    #     interfases=kwargs.get('interfases':'double')
    #     self.inversion_intersismico_slab(interfases=interfases)


        #L = self.planos_falla_obj.get('AB')[-3]
        # print('L')
        # print(L)

        ## POR AHORA VER SOLO A Y B
        # print(' -----------------------------------------------------------------------')
        # print(' ETAPA  6.2 == Caracterizacion geometrica: C y D                        ')
        # print(' -----------------------------------------------------------------------')    
        # #C = self.planos_falla_obj.get('CD')[-2]
        #D = self.planos_falla_obj.get('CD')[-1]
#        print('Matriz C')
#        print(C)
#        print('Matriz D')
# #        print(D)
#         print(' -----------------------------------------------------------------------')
#         print(' ETAPA  6.3 == Caracterizacion geometrica: E                            ')
#         print(' -----------------------------------------------------------------------')       

#         #1. Caracterizacion geometrica para el plano de falla E
#         print('6.3.1 Calculando matriz E...')    
#         E = self.planos_falla_obj.get('E')[-1]
# #        print('Matriz E')
# #        print(E)

### ESTO SOLO PARA INTERSISMICO 
        # print(' -----------------------------------------------------------------------')
        # print(' ETAPA  7 == Proceso de inversion                                       ')
        # print(' -----------------------------------------------------------------------')

        # #Se establecen velocidades libres en los planos de falla C, D y E.
        # print('7.1 Incluyendo velocidad de placa     ...')
        # l_CD  = self.planos_falla_obj.get('CD')[-3]
        # dip_radianes_CD_final = self.planos_falla_obj.get('CD')[0].dip_slab_rad
        # num_subfallas_CD = len(dip_radianes_CD_final)
        # #Velocidades libres plano de falla C y D.
        # for subfalla in range(num_subfallas_CD):  
        #     if subfalla == 0:
        #       Vp_CD = self.v_placa
        #     else:
        #       Vp_CD = np.hstack((Vp_CD, self.v_placa)) 
        # E = self.planos_falla_obj.get('E')[-1]
        # PF_E = self.planos_falla.get('E')
        # for subfalla in range(PF_E.get("nx_E")*PF_E.get("ny_E")):
        #     if subfalla == 0:
        #       Vp_E = self.v_placa
        #     else:
        #       Vp_E = np.hstack((Vp_E, self.v_placa))  
        '''
        #############################################################################
        SE REALIZA INVERSION
        #############################################################################
        '''

    
    # def proj_mesh2okada(self,lat_falla,lon_falla,strike): 
    #     """ this function obtains the (xi,yi) coordinates in the okada reference frame for given data in the 
    #     geographic reference frame
    #     lat_falla : subfault latitude
    #     lon_falla : subfault longitude
    #     strike: strike of the subfault  """  
    #     # OUTPUT: coordenadas xi,yi proyectadas en los ejes de Okada
    #     #OBS FELIPE: La funcion determina las coordenadas okada (X Y) de un un punto
    #     #de observacion a partir de sus coordenadas geograficas.        
    #     lon_estaciones = [elem.get('Longitud') for elem in self.data_estaciones]
    #     lat_estaciones = [elem.get('Latitud') for elem in self.data_estaciones]
    #     name_estaciones=[elem.get('Station') for elem in self.data_estaciones]
    #     print('namens')
    #     print(self.data_estaciones)
    #     print('cod')
    #     print(self.fuente_estaciones)
    #     dist_lat=[]
    #     dist_lon=[]
    #     for (lat_sta,lon_sta) in zip(lat_estaciones,lon_estaciones):
    #         l,az,baz=vinc_dist(lat_falla,lon_falla,lat_sta,lon_sta)
    #         if lon_sta >= lon_falla 
    #         print('distancias,azimutes y backaz sin editar')
    #         print(l,az,baz)
    #         # if az > 300:
    #         #     print('problema con los azimuts... rescribiendo')
    #         #     az = abs(az-360)
    #         #     print(az)
            

    #         dist_lat.append(l*np.cos(np.radians(az)))
    #         dist_lon.append(l*np.sin(np.radians(az)))
    #     ## transformar a array
    #     dist_lat_arr=np.array(dist_lat)
    #     dist_lon_arr=np.array(dist_lon)
    #     # print(len(dist_lat_arr))
    #     # print(len(dist_lon_arr))
    #     # print(strike)
    #     xi = dist_lat_arr*np.cos(strike) + dist_lon_arr*np.sin(strike)  
    #     yi = dist_lat_arr*np.sin(strike) - dist_lon_arr*np.cos(strike)

    #     return xi,yi



    
       
  
    ################################################################################
    
    def proj_mesh2okada(self,lat_falla,lon_falla,strike): 
        """ this function obtains the (xi,yi) coordinates in the okada reference frame for given data in the 
        geographic reference frame
        lat_falla : subfault latitude
        lon_falla : subfault longitude
        strike: strike of the subfault  """  
        # OUTPUT: coordenadas xi,yi proyectadas en los ejes de Okada
        #OBS FELIPE: La funcion determina las coordenadas okada (X Y) de un un punto
        #de observacion a partir de sus coordenadas geograficas.        
        lon_estaciones = [elem.get('Longitud') for elem in self.data_estaciones]
        lat_estaciones = [elem.get('Latitud') for elem in self.data_estaciones]
        name_estaciones=[elem.get('Station') for elem in self.data_estaciones]
        #print(self.data_estaciones)
        #print('cod')
        #print(self.fuente_estaciones)
        dist_lat=[]
        dist_lon=[]
        az_m=[]
        l_m=[]
        baz_m=[]
        bandera= False
        for (lat_sta,lon_sta) in zip(lat_estaciones,lon_estaciones):
          
           l,az,baz=vinc_dist(lat_falla,lon_falla,lat_sta,lon_sta)
           
           az_m.append(az)
           l_m.append(l)
           baz_m.append(baz)
           # print('az')  
           # print(l,az,baz)
           if bandera:
               if az <= 180:
                    dist_lat.append(l*np.cos(np.radians(az)))
                    dist_lon.append(l*np.sin(np.radians(az)))
               if az > 180 :
                    dist_lat.append(l*np.cos(np.radians(360-az)))
                    dist_lon.append(l*np.sin(np.radians(360-az)))

               # if az > 355:
               #      print('aca')
               #      dist_lat.append(l*np.cos(np.radians(370-az)))
               #      dist_lon.append(l*np.sin(np.radians(370-az)))
               # if az > 90 and  az :
               #      dist_lat.append(l*np.cos(np.radians(360-az)))
               #      dist_lon.append(l*np.sin(np.radians(360-az)))

               # if az > 90 and az <= 180:
               #      dist_lat.append(l*np.cos(np.radians(90-az)))
               #      dist_lon.append(l*np.sin(np.radians(90-az))) 

               # if az > 180 and az <= 270:
               #      dist_lat.append(l*np.cos(np.radians(290-az)))
               #      dist_lon.append(l*np.sin(np.radians(290-az)))
               
               # if az > 270:
               #      dist_lat.append(l*np.cos(np.radians(380-az)))
               #      dist_lon.append(l*np.sin(np.radians(380-az))) 
       
           if not bandera:
               dist_lat.append(l*np.cos(np.radians(az)))
               dist_lon.append(l*np.sin(np.radians(az)))


       
        dist_lat_arr=np.array(dist_lat)
        dist_lon_arr=np.array(dist_lon)         
        xi = dist_lat_arr*np.cos(strike) + dist_lon_arr*np.sin(strike)
        yi = dist_lat_arr*np.sin(strike) - dist_lon_arr*np.cos(strike)

        #yi = -dist_lat_arr*np.sin(strike) + dist_lon_arr*np.cos(strike)
        #yi = dist_lat_arr*np.sin(strike) - dist_lon_arr*np.cos(strike)
        
        return xi,yi
   

    def subfalla(self,dip,strike):
        """ we get the central coordinates of the subfault assumming a rectangular fault """

        ### loading AB planes data
        AB   = self.planos_falla.get('AB')
        nx   = self.grid_size[0]
        ny   = self.grid_size[1]

        latf = AB.get('inferior')[0]
        lonf = AB.get('inferior')[1]
        lat1 = AB.get('superior')[0]
        lon1 = AB.get('superior')[1]

        alfa = np.degrees(strike)
        beta = alfa  + 270.0
        
        dx = self.L/nx
        ## projected length of the element of line along dip
        dy=self.ancho_placa*np.cos(dip)/ny
        #dy = self.ancho_placa*np.cos(dip)/ny
        
        ## we get the coordinates of the deeper point of the subfault
        lat_prima,lon_prima,alpha21=vinc_pt(latf,
                                            lonf,
                                            np.degrees(strike+np.pi/2),
                                            self.ancho_placa*np.cos(self.dip_plano_inicial)                                            
                                            )
        # lat_prima,lon_prima,alpha21=vinc_pt(latf,
        #                                     lonf,
        #                                     np.degrees(strike+np.pi/2),
        #                                     self.ancho_placa*np.cos(self.dip_plano_inicial)
        #                                     )
        ## we move the vertex to the midpoint of the subfault along strike
        lat_step1,lon_step1,alpha21 = vinc_pt(lat_prima,lon_prima,alfa, dx/2)

        ## we move the vertex to the midpoint of the subfault along dip. Now the 
        ## point is at the midpoint of the subfault. the azimut is beta = strike + 270 
        ## because we start meshing at the bottom-south of the subfault.
        lat_c,lon_c,alpha21 = vinc_pt(lat_step1,lon_step1,beta,dy/2)

       
        xs=[]
        ys=[]
        for j in range(ny):
            for i in range(nx):
                lat_step1 = lat_c
                lon_step1 = lon_c
                ## if i > 0, we move the point along strike
                if i > 0:
                    lat_step1,  lon_step1, alpha21 = vinc_pt( lat_c, lon_c, alfa , i*dx )
                lat_save = lat_step1
                lon_save = lon_step1
                ## if j > 0, we move the point along dip
                if j > 0 :
                    lat_save , lon_save, alpha21 = vinc_pt( lat_step1, lon_step1,  beta , j*dy )
                xs.append(lat_save)
                ys.append(lon_save)
        
        lon_slip =np.array([ys])[0]
        lat_slip=np.array([xs])[0]
       
         ## the outputs are the points where we calculate the slip of the subfault.
        
        return lon_slip,lat_slip







    def coordenadas_subfallas(self):
        """ this function gets the subfault coordinates but now we consider a geometry
        that varies with the trench """
        AB   = self.planos_falla.get('AB')
        latf = AB.get('inferior')[0]
        lonf = AB.get('inferior')[1]
        lat1 = AB.get('superior')[0]
        lon1 = AB.get('superior')[1]
        print('larrss',lonf,latf,lon1,lat1)
        fosa_lon = [elem.get('Longitud') for elem in self.data_falla.get('fosa')]
        fosa_lat = [elem.get('Latitud') for elem in self.data_falla.get('fosa')] 
        nx=self.grid_size[0]
        ny=self.grid_size[1]
        strike_rad = np.radians(self.strike_aux_deg)
        ## we build the rake of reference for normal faults with the reference rake for
        ## thrust fault
        rake_norm_ref_deg=self.rake_referencia + 180
        # ## we get the coordinates of the bottom-south of the system
        # lat_dep,lon_dep,alpha21 = vinc_pt(latf,
        #                                   lonf,
        #                                   np.degrees(strike_rad+np.pi/2),
        #                                   self.ancho_placa*np.cos
        #                                   )
        
        # lat_dep,lon_dep,alpha21 = vinc_pt(latf,
        #                                   lonf,
        #                                   np.degrees(strike_rad+np.pi/2),
        #                                   self.ancho_placa*np.cos(self.dip_plano_inicial)
        #                                   )
        
       
       
        ## we get the coordinates of the subfaults assuing the same config. for every fault
        lon_cent_pre,lat_cent_pre= self.subfalla(self.dip_plano_inicial,strike_rad)
        
        lon_cent_pre2 = []
        lat_cent_pre2 = []
        
        k = 0
        for i_nx in range(nx):
            for j_ny in range(ny):
                lon_cent_pre2.append(lon_cent_pre[k])
                lat_cent_pre2.append(lat_cent_pre[k])       
                k = k+1
        
        dx = self.L/nx
        dy = self.ancho_placa*np.cos(self.dip_plano_inicial)/ny

        #dy = self.ancho_placa*np.cos(self.dip_plano_inicial)/ny

        ## we will get the points of every subfault hence nx*ny
        FIL=nx*ny
        ## we will consider 5 vertex per fault, the last vertex is the same than the first
        COL= 5


        vertices_lon=[]
        vertices_lat=[]
        ## number of rows of the matrix. COL is the number of colums of the matrix.
        ## for example if we have 90 subfaults the size of the matrix will be (90,5)
        for i in range(FIL):               
            vertices_lon.append([0]*COL)    
            vertices_lat.append([0]*COL)
    
        ## we set up the key vertexes. These are the northern and souther vertexes
        ## along the trench. We need them for searching reference values with slab1.0 model
        vertice_clave_lon_inf=[]
        vertice_clave_lat_inf=[]
        vertice_clave_lon_sup=[]
        vertice_clave_lat_sup=[]
    
        ## the size of this array is the number of faults along strike
        for i in range(nx):                       
            vertice_clave_lon_inf.append([0]*1)    
            vertice_clave_lat_inf.append([0]*1)
            vertice_clave_lon_sup.append([0]*1)    
            vertice_clave_lat_sup.append([0]*1)



      
        k=0
        for i_ny in range(ny):
            fila=0
            for j_nx in range(nx):
                frac = Fraction((ny-i_ny),ny)
                # latitude and longitude of the first vertex  (south-east vertex)
                lat1_step1,lon1_step1,a= vinc_pt(lat_cent_pre2[k],
                                                 lon_cent_pre2[k],
                                                 np.degrees(strike_rad)+90,
                                                 dy/2
                                                 )
                
                lat1_vert,lon1_vert,a = vinc_pt(lat1_step1,
                                                lon1_step1,
                                                np.degrees(strike_rad)+180,
                                                dx/2
                                                )
                
                # latitude and longitude of the second vertex (north-east vertex)
                lat2_step1,lon2_step1,a = vinc_pt(lat_cent_pre2[k],
                                                 lon_cent_pre2[k],
                                                 np.degrees(strike_rad)+90,
                                                 dy/2
                                                 )
                
                lat2_vert,lon2_vert,a = vinc_pt(lat2_step1,
                                                lon2_step1,
                                                np.degrees(strike_rad),
                                                dx/2
                                                )

                # latitude and longitude of the third vertex (north-west vertex)

                lat3_vert,lon3_vert,a = vinc_pt(lat2_vert,
                                                lon2_vert,
                                                np.degrees(strike_rad) + 270,
                                                frac*self.ancho_placa*np.cos(self.dip_plano_inicial)
                                                )

                # lat3_vert,lon3_vert,a = vinc_pt(lat2_vert,
                #                                 lon2_vert,
                #                                 np.degrees(strike_rad) + 270,
                #                                 frac*self.ancho_placa*np.cos(self.dip_plano_inicial)
                #                                 )

                # latitude and longitude of the fourth vertex (south-west vertex)
                lat4_vert,lon4_vert,a = vinc_pt(lat3_vert,
                                                lon3_vert,
                                                np.degrees(strike_rad) + 180,
                                                dx
                                                )
                # latitude and longitud of the fifth vertex (sane first vertex)
                lat5_vert = lat1_vert
                lon5_vert = lon1_vert
       
                ## if we are in the last row (with the trench)
                if (i_ny == ny-1):
                    vertice_clave_lon_inf[j_nx][0]=lon4_vert
                    vertice_clave_lat_inf[j_nx][0]=lat4_vert
                    vertice_clave_lon_sup[j_nx][0]=lon3_vert
                    vertice_clave_lat_sup[j_nx][0]=lat3_vert
                k = k+1
        fila = fila+1


        ## with the key vertexes we obtain the distance of them to the rest of the trench (slab 1.0)
        desplazo=[]
        ## we loop over the subfault and we search the maximum longitude projected to the trench
        for i_nx in range(nx):
            minlon = vertice_clave_lon_inf[i_nx][0]
        ## we loop over the trench points
        ## DUDA EN ESTA PARTE
            for j in range(len(fosa_lat)):
                if (fosa_lat[j] >= vertice_clave_lat_inf[i_nx][0] + self.delta_lat and
                    fosa_lat[j] <= vertice_clave_lat_sup[i_nx][0] + self.delta_lat):
                    if fosa_lon[j] < minlon:
                        minlon=fosa_lon[j]
                    # else:
                    #     print("algo inesperado a ocurrido")
            #Se calcula distancia a desplazar para todos los puntos centrales.
            #Se esta probando delta LAT, TEST
            dl, aux, aux = vinc_dist(vertice_clave_lat_inf[i_nx][0],
                                     vertice_clave_lon_inf[i_nx][0],
                                     vertice_clave_lat_inf[i_nx][0],
                                     minlon
                                    )

            desplazo.append(dl)

        lon_central_final=[]
        lat_central_final=[]

        k = 0
        for i_ny in range(ny):
            ll = 0
            for i_nx in range(nx):
                dist = desplazo[ll]
                lat_central, lon_central, alpha21 = vinc_pt(lat_cent_pre[k],
                                                            lon_cent_pre[k],
                                                            np.degrees(strike_rad)+270,
                                                            dist)
                lat_central_final.append(lat_central)
                lon_central_final.append(lon_central)
                k=k+1
                ll+=1

        ## se reescriben los verices desde una distribucion rectangular a una
        ## adaptada al slab

        k = 0
        for i_ny in range(ny):
            fila = 0
            for j_nx in range(nx):
                frac_W = Fraction(self.ancho_placa,ny)
                 # Latitude and longitude of the first vertex
                lat1_step1,lon1_step1,a1 = vinc_pt(lat_central_final[k],
                                                   lon_central_final[k],
                                                   np.degrees(strike_rad)+90,
                                                   dy/2
                                                   )
                
                lat1_vert,lon1_vert,a1 = vinc_pt(lat1_step1,
                                                 lon1_step1,
                                                 np.degrees(strike_rad)+180,
                                                 dx/2
                                                 )
                
                # Latitude and longitude of the second vertex
                lat2_step1,lon2_step1,a1 = vinc_pt(lat_central_final[k],
                                                   lon_central_final[k],
                                                   np.degrees(strike_rad)+90,
                                                   dy/2
                                                    )
                
                lat2_vert,lon2_vert,a1 = vinc_pt(lat2_step1,
                                                 lon2_step1,
                                                 np.degrees(strike_rad),
                                                 dx/2
                                                 )

                # Latitude and longitude of the third vertex
                lat3_vert,lon3_vert,a1 = vinc_pt(lat2_vert,
                                                 lon2_vert,
                                                 np.degrees(strike_rad) + 270,
                                                 frac_W*np.cos(self.dip_plano_inicial)
                                                 )

                # lat3_vert,lon3_vert,a1 = vinc_pt(lat2_vert,
                #                                  lon2_vert,
                #                                  np.degrees(strike_rad) + 270,
                #                                  frac_W*self.ancho_placa*np.cos(self.dip_plano_inicial)
                #                                  )

                # latitude and longitude of the fourth vertex
                lat4_vert,lon4_vert,a1 = vinc_pt(lat3_vert,
                                                 lon3_vert,
                                                 np.degrees(strike_rad) + 180,
                                                 dx
                                                 )

                # quinto vertice
                lat5_vert = lat1_vert
                lon5_vert = lon1_vert

                vertices_lon[k][0]=lon1_vert
                vertices_lon[k][1]=lon2_vert
                vertices_lon[k][2]=lon3_vert
                vertices_lon[k][3]=lon4_vert
                vertices_lon[k][4]=lon5_vert

                vertices_lat[k][0]=lat1_vert
                vertices_lat[k][1]=lat2_vert
                vertices_lat[k][2]=lat3_vert
                vertices_lat[k][3]=lat4_vert
                vertices_lat[k][4]=lat5_vert

                k = k + 1
            fila = fila + 1

        return lon_central_final,lat_central_final,vertices_lon,vertices_lat
    #return lon_central_final,lat_central_final,vertices_lon,vertices_lat
    
    def generar_subfallas(self):
        print('generar_subfallas')
        pass
        """"we generate the subfaults and call subfallas_Slabmodel class which updates 
            our information about the geomtry of the problem  """
    

    # #   load AB subfault data

    #     AB=self.planos_falla.get('AB')
    #     latf = AB.get('inferior')[0]
    #     lonf = AB.get('inferior')[1]
    #     lat1 = AB.get('superior')[0]
    #     lon1 = AB.get('superior')[1]
    #     ## cargar data de las estaciones
    #     lon = [elem.get('Longitud') for elem in self.data_estaciones]
    #     lat = [elem.get('Latitud') for elem in self.data_estaciones]
       
    #     strike_rad = None
    #     if self.sentido_subduccion == 'EW':
    #         strike_rad = np.radians(self.strike_aux_deg+180)
    #     if self.sentido_subduccion == 'WE':
    #         strike_rad = np.radians(self.strike_aux_deg)
    #     #rake_norm_ref_deg = self.rake_referencia+180      
    #     nx = self.grid_size[0]
    #     ny = self.grid_size[1]
    #     lon_central_final,lat_central_final,vertices_lon,vertices_lat=self.coordenadas_subfallas()
    #     ## we call the subfallas_Slabmodel class
    #     subfallas = sub.subfallas_SlabModel(
    #                                         nx,
    #                                         ny,
    #                                         self.ancho_placa,
    #                                         self.L,
    #                                         strike_rad,
    #                                         vertices_lon,
    #                                         vertices_lat,
    #                                         self.file_profundidad,
    #                                         self.file_dip,
    #                                         self.file_strike,
    #                                         latf,
    #                                         lat1)
    #     ## we update the values of this dict for using it later in other functions
    #     self.planos_falla_obj.update({'subfallas':(subfallas,
    #                                                self.L,
    #                                                nx,
    #                                                ny,
    #                                                strike_rad,
    #                                                vertices_lon,
    #                                                vertices_lat)
    #                                                })


    def model_matrix_slab(self,falla='inversa',save_subfallas_methods=True, generar_subfallas = True,save_GF=True):
        """ we generate the subfaults if its needed (it is not needed, for example, if we are building B after A.
         the geometry is the same, we only change the depth and the rake """
        if generar_subfallas:
            self.subfallas_slabmodel()
        
        ## we load the data generated with the prior function

        #load_slab    = self.subfallas_slabmodel
        lon_cent=self.subfallas_model_dict.get('lon_central')
        lat_cent=self.subfallas_model_dict.get('lat_central')
        lon_vert=self.subfallas_model_dict.get('lon_vertices')
        lat_vert=self.subfallas_model_dict.get('lat_vertices')
        dip_model=self.subfallas_model_dict.get('dip_model')
        strike_model=self.subfallas_model_dict.get('strike_model')
        depth_model=self.subfallas_model_dict.get('depth_model')


        # subfallas    = load_slab[0]
        # L            = load_slab[1]
        nx           = self.grid_size[0]
        ny           = self.grid_size[1]
        # strike_rad   = load_slab[4]
        # vertices_lon = load_slab[5]
        # vertices_lat = load_slab[6]

        ### ALL THE DATA GENERATED BELOW IS USING SLAB 1.0 MODEL
        ## profundidad de subfallas correspondientes a los planos de falla A y B
        ## depth of the fault plane 
        profundidad_subfallas_inversa = self.subfallas_model_dict.get('depth_model')
        # print('prof')
        # print(profundidad_subfallas_inversa)

        ## dip of the fault plane (will work for both planes)
        dip_subfallas_radianes  = np.radians(self.subfallas_model_dict.get('dip_model'))
        # print('dip')
        # print(dip_subfallas_radianes)

        ## strike of the fault plane (will work for both planes)
        strike_subfallas_deg = self.subfallas_model_dict.get('strike_model')
        # print('strike')
        # print(strike_subfallas_deg)

        ## longitude of the vertex of fault plane (will work for both)
        lon_vertices_subfallas_slab = self.subfallas_model_dict.get('lon_vertices')
        # print('lon vert')
        # print(lon_vertices_subfallas_slab)

        ## latitude of the vertex of fault plane (will work for both)
        lat_vertices_subfallas_slab = self.subfallas_model_dict.get('lat_vertices')
        # print('lat vert')
        # print(lat_vertices_subfallas_slab)

        ## central longitude of the subfaults  of the fault plane (will work for both)
        lon_central_subfallas_slab = self.subfallas_model_dict.get('lon_cent')
        # print('lon central')
        # print(lon_central_subfallas_slab)

        ## central latitude of the subfaults  of the fault plane (will work for both)

        lat_central_subfallas_slab = self.subfallas_model_dict.get('lat_cent')
        # print('lat_central')
        # print(lat_central_subfallas_slab)


        ## a strike of reference    
        strike_rad=self.subfallas_model_dict.get('strike_rad')

        #we get the depth of the normal fault using a constant thickness of the slab 
        profundidad_subfallas_normal = profundidad_subfallas_inversa + \
                                       (self.plate_thickness/1000 * np.cos(dip_subfallas_radianes))

        phi_placa_rad = self.subfallas_model_dict.get('phi_placa_rad')


        ## rake for inverse fault
        rake_inv_rad  = self.subfallas_model_dict.get('rake_inv_rad')
        ## rake for normal fault
        rake_norm_rad = self.subfallas_model_dict.get('rake_norm_rad')

       
        print('profundidades')
        print(profundidad_subfallas_normal,profundidad_subfallas_inversa,dip_subfallas_radianes)

        if falla == 'inversa':
            rake_usado = rake_inv_rad
            prof_usada = profundidad_subfallas_inversa
        if falla == 'normal':
            rake_usado = rake_norm_rad
            prof_usada = profundidad_subfallas_normal
        
        ndim=self.n_dim
        l = self.L/nx
        
        subfalla=0
        for j_ny in range(ny):
            for i_nx in range(nx):
                ## in this case we use real width of the fault since we are using
                ## green functions
                w=self.ancho_placa/ny

                if i_nx == 0  and j_ny == 0:
                    # print(lat_vertices_subfallas_slab[subfalla][0])
                    ddx,ddy= self.proj_mesh2okada(
                                                lat_vertices_subfallas_slab[subfalla][0],
                                                lon_vertices_subfallas_slab[subfalla][0],
                                                np.radians(strike_subfallas_deg[subfalla])

                                                )                      
                    ve,vn,vz = desplaz_okada(
                                             ddx,
                                             ddy,
                                             dip_subfallas_radianes[subfalla],
                                             1000*prof_usada[subfalla],
                                             w,
                                             l,
                                             rake_usado[subfalla],
                                             np.radians(strike_subfallas_deg[subfalla])
                                             )
                                            
                    if ndim == 1:                
                       M = ve
                    elif ndim == 2:
                       M = np.hstack((ve,vn))
                    elif ndim == 3:
                       M = np.hstack((ve,vn,vz))

                else:
                    
                    print('subfalla',subfalla)
                    ddx,ddy = self.proj_mesh2okada(lat_vertices_subfallas_slab[subfalla][0],
                                                   lon_vertices_subfallas_slab[subfalla][0],
                                                   np.radians(strike_subfallas_deg[subfalla])
                                              )

                    ve,vn,vz = desplaz_okada(ddx,
                                             ddy,
                                             dip_subfallas_radianes[subfalla],
                                             1000*prof_usada[subfalla],
                                             w,
                                             l,
                                             rake_usado[subfalla],
                                             np.radians(strike_subfallas_deg[subfalla])
                                             )
                    if self.n_dim == 1 :                
                        tempstack= ve
                    elif self.n_dim == 2 :
                        tempstack = np.hstack((ve,vn))
                    elif self.n_dim == 3 :
                        tempstack=  np.hstack((ve,vn,vz))
                    M = np.vstack((M,tempstack))
                    print(np.shape(ve),np.shape(vn),np.shape(vz))
                    print(np.shape(M))

                subfalla=subfalla+1
        print('M')
        print(M)
        A= M.T
        
        if save_subfallas_methods:
            self.planos_falla_obj.update({'subfallas_methods':(profundidad_subfallas_inversa,
                                                          dip_subfallas_radianes,
                                                          strike_subfallas_deg,
                                                          lon_vertices_subfallas_slab,
                                                          lat_vertices_subfallas_slab,
                                                          lon_central_subfallas_slab,
                                                          lat_central_subfallas_slab,
                                                          profundidad_subfallas_normal,
                                                          phi_placa_rad,
                                                          rake_inv_rad,
                                                          rake_norm_rad)
                                                                        }) 
        if save_GF:
            print('A',A)
            print(np.shape(A))
            #print(np.shape(A[:,0]))
            stations=[elem.get('Station') for elem in self.data_estaciones]
            A_L=A.tolist()
            #G_sta=[]
            first_lon_vert=["{0:.4f}".format(x[0]) for x in lon_vertices_subfallas_slab ]
            first_lat_vert=["{0:.4f}".format(x[0]) for x in lat_vertices_subfallas_slab ]
            print('AL',A_L)
            nsta=int(len(A_L)/3)
            if nx*ny > 1:
                for i in range(nsta):
                    if nx*ny > 1:    
                        GE=["{0:.8f}".format(x) for x in A_L[i]]
                        GN=["{0:.8f}".format(x) for x in A_L[i+nsta]]
                        GZ=["{0:.8f}".format(x) for x in A_L[i+2*nsta]]
                    
                    # GN=A_L[k+1]
                    # GZ=A_L[k+2]
                    #print('aaa')
                    #print(GE,GN,GZ)
                    #print(lon_vertices_subfallas_slab)
                    #print(lat_vertices_subfallas_slab)
                    #print(GE,GN,GZ)
                    gf_output='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/gf/'

                    with open(gf_output+'GF_'+falla[:3].upper()+'_'+stations[i]+'.txt','w') as csvfile:
                        writer=csv.writer(csvfile,delimiter=';')
                        writer.writerow(['lon_subfalla','lat_subfalla','GE','GN','GZ'])
                        for row in zip(first_lon_vert,
                                       first_lat_vert,
                                       GE,
                                       GN,
                                       GZ):
                            writer.writerow(row)

           
            ## special case needed when 1 subfault for not failing
            elif nx*ny == 1:
                        GE=["{0:.8f}".format(x) for x in A_L[:nsta]]
                        GN=["{0:.8f}".format(x) for x in A_L[nsta:2*nsta]]
                        GZ=["{0:.8f}".format(x) for x in A_L[2*nsta:3*nsta]]
                        print('GE',GE,GN,GZ)
                        gf_output='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/gf/'
                        for i in range(nsta):
                            with open(gf_output+'GF_'+falla[:3].upper()+'_'+stations[i]+'.txt','w') as csvfile:
                                writer=csv.writer(csvfile,delimiter=';')
                                writer.writerow(['lon_subfalla','lat_subfalla','GE','GN','GZ'])
                                writer.writerow([float(first_lon_vert[0]),float(first_lat_vert[0]),GE[i],GN[i],GZ[i]])
                                
           
        print('A',A,np.shape(A))
        return A


    def model_matrix_slab_dividido(self,falla='inversa',save_subfallas_methods=True, generar_subfallas = True,save_GF=True):
        """ we generate the subfaults if its needed (it is not needed, for example, if we are building B after A.
         the geometry is the same, we only change the depth and the rake """
        if generar_subfallas:
            self.subfallas_slabmodel()
        
        ## we load the data generated with the prior function

        lon_cent=self.subfallas_model_dict.get('lon_central')
        lat_cent=self.subfallas_model_dict.get('lat_central')
        lon_vert=self.subfallas_model_dict.get('lon_vertices')
        lat_vert=self.subfallas_model_dict.get('lat_vertices')
        dip_model=self.subfallas_model_dict.get('dip_model')
        strike_model=self.subfallas_model_dict.get('strike_model')
        depth_model=self.subfallas_model_dict.get('depth_model')


       
        nx           = self.grid_size[0]
        ny           = self.grid_size[1]
       

        ### ALL THE DATA GENERATED BELOW IS USING SLAB 1.0 MODEL
        ## profundidad de subfallas correspondientes a los planos de falla A y B
        ## depth of the fault plane 
        profundidad_subfallas_inversa = self.subfallas_model_dict.get('depth_model')
        # print('prof')
        # print(profundidad_subfallas_inversa)

        ## dip of the fault plane (will work for both planes)
        dip_subfallas_radianes  = np.radians(self.subfallas_model_dict.get('dip_model'))
        # print('dip')
        # print(dip_subfallas_radianes)

        ## strike of the fault plane (will work for both planes)
        strike_subfallas_deg = self.subfallas_model_dict.get('strike_model')
        # print('strike')
        # print(strike_subfallas_deg)

        ## longitude of the vertex of fault plane (will work for both)
        lon_vertices_subfallas_slab = self.subfallas_model_dict.get('lon_vertices')
        # print('lon vert')
        # print(lon_vertices_subfallas_slab)

        ## latitude of the vertex of fault plane (will work for both)
        lat_vertices_subfallas_slab = self.subfallas_model_dict.get('lat_vertices')
        # print('lat vert')
        # print(lat_vertices_subfallas_slab)

        ## central longitude of the subfaults  of the fault plane (will work for both)
        lon_central_subfallas_slab = self.subfallas_model_dict.get('lon_cent')
        # print('lon central')
        # print(lon_central_subfallas_slab)

        ## central latitude of the subfaults  of the fault plane (will work for both)

        lat_central_subfallas_slab = self.subfallas_model_dict.get('lat_cent')
        # print('lat_central')
        # print(lat_central_subfallas_slab)


        ## a strike of reference    
        strike_rad=self.subfallas_model_dict.get('strike_rad')

        #we get the depth of the normal fault using a constant thickness of the slab 
        profundidad_subfallas_normal = profundidad_subfallas_inversa + \
                                       (self.plate_thickness/1000 * np.cos(dip_subfallas_radianes))

        phi_placa_rad = self.subfallas_model_dict.get('phi_placa_rad')


        ## rake for inverse fault
        rake_inv_rad  = self.subfallas_model_dict.get('rake_inv_rad')
        ## rake for normal fault
        rake_norm_rad = self.subfallas_model_dict.get('rake_norm_rad')

       
        #print('profundidades')
        #print(profundidad_subfallas_normal,profundidad_subfallas_inversa,dip_subfallas_radianes)

        if falla == 'inversa':
            rake_usado = rake_inv_rad
            prof_usada = profundidad_subfallas_inversa
        if falla == 'normal':
            rake_usado = rake_norm_rad
            prof_usada = profundidad_subfallas_normal
        
        ndim=self.n_dim
        l = self.L/nx
        
        subfalla=0
        for j_ny in range(ny):
            for i_nx in range(nx):
                ## in this case we use real width of the fault since we are using
                ## green functions
                w=self.ancho_placa/ny

                if i_nx == 0  and j_ny == 0:
                    # print(lat_vertices_subfallas_slab[subfalla][0])
                    ddx,ddy= self.proj_mesh2okada(
                                                lat_vertices_subfallas_slab[subfalla][0],
                                                lon_vertices_subfallas_slab[subfalla][0],
                                                np.radians(strike_subfallas_deg[subfalla])
                                                  )

                 ## inicializar la matriz G_strike_slip                                                     
                    gess,gnss,gzss = desplaz_okada_dividido(
                                             ddx,
                                             ddy,
                                             dip_subfallas_radianes[subfalla],
                                             1000*prof_usada[subfalla],
                                             w,
                                             l,
                                             np.radians(strike_subfallas_deg[subfalla]),
                                             mov='strike_slip')
                                            
                    if ndim == 1:                
                       Gss = gess
                    elif ndim == 2:
                       Gss = np.hstack((gess,gnss))
                    elif ndim == 3:
                       Gss = np.hstack((gess,gnss,gzss))
                ## inicializar la matriz G_dip_slip                                                     
                    geds,gnds,gzds = desplaz_okada_dividido(
                                             ddx,
                                             ddy,
                                             dip_subfallas_radianes[subfalla],
                                             1000*prof_usada[subfalla],
                                             w,
                                             l,
                                             np.radians(strike_subfallas_deg[subfalla]),
                                             mov='dip_slip')
                                            
                    if ndim == 1:                
                       Gds = geds
                    elif ndim == 2:
                       Gds = np.hstack((geds,gnds))
                    elif ndim == 3:
                       Gds = np.hstack((geds,gnds,gzds))


                else:
                    ## rellenar la matriz G_strike_slip
                    print('subfalla',subfalla)
                    ddx,ddy = self.proj_mesh2okada(lat_vertices_subfallas_slab[subfalla][0],
                                                   lon_vertices_subfallas_slab[subfalla][0],
                                                   np.radians(strike_subfallas_deg[subfalla])
                                              )

                    gess,gnss,gzss = desplaz_okada_dividido(ddx,
                                             ddy,
                                             dip_subfallas_radianes[subfalla],
                                             1000*prof_usada[subfalla],
                                             w,
                                             l,
                                             np.radians(strike_subfallas_deg[subfalla]),
                                             mov='strike_slip')
                    if self.n_dim == 1 :                
                        tempstack= gess
                    elif self.n_dim == 2 :
                        tempstack = np.hstack((gess,gnss))
                    elif self.n_dim == 3 :
                        tempstack=  np.hstack((gess,gnss,gzss))

                    Gss = np.vstack((Gss,tempstack))

                ## rellenar la matriz G_dip_slip
                    print('subfalla',subfalla)
                    ddx,ddy = self.proj_mesh2okada(lat_vertices_subfallas_slab[subfalla][0],
                                                   lon_vertices_subfallas_slab[subfalla][0],
                                                   np.radians(strike_subfallas_deg[subfalla])
                                              )

                    geds,gnds,gzds = desplaz_okada_dividido(ddx,
                                             ddy,
                                             dip_subfallas_radianes[subfalla],
                                             1000*prof_usada[subfalla],
                                             w,
                                             l,
                                             np.radians(strike_subfallas_deg[subfalla]),
                                             mov='dip_slip')
                    if self.n_dim == 1 :                
                        tempstack= geds
                    elif self.n_dim == 2 :
                        tempstack = np.hstack((geds,gnds))
                    elif self.n_dim == 3 :
                        tempstack=  np.hstack((geds,gnds,gzds))

                    Gds = np.vstack((Gds,tempstack))

                subfalla=subfalla+1
        print('Gss','Gds')
        print(Gss,Gds)
        Gss_T= Gss.T
        Gds_T= Gds.T
        
        if save_subfallas_methods:
            self.planos_falla_obj.update({'subfallas_methods':(profundidad_subfallas_inversa,
                                                          dip_subfallas_radianes,
                                                          strike_subfallas_deg,
                                                          lon_vertices_subfallas_slab,
                                                          lat_vertices_subfallas_slab,
                                                          lon_central_subfallas_slab,
                                                          lat_central_subfallas_slab,
                                                          profundidad_subfallas_normal,
                                                          phi_placa_rad,
                                                          rake_inv_rad,
                                                          rake_norm_rad)
                                                                        }) 
        if save_GF:
            # print('A',A)
            # print(np.shape(A))
            # print(np.shape(A[:,0]))
            stations=[elem.get('Station') for elem in self.data_estaciones]
            Gss_L=Gss_T.tolist()
            Gds_L=Gds_T.tolist()
            print('pancreas')
            print(Gss_L)
            #G_sta=[]
            k=0
            first_lon_vert=["{0:.4f}".format(x[0]) for x in lon_vertices_subfallas_slab ]
            first_lat_vert=["{0:.4f}".format(x[0]) for x in lat_vertices_subfallas_slab ]


            nsta=int(len(Gss_L)/3)
            if nx*ny > 1 :

                for i in range(nsta):        
                    GESS=["{0:.8f}".format(x) for x in Gss_L[i]]
                    GNSS=["{0:.8f}".format(x) for x in Gss_L[i+nsta]]
                    GZSS=["{0:.8f}".format(x) for x in Gss_L[i+2*nsta]]

                    GEDS=["{0:.8f}".format(x) for x in Gds_L[i]]
                    GNDS=["{0:.8f}".format(x) for x in Gds_L[i+nsta]]
                    GZDS=["{0:.8f}".format(x) for x in Gds_L[i+2*nsta]]
                    

                    
                    #print('aaa')
                    #print(GESS,GNSS,GZSS)
                    #print(lon_vertices_subfallas_slab)
                    #print(lat_vertices_subfallas_slab)
                    gf_output='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/gf/'
                    with open(gf_output+'GF_'+falla[:3].upper()+'_'+stations[i]+'.txt','w') as csvfile:
                        writer=csv.writer(csvfile,delimiter=';')
                        writer.writerow(['lon_subfalla','lat_subfalla','GESS','GNSS','GZSS','GEDS','GNDS','GZDS'])
                        for row in zip(first_lon_vert,
                                       first_lat_vert,
                                       GESS,
                                       GNSS,
                                       GZSS,
                                       GEDS,
                                       GNDS,
                                       GZDS):
                            writer.writerow(row)
                              
            elif nx*ny ==1:
                GESS=["{0:.8f}".format(x) for x in Gss_L[:nsta]]
                GNSS=["{0:.8f}".format(x) for x in Gss_L[nsta:2*nsta]]
                GZSS=["{0:.8f}".format(x) for x in Gss_L[2*nsta:3*nsta]]

                GEDS=["{0:.8f}".format(x) for x in Gds_L[:nsta]]
                GNDS=["{0:.8f}".format(x) for x in Gds_L[nsta:2*nsta]]
                GZDS=["{0:.8f}".format(x) for x in Gds_L[2*nsta:3*nsta]]  
                gf_output='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/gf/'
                for i in range(nsta):
                    with open(gf_output+'GF_'+falla[:3].upper()+'_'+stations[i]+'.txt','w') as csvfile:
                        writer = csv.writer(csvfile,delimiter=';')
                        writer.writerow(['lon_subfalla','lat_subfalla','GESS','GNSS','GZSS','GEDS','GNDS','GZDS'])
                        writer.writerow([float(first_lon_vert[0]),float(first_lat_vert[0]),GESS[i],GNSS[i],GZSS[i],GEDS[i],GNDS[i],GZDS[i]])

        print('tutanca0')
        print(np.shape(Gss_T),np.shape(Gds_T))
        return Gss_T,Gds_T,self.L

    def load_greenfunctions(self):
        #path_gf='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/gf/'.replace(self.output,'direct')
        path_gf='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/gf/'

        print('path_gf1',path_gf)
        dummy = 2
        if self.output == 'inversion' and self.synthetic_test:
            path_gf=path_gf.replace('inversion','direct')
            print('xdddd',self.output,path_gf)
        # else:
        #     path_gf='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/gf/'

        print('p',path_gf)
        #gf = sorted(os.listdir(path_gf))
        gfs=sorted(os.listdir(path_gf))
       

        # try:

        # except FileNotFoundError:
        #      print('creando carpetas correspondientes')
        #      print('path',path_gf)
        #      os.mkdir(path_gf.replace('double/inversion/gf','double'))
        #      os.mkdir(path_gf.replace('single/inversion/gf','double/direct'))
        #      os.mkdir(path_gf.replace('single/inversion/gf','double/direct/gf'))





        gfs = [path_gf+x for  x in gfs]
        print(gfs)
        lon_falla=[]
        lat_falla=[]
        gf_list=[]
        geste=[]
        gnorte=[]
        gup=[]
        bandera=True
        print('aa')
        if self.interface == 'single' and not self.split_G:
            for gf in gfs:
                with open(gf,'r') as csv_file:
                    reader = csv.reader(csv_file, delimiter =';')
                    for row in reader:
                        if row[0][0] != 'l':
                            row =[float(x) for x in row]
                            geste.append(row[2])
                            gnorte.append(row[3])
                            gup.append(row[4])
                            #gf_list.append(row[2:])
                            if bandera:
                                lon_falla.append(row[0])
                                lat_falla.append(row[1])
                                ## OJO CON ESTA BANDERITA
                bandera = False
            nx=self.grid_size[0]
            ny=self.grid_size[1]
            datos=len(gfs)
            geste=np.asarray(geste).reshape((datos,nx*ny))
            gnorte=np.asarray(gnorte).reshape((datos,nx*ny))
            gup=np.asarray(gup).reshape((datos,nx*ny))

            print(np.shape(geste),np.shape(gnorte),np.shape(gup))
            G=np.vstack((geste,gnorte,gup))
            print('G',np.shape(G),G)
            coord_fallas=(lon_falla,lat_falla)

        if self.interface == 'double':
            Aeste=[]
            Anorte=[]
            Aup=[]
            Beste=[]
            Bnorte=[]
            Bup=[]
            datos=len(gfs)
            nx=self.grid_size[0]
            ny=self.grid_size[1]
            for gf in gfs:
                with open(gf,'r') as csv_file:
                    reader = csv.reader(csv_file, delimiter =';')
                    if 'gf/GF_INV' in gf:
                        for row in reader:
                            if row[0][0] != 'l':
                                row = [float(x) for x in row]
                                Aeste.append(row[2])
                                Anorte.append(row[3])
                                Aup.append(row[4])
                                if bandera:
                                    lon_falla.append(row[0])
                                    lat_falla.append(row[1])
                                    #bandera = False
                    
                    elif 'gf/GF_NOR' in gf:
                        for row in reader:
                            if row[0][0] != 'l':
                                row = [float(x) for x in row]
                                Beste.append(row[2])
                                Bnorte.append(row[3])
                                Bup.append(row[4])
                                # if bandera:
                                #     lon_falla.append(row[0])
                                #     lat_falla.append(row[1])
                bandera = False
            Aeste=np.asarray(Aeste).reshape((int(datos/2),nx*ny))
            Anorte=np.asarray(Anorte).reshape((int(datos/2),nx*ny))
            Aup=np.asarray(Aup).reshape((int(datos/2),nx*ny))

            Beste=np.asarray(Beste).reshape((int(datos/2),nx*ny))
            Bnorte=np.asarray(Bnorte).reshape((int(datos/2),nx*ny))
            Bup=np.asarray(Bup).reshape((int(datos/2),nx*ny))



            A=np.vstack((Aeste,Anorte,Aup))
            B=np.vstack((Beste,Bnorte,Bup))
            G=np.hstack((A,B))
            #G=np.hstack((Aeste,Anorte,Aup,Beste,Bnorte,Bup)).reshape(int(len(Aeste)*3),2)
            coord_fallas=(lon_falla,lat_falla)
            #print('G1',G1.T.shape,G1.T)
            #G=np.vstack((A,B)).T
            #G=np.hstack((A,B)).reshape((int(len(gfs)*self.n_dim/2)),2*self.grid_size[0]*self.grid_size[1])
            print('AESTE',len(Aeste),len(Anorte),len(Aup))
            print('A',A.shape,'B',B.shape)
            print('Gg',G.shape,A.shape,B.shape)

        if self.interface == 'single' and self.split_G:
            print('tronco')
            Ge_ss=[]
            Gn_ss=[]
            Gz_ss=[]
            Ge_ds=[]
            Gn_ds=[]
            Gz_ds=[]
            datos=len(gfs)
            nx=self.grid_size[0]
            ny=self.grid_size[1]
            for gf in gfs:
                with open(gf,'r') as csv_file:
                    reader = csv.reader(csv_file, delimiter =';')
                    if 'gf/GF_INV' in gf:
                        for row in reader:
                            if row[0][0] != 'l':
                                row = [float(x) for x in row]
                                Ge_ss.append(row[2])
                                Gn_ss.append(row[3])
                                Gz_ss.append(row[4])
                                Ge_ds.append(row[5])
                                Gn_ds.append(row[6])
                                Gz_ds.append(row[7])

                                if bandera:
                                    lon_falla.append(row[0])
                                    lat_falla.append(row[1])
                                        #bandera = False
                        
                       
                bandera = False
        
            Ge_ss=np.asarray(Ge_ss).reshape((int(datos),nx*ny))
            Gn_ss=np.asarray(Gn_ss).reshape((int(datos),nx*ny))
            Gz_ss=np.asarray(Gz_ss).reshape((int(datos),nx*ny))

            Ge_ds=np.asarray(Ge_ds).reshape((int(datos),nx*ny))
            Gn_ds=np.asarray(Gn_ds).reshape((int(datos),nx*ny))
            Gz_ds=np.asarray(Gz_ds).reshape((int(datos),nx*ny))



            Gss=np.vstack((Ge_ss,Gn_ss,Gz_ss))
            Gds=np.vstack((Ge_ds,Gn_ds,Gz_ds))
            G=np.hstack((Gss,Gds))
            #G=np.hstack((Aeste,Anorte,Aup,Beste,Bnorte,Bup)).reshape(int(len(Aeste)*3),2)
            coord_fallas=(lon_falla,lat_falla)
            #print('G1',G1.T.shape,G1.T)
            #G=np.vstack((A,B)).T
            #G=np.hstack((A,B)).reshape((int(len(gfs)*self.n_dim/2)),2*self.grid_size[0]*self.grid_size[1])
            # print('AESTE',len(Aeste),len(Anorte),len(Aup))
            # print('A',A.shape,'B',B.shape)
            # print('Gg',G.shape,A.shape,B.shape)
            print('luciernaga')

        #print(G[31,:])
        return G,coord_fallas
        

    def make_directo_new(self,**kwargs):

        A = self.model_matrix_slab(falla='inversa',save_subfallas_methods=True,
                              generar_subfallas = True,
                              save_GF=True)
        slip_A = kwargs.get('slipA',np.ones(self.grid_size[0]*self.grid_size[1]))

        if self.interface == 'double':
            B = self.model_matrix_slab(falla='normal',save_subfallas_methods=True,
                              generar_subfallas = True,
                              save_GF=True)
            nx=self.grid_size[0]
            ny=self.grid_size[1]
            slip_B = kwargs.get('slipB',np.ones(self.grid_size[0]*self.grid_size[1]))
            slip=np.hstack((slip_A,slip_B)).reshape(2*nx*ny,1)
            #slip==np.hstack((slip_A,slip_B))
            if nx*ny==1:
                G=np.vstack((A,B)).T
            else:
                G=np.hstack((A,B))

            print('G',G.shape,A.shape,B.shape)

            #G=np.hstack((A,B)).reshape(A.shape[0],2*nx*ny)
            daux=(1000*np.matmul(G,slip))
            dobs = [x[0] for x in daux]
            print('G',G.shape,slip.shape,dobs)
        ## define the input slip that will be applied to the system:

        if self.interface == 'single':
            try :
                slip = slip_A
                G=A
                dobs = (1000*np.matmul(G,slip)).tolist()

            except ValueError:
                print('solo una subfalla, arreglando error...')
                slip = slip_A
                G=A.reshape(A.shape[0],1)
                dobs=(1000*np.matmul(G,slip)).tolist()

        ## anteponemos un mil para que el resultado este en milimetros
        Ue_teo=[]
        Un_teo=[]
        Uz_teo=[]
        nsta=int(len(dobs)/self.n_dim)
        print('nsta',nsta)
        for i in range(nsta):
            Ue_teo.append(dobs[i])
            Un_teo.append(dobs[i+nsta])
            Uz_teo.append(dobs[i+2*nsta])

        self.data_sintetica.update({'UE': Ue_teo,
                                    'UN': Un_teo,
                                    'UU': Uz_teo})
        print('UEteo',Ue_teo,Un_teo,Uz_teo)

        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        sta = [elem.get('Station') for elem in self.data_estaciones]


        ## archivo de movimientos sinteticos
        #name_archivo='Outputs'+ self.id_proceso +self.proceso+ '/Cosismico/directo_components_'+label+'.txt'
        #name_archivo='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/txt/direct_components_'+label+'.txt' 
        name_archivo = self.desplaz_output
        name_slips_upper = self.upper_interface_output
        name_slips_lower = self.lower_interface_output
        print('desplaz_output')
        print(self.desplaz_output)
        lon_central = self.subfallas_model_dict.get('lon_central')
        lat_central = self.subfallas_model_dict.get('lat_central')
        # print('lon central')
        # print(lon_central_subfallas_slab)

        ## central latitude of the subfaults  of the fault plane (will work for both)

        with open(name_archivo,'w') as csvfile:
            writer=csv.writer(csvfile,delimiter=';')
            writer.writerow( ['Station','Longitud','Latitud'] + list(self.data_sintetica.keys()) )
            for row in zip(sta,lon,lat,Ue_teo,Un_teo,Uz_teo):
                writer.writerow(row)

        shutil.copy(name_archivo,'./Fuentes/')


        with open(name_slips_upper,'w') as csvfile:
            writer=csv.writer(csvfile,delimiter=';')
            writer.writerow( ['lon_central','lat_central','slip_upper'] )
            for row in zip(lon_central,lat_central,slip_A):
                writer.writerow(row)
                
        if self.interface == 'double':
            with open(name_slips_lower,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['lon_central','lat_central','slip_lower'] )
                for row in zip(lon_central,lat_central,slip_B):
                    writer.writerow(row)
        #print('A',np.hstack((A,A)).shape)
        return dobs,G

    def make_directo_new_dividido(self,**kwargs):
        Ass,Ads = self.model_matrix_slab_dividido(falla='inversa',save_subfallas_methods=True,
                              generar_subfallas = True,
                              save_GF=True)
        nx=self.grid_size[0]
        ny=self.grid_size[1]
        slip_Ass=kwargs.get('slip_Ass',np.ones((nx,ny)))
        slip_Ads=kwargs.get('slip_Ads',np.ones((nx,ny)))
        slip_A=np.hstack((slip_Ass,slip_Ads)).reshape(2*nx*ny,1)
        if nx*ny==1:
            G=np.vstack((Ass,Ads)).T
        else:
            G=np.hstack((Ass,Ads))
        slips = slip_A
        # ## NO HE PROGRAMADO LA INTERFAZ DIFERENCIADA PARA SS Y DS
        # if self.interface == 'double':
        #     Bss,Bds= self.model_matrix_slab_dividido(falla='normal',save_subfallas_methods=True,
        #                       generar_subfallas = True,
        #                       save_GF=True)
        # ## define the input slip that will be applied to the system:

        # slips = np.vstack((np.ones(self.grid_size[0]*self.grid_size[1]),
        #                  np.ones(self.grid_size[0]*self.grid_size[1]))).T
        # slips=slips.reshape((np.shape(slips)[0]*np.shape(slips)[1],1))

        #print('slips',np.shape(slips))
        print('A',G.shape,Ass.shape,Ads.shape)
        #A=A.reshape(np.s)
        #print('AA',np.shape(A),'slips',np.shape(slips))
        daux = 1000*np.matmul(G,slips)
        dobs = [x[0] for x in daux]

        #print('A',np.shape('A'),np.shape(Ass),np.shape(Ads),dobs,np.shape(dobs))

        Ue_teo=[]
        Un_teo=[]
        Uz_teo=[]
        nsta=int(len(dobs)/3)
        print('nsta',nsta)
        for i in range(nsta):
            Ue_teo.append(dobs[i])
            Un_teo.append(dobs[i+nsta])
            Uz_teo.append(dobs[i+2*nsta])

        self.data_sintetica.update({'UE': Ue_teo,
                                    'UN': Un_teo,
                                    'UU': Uz_teo})
        print('Us',Ue_teo,Un_teo,Uz_teo)

        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        sta = [elem.get('Station') for elem in self.data_estaciones]


        ## archivo de movimientos sinteticos
        #name_archivo='Outputs'+ self.id_proceso +self.proceso+ '/Cosismico/directo_components_'+label+'.txt'
        #name_archivo='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/txt/direct_components_'+label+'.txt' 
        name_archivo = self.desplaz_output
        name_slips_upper=self.upper_interface_output
        print('desplaz_output')
        print(self.desplaz_output)
        lon_central = self.subfallas_model_dict.get('lon_central')
        lat_central = self.subfallas_model_dict.get('lat_central')
        with open(name_archivo,'w') as csvfile:
            writer=csv.writer(csvfile,delimiter=';')
            writer.writerow( ['Station','Longitud','Latitud'] + list(self.data_sintetica.keys()) )
            for row in zip(sta,lon,lat,Ue_teo,Un_teo,Uz_teo):
                writer.writerow(row)
        with open(name_slips_upper,'w') as csvfile:
            writer=csv.writer(csvfile,delimiter=';')
            writer.writerow( ['lon_central','lat_central','slip_upp_strslip','slip_upp_dipslip'] )
            for row in zip(lon_central,lat_central,slip_Ass,slip_Ads):
                writer.writerow(row)
        return dobs,G

    def make_directo(self,**kwargs):
        #interfases=kwargs.get('interfases','double')
        #save_slips=kwargs.get('save_slips',False)


        num_sta=len([elem.get('Longitud') for elem in self.data_estaciones])
        nx=self.grid_size[0]
        ny=self.grid_size[1]

       
        ## we generate synthetic slips for subfaults. In this case we use an alternate checkerboard slip for both interfaces
        slipA=[]
        for i in range(nx*ny):
            if i%2 == 0:
                slipA.append(1.0)
            if i%2 == 1:
                slipA.append(1.0)

        slipB=[]
        for i in range(nx*ny):
            if i%2 == 0:
                slipB.append(0.0)
            if i%2 == 1:
                slipB.append(1.0)

        if self.interface =='double':
            label='interfaz_doble'
            UeA,UnA,UzA = self.pdirecto(slipA,plano='A',save_slips=True)
            UeB,UnB,UzB = self.pdirecto(slipB,plano='B',save_slips=True)
            Ue_teo=[]
            Un_teo=[]
            Uz_teo=[]


            for i  in range(num_sta):
                Ue_teo.append(1000*(UeA[i] +  UeB[i]))
                Un_teo.append(1000*(UnA[i] +  UnB[i]))
                Uz_teo.append(1000*(UzA[i] +  UzB[i]))


        if self.interface =='single':
            label='interfaz_single'
            print(slipA)
            Ue_teok,Un_teok,Uz_teok = self.pdirecto(slipA,plano='A',save_slips=True)
            
            Ue_teo=[1000*x  for x in Ue_teok]
            Un_teo=[1000*x for x in Un_teok]
            Uz_teo=[1000*x for x in Uz_teok]



        self.data_sintetica.update({'UE': Ue_teo,
                                    'UN': Un_teo,
                                    'UU': Uz_teo})

        print(Ue_teo,Un_teo,Uz_teo)

        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        sta = [elem.get('Station') for elem in self.data_estaciones]


        ## archivo de movimientos sinteticos
        #name_archivo='Outputs'+ self.id_proceso +self.proceso+ '/Cosismico/directo_components_'+label+'.txt'
        #name_archivo='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/txt/direct_components_'+label+'.txt' 
        name_archivo = self.desplaz_output
        print('desplaz_output')
        print(self.desplaz_output)
        with open(name_archivo,'w') as csvfile:
            writer=csv.writer(csvfile,delimiter=';')
            writer.writerow( ['Station','Longitud','Latitud'] + list(self.data_sintetica.keys()) )
            for row in zip(sta,lon,lat,Ue_teo,Un_teo,Uz_teo):
                writer.writerow(row)


        return Ue_teo,Un_teo,Uz_teo


 # subfalla = 0
        # for iy in range(ny):
        #     for ix in range(nx):
        #         tap = slip[subfalla]
        #         if ix == 0 and iy == 0:
        #             ddx, ddy = self.proj_mesh2okada(lat_vert_subf_slab[subfalla][0],
        #                                             lon_vert_subf_slab[subfalla][0],
        #                                             np.radians(strike_subf_deg[subfalla])
        #                                             )
        #             ue,un,uz= desplaz_okada(ddx,
        #                                     ddy,
        #                                     dip_subf_rad[subfalla],
        #                                     prof_subfallas[subfalla],
        #                                     w,
        #                                     l,
        #                                     rake_subfallas[subfalla],
        #                                     np.radians(strike_subf_deg[subfalla])
        #                                     )
        #             ve = tap*ue
        #             vn = tap*un
        #             vz = tap*uz
        #             print(ddx,ddy)
        #             print(ve,vn,vz)
        #         else:
        #             ddx, ddy = self.proj_mesh2okada(lat_vert_subf_slab[subfalla][0],
        #                                             lon_vert_subf_slab[subfalla][0],
        #                                             np.radians(strike_subf_deg[subfalla])
        #                                             )
        #             ue,un,uz= desplaz_okada(ddx,
        #                                     ddy,
        #                                     dip_subf_rad[subfalla],
        #                                     prof_subfallas[subfalla],
        #                                     w,
        #                                     l,
        #                                     rake_subfallas[subfalla],
        #                                     np.radians(strike_subf_deg[subfalla])
        #                                     )
        #             ve += tap*ue
        #             vn += tap*un
        #             vz += tap*uz
        #         subfalla+=1



    def pdirecto(self,slip,plano='A',save_slips=False):

        nx = self.grid_size[0]
        ny = self.grid_size[1]
        l = self.L/nx
        #w = self.ancho_placa/ny

        ## UN SLIP FICTICIOS XD
        #slip=[3]*nx*ny

        #subfallas_methods = self.planos_falla_obj.get('subfallas_methods')



        prof_subf_inv=self.subfallas_model_dict.get('depth_model')
        strike_subf_deg=self.subfallas_model_dict.get('strike_model')
        dip_subf_rad=np.radians(self.subfallas_model_dict.get('dip_model'))
        lon_vert_subf_slab=self.subfallas_model_dict.get('lon_vertices')
        lat_vert_subf_slab=self.subfallas_model_dict.get('lat_vertices')
        lon_cent_subf_slab=self.subfallas_model_dict.get('lon_central')
        lat_cent_subf_slab=self.subfallas_model_dict.get('lat_central')
        strike_rad=self.subfallas_model_dict.get('strike_rad')
        prof_subf_norm=prof_subf_inv+self.plate_thickness/1000*np.cos(dip_subf_rad)
        rake_inv_rad=self.subfallas_model_dict.get('rake_inv_rad')
        rake_norm_rad=self.subfallas_model_dict.get('rake_norm_rad')
        

        print('lon_centss')
        print(lon_cent_subf_slab)
        print('lat_cent')
        print(lat_cent_subf_slab)



        if plano == 'A':
            prof_subfallas = prof_subf_inv
            rake_subfallas = rake_inv_rad
        if plano == 'B':
            prof_subfallas = prof_subf_norm
            rake_subfallas = rake_norm_rad
        #print('lat_vert_subf_slab')
        #print(lat_vert_subf_slab)
        #print('lon_vert_subf_slab')
        #print(lon_vert_subf_slab)
        #print('prof_subf_inv')
        #print(prof_subf_inv)
        #print('lon_cent_subf_slab')
        #print(lon_cent_subf_slab)
        #print('lat_cent_subf_slab')
        #print(lat_cent_subf_slab)
        #print('dip_subf_deg')
        #print(np.degrees(dip_subf_rad))
        #print('rake_inv_deg')
        #print(np.degrees(rake_inv_rad))
        #print('strike_deg')
        #print(np.degrees(strike_rad))
        subfalla = 0
        ve=[]
        vn=[]
        vz=[]
        for iy in range(ny):
            for ix in range(nx):
                ## in this case we use real w because its and input for green functions
                ## in following function
                w = self.ancho_placa/ny
                tap = slip[subfalla]
                ddx, ddy = self.proj_mesh2okada(lat_vert_subf_slab[subfalla][0],
                                                lon_vert_subf_slab[subfalla][0],
                                                np.radians(strike_subf_deg[subfalla]))
                

                #print('okada')
                #print(dip_subf_rad[subfalla],
                 #     prof_subfallas[subfalla],
                 #     w,
                 #     l,
                 #     rake_subfallas[subfalla],
                 #     np.radians(strike_subf_deg[subfalla]))
                     
                                                     
                ue,un,uz= desplaz_okada(ddx,
                                        ddy,
                                        dip_subf_rad[subfalla],
                                        1000*prof_subfallas[subfalla],
                                        w,
                                        l,
                                        rake_subfallas[subfalla],
                                        np.radians(strike_subf_deg[subfalla])
                                        )
                ve.append(tap*ue)
                vn.append(tap*un)
                vz.append(tap*uz)
                #print(ddx,ddy)
                #print(ve,vn,vz)
                subfalla+=1


       

       
        if plano=='A' and save_slips:
            label='upper'
            #name_archivo='Outputs'+ self.id_proceso +self.proceso+ '/Cosismico/coseismic_slip_'+label+'_interface.txt'
            #name_archivo='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+
            #              self.output+'/txt/'+self.proceso.lower+'_slip_'+label+'.txt' 
            name_archivo=self.upper_interface_output

            with open(name_archivo,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['lon_central','lat_central','slip_'+label]  )
                for row in zip(lon_cent_subf_slab,lat_cent_subf_slab,slip):
                    writer.writerow(row)

        if plano=='B' and save_slips:
            label='lower'
            #name_archivo='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+
            #              self.output+'/txt/'+self.proceso.lower+'_slip_'+label+'.txt' 
            name_archivo=self.lower_interface_output

            with open(name_archivo,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['lon_central','lat_central','slip_'+label]  )
                for row in zip(lon_cent_subf_slab,lat_cent_subf_slab,slip):
                    writer.writerow(row)

        return sum(ve),sum(vn),sum(vz)
                        
    

    def make_directo_dividido(self,**kwargs):
        #interfases=kwargs.get('interfases','double')
        #save_slips=kwargs.get('save_slips',False)


        num_sta=len([elem.get('Longitud') for elem in self.data_estaciones])
        nx=self.grid_size[0]
        ny=self.grid_size[1]

       
        ## we generate synthetic slips for subfaults. In this case we use an alternate checkerboard slip for both interfaces
        slipA_ss=[]
        for i in range(nx*ny):
            if i%2 == 0:
                slipA_ss.append(0.0)
            if i%2 == 1:
                slipA_ss.append(0.0)

        slipA_ds=[]
        for i in range(nx*ny):
            if i%2 == 0:
                slipA_ds.append(1.0)
            if i%2 == 1:
                slipA_ds.append(1.0)

        if self.interface =='single':
            label='interfaz_doble'
            Ue_teo,Un_teo,Uz_teo = self.pdirecto_dividido(slipA_ss,slipA_ds,plano='A',save_slips=False)
            print('congrio')
            # Ueds,Unds,Uzds = self.pdirecto_dividido(slipA_ds,plano='A',save_slips=False)
            # Ue_teo=[]
            # Un_teo=[]
            # Uz_teo=[]


            # for i  in range(num_sta):
            #     Ue_teo.append(1000*(Uess[i] +  Ueds[i]))
            #     Un_teo.append(1000*(Unss[i] +  Unds[i]))
            #     Uz_teo.append(1000*(Uzss[i] +  Uzds[i]))


        # if self.interface =='single':
        #     label='interfaz_single'
        #     print(slipA)
        #     Ue_teok,Un_teok,Uz_teok = self.pdirecto_dividido(slipA,plano='A',save_slips=True)
            
        #     Ue_teo=[1000*x  for x in Ue_teok]
        #     Un_teo=[1000*x for x in Un_teok]
        #     Uz_teo=[1000*x for x in Uz_teok]



        self.data_sintetica.update({'UE': Ue_teo,
                                    'UN': Un_teo,
                                    'UU': Uz_teo})

        print(Ue_teo,Un_teo,Uz_teo)

        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        sta = [elem.get('Station') for elem in self.data_estaciones]


        ## archivo de movimientos sinteticos
        #name_archivo='Outputs'+ self.id_proceso +self.proceso+ '/Cosismico/directo_components_'+label+'.txt'
        #name_archivo='Outputs'+ self.id_proceso+'/'+self.proceso+'/'+self.interface+'/'+self.output+'/txt/direct_components_'+label+'.txt' 
        name_archivo = self.desplaz_output
        print('desplaz_output')
        print(self.desplaz_output)
        with open(name_archivo,'w') as csvfile:
            writer=csv.writer(csvfile,delimiter=';')
            writer.writerow( ['Station','Longitud','Latitud'] + list(self.data_sintetica.keys()) )
            for row in zip(sta,lon,lat,Ue_teo,Un_teo,Uz_teo):
                writer.writerow(row)



        plt.quiver(lon,lat,Ue_teo,Un_teo,color='k',label='directas')
        #im6=axs[0,2].quiver(lon,lat,Ue_inv,Un_inv,color='r',label='inversas')
        plt.legend()
        plt.title('Desplazamientos Directos')
         


        return Ue_teo,Un_teo,Uz_teo


 


    def pdirecto_dividido(self,slip_ss,slip_ds,plano='A',save_slips=False):

        GSS,GDS=self.model_matrix_slab_dividido(falla='inversa',save_subfallas_methods=False,generar_subfallas=True)
        ## multiplicar la funcion de  green de cada  sybfalla por el slip asociado a cada subfalla
        #GmSS=1000*np.matmul(GSS,np.array(slip_ss))
        #GmDS=1000*np.matmul(GDS,np.array(slip_ds))
        GmSS=0*GSS
        GmDS=1000*GDS


        print('xDD')
        print(np.shape(GmSS),np.shape(GmDS))
        print(GmSS,GmDS)
        print('lengss')
        print(len(GSS))
        mov_estacion_este=[]
        mov_estacion_norte=[]
        mov_estacion_up=[]


        for i in range(0,len(GSS) - 3,3):
            disp_sta_ss_este=np.sum(GmSS[i])
            disp_sta_ss_norte=np.sum(GmSS[i+1])
            disp_sta_ss_up=np.sum(GmSS[i+2])
            disp_sta_ds_este=np.sum(GmDS[i])
            disp_sta_ds_norte=np.sum(GmDS[i+1])
            disp_sta_ds_up=np.sum(GmDS[i+2])
            

            mov_estacion_este.append(disp_sta_ss_este+disp_sta_ds_este)
            mov_estacion_norte.append(disp_sta_ss_norte+disp_sta_ds_norte)
            mov_estacion_up.append(disp_sta_ss_up+disp_sta_ds_up)


        return mov_estacion_este,mov_estacion_norte,mov_estacion_up


    def inversion_cosismico_slab(self):
        pass

    def inversion_postsismico_slab(self):
        pass
    
    # def objective_function(self,s,map_g,G,Cd,d,alpha,L): 
    #     e=np.exp(s)
    #     #map_g=0.8
    #     hessiana=np.multiply(2*np.exp(s)*np.exp(s).T,np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),G)) + \
    #              2*np.diag(np.multiply(np.exp(s),
    #              np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),np.matmul(G,np.exp(s))-d))) + \
    #              2*alpha**(-2)*np.matmul(L.T,L)

    #     #objetive= map_g + (0.5(s-map_g)).T*hessiana(s,G,Cd,d,alpha,L)*(0.5(s-map_g))
    #     print('hessiana',hessiana,np.shape(hessiana))
    #     objetive= map_g + (0.5*(s-map_g)).T*hessiana*(0.5*(s-map_g))
    #     return objetive


    
    # def hessian(self,s,*args):
    #     hessiana=np.multiply(2*np.exp(s)*np.exp(s).T,np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),G))+ \
    #              2*np.diag(np.multiply(np.exp(s),
    #              np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),np.matmul(G*np.exp(s)-d))))+ \
    #              2*alpha**(-2)*np.matmul(L.T,L)
    #     return hessiana


 
    
    def obj(self,s,*args):
        """ funcion objetivo """
        # Ue_load = [elem.get('UE') for elem in self.data_estaciones]
        # Un_load = [elem.get('UN') for elem in self.data_estaciones]
        # Uz_load = [elem.get('UU') for elem in self.data_estaciones]
        # ## numero de datos que tenemos
        # N=len(Ue_load)
        # ## numero de parametros a ajustar a partir de los datos
        # M=self.grid_size[0]*self.grid_size[1]
        #  ## La funcion de Green
        # G=self.green_functions  
        # ## Un vector de datos 
        # d = np.hstack((Ue_load,Un_load,Uz_load))
        # ## una matriz de covarianza de los datos
        # Cd=0.03*np.eye(N)
        # ## un alpha grande
        # alpha = 100
        # ## El operador L para la matriz de covarianza de los parametros
        # L=np.eye(M)
        G=args[0]
        d=args[1]
        L=args[2]
        Cd=args[3]
        alpha=args[4]
        print('shapes',G.shape,d.shape,L.shape,Cd.shape)
        psi2= alpha**(-2)*np.matmul(np.matmul(L,s).T,np.matmul(L,s))
        print('G',G.shape)
        #print('psi',np.matmul(L,s).T.shape)
        #print('psi',np.matmul((np.matmul(G,np.exp(s))-d).T,Cd).shape)
        psi = np.matmul(np.matmul((np.matmul(G,np.exp(s))-d).T,np.linalg.inv(Cd)),np.matmul(G,np.exp(s))-d) + \
             alpha**(-2)*np.matmul(np.matmul(L,s).T,np.matmul(L,s))
        return psi


    def jacobian(self,s,*args):
        """jacobiano de la funcion objetivo """
        # ## numero de datos que tenemos
        # N=20
        # ## numero de parametros a ajustar a partir de los datos
        # M=len(s)
        # ## La funcion de Green
        # G=np.random.rand(N*M).reshape(N,M)    
        # ## El operador L para la matriz de covarianza de los parametros
        # L=np.eye(M)
        # ## un alpha grande
        # alpha = 100
        # ## Un vector de datos 
        # d = np.random.rand(N)
        # ## una matriz de covarianza de los datos
        # Cd=0.3*np.eye(N)
        
        G=args[0]
        d=args[1]
        L=args[2]
        Cd=args[3]
        alpha=args[4]
        jac1=2*np.multiply(np.exp(s),np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),np.matmul(G,np.exp(s))-d))
        jac2=2*alpha**(-2)*np.matmul(np.matmul(L.T,L),s)
        #print('jac',jac1.shape,jac2.shape)
        jac=jac1+jac2
        # jac= 2*np.multiply(np.exp(s),np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),np.matmul(G,np.exp(s))-d)) + \
        #      2*alpha**(-2)*np.matmul(np.matmul(L.T,L),s)
        return jac

    def hessian(self,s,*args):
        """ hessiano de la funcion objetivo"""
        # ## numero de datos que tenemos
        # N=20
        # ## numero de parametros a ajustar a partir de los datos
        # M=len(s)
        # ## La funcion de Green
        # G=np.random.rand(N*M).reshape(N,M)    
        # ## El operador L para la matriz de covarianza de los parametros
        # L=np.eye(M)
        # ## un alpha grande
        # alpha = 100
        # ## Un vector de datos 
        # d = np.random.rand(N)
        # ## una matriz de covarianza de los datos
        # Cd=0.3*np.eye(N)

        ## reescribir e(s) para poder operarlo como matriz
        e=np.exp(s).reshape(len(s),1)
        G=args[0]
        d=args[1]
        L=args[2]
        Cd=args[3]
        alpha=args[4]
        hes1 = 2*np.multiply(np.matmul(e,e.T),np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),G))
        hes2 = 2*alpha**(-2)*np.matmul(L.T,L)
        #hes3 = 2*np.diag(np.multiply(e,np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),np.matmul(G,e)-d.reshape(len(d),1))))
        #hes3=np.multiply(np.diag(e),np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),np.matmul(G,e)-d))
        hes3a=np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),np.matmul(G,e)-d.reshape(len(d),1)).reshape(len(e),)
        hes3f=2*np.diag(np.multiply(e.reshape(len(e),),hes3a))
        #print('hes3a',hes3a.shape,'hes3f',hes3f.shape,np.multiply(e.reshape(len(e),),hes3a).shape)
        #hes3=np.multiply(e,np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),np.matmul(G,e)-d))
        hes = hes1+hes2+hes3f
        #print('cho',G.shape,hes.shape,hes3.shape)
        # hes = 2*np.multiply(np.matmul(e,e.T),np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),G)) + \
        #       2*alpha**(-2)*np.matmul(L.T,L) + \
        #       2*np.diag(np.multiply(e,np.matmul(np.matmul(G.T,np.linalg.inv(Cd)),np.matmul(G,e)-d)))
        return hes


    def psi_aprox(self,s,cg_sol,*args):
        ## calcular la hessiana en el MAP
        G=args[0]
        d=args[1]
        L=args[2]
        Cd=args[3]
        alpha=args[4]
        hess_map=self.hessian(cg_sol.x,G,d,L,Cd,alpha)
        #print('aaa')
        #print(np.shape(hess_map))
        #print(np.matmul(s,hess_map).shape)
        #print('xd')
        #print('s',s)

        ter_rodeador=(s-cg_sol.x).reshape(len(s),1)
        #print('rodeador',ter_rodeador.shape,hess_map.shape)
        
        #print((s-cg_res.x).T)
        #psi_app=cg_sol.fun + 0.5*np.matmul(np.matmul(ter_rodeador.T,hess_map),ter_rodeador)
        psi_app= cg_sol.fun+0.5*np.matmul(np.matmul(ter_rodeador.T,hess_map),ter_rodeador)
        return psi_app

    def likelihood_function(self,d,*args):
        ## como ploteo esto
        G=args[0]
        s=args[1]
        L=args[2]
        Cd=args[3]
        alpha=args[4]
        ## reescribir e(s) para poder operarlo como matriz
        e=np.exp(s).reshape(len(s),1)
        factor=(2*np.pi)**(-len(d)/2)*(np.linalg.det(Cd))**(-0.5)
        ter_rodeador=np.matmul(G,e)-d.reshape(len(d),1)

        #print('bro')
        #print(np.mean(ter_rodeador))
        #print(np.linalg.inv(Cd))
        #print(np.shape(ter_rodeador),np.shape(G),np.shape(e))

        inside_exp= -0.5*(np.matmul(np.matmul(ter_rodeador.T,np.linalg.inv(Cd)),ter_rodeador))
        #print('ter',ter_rodeador)
        #print('inside',inside_exp,ter_rodeador)
        #print('cd',np.linalg.inv(Cd))
        #print('inside_exp',inside_exp,(np.linalg.det(Cd))**(-0.5))
        likelihood=factor*np.exp(inside_exp)
        return likelihood
    def prior_function(self,s,*args):
        G=args[0]
        d=args[1]
        L=args[2]
        Cd=args[3]
        alpha=args[4]
        ## reescribir e(s) para poder operarlo como matriz
        e=np.exp(s).reshape(len(s),1)
        #print('e',e,len(e))
        factor=(2*np.pi*alpha**2)**(-len(s)/2)*(np.linalg.det(np.matmul(L.T,L)))**(0.5)
        #print('factora',factor)
        ter_rodeador=np.matmul(L,s)
       # print('broda')
        #print(ter_rodeador)
        #print(np.shape(ter_rodeador),np.shape(G),np.shape(e))

        inside_exp= -0.5*2*alpha**(-2)*(np.matmul(ter_rodeador.T,ter_rodeador))
        #print('ter',ter_rodeador)
        #print('inside',inside_exp)
        #print('cd',np.linalg.inv(Cd))
        #print(inside_exp)
        prior=factor*np.exp(inside_exp)
        return prior
    def bic_and_evidence(self,d,mapsol,G,L,alpha,Cd):
        #trampa=np.array([3,1])
        #trampa=np.log(np.array([3,1,7,2,3,4]))
        #print('trampa',trampa.shape)
        ## likelihood en el MAP
        likelihood_map=self.likelihood_function(d,G,mapsol.x,L,Cd,alpha)
        #likelihood_map=self.likelihood_function(d,G,trampa,L,Cd,alpha)

        ## prior en el MAP
        prior_map=self.prior_function(mapsol.x,G,d,L,Cd,alpha)
        #prior_map=self.prior_function(trampa,G,d,L,Cd,alpha)

        ## matriz de covarianza del posterior en el MAP
        Cov_post=np.linalg.inv(0.5*self.hessian(mapsol.x,G,d,L,Cd,alpha))
        #Cov_post=np.linalg.inv(0.5*self.hessian(trampa,G,d,L,Cd,alpha))
        if self.interface == 'single':
            k = self.grid_size[0]*self.grid_size[1]
        if self.interface == 'double':
            k = 2*self.grid_size[0]*self.grid_size[1]

        ## construir terminos de la evidencia
        log_like=np.log(likelihood_map)
        log_prior=np.log(prior_map)
        log_constant=k/2*np.log(2*np.pi)
        sign,log_posterior=np.linalg.slogdet(Cov_post)
        log_posterior_term = 0.5*log_posterior
        #print('post',log_posterior)
        evidence=log_like+log_prior+log_constant+log_posterior
        print('evidence',log_like,log_prior,log_constant,log_posterior)
        #evidence=likelihood_map*prior_map*(2*np.pi)**(k/2)*(np.linalg.det(Cov_post))**0.5
        #print('lmap',likelihood_map)
        #print('prior_map',prior_map)
        #print('cov',np.linalg.det(Cov_post))
        bic=-2*np.log(likelihood_map)+k*np.log(len(d))
        return evidence,bic
    def posterior_function(self,*args):
        pass
    def bayesian_inversion(self,**kwargs):
        """ we invert the coseismic data  and obtain the slips in every subfault
        , with these slips, we generate synthethic displacementes in surface and compute
        the mean cuadratic error"""

        ## en realidad es inversion cosismico... per no es muy  relevante la diferencia 
        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        sta = [elem.get('Station') for elem in self.data_estaciones]
        nx=self.grid_size[0]
        ny=self.grid_size[1]
        seed=550000
        np.random.seed(seed)
        ## we must decide if whether we will impose a condition over the model parameters
        ##  to decrease the degrees of freedom of the system 
        regul_kind=kwargs.get('how2regularize','min_smooth')
        ## if we need to generate noisy date e.g for a resolution test
        add_noise=kwargs.get('add_noise',True)


        ## we will asume all data has the same std dev regardless of the coordinate we will be inverting
        ## standard deviation of the data
        #std=0.01
        ## 1 centimeto de desviacion estandar de los datos
        std=0.01
        # 0..1 cm de desviacion estandar
        #std=0.001
        ## load data
        Ue_load = [elem.get('UE') for elem in self.data_estaciones]
        Un_load = [elem.get('UN') for elem in self.data_estaciones]
        Uz_load = [elem.get('UU') for elem in self.data_estaciones]
        if self.load_incertezas:
            Stde_load = [elem.get('SE') for elem in self.data_estaciones]                
            Stdn_load = [elem.get('SN') for elem in self.data_estaciones]
            Stdz_load = [elem.get('SU') for elem in self.data_estaciones]
            print('stds',Stde_load,Stdn_load,Stdz_load)
            covar_vector=np.hstack((Stde_load,Stdn_load,Stdz_load))

        ## if we will be adding noise to the data:
        if add_noise and not self.load_incertezas:

            # Ue_obs = Ue_load + np.random.randn(len(Ue_load))
            # Un_obs = Un_load + np.random.randn(len(Un_load))
            # Uz_obs = Uz_load + np.random.randn(len(Uz_load))


           Ue_obs = [x+np.random.normal(0,std) for x in Ue_load]
           Un_obs = [x+np.random.normal(0,std) for x in Un_load]
           Uz_obs = [x+np.random.normal(0,std) for x in Uz_load]
           #Ue_obs = [x+np.random.normal(0,abs(np.mean(Ue_load))/100) for x in Ue_load]
           #Un_obs = [x+np.random.normal(0,abs(np.mean(Un_load))/100) for x in Un_load]
           #Uz_obs = [x+np.random.normal(0,abs(np.mean(Uz_load))/100) for x in Uz_load]
        elif add_noise and self.load_incertezas:
           Ue_obs = [x+np.random.normal(0,std) for x,std in zip(Ue_load,Stde_load)]
           Un_obs = [x+np.random.normal(0,std) for x,std in zip(Un_load,Stdn_load)]
           Uz_obs = [x+np.random.normal(0,std) for x,std in zip(Uz_load,Stdz_load)]
        else:
            Ue_obs = Ue_load
            Un_obs = Un_load
            Uz_obs = Uz_load

        ## the concatenated data
        d=np.hstack((Ue_obs,Un_obs,Uz_obs))

        #print('d')
        ## covariance matrix of the data, simple Cd 
        if not self.load_incertezas:
            covar_data_matrix=std*np.eye(3*len(Ue_obs))
        if self.load_incertezas:
            covar_data_matrix=np.diag(covar_vector)
        ## The Green functions
        G = self.green_functions
        print('GGG',G)
        trampa = np.array([3,1])
        #trampa=np.array([3,1,7,2,3,4])
        #print('G',G)
        #print('sol',np.matmul(G,trampa)-d)
        #print('g',G.shape,G,G*np.array([3,1]))
        ## The model covariance matrix.
        if regul_kind == 'min_smooth':
        ## ME FALTA PROGRAMAR ESTO
            a=0.3
        if regul_kind == 'minimize':
            ## minimizing the slip
            print('min')
            if self.interface == 'single' and not self.split_G:
                L=np.eye(nx*ny)
            if self.interface == 'single' and self.split_G:
                L=np.eye(nx*ny*2)
            if self.interface == 'double':
                L = np.eye(2*nx*ny)
        if regul_kind == 'smooth':
            L=self.gen_matriz_suavidad()
               # L =np.zeros((2*nx*ny,2*nx*ny))
        ## a very large alpha for an uninformative prior
        #alpha = 10**(5)
        ## aka desviacion estandar de los parametro
        alpha=10**(2)
        #print('d')
        #print(d)

        # s=np.random.rand(nx*ny)
        # psi_real=self.obj(s,G,d,L,covar_data_matrix,alpha)
        # print('psi')
        # print(psi_real)
        # hessian=self.hessian(s,G,d,L,covar_data_matrix,alpha)
        # print('hessian')
        # print(np.shape(hessian))
        # jac=self.jacobian(s,G,d,L,covar_data_matrix,alpha)
        # print('jac')
        # print(np.shape(jac))
        ##  minimizing the objective function
        print('minimizing the objective function')
        # we evaluate the the objective function at a random place, assuming 
        ## that our function is roughly convex we should not have problem with convergence
        if self.interface == 'single':
            map_guess=np.random.rand(nx*ny)
            #map_guess=np.array([0,0])
        if self.interface == 'double':
                map_guess=np.random.rand(nx*ny*2)
                #map_guess=np.random.rand(2*nx*ny)
                #print('guess',map_guess)
                        #map_guess=np.random.rand(2*nx*ny)
        if self.interface == 'single' and self.split_G:
            map_guess=np.random.rand(nx*ny*2)
        funcparams=(G,d,L,covar_data_matrix,alpha)
        print('prueba',G.shape,G,map_guess)
        sol=minimize(self.obj,
                     map_guess,
                     args=funcparams,
                     method='Newton-CG',
                     jac=self.jacobian,
                     hess=self.hessian,
                     options={'xtol': 1e-6, 'disp': True})

        #print(sol,sol.fun,sol.jac)
        evidence,bic=self.bic_and_evidence(d,sol,G,L,alpha,covar_data_matrix)
        print('evbic',evidence,bic)
        print(sol,sol.fun,type(sol.x))
        print('m',np.exp(sol.x))
        #return G
        posteriors=[]
        priors=[]
        likelihoods=[]
        dsint=np.matmul(G,np.exp(sol.x))
        print('residuales',dsint-d,np.sum(dsint-d))
        ecm_d=1/len(d)*np.sum((dsint-d)**2)
        uenz_sint=np.split(dsint,3)
        Ue_sint=1000*uenz_sint[0]
        Un_sint=1000*uenz_sint[1]
        Uz_sint=1000*uenz_sint[2]
        #print('ecm',ecm_d)
        #print(dsint)
        #print('uenz',Uz_obs,Uz_sint)

        # self.outputs_inv.update({'slips_subfallas':sol.x,
        #                          'ECM_data': ecm_d,
        #                           'UE': Ue_sint,
        #                           'UN': Un_sint,
        #                           'UZ': Uz_sint
        #                           })

        
        # print('desplaz_output')
        # print(self.desplaz_output)
        # lon_central = self.subfallas_model_dict.get('lon_central')
        # lat_central = self.subfallas_model_dict.get('lat_central')
        # print('lon central')
        # print(lon_central_subfallas_slab)

        ## central latitude of the subfaults  of the fault plane (will work for both)

        name_archivo = self.desplaz_output.replace('_generated.txt','_inverted.txt')
        print('namearc',name_archivo)
        name_slips_upper = self.upper_interface_output
        name_slips_lower = self.lower_interface_output
        name_params = name_archivo.replace('_inverted.txt','_inv_params.txt')
        #print('coord_fallas',len((self.coord_fallas)[0]),sol.x,sol.x.shape)

        #print('nparams',name_params,name_archivo)
        if self.interface == 'single' and not self.split_G:
            with open(name_archivo,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['Station','Longitud','Latitud','UE', 'UN', 'UZ'] )
                for row in zip(sta,lon,lat,Ue_sint,Un_sint,Uz_sint):
                    writer.writerow(row)

            with open(name_slips_upper,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['lon_central','lat_central','slip_upper'] )
                for row in zip(self.coord_fallas[0],self.coord_fallas[1],np.exp(sol.x)):
                    writer.writerow(row)
                    #print('row',row)
            
        #print('pana',np.exp(sol.x))

        if self.interface == 'single' and  self.split_G:
            solx_asmatrix=sol.x.reshape(len(sol.x),1)
            solx_ss=np.split(sol.x,2)[0]
            solx_ds=np.split(sol.x,2)[1]
            #print('solx',solx_asmatrix.shape,solx_ss,solx_ds,np.exp(sol.x),sol.x.shape)
            with open(name_archivo,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['Station','Longitud','Latitud','UE', 'UN', 'UZ'] )
                for row in zip(sta,lon,lat,Ue_sint,Un_sint,Uz_sint):
                    writer.writerow(row)

            with open(name_slips_upper,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['lon_central','lat_central','slip_upp_strslip','slip_upp_dipslip'] )
                for row in zip(self.coord_fallas[0],self.coord_fallas[1],np.exp(solx_ss),np.exp(solx_ds)):
                    writer.writerow(row)

        if self.interface == 'double' and not self.split_G:
            solx1=np.split(sol.x,2)[0]
            solx2=np.split(sol.x,2)[1]

            with open(name_archivo,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['Station','Longitud','Latitud','UE', 'UN', 'UZ'] )
                for row in zip(sta,lon,lat,Ue_sint,Un_sint,Uz_sint):
                    writer.writerow(row)


            with open(name_slips_upper,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['lon_central','lat_central','slip_upper'] )
                for row in zip(self.coord_fallas[0],self.coord_fallas[1],np.exp(solx1)):
                    writer.writerow(row)
                    #print('rowga',row)
            with open(name_slips_lower,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['lon_central','lat_central','slip_lower'] )
                for row in zip(self.coord_fallas[0],self.coord_fallas[1],np.exp(solx2)):
                    writer.writerow(row)
                    #print('rowga',row)

        with open(name_params,'w') as csvfile:
            writer=csv.writer(csvfile,delimiter=';')
            writer.writerow( ['log_evi','bic','ecm','alpha'] )
            writer.writerow([evidence[0][0],bic[0][0],ecm_d,alpha])

        return G, d,dsint
                
        # s=np.linspace(3.5,4.5,500)
        # s2=np.linspace(-10,10,500)
        # for i in range(500):
        #     #s=np.random.rand(nx*ny)
        #     #s=np.random.lognormal(0,0.01,nx*ny)
        #     #s=np.ones(nx*ny)+1
        #     psi_app=self.psi_aprox(np.array([s[i]]),sol,G,d,L,covar_data_matrix,alpha)
        #     prior=self.prior_function(np.array([s[i]]),G,d,L,covar_data_matrix,alpha)
        #     #likelihood=self.likelihood_function()
        #     posterior=np.exp(-psi_app/2)
        #     #print('prior',prior)
        #     posteriors.append(posterior[0][0])
        #     priors.append(prior)

        # #print(np.mean(posteriors),np.std(posteriors))
        # plt.figure(1)
        # plt.plot(s,posteriors)
        # plt.xlabel('s = log(m)')
        # plt.ylabel('P(s)')
        # plt.title('Posterior PDF'+' EV orden: '+str(int(np.log10(evidence))) + ' BIC: ' +str(int(bic)))
        # plt.suptitle('Input: 3m de slip upper fault: Output: slip en upper fault')
        # plt.figure(2)
        # plt.plot(s2,priors)
        # plt.xlabel('s = log(m)')
        # plt.ylabel('P(s)')
        # plt.title('Prior PDF')

        
        # plt.figure(3)
        # plt.hist(posteriors,100)
        # plt.title('posterior PDF')

        # s=np.random.lognormal(1.0,0.01,nx*ny)
        # psi_app=self.psi_aprox(s,sol,G,d,L,covar_data_matrix,alpha)
        # print(psi_app)
        # ## muestrear el likelihood
        # likes=[]
        # for i in range(500):
        #     #s=np.random.lognormal(0.0,0.01,nx*ny)
        #     s=np.array([1])
        #     Ue_obs = [x+np.random.normal(0,abs(np.mean(Ue_load))/10) for x in Ue_load]
        #     Un_obs = [x+np.random.normal(0,abs(np.mean(Un_load))/10) for x in Un_load]
        #     Uz_obs = [x+np.random.normal(0,abs(np.mean(Uz_load))/10) for x in Uz_load]
        #     d=np.hstack((Ue_obs,Un_obs,Uz_obs))
        # #    print('s',s)
        #     likelihood=self.likelihood_function(d,G,s,L,covar_data_matrix,alpha)
        #     likes.append(likelihood[0][0])
        # #    print(likes)
        # #    print(likelihood)
        # #print(likes)
        # plt.figure(1)
        # plt.hist(likes)
        # plt.title('likelihood PDF')

        # ## muestrar el prior
        # priors=[]
        # for i in range(500):
        #     s=np.random.lognormal(np.e,0.1,nx*ny)
        #     prior=self.prior_function(s,G,d,L,covar_data_matrix,alpha)
        #     priors.append(prior)
        # print(priors)
        # plt.figure(2)
        # plt.hist(priors)
        # plt.title('prior pdf')

        # ### obtener una expresion para el posterior PDF
        # #s=np.random.lognormal(0.2,0.01,nx*ny)
        # #print(psi_app)    
        
        # posteriors=[]
        # for i in range(500):
        #     #s=np.random.normal(0,0.01,nx*ny)
        #     s=np.random.rand(nx*ny)
        #     psi_app=self.psi_aprox(s,sol,G,d,L,covar_data_matrix,alpha)
        #     posterior=np.exp(-psi_app/2)
        #     posteriors.append(posterior[0][0])

        # print(posteriors)
        # plt.figure(3)
        # plt.hist(posteriors)
        # plt.title('posterior PDF')
        ## chequear si la matriz es definida positiva
        #print(np.linalg.cholesky(hessian))
        #psi_real=(s,)
        # print('UENZ')
        # print(Ue_obs,Un_obs,Uz_obs)

   # def inversion_intersismico_slab(self, interfases='double'):
    def gen_matriz_suavidad(self):
        #subfallas_methods=self.planos_falla_obj.get('subfallas_methods')
        #dip_subf_rad=subfallas_methods[1]

        nx=self.grid_size[0]
        ny=self.grid_size[1]
        n=nx*ny
        if not self.split_G:
            F = np.zeros(n*n).reshape(n,n)
        if self.split_G:
            F=np.zeros((2*n,2*n))
            pos = []
            coef = []
            subfalla = 0
            self.L = 436369.273177
            for j in range(2*ny):
                ## primera fila
                if j == 0: 
                    for i in range(2*nx):
                        dx=self.L / nx
                        #dy =self.ancho_placa/np.cos(dip_subf_rad[subfalla]) / ny
                        dy =self.ancho_placa/ny
                        dxi = 1.0 / (dx*dx) 
                        dyi = 1.0 / (dy*dy)
                        #print(dx)
                        #print(dy)
                        #print(dyi)
                        #print(dxi)
                        dr = dyi/dxi
                        #print(dr)
                        ## vertice inferior izquierdo
                        if i == 0:
                           print('jauaj')
                           pos.append(np.array([ (i+1,j), (i+2,j), (i,j+1), (i,j+2), (i,j) ]))
                           coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
                        ## vertice inferior derecho
                        elif i == 2*nx-1 :
                           pos.append(np.array([ (i-1,j), (i-2,j), (i,j+1), (i,j+2), (i,j) ]))
                           coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
                        ## arista inferior
                        else :
                           pos.append(np.array([ (i+1,j), (i-1,j), (i,j+1), (i,j+2), (i,j) ]))
                           coef.append(np.array([ 1. , 1. , -2.*dr , dr , -2. + dr ]))
                        subfalla = subfalla + 1
                ## ultima fila
                elif j == 2*ny-1 : 
                    for i in range(nx):
                    ##################################################################
                        dx = self.L / nx
                        #dy = self.ancho_placa / np.cos(dip_subf_rad[subfalla]) / ny
                        dy = self.ancho_placa/ ny  
                          
                        
                        dxi = 1.0 / (dx*dx)
                        dyi = 1.0 / (dy*dy)
                        dr = dyi / dxi 
                    ###################################################################                
                        ## vertice superior izquierdo
                        if i == 0 :
                            pos.append(np.array([ (i+1,j), (i+2,j), (i,j-1), (i,j-2), (i,j) ]))
                            coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
                        ## vertice superior derecho
                        elif i == 2*nx-1 :
                            pos.append(np.array([ (i-1,j), (i-2,j), (i,j-1), (i,j-2), (i,j) ]))
                            coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))             
                        ## arista superior
                        else :
                            pos.append(np.array([ (i+1,j), (i-1,j), (i,j-1), (i,j-2), (i,j) ]))
                            coef.append(np.array([ 1. , 1. , -2.*dr , dr , -2. + dr ]))
                        subfalla = subfalla +1
                else :         #<------------ filas restantes        
                    for i in range(2*nx):
                    ##################################################################
                        dx = self.L / nx
                        #dy = self.ancho_placa * np.cos(dip_subf_rad[subfalla]) / ny
                        dy = self.ancho_placa / ny  
      
                        dxi = 1.0 / (dx*dx)
                        dyi = 1.0 / (dy*dy)
                        dr = dyi / dxi 
                    ###################################################################
                        ## arista izquierda
                        if i == 0:
                            pos.append(np.array([ (i+1,j), (i+2,j), (i,j+1), (i,j-1), (i,j) ]))
                            coef.append(np.array([ -2. , 1. , dr , dr , 1. - 2.*dr ]))
                        ## arista derecha
                        elif i == 2*nx-1 :
                            pos.append(np.array([ (i-1,j), (i-2,j), (i,j+1), (i,j-1), (i,j) ]))
                            coef.append(np.array([ -2. , 1. , dr , dr , 1. - 2.*dr ]))             
                        ## punto central (general )
                        else :
                            pos.append(np.array([ (i+1,j), (i-1,j), (i,j+1), (i,j-1), (i,j) ]))
                            coef.append(np.array([ 1. , 1. , dr , dr , -2. - 2.*dr ]))

                        subfalla = subfalla + 1
            print(pos,coef)
            for k in range(2*nx*ny):
                index = pos[k]
                m = 0
                print(index)
                for i,j in index:
                    F[k][j*nx+i] = coef[k][m]
                    m += 1
            return F

    def inversion_intersismico_slab(self, **kwargs):
        """ we invert the coseismic data  and obtain the slips in every subfault
        , with these slips, we generate synthethic displacementes in surface and compute
        the mean cuadratic error"""

        ## en realidad es inversion cosismico... per no es muy  relevante la diferencia 
        nx=self.grid_size[0]
        ny=self.grid_size[1]
        regul_kind=kwargs.get('how2regularize','min_smooth')
        add_noise=kwargs.get('add_noise',True)

        #interfases=kwargs.get('interfases','double')
        A=self.planos_falla_obj.get('A')
        print('A')
        print(np.shape(A))

        if self.interface == 'double':
            B=self.planos_falla_obj.get('B')


        #prof_subf_inv=subfallas_methods[0]
        # strike_subf_deg=subfallas_methods[2]
        # lonv_vert_subf_slab=subfallas_methods[3]
        # lat_vert_subf_slab=subfallas_methods[4]
        # lon_cent_subf_slab=subfallas_methods[5]
        # lat_cent_subf_slab=subfallas_methods[6]
        # prof_subf_norm=subfallas_methods[7]
        # phi_placa_rad=subfallas_methods[8]
        # rake_inv_rad=subfallas_methods[9]
        # rake_norm_rad=subfallas_methods[10]
        

        Ue_load = [elem.get('UE') for elem in self.data_estaciones]
        Un_load = [elem.get('UN') for elem in self.data_estaciones]
        Uz_load = [elem.get('UU') for elem in self.data_estaciones]
        print('AAAA')
        #print(Ue_load,Un_load,Uz_load)
        if add_noise:
           Ue_obs = [x+np.random.normal(0,abs(x)/10) for x in Ue_load]
           Un_obs = [x+np.random.normal(0,abs(x)/10) for x in Un_load]
           Uz_obs = [x+np.random.normal(0,abs(x)/10) for x in Uz_load]

        else:
            Ue_obs = Ue_load
            Un_obs = Un_load
            Uz_obs = Uz_load


        print('UENZ')
        print(Ue_obs,Un_obs,Uz_obs)
       
                     

        ## we build the Identity Matrix which is used for constraining
        ## minimum displacements in our solution
    
        if regul_kind ==  'smooth':
           F=self.gen_matriz_suavidad()
           regul_array=F
        if regul_kind == 'minimize':
           I=np.identity(nx*ny)
           regul_array=I
        if regul_kind == 'min_smooth':
            I = np.identity(nx*ny)
            F = self.gen_matriz_suavidad()
            regul_array=[I,F]


        if self.n_dim == 1:
            U_obs = Ue_obs
        if self.n_dim == 2:
            U_obs = np.hstack((Ue_obs,Un_obs))
        if self.n_dim == 3:
            U_obs = np.hstack((Ue_obs,Un_obs,Uz_obs))
        print(np.shape(A))
        print(len(np.shape(A)))
        if len(np.shape(A)) == 1:
            A=A.reshape((len(A),1))
            
        if self.interface=='double':
           if  len(np.shape(B)) == 1:
                B=B.reshape((len(B),1))
            

        print(np.shape(A))
        if not self.condicion_borde_fosa:
            ceros = np.zeros(nx*ny)
            delta = U_obs
            stack = lambda x,l1,l2: np.vstack((l1*x[0],l2*x[1]))
            if self.interface =='single':
                if regul_kind == 'min_smooth':
                    r_matrix = stack(regul_array,self.lmbd1,self.lmbd2)
                    A_m= np.vstack((A,r_matrix))
                    Uobs_m = np.hstack((delta,ceros,ceros))
                if regul_kind == 'minimize':
                    r_matrix = 0.25*regul_array
                    A_m= np.vstack((A,r_matrix))
                    Uobs_m = np.hstack((delta,ceros))
                    print('shape')
                    print(A_m)
                    print(Uobs_m)
                    print(np.shape(A_m))
                    print(np.shape(Uobs_m))
                if regul_kind == 'smooth':
                    r_matrix  = self.lmbd2*regul_array
                    A_m= np.vstack((A,r_matrix))
                    Uobs_m = np.hstack((delta,ceros))
                    print('shape')
                    print(np.shape(A_m))
                    print(np.shape(Uobs_m))
                if regul_kind == 'none':
                    A_m=A
                    Uobs_m = U_obs
                    print(np.shape(A_m))
                    print(np.shape(Uobs_m))


            if self.interface == 'double':
                if regul_kind == 'min_smooth':

                    print('shapes')
                    print(np.mean(A),np.mean(B))
                    A_m=np.vstack((A,self.lmbd1*I,self.lmbd2*F,F*0))
                    B_m=np.vstack((B,self.lmbd3*I,F*0,self.lmbd4*F))
                    G_m=np.hstack((A_m,B_m))
                    Uobs_m=np.hstack((delta,ceros,ceros,ceros))
                    print('A','B')
                    print(A,B)
                    print(U_obs)
                    print('end')
                    # r1= stack(regul_array,self.lmbd1,self.lmbd2)
                    # A_m= np.vstack((A,r1))
                    # r2= stack(regul_array,self.lmbd3,self.lmbd4)
                    # B_m=np.vstack((B,r2))
                    # Uobs_m = np.hstack((delta,ceros,ceros,ceros))
                    # G_m=np.hstack(A_m,B_m)
                    # print('shape')
                    # print(G_m)
                    #print(Uobs_m)
                    #print(np.shape(A_m))
                    #print(np.shape(Uobs_m))
                if regul_kind == 'minimize':
                    A_m= np.vstack((A,self.lmbd1*I))
                    B_m=np.vstack((B,self.lmbd3*I))
                    G_m=np.hstack((A_m,B_m))
                    Uobs_m = np.hstack((delta,ceros))
                if regul_kind == 'smooth':
                    r_matrix  = self.lmbd2*regul_array
                    A_m= np.vstack((A,r_matrix))
                    Uobs_m = np.hstack((delta,ceros))
                    print('shape')
                    print(np.shape(A_m))
                    print(np.shape(Uobs_m))
                if regul_kind == 'none':
                    print('lol')
                    print(np.shape(A),np.shape(B))
                    A_m=A
                    B_m=B
                    G_m=np.hstack((A_m,B_m))
                    Uobs_m = U_obs
                    print(np.shape(A_m))
                    print(np.shape(Uobs_m))


               

            # if interfases =='double':
            #     if regul_kind == 'min_smooth':
            #         r_matrix = stack(regularray,self.lmbd1,self.lmbd2)
            #         Uobs_m = np.hstack((delta,ceros,ceros))
            #     if regul_kind == 'minimize':
            #         r_matrix = self.lmbd1*regul_array
            #         Uobs_m = np.hstack((delta,ceros))
            #     if regul_kind == 'smooth':
            #         r_matrix  = self.lmbd2*regul_array
            #         ceros_matrix = np.vstack((ceros))
            #         Uobs_m = np.hstack((delta,ceros))




        # if not self.condicion_borde_fosa :
        #     ## coseismic, only A and B(if apply) contribute to the deformation
        #     ceros = np.zeros(nx*ny)
        #     ## solo contribuye las fallas A y B
        #     #delta = U_obs - (np.dot(C,Vp_CD) + np.dot(D,Vp_CD) + np.dot(E,Vp_E))
        #     delta = U_obs
        #     stackear = lambda x,y: np.vstack((x,y)) 


        #     ## our system is Gm= U, we build G and u
            
         
        #     if interfases == 'double':
        #         ## if smoothing matrix has been built:
        #         if self.makeF:
        #             A_m=np.hstack((A,self.lmbd1*I,self.lmbd2*F,0))
        #             B_m=np.hstack((B,self.lmbd3*I,0,self.lmbd4*F))
        #             Uobs_m=np.hstack((delta,ceros,ceros,ceros))
        #         ## if no smooth matrix
        #         if not self.makeF:
        #             A_m=np.hstack((A,self.lmbd1*I))
        #             B_m=np.hstack((B,self.lmbd3*I))
        #             Uobs_m=np.hstack((delta,ceros))
        #     if interfases == 'single':
        #         if self.makeF:
        #             A_m=np.hstack((A,self.lmbd1*I,self.lmbd2*F))
        #             Uobs_m=np.hstack((delta,ceros,ceros))
        #         if not self.makeF:
        #             print(np.shape(A),np.shape(I))
        #             try:

        #                 A_m=np.vstack((A,0.02*I))
        #                 Uobs_m=np.hstack((delta,ceros))
        #             except ValueError:
        #                 print('inverting only one subfault, must reshape matrix')
        #                 A=A.reshape((len(A),1))
        #                 A_m=np.vstack((A,0.7*I))
        #                 Uobs_m=np.hstack((delta,ceros))


       


        else:
            #interseismic, will fix this later
            print('Setting the boundary condition in the trench ')
             #Condicion de borde lateral (la de las velocidades intersismicas)
       
            g = []
            for jy in range(ny):
                for ix in range(nx):
                    if jy == (ny-1) or jy==0:
                        g.append(1)
                    else:
                        g.append(0)
                   
            g = np.array(g)
            G = np.zeros(nx*ny*nx*ny).reshape(nx*ny,nx*ny) 
            np.fill_diagonal(G,g) 
       
       
            V = np.ones(nx*ny)
            for i in range(nx*ny):
                 V[i] = self.velocidad_placa

      
       # '''
       # ###########################################################################
       # Se constuye igualdad del sistema de ecuaciones: Termino derecho
       # ###########################################################################
       # '''
            if self.interface == 'double':
                ceros   = np.zeros(nx*ny)
                print("C:",C,
                      "Vp:",Vp_CD,
                      "D:",D,
                      "E:",E,
                      "Vp_E:",Vp_E)
                delta   = U_obs - (np.dot(C,Vp_CD) + np.dot(D,Vp_CD) + np.dot(E,Vp_E))  
                #Uobs_m  = hstack((delta,ceros,ceros,ceros,ceros))  
                Uobs_m  = np.hstack((delta,ceros,ceros,ceros,V,V))  
   
       # '''
       # ###########################################################################
       # Se construye termino izquierdo del sistema de ecuaciones
       # ###########################################################################
       # '''
     

                A_m = np.vstack((A, self.lmbd1*I,self.lmbd2*F, F*0,G,G*0))  
                B_m = np.vstack((B, self.lmbd3*I,    F*0, self.lmbd4*F,G*0,G)) 

            if self.interface == 'single':
                ceros = np.zeros(nx*ny)
                delta = U_obs
                Uobs_m = np.hstack((delta,ceros))
                A_m=np.vstack((A,self.lmbd1*I))
        #print('B')
        #print(B)

        Ue_teo_total = None
        Un_teo_total = None
        Uz_teo_total = None
        ECM_Ue = None
        ECM_Un = None
        ECM_Uz = None
        horror_total = None
        horror_suave = None

        if self.interface == 'double':
            #AA=np.hstack((A_m,B_m))
            xx = nnls(G_m,Uobs_m)
            slip = np.array([xx[0]])[0]
            print('slip')
            print(slip)
            AB=np.hstack((A,B))
            horror = np.linalg.norm( np.dot(AB,slip) - U_obs) 
            horror_slip = np.linalg.norm( slip )
            if self.makeF:
                FF=np.hstack((F,F))
                horror_suave = dxi*np.linalg.norm( np.dot(FF,slip))

            ## making the direct problem with the inverted slips
            sl_oka_inv=[]
            sl_oka_norm=[]
            k = nx*ny

            for i in range(nx*ny):
                sl_oka_inv.append(slip[i])
                sl_oka_norm.append(slip[i+k])
            
            # para falla inversa
            Ue_teo_inv,Un_teo_inv,Uz_teo_inv=self.pdirecto(sl_oka_inv,plano='A')
            # para falla normal
            Ue_teo_norm,Un_teo_norm, Uz_teo_norm=self.pdirecto(sl_oka_norm,plano='B')

            Ue_teo_total=[x + y for x,y in zip(Ue_teo_inv,Ue_teo_norm) ]
            Un_teo_total=[x + y for x,y in zip(Un_teo_inv,Un_teo_norm) ] 
            Uz_teo_total=[x + y for x,y in zip(Uz_teo_inv,Uz_teo_norm) ]
            print(Ue_teo_total)
            print(Un_teo_total)

     
            ## getting the mean squared error (MSE)

            delta_Ue= [ x - y for x,y in zip(Ue_obs,Ue_teo_total) ]
            delta_Un= [ x - y for x,y in zip(Un_obs,Un_teo_total) ]

            ECM_Ue=sum([x**2 for x in delta_Ue])/len(delta_Ue)
            ECM_Un=sum([x**2 for x in delta_Un])/len(delta_Un)

            if self.n_dim == 3:    
                delta_Uz= [ x - y for x,y in zip(Uz_obs,Uz_teo_total) ]
                ECM_Uz=sum([x**2 for x in delta_Uz])/len(delta_Uz)

            ##  total residual
            if self.makeF:
                horror_total=horror + self.lmbd1*horror_slip + self.lmbd2*horror_suave        
            if not self.makeF:
                horror_total = horror + self.lmbd1*horror_slip
               

        if self.interface == 'single':
            AA=A_m
            print('bode')
            print(np.shape(Uobs_m))
            xx = scp.optimize.nnls(AA,Uobs_m)
            print(xx)
            slip = np.array([xx[0]])[0]
            print(slip)
            horror = np.linalg.norm( np.dot(A,slip) - U_obs) 
            horror_slip = np.linalg.norm( slip )
            if self.makeF:
                FF=F
                horror_suave = dxi*np.linalg.norm( np.dot(FF,slip) )  

            ## making direct problem with the inverted slip 
            Ue_teo_total,Un_teo_total,Uz_teo_total=self.pdirecto(slip,plano='A')

            delta_Ue= [ x - y for x,y in zip(Ue_obs,Ue_teo_total) ]
            delta_Un= [ x - y for x,y in zip(Un_obs,Un_teo_total) ]

            ECM_Ue=sum([x**2 for x in delta_Ue])/len(delta_Ue)
            ECM_Un=sum([x**2 for x in delta_Un])/len(delta_Un)

            if self.n_dim == 3:    
                delta_Uz= [ x - y for x,y in zip(Uz_obs,Uz_teo_total) ]
                ECM_Uz=sum([x**2 for x in delta_Uz])/len(delta_Uz)

            ## total residual
            if self.makeF:
                horror_total=horror + self.lmbd1*horror_slip + self.lmbd2*horror_suave
            if not self.makeF:
                horror_total=horror + self.lmbd1*horror_slip

        if self.proceso == 'coseismic':
            ## desplazamientos invertidos a partir de un problema directo
            self.outputs_inv.update({'slips_subfallas':slip,
                                            'horror': horror,
                                            'horror_slip': horror_slip,
                                            'horror_suave': horror_suave,
                                            'horror_total': horror_total,
                                            'UE': Ue_teo_total,
                                            'UN': Un_teo_total,
                                            'UU': Uz_teo_total,
                                            'ECM_Ue': ECM_Ue,
                                            'ECM_Un': ECM_Un,
                                            'ECM_Uz': ECM_Uz,
                                            })




    def load_slip_sint_fuentedoble(self,plano='upper',data='inversion',interface='single',cargar_datos=True):
        ## cargar el plano superior inverso si la interfaz es doble
        if plano == 'upper' and data == 'inversion' and interface == 'double':
            filename=self.upper_interface_output.replace('single','double')
            filename_params=self.desplaz_output.replace('generated.txt','inv_params.txt').replace('single','double')                                                      
            slip='slip_upper'
        ## cargar el plano inferior inverso si la interfaz es doble
        if plano == 'lower' and data == 'inversion' and interface == 'double':
            filename=self.lower_interface_output.replace('single','double')
            #filename_params=self.desplaz_output.replace('synthetic_data.txt','inv_params.txt')
            slip='slip_lower'
        ## cargar el plano superior inverso si la interfaz es single
        if plano == 'upper' and data == 'inversion' and interface == 'single':
            print('maggie')           
            filename=self.upper_interface_output
            filename_params=self.desplaz_output.replace('generated.txt',
                            'inv_params.txt')
            slip='slip_upper'
            print('fparsss',filename_params,filename)

        # if plano == 'lower' and data == 'inversion' and interface == 'double':
        #     filename=self.lower_interface_output.replace('single','double')
                    
        #     slip='slip_lower'

        
        # if plano == 'upper' and data == 'direct' and interface == 'single':
        #     filename = self.upper_interface_output.replace('inversion','direct')
        #     slip = 'slip_upper'

        ## cargar el plano superior directo si la interfaz es doble
        if plano == 'upper' and data == 'direct' and interface == 'double':
            filename = self.upper_interface_output.replace('inversion',
                                                   'direct').replace('single','double')
            slip = 'slip_upper'

        ## cargar el plano inferior directo si la interfaz es doble
        if plano == 'lower' and data == 'direct' and interface == 'double':
            filename = self.lower_interface_output.replace('inversion',
                                                   'direct').replace('single','double')
            slip = 'slip_lower'



        ## imprimir parametros de las fallas 
        data_type = { 
                     'Longitud': float,
                     'Latitud': float,
                      slip: float,
                     }
        #print('fparxx',filename,filename_params)
        data_slip = cut_file(filename,data_type)
        lon_fallas =[float(x.get('lon_central')) for x in data_slip]
        lat_fallas =[float(x.get('lat_central')) for x in data_slip]
        slips=[float(x.get(slip)) for x in data_slip]

        falla_params=dict(zip([slip,'lon_central','lat_central'],
                              [slips,lon_fallas,lat_fallas]))

        

        try:
            rows=[]
            with open(filename_params,'r') as csvfile:
                reader= csv.reader(csvfile,delimiter=';')
                for row in reader:
                    rows.append(row)

            bayes_params=dict(zip(rows[0],rows[1]))

            return falla_params, bayes_params

        
        except NameError:
            print('no es necesario cargar en lo parametros bayesianos!')
            return falla_params


    
    def load_slip_sint(self,plano='upper',data='inversion',interface='single',cargar_datos=True):


        # if not self.split_G:
        #     if plano == 'upper' and data == 'inversion' and interface == 'single':
        #         filename=self.upper_interface_output
        #         filename_params=self.desplaz_output.replace('synthetic_data.txt','inv_params.txt')
        #         slip='slip_upper'

        #     if plano == 'upper' and data == 'inversion' and interface == 'double':
        #         filename=self.upper_interface_output.replace('single','double')
        #         filename_params=self.desplaz_output.replace('synthetic_data.txt',
        #                         'inv_params.txt').replace('single','double')
        #         slip='slip_upper'
        #         #print('fpar',filename_params)

        #     if plano == 'lower' and data == 'inversion' and interface == 'double':
        #         filename=self.lower_interface_output.replace('single','double')
                        
        #         slip='slip_lower'

            
        #     if plano == 'upper' and data == 'direct' and interface == 'single':
        #         filename = self.upper_interface_output.replace('inversion','direct')
        #         slip = 'slip_upper'

        #     if plano == 'upper' and data == 'direct' and interface == 'double':
        #         filename = self.upper_interface_output.replace('inversion','direct').replace('single','double')
        #         slip = 'slip_upper'

        #     if plano == 'lower' and data == 'direct' and interface == 'double':
        #         filename = self.lower_interface_output.replace('inversion','direct').replace('single','double')
        #         slip = 'slip_lower'

        if not self.split_G:
            ## cargar los slips invertidos si la interfaz es single
            if plano == 'upper' and data == 'inversion' and interface == 'single':
                print('A')
                print(self.desplaz_output,'end')

                filename=self.upper_interface_output.replace('double','single')
                filename_params=self.desplaz_output.replace('generated.txt',
                                            'inv_params.txt').replace('double','single')
                slip='slip_upper'

            ## cargar los slips invertidos superiores si la interfaz es doble
            if plano == 'upper' and data == 'inversion' and interface == 'double':
                print('B')

                filename=self.upper_interface_output
                filename_params=self.desplaz_output.replace('generated.txt',
                                'inv_params.txt')
                slip='slip_upper'
                #print('fpar',filename_params)
            
            ## cargar los slips invertidos inferiores si la interfaz es doble
            if plano == 'lower' and data == 'inversion' and interface == 'double':
                filename=self.lower_interface_output
                slip='slip_lower'

            ## cargar los slips directos si la interfaz es single
            if plano == 'upper' and data == 'direct' and interface == 'single':
                filename = self.upper_interface_output.replace('inversion',
                                'direct').replace('double','single')
                slip = 'slip_upper'
            ## cargar los slips directos superiores si la interfaz es doble
            if plano == 'upper' and data == 'direct' and interface == 'double':
                filename = self.upper_interface_output.replace('inversion','direct')
                                    
                slip = 'slip_upper'
            ## cargar los slips directos inferiores si la interfaz es doble
            if plano == 'lower' and data == 'direct' and interface == 'double':
                filename = self.lower_interface_output.replace('inversion','direct')
                slip = 'slip_lower'

        ## para la division de las f de green
        if self.split_G:
            if plano =='upper' and data== 'direct' and interface == 'single':
                filename=self.upper_interface_output.replace('inversion','direct')
                slip=('slip_upp_strslip','slip_upp_dipslip')

            if plano =='upper' and data == 'inversion' and interface == 'single':
                filename=self.upper_interface_output
                filename_params=self.desplaz_output.replace('inverted.txt','inv_params.txt')
                slip=('slip_upp_strslip','slip_upp_dipslip')



        
        ## imprimir parametros de las fallas 
        if not self.split_G:
            data_type = { 
                         'Longitud': float,
                         'Latitud': float,
                          slip: float,
                         }
            #print('fparxx',filename,filename_params)
            data_slip = cut_file(filename,data_type)
            print('filename',filename)
            lon_fallas =[float(x.get('lon_central')) for x in data_slip]
            lat_fallas =[float(x.get('lat_central')) for x in data_slip]
            slips=[float(x.get(slip)) for x in data_slip]

            falla_params=dict(zip([slip,'lon_central','lat_central'],
                                  [slips,lon_fallas,lat_fallas]))
        if self.split_G:
            data_type = { 
                         'Longitud': float,
                         'Latitud': float,
                          slip[0]: float,
                          slip[1]: float

                         }
            #print('fparxx',filename,filename_params)
            data_slip = cut_file(filename,data_type)
            lon_fallas =[float(x.get('lon_central')) for x in data_slip]
            lat_fallas =[float(x.get('lat_central')) for x in data_slip]
            slips_ss=[float(x.get(slip[0])) for x in data_slip]
            slips_ds=[float(x.get(slip[1])) for x in data_slip]

            falla_params=dict(zip([slip[0],slip[1],'lon_central','lat_central'],
                                  [slips_ss,slips_ds,lon_fallas,lat_fallas]))

        
        ## no me acuerdo para que es esto lol
        try:
            rows=[]
            print('cardio',filename_params)
            with open(filename_params,'r') as csvfile:
                reader= csv.reader(csvfile,delimiter=';')
                for row in reader:
                    rows.append(row)

            bayes_params=dict(zip(rows[0],rows[1]))

            return falla_params, bayes_params

        
        except NameError:
            print('no es necesario cargar en lo parametros bayesianos!')
            return falla_params
        # data_type = { 
        #              'log_evi': float,
        #              'bic': float,
        #              'ecm': float,
        #              'alpha': float
        #              }
        # data_slip = cut_file(filename,data_type)
        # lon_fallas =[float(x.get('lon_central')) for x in data_slip]
        # lat_fallas =[float(x.get('lat_central')) for x in data_slip]
        # slips=[float(x.get(slip)) for x in data_slip]
        # #slip_array=np.array(slips)[::-1].reshape(self.grid_size[0],self.grid_size[1])
        # return lon_fallas,lat_fallas,slips
        # print(slip_array)
        # plt.matshow(slip_array)
        # plt.colorbar()


    def cargar_datosinv(self,inversion='single'):
        ## imprimir observaciones
        data_type = { 
                     'Latitud': float,
                     'Longitud': float,
                     'UE':  float,
                     'UN':  float,
                     'UU':  float}
        if inversion =='single':
            data_inv = cut_file(self.desplaz_output.replace('generated.txt','inverted.txt'),data_type)
            print('dslip',data_inv)

        if inversion =='double':
            data_inv = cut_file(self.desplaz_output.replace('generated.txt','inverted.txt').replace('single','double'),data_type)
            print('dslip',data_inv)
        return data_inv

    
    def plot_slips_arrows_fuentedoble(self,load_syntethic_slips=True):
        nx=self.grid_size[0]
        ny=self.grid_size[1]

        ## cargar la inversion de una sola interfaz
        print('a')
        slip_uppersingleinv,params_uppersingleinv=self.load_slip_sint_fuentedoble(plano='upper',
                                                                      data='inversion',
                                                                      interface='single')

        print('b')
        ## cargar la inversion de las dos interfases
        slip_upperdoubleinv,params_upperdoubleinv=self.load_slip_sint_fuentedoble(plano='upper',
                                                                      data='inversion',
                                                                      interface='double')
        slip_lowerdoubleinv=self.load_slip_sint_fuentedoble(plano='lower',
                                                data='inversion',
                                                interface='double')
        
        ## cargar slips sinteticos
        if load_syntethic_slips:
            
            slip_upperdoubledirect=self.load_slip_sint_fuentedoble(plano='upper',
                                                       data='direct',
                                                       interface='double')

            slip_lowerdoubledirect=self.load_slip_sint_fuentedoble(plano='lower',
                                                       data='direct',
                                                       interface='double')

            print('as',slip_upperdoubledirect)
            slip_upperdoubledirect=np.array(slip_upperdoubledirect['slip_upper'][::-1]).reshape(ny,nx).T
            slip_lowerdoubledirect=np.array(slip_lowerdoubledirect['slip_lower'][::-1]).reshape(ny,nx).T


        ## cargar data invertida
        data_singleinv=self.cargar_datosinv(inversion='single')
        data_doubleinv=self.cargar_datosinv(inversion='double')


        ## cargar data real
        lat=[elem.get('Latitud') for elem in self.data_estaciones]
        lon=[elem.get('Longitud') for elem in self.data_estaciones]
        Ue_obs=[elem.get('UE') for elem in self.data_estaciones]
        Un_obs=[elem.get('UN') for elem in self.data_estaciones]



        Ue_invsingle=[elem.get('UE') for elem in data_singleinv]
        Un_invsingle=[elem.get('UN') for elem in data_singleinv]

        Ue_invdouble=[elem.get('UE') for elem in data_doubleinv]
        Un_invdouble=[elem.get('UN') for elem in data_doubleinv]

        print('xD',Ue_invsingle,Ue_invdouble)
        # slip_both_faultss,params_both_faultss=self.load_slip_sint(plano='lower',
        #                                                       data='inversion',
        #                                                       interface='double')

        print('qq',slip_uppersingleinv,params_uppersingleinv)
        print('kk',slip_upperdoubleinv,params_upperdoubleinv)
        print('dd',slip_lowerdoubleinv)

        slip_uppersingleinv=np.array(slip_uppersingleinv['slip_upper'][::-1]).reshape(ny,nx).T
        slip_lowersingleinv=np.zeros((ny,nx)).T
        slip_upperdoubleinv=np.array(slip_upperdoubleinv['slip_upper'][::-1]).reshape(ny,nx).T
        slip_lowerdoubleinv=np.array(slip_lowerdoubleinv['slip_lower'][::-1]).reshape(ny,nx).T



        print('slips')
        print(slip_uppersingleinv,slip_lowersingleinv,slip_upperdoubleinv,slip_lowerdoubleinv)
        print('capa',slip_upperdoubledirect,slip_lowerdoubledirect)

        # data=[slip_upperdoubledirect,
        #       slip_lowerdoubledirect,
        #       slip_uppersingleinv,
        #       slip_lowersingleinv,
        #       slip_upperdoubleinv,
        #       slip_lowerdoubleinv]

        data = np.array([[slip_upperdoubledirect,slip_uppersingleinv,slip_upperdoubleinv],
                         [slip_lowerdoubledirect,slip_lowersingleinv,slip_lowerdoubleinv]])
        titulos=np.array([['Directo Thrust',
                 'Inverso Thrust Single','Inverso Thrust Double'],
                 ['Directo Normal',
                 'Inverso Normal Single','Inverso Normal Double']])
        
        print(titulos.shape)
        Nr = 2
        Nc = 4
        cmap='cool'
        fig, axs = plt.subplots(nrows=Nr, ncols=Nc, figsize=(3, 6))
        print('fig',fig,axs[:,:3],'axs',axs,'axsmap',axs[:,3])
        matrixes = []
        bic_single=str(int(float(params_uppersingleinv['bic'])))
        log_evi_single=str(int(float(params_uppersingleinv['log_evi'])))
        ecm_single = "{0:.4f}".format(float(params_uppersingleinv['ecm']))

        bic_double=str(int(float(params_upperdoubleinv['bic'])))
        log_evi_double=str(int(float(params_upperdoubleinv['log_evi'])))
        ecm_double = "{0:.4f}".format(float(params_upperdoubleinv['ecm']))

        #print('bic',bic,log_evi)
        for i in range(Nr):
            for j in range(Nc):
                if j != 3:
                    matrixes.append(axs[i,j].imshow(data[i,j],cmap=cmap))
                    #axs[i,j].label_outer()
                    axs[i,j].set_title(titulos[i,j])

                #axs[i,j].set_visible(False)                
        vmin = min(matrix.get_array().min() for matrix in matrixes)
        vmax = max(matrix.get_array().max() for matrix in matrixes)
        norm = colors.Normalize(vmin=vmin,vmax=vmax)
        for matrix in matrixes:
            matrix.set_norm(norm)

        fig.colorbar(matrixes[0],ax = axs[:,:3], orientation = 'vertical').set_label('slip (m)')

        fig.suptitle('inversion single: '+' bic= '+bic_single +' log evi= ' +log_evi_single+' ecm= ' 
                     +ecm_single+ '\n' + 'inversion double: '+' bic= '+bic_double +' log evi= ' +log_evi_double+' ecm= ' 
                     +ecm_double,fontsize='xx-large')
    
        
        # ## fig 6 map of the zone and the fault
        #axsmap=axs[:,3]
        #lat_fosa=[elem.get('Latitud') for elem in self.data_falla.get('fosa')]
        #lon_fosa=[elem.get('Longitud') for elem in self.data_falla.get('fosa')]
        lon_fallas_ab=self.subfallas_model_dict.get('lon_vertices')
        lat_fallas_ab=self.subfallas_model_dict.get('lat_vertices')
        lon_fallas_ab=self.coord_fallas[0]
        lat_fallas_ab=self.coord_fallas[1]


        ## SUBPLOT 6: MAP OF THE ZONE AND SUBFAULTS
        
        gs=axs[0,3].get_gridspec()
        print(gs)
        for ax in axs[:,3]:
            ax.remove()

        axbig=fig.add_subplot(gs[:,3])

        axsbig = Basemap(**self.map_params)               
        axsbig.drawcoastlines()
        axsbig.drawcountries()
        #Se trazan los paralelos
        axsbig.drawparallels(np.arange(-90, 90, 5),
                               labels=[1, 1, 1, 1])
        axsbig.drawmeridians(np.arange(-90, 90, 5),
                               labels=[0, 0, 0, 1])      

           
        x,y = axsbig(lon,lat)
        axsbig.plot(x,y,'go',markersize=3)
        axsbig.quiver(x,y,Ue_obs,Un_obs,color='k')
        plt.title('Mapa y estaciones de la Zona')


        axsbig = Basemap(**self.map_params)               
        axsbig.drawcoastlines()
        axsbig.drawcountries()
        #Se trazan los paralelos.
        axsbig.drawparallels(np.arange(-90, 90, 5),
                               labels=[1, 1, 1, 1])
        axsbig.drawmeridians(np.arange(-90, 90, 5),
                               labels=[0, 0, 0, 1])      

           
        x,y = axsbig(lon,lat)
        axsbig.plot(x,y,'g^',markersize=6)
        axsbig.quiver(x,y,Ue_obs,Un_obs,color='k',label='obs')
        axsbig.quiver(x,y,Ue_invsingle,Un_invsingle,color='r',label='inv1interf')
        axsbig.quiver(x,y,Ue_invdouble,Un_invdouble,color='m',label='inv2interf')
        plt.legend()

        
        plt.title('Mapa y estaciones de la Zona')

        #print('axs',type(axs[0,3]),type(axs[1,3]),axs.shape)


        # ## SUBPLOT 6: MAP OF THE ZONE AND SUBFAULTS
        # #gs=axs[]
        # axs[0,3] = Basemap(**self.map_params)               
        # axs[0,3].drawcoastlines()
        # axs[0,3].drawcountries()
        # #Se trazan los paralelos.
        # axs[0,3].drawparallels(np.arange(-90, 90, 5),
        #                        labels=[1, 1, 1, 1])
        # axs[0,3].drawmeridians(np.arange(-90, 90, 5),
        #                        labels=[0, 0, 0, 1])      

           
        # x,y = axs[0,3](lon,lat)
        # axs[0,3].plot(x,y,'go',markersize=3)
        # axs[0,3].quiver(x,y,Ue_obs,Un_obs,color='k')
        # plt.title('Mapa y estaciones de la Zona')


        # axs[1,3] = Basemap(**self.map_params)               
        # axs[1,3].drawcoastlines()
        # axs[1,3].drawcountries()
        # #Se trazan los paralelos.
        # axs[1,3].drawparallels(np.arange(-90, 90, 5),
        #                        labels=[1, 1, 1, 1])
        # axs[1,3].drawmeridians(np.arange(-90, 90, 5),
        #                        labels=[0, 0, 0, 1])      

           
        # x,y = axs[1,3](lon,lat)
        # axs[1,3].plot(x,y,'go',markersize=3)
        # axs[1,3].quiver(x,y,Ue_obs,Un_obs,color='r')
        # axs[1,3].quiver(x,y,Ue_invsingle,Un_invsingle,color='g')
        

        # print('axs',type(axs[0,3]),type(axs[1,3]),axs.shape)
        # plt.title('Mapa y estaciones de la Zona')


      # geometria de la zona: ver después
      # lat_fosa=[elem.get('Latitud') for elem in self.data_falla.get('fosa')]
      # lon_fosa=[elem.get('Longitud') for elem in self.data_falla.get('fosa')]
      # x,y=axs[1,2](lon_fosa,lat_fosa)
        #axs[0,3].plot(x,y,color='r',linewidth='2')

        # lon_fallas_ab=self.subfallas_model_dict.get('lon_vertices')
        # lat_fallas_ab=self.subfallas_model_dict.get('lat_vertices')
        # lon_fallas_ab=self.coord_fallas[0]
        # lat_fallas_ab=self.coord_fallas[1]
        # print('quaker',lon_fallas_ab,lat_fallas_ab)
        # for lonx,laty in zip(lon_fallas_ab,lat_fallas_ab):
        #     x,y=axs[0,3](lonx,laty)
        #     axs[1,2].plot(x,y,color='b',  linestyle = 'o', linewidth = 0.15,zorder=21)
        plt.title('Mapa y estaciones de la Zona')


        # print('axsmap',axsmap)
        # for i in range(len(axsmap)):

        #     axsmap[i] = Basemap(**self.map_params)               
        #     axsmap[i].drawcoastlines()
        #     axsmap[i].drawcountries()
        #     #Se trazan los paralelos.
        #     axsmap[i].drawparallels(
        #     np.arange(-90, 90, 5),
        #     labels=[1, 1, 1, 1])
        #     axsmap[i].drawmeridians(
        #     np.arange(-90, 90, 5),
        #     labels=[0, 0, 0, 1])      

        #     # lon = [elem.get('Longitud') for elem in self.data_estaciones]
        #     # lat = [elem.get('Latitud') for elem in self.data_estaciones]
        #     x,y = axsmap[i](lon,lat)
        #     axsmap[i].plot(x,y,'go',markersize=10)
        #     plt.title('Mapa y estaciones de la Zona')
        #     print('i',i,axsmap[i])

            
            #x,y=axsmap[i](lon_fosa,lat_fosa)
            #axsmap[i].plot(x,y,color='r',linewidth='2')
            ## plotear las subfallas o la zona de falla (not yet)
            # print('quaker',lon_fallas_ab,lat_fallas_ab)
            # for lonx,laty in zip(lon_fallas_ab,lat_fallas_ab):
            #     x,y=axsmap[i](lonx,laty)
            #     axsmap[i].plot(x,y,color='b',linewidth=0.2,zorder=21)

    def plot_slips_arrows_fuentesingle(self,load_syntethic_slips=True):
        nx=self.grid_size[0]
        ny=self.grid_size[1]

        ## cargar la inversion de una sola interfaz

        slip_uppersingleinv,params_uppersingleinv=self.load_slip_sint(plano='upper',
                                                                      data='inversion',
                                                                      interface = 'single')


        ## cargar la inversion de las dos interfases
        slip_upperdoubleinv,params_upperdoubleinv=self.load_slip_sint(plano='upper',
                                                                      data='inversion',
                                                                      interface='double')
        slip_lowerdoubleinv=self.load_slip_sint(plano='lower',
                                                data='inversion',
                                                interface='double')
        
        ## cargar slips sinteticos
        if load_syntethic_slips:
            slip_uppersingledirect=self.load_slip_sint(plano='upper',
                                                       data='direct',
                                                       interface='single')
            print('slip_uppersing',slip_uppersingledirect['slip_upper'][::-1])
            #slip_uppersingledirect[::-1]
            slip_lowersingledirect = np.zeros((nx,ny))
            # slip_upperdoubledirect=self.load_slip_sint(plano='upper',
            #                                            data='direct',
            #                                            interface='double')

            # slip_lowerdoubledirect=self.load_slip_sint(plano='lower',
            #                                            data='direct',
            #                                            interface='double')


            slip_uppersingledirect=np.array(slip_uppersingledirect['slip_upper'][::-1]).reshape(ny,nx).T
            # slip_upperdoubledirect=np.array(slip_upperdoubledirect['slip_upper']).reshape(nx,ny)
            #slip_lowerdoubledirect=np.array(slip_lowerdoubledirect['slip_lower']).reshape(nx,ny)


        ## cargar data invertida
        data_singleinv=self.cargar_datosinv(inversion='single')
        data_doubleinv=self.cargar_datosinv(inversion='double')


        ## cargar data real
        lat=[elem.get('Latitud') for elem in self.data_estaciones]
        lon=[elem.get('Longitud') for elem in self.data_estaciones]
        Ue_obs=[elem.get('UE') for elem in self.data_estaciones]
        Un_obs=[elem.get('UN') for elem in self.data_estaciones]



        Ue_invsingle=[elem.get('UE') for elem in data_singleinv]
        Un_invsingle=[elem.get('UN') for elem in data_singleinv]

        Ue_invdouble=[elem.get('UE') for elem in data_doubleinv]
        Un_invdouble=[elem.get('UN') for elem in data_doubleinv]

        print('xD',Ue_invsingle,Ue_invdouble)
        # slip_both_faultss,params_both_faultss=self.load_slip_sint(plano='lower',
        #                                                       data='inversion',
        #                                                       interface='double')

        print('qq',slip_uppersingleinv,params_uppersingleinv)
        print('kk',slip_upperdoubleinv,params_upperdoubleinv)
        print('dd',slip_lowerdoubleinv)
        print('mm',slip_uppersingledirect)

        slip_uppersingleinv=np.array(slip_uppersingleinv['slip_upper'][::-1]).reshape(ny,nx).T
        slip_lowersingleinv=np.zeros((ny,nx)).T
        slip_upperdoubleinv=np.array(slip_upperdoubleinv['slip_upper'][::-1]).reshape(ny,nx).T
        slip_lowerdoubleinv=np.array(slip_lowerdoubleinv['slip_lower'][::-1]).reshape(ny,nx).T



        print('slips')
        print(slip_uppersingleinv,slip_lowersingleinv,slip_upperdoubleinv,slip_lowerdoubleinv)
       
        data = np.array([[slip_uppersingledirect,slip_uppersingleinv,slip_upperdoubleinv],
                         [slip_lowersingledirect,slip_lowersingleinv,slip_lowerdoubleinv]])
        titulos=np.array([['Input Thrust',
                 'Inverso Thrust Single','Inverso Thrust Double'],
                 ['Input Normal',
                 'Inverso Normal Single','Inverso Normal Double']])
        
        print(titulos.shape)
        Nr = 2
        Nc = 4
        cmap='cool'
        fig, axs = plt.subplots(nrows=Nr, ncols=Nc, figsize=(3, 6))
        print('data',data,fig,axs)
        matrixes = []
        bic_single=str(int(float(params_uppersingleinv['bic'])))
        log_evi_single=str(int(float(params_uppersingleinv['log_evi'])))
        ecm_single = "{0:.4f}".format(float(params_uppersingleinv['ecm']))

        bic_double=str(int(float(params_upperdoubleinv['bic'])))
        log_evi_double=str(int(float(params_upperdoubleinv['log_evi'])))
        ecm_double = "{0:.4f}".format(float(params_upperdoubleinv['ecm']))

        #print('bic',type(bic_single),type(log_evi_single))
        for i in range(Nr):
            for j in range(Nc):
                if j !=3:
                    matrixes.append(axs[i,j].imshow(data[i,j],cmap=cmap))
                    axs[i,j].label_outer()
                    axs[i,j].set_title(titulos[i,j])

                #axs[i,j].set_visible(False)                
        vmin = min(matrix.get_array().min() for matrix in matrixes)
        vmax = max(matrix.get_array().max() for matrix in matrixes)
        norm = colors.Normalize(vmin=vmin,vmax=vmax)
        for matrix in matrixes:
            matrix.set_norm(norm)

        fig.colorbar(matrixes[0],ax = axs[:,:3], orientation = 'vertical').set_label('slip (m)')
        fig.suptitle('inversion single: '+' bic= '+bic_single +' log evi= ' +log_evi_single+' ecm= ' 
                     +ecm_single+ '\n' + 'inversion double: '+' bic= '+bic_double +' log evi= ' +log_evi_double+' ecm= ' 
                     +ecm_double,fontsize='xx-large')


        ## SUBPLOT 6: MAP OF THE ZONE AND SUBFAULTS
        
        gs=axs[0,3].get_gridspec()
        print(gs)
        for ax in axs[:,3]:
            ax.remove()

        axbig=fig.add_subplot(gs[:,3])

        axsbig = Basemap(**self.map_params)               
        axsbig.drawcoastlines()
        axsbig.drawcountries()
        #Se trazan los paralelos
        axsbig.drawparallels(np.arange(-90, 90, 5),
                               labels=[1, 1, 1, 1])
        axsbig.drawmeridians(np.arange(-90, 90, 5),
                               labels=[0, 0, 0, 1])      

           
        x,y = axsbig(lon,lat)
        axsbig.plot(x,y,'go',markersize=3)
        axsbig.quiver(x,y,Ue_obs,Un_obs,color='k')
        plt.title('Mapa y estaciones de la Zona')


        axsbig = Basemap(**self.map_params)               
        axsbig.drawcoastlines()
        axsbig.drawcountries()
        #Se trazan los paralelos.
        axsbig.drawparallels(np.arange(-90, 90, 5),
                               labels=[1, 1, 1, 1])
        axsbig.drawmeridians(np.arange(-90, 90, 5),
                               labels=[0, 0, 0, 1])      

           
        x,y = axsbig(lon,lat)
        axsbig.plot(x,y,'g^',markersize=6)
        axsbig.quiver(x,y,Ue_obs,Un_obs,color='k',label='obs')
        axsbig.quiver(x,y,Ue_invsingle,Un_invsingle,color='r',label='inv1interf')
        axsbig.quiver(x,y,Ue_invdouble,Un_invdouble,color='m',label='inv2interf')
        plt.legend()

        
        plt.title('Mapa y estaciones de la Zona')




    def plot_slips_arrows_dsyss(self,load_syntethic_slips=True):
        nx=self.grid_size[0]
        ny=self.grid_size[1]

        ## cargar la inversion de una sola interfaz

        slip_uppersingleinv,params_uppersingleinv=self.load_slip_sint(plano='upper',
                                                                      data='inversion',
                                                                      interface='single')


        
        ## cargar slips sinteticos
        if load_syntethic_slips:
            slip_uppersingledirect=self.load_slip_sint(plano='upper',
                                                       data='direct',
                                                       interface='single')
            
            slip_uppss_direct=np.array(slip_uppersingledirect['slip_upp_strslip'][::-1]).reshape(ny,nx).T
            slip_uppds_direct=np.array(slip_uppersingledirect['slip_upp_dipslip'][::-1]).reshape(ny,nx).T



        ## cargar data invertida
        data_singleinv=self.cargar_datosinv(inversion='single')

        Ue_invsingle=[elem.get('UE') for elem in data_singleinv]
        Un_invsingle=[elem.get('UN') for elem in data_singleinv]

       

        ## cargar data real
        lat=[elem.get('Latitud') for elem in self.data_estaciones]
        lon=[elem.get('Longitud') for elem in self.data_estaciones]
        Ue_obs=[elem.get('UE') for elem in self.data_estaciones]
        Un_obs=[elem.get('UN') for elem in self.data_estaciones]



        

        print('xD',Ue_invsingle)
    
        print('qq',slip_uppersingleinv,params_uppersingleinv)

        slip_uppss_invert=np.array(slip_uppersingleinv['slip_upp_strslip'][::-1]).reshape(ny,nx).T
        slip_uppds_invert=np.array(slip_uppersingleinv['slip_upp_dipslip'][::-1]).reshape(ny,nx).T



        print('slips')
        #print(slip_uppss_invert,slip_uppds_invert,slip_uppss_direct,slip_uppds_direct)
        print('slipsclose')
        if load_syntethic_slips:
            data = np.array([[slip_uppss_direct,slip_uppss_invert],
                             [slip_uppds_direct,slip_uppds_invert]])
            titulos=np.array([['Input Strike Slip',
                                'Inversión Strike Slip'],
                                ['Input Dip Slip',
                                 'Inversión Dip  Slip']])
        if not load_syntethic_slips:
            data = np.array([[slip_uppss_invert],
                             [slip_uppds_invert]])
            titulos=np.array([['Inversión Strike Slip'],
                                ['Inversión Dip  Slip']])
        
        print(titulos.shape)
        if load_syntethic_slips:
            Nr = 2
            Nc = 3
        if not load_syntethic_slips:
            Nr = 2
            Nc = 2
        cmap='cool'
        fig, axs = plt.subplots(nrows=Nr, ncols=Nc, figsize=(3, 6))
        print('data',data,fig,axs)
        matrixes = []
        bic_single=str(int(float(params_uppersingleinv['bic'])))
        try:
            log_evi_single=str(int(float(params_uppersingleinv['log_evi'])))
        except OverflowError:
                print('overflow error')
                log_evi_single=str(0)
        ecm_single = "{0:.4f}".format(float(params_uppersingleinv['ecm']))

        # bic_double=str(int(float(params_upperdoubleinv['bic'])))
        # log_evi_double=str(int(float(params_upperdoubleinv['log_evi'])))
        # ecm_double = "{0:.4f}".format(float(params_upperdoubleinv['ecm']))

        #print('bic',type(bic_single),type(log_evi_single))
        for i in range(Nr):
            for j in range(Nc):
                if j != Nc-1:
                    matrixes.append(axs[i,j].imshow(data[i,j],cmap=cmap))
                    axs[i,j].label_outer()
                    axs[i,j].set_title(titulos[i,j])

                #axs[i,j].set_visible(False)                
        vmin = min(matrix.get_array().min() for matrix in matrixes)
        vmax = max(matrix.get_array().max() for matrix in matrixes)
        norm = colors.Normalize(vmin=vmin,vmax=vmax)
        for matrix in matrixes:
            matrix.set_norm(norm)

        fig.colorbar(matrixes[0],ax = axs[:,:Nc-1], orientation = 'vertical').set_label('slip (m)')
        fig.suptitle('inversion single: '+' bic= '+bic_single +' log evi= ' +log_evi_single+' ecm= ' 
                     +ecm_single,fontsize='xx-large')


        ## SUBPLOT 6: MAP OF THE ZONE AND SUBFAULTS
        
        gs=axs[0,Nc-1].get_gridspec()
        print(gs)
        for ax in axs[:,Nc-1]:
            ax.remove()

        axbig=fig.add_subplot(gs[:,Nc-1])

        axsbig = Basemap(**self.map_params)               
        axsbig.drawcoastlines()
        axsbig.drawcountries()
        #Se trazan los paralelos
        axsbig.drawparallels(np.arange(-90, 90, 5),
                               labels=[1, 1, 1, 1])
        axsbig.drawmeridians(np.arange(-90, 90, 5),
                               labels=[0, 0, 0, 1])      

           
        x,y = axsbig(lon,lat)
        axsbig.plot(x,y,'go',markersize=3)
        axsbig.quiver(x,y,Ue_obs,Un_obs,color='k')
        plt.title('Mapa y estaciones de la Zona')


        axsbig = Basemap(**self.map_params)               
        axsbig.drawcoastlines()
        axsbig.drawcountries()
        #Se trazan los paralelos.
        axsbig.drawparallels(np.arange(-90, 90, 5),
                               labels=[1, 1, 1, 1])
        axsbig.drawmeridians(np.arange(-90, 90, 5),
                               labels=[0, 0, 0, 1])      

           
        x,y = axsbig(lon,lat)
        axsbig.plot(x,y,'g^',markersize=6)
        axsbig.quiver(x,y,Ue_obs,Un_obs,color='k',label='simulated')
        axsbig.quiver(x,y,Ue_invsingle,Un_invsingle,color='r',label='inverted')
        plt.legend()

        
        plt.title('Mapa y estaciones de la Zona')


    def load_vectores(self,interfaz='single'):
        data_inv = self.data_sintetica
        

        #file_inversion=
        data_type= {'Longitud': float,
                    'Latitud': float,
                    'UE': float,
                    'UN': float,
                    'UU': float
                    }

        data_obs= cutfile(self.fuente_estaciones,data_type)
        data_model = cutfile()
    def plot_vectores(self):
        
        fig = plt.figure()
        fig.quiver(lon,lat,Ue_inv,Un_inv)
        fig.show()
        return fig

         


    def almacenar_resultados(self,**kwargs):

        interfases=kwargs.get('interfases','double')
        data_output = self.outputs_inv

        lon_sta = [elem.get('Longitud') for elem in self.data_estaciones]
        lat_sta = [elem.get('Latitud') for elem in self.data_estaciones]
        name_sta = [elem.get('Station') for elem in self.data_estaciones]

        Ue_inv=data_output.get('UE')
        Ue_inv=[1000*x for  x in Ue_inv]
        Un_inv=data_output.get('UN')
        Un_inv=[1000*x for  x in Un_inv]
        Uz_inv=data_output.get('UU')
        Uz_inv=[1000*x for  x in Uz_inv]


        data_sta={}
        data_sta.update({'Latitud':lat_sta})
        data_sta.update({'Longitud': lon_sta})
        data_sta.update({'Station': name_sta})

      
        merged_dict={**data_sta,**data_output}
        
        subfallas_methods = self.planos_falla_obj.get('subfallas_methods')

        # prof_subf_inv=subfallas_methods[0]
        # dip_subf_rad=subfallas_methods[1]
        # strike_subf_deg=subfallas_methods[2]
        # lonv_vert_subf_slab=subfallas_methods[3]
        # lat_vert_subf_slab=subfallas_methods[4]
        # lon_cent_subf_slab=subfallas_methods[5]
        # lat_cent_subf_slab=subfallas_methods[6]
        # prof_subf_norm=subfallas_methods[7]
        # phi_placa_rad=subfallas_methods[8]
        # rake_inv_rad=subfallas_methods[9]
        # rake_norm_rad=subfallas_methods[10]

        prof_subf_inv=self.subfallas_model_dict.get('depth_model')
        strike_subf_deg=self.subfallas_model_dict.get('strike_model')
        dip_subf_rad=np.radians(self.subfallas_model_dict.get('dip_model'))
        lonv_vert_subf_slab=self.subfallas_model_dict.get('lon_vertices')
        lat_vert_subf_slab=self.subfallas_model_dict.get('lat_vertices')
        lon_cent_subf_slab=self.subfallas_model_dict.get('lon_central')
        lat_cent_subf_slab=self.subfallas_model_dict.get('lat_central')
        strike_rad=self.subfallas_model_dict.get('strike_rad')
        prof_subf_norm=prof_subf_inv+self.plate_thickness/np.cos(dip_subf_rad)
        rake_inv_rad=self.subfallas_model_dict.get('rake_inv_rad')
        rake_norm_rad=self.subfallas_model_dict.get('rake_norm_rad')
        print('lala')
        print(prof_subf_inv)

        print(' -----------------------------------------------------------------------')
        print('Resumen de datos: ')
        print('1. lambda1      : ', self.lmbd1)
        print('2. lambda2      : ', self.lmbd2)
        print('3. lambda3      : ', self.lmbd3)
        print('4. lambda4      : ', self.lmbd4)
        print('5. Inv. ECM_Ue  : ', data_output.get('ECM_Ue'))
        print('6. Inv. ECM_Un  : ', data_output.get('ECM_Un'))
        print('7. Res. ||U-AS||: ', data_output.get('horror'))
        print('8. Res. ||S||   : ', data_output.get('horror_slip'))
        print('9. Res. ||FS||  : ', data_output.get('horror_suave'))
        print('10. Res. Total  : ', data_output.get('horror_total'))
        print(' -----------------------------------------------------------------------')
        print(self.desplaz_output)


        with open(self.desplaz_output,'w') as csvfile:
            writer = csv.writer(csvfile, delimiter= ';')
            writer.writerow(['Station','Longitud','Latitud','UE','UN','UU'])
            for row in zip(name_sta,
                            lon_sta,
                            lat_sta,
                            Ue_inv,
                            Un_inv,
                            Uz_inv):

                writer.writerow(row)

        # with open(self.desplaz_output,'w') as csvfile:
        #     if self.n_dim == 2:
        #         #wanted_keys=['Ue_obs','Un_obs','Ue_teo','Un_teo','Longitud','Latitud']
        #          wanted_keys=['Station','Latitud','Longitud','UE','UN','UU']
        #     if self.n_dim == 3:
        #         #wanted_keys=['Ue_obs','Un_obs','Uz_obs','Ue_teo','Un_teo','Uz_teo','Longitud','Latitud']
        #          wanted_keys=['Station','Latitud','Longitud','UE','UN','UU']
        #     #dict( (k,a[k]) for k in wanted_keys if k in a)
        #     wanted_dict= dict((k,merged_dict[k]) for k in wanted_keys)
        #     column_names=[x for x in  wanted_dict.keys()]
        #     writer = csv.writer(csvfile, delimiter = ';')
        #     writer.writerow(column_names)
        #     for row in wanted_dict.values():
        #          writer.writerow(row)

        if self.interface == 'double':
            slip_upper = []
            slip_lower = []
            k = reduce(lambda x, y: x*y, self.grid_size)
            #k = self.grid_size[0]*self.grid_size[1]
            slips_invertidos= data_output.get('slips_subfallas')
            print('slips')
            print('k:', k)

            print(slips_invertidos)
            for i in range(k):
                slip_upper.append(slips_invertidos[i])
                slip_lower.append(slips_invertidos[i+k])


            with open(self.lower_interface_output,'w') as csvfile:
                writer = csv.writer(csvfile, delimiter = ';')
                writer.writerow(['lon_central','lat_central','slip_lower'])
                for row in zip(lon_cent_subf_slab,
                               lat_cent_subf_slab,
                               slip_lower):
                    writer.writerow(row)

            
            with open(self.upper_interface_output,'w') as csvfile:
                writer = csv.writer(csvfile, delimiter = ';')
                writer.writerow(['lon_central','lat_central','slip_upper'])
                for row in zip(lon_cent_subf_slab,
                               lat_cent_subf_slab,
                               slip_upper):
                    writer.writerow(row)

        if self.interface == 'single':
            slip_lower= 'placeholder'
            slip_upper = data_output.get('slips_subfallas')
            with open(self.upper_interface_output,'w') as csvfile:
                writer = csv.writer(csvfile, delimiter = ';')
                writer.writerow(['lon_central','lat_central','slip_upper'])
                for row in zip(lon_cent_subf_slab,
                               lat_cent_subf_slab,
                               slip_upper):
                    writer.writerow(row)



 #Se almacenan datos de la generación de la inversión:
        name= self.inversion_params
        file = open(name, "w")
        file.write( ' -----------------------------------------------------------------------')
        file.write('\n')
        file.write( 'Resumen de datos: ')
        file.write('\n')
        file.write( '1. lambda1      : '+str(self.lmbd1))
        file.write('\n')
        file.write( '2. lambda2      : '+str(self.lmbd2))
        file.write('\n')
        file.write( '3. lambda3      : '+str(self.lmbd3))
        file.write('\n')
        file.write( '4. lambda4      : '+str(self.lmbd4))
        file.write('\n')
        file.write( '5. Inv. ECM_Ue  : '+ str(data_output.get('ECM_Ue')))
        file.write('\n')
        file.write( '6. Inv. ECM_Un  : '+str(data_output.get('ECM_Un')))
        file.write('\n')
        file.write( '6. Inv. ECM_Uz  : '+str(data_output.get('ECM_Uz')))
        file.write('\n')
        file.write( '7. Res. ||U-AS||: '+str(data_output.get('horror')))
        file.write('\n')
        file.write( '8. Res. ||S||   : '+str(data_output.get('horror_slip')))
        file.write('\n')
        file.write( '9. Res. ||FS||  : '+str(data_output.get('horror_suave')))
        file.write('\n')
        file.write( '10. Res. Total  : '+str(data_output.get('horror_total')))
        file.write('\n')
        file.write( ' -----------------------------------------------------------------------')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( ' ETAPA  9 == RESUMEN PARAMETROS GEOMETRICOS Y RESULTADOS')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( '                                                        ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE SUPERIOR (A):                                 ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')

       
        file.write('\n')
        file.write( 'Min Depth: '+str(min(prof_subf_inv))+' km')
        file.write('\n')
        file.write( 'Max Depth: '+str(max(prof_subf_inv))+' km')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Dip  : '+str(min(np.degrees(dip_subf_rad)))+' degrees')
        file.write('\n')
        file.write( 'Max Dip  : '+str(max(np.degrees(dip_subf_rad)))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Strike : '+str(min(strike_subf_deg))+' degrees')
        file.write('\n')
        file.write( 'Max Strike : '+str(max(strike_subf_deg))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Rake  : '+str(min(np.degrees(rake_inv_rad)))+' degrees')
        file.write('\n')
        file.write( 'Max Rake  : '+str(max(np.degrees(rake_inv_rad)))+' degrees')
        file.write('\n')
        file.write( '                                                        ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE INFERIOR (B):')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'Min Depth: '+str(min(prof_subf_norm))+' km')
        file.write('\n')
        file.write( 'Max Depth: '+str(max(prof_subf_norm))+' km')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Dip  : '+str(min(np.degrees(dip_subf_rad)))+' degrees')
        file.write('\n')
        file.write( 'Max Dip  : '+str(max(np.degrees(dip_subf_rad)))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Strike : '+str(min(strike_subf_deg))+' degrees')
        file.write('\n')
        file.write( 'Max Strike : '+str(max(strike_subf_deg))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Rake  : '+str(min(np.degrees(rake_norm_rad)))+' degrees')
        file.write('\n')
        file.write( 'Max Rake  : '+str(max(np.degrees(rake_norm_rad)))+' degrees')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( '          VELOCIDADES INVERTIDAS                         ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE SUPERIOR (A):                                 ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'Max. Vel: '+str(max(slip_upper))+'m/yr')
        file.write('\n')
        file.write( 'Min. Vel: '+str(min(slip_upper))+'m/yr')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE INFERIOR (B):                                 ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'Max. Vel: '+str(max(slip_lower))+'m/yr')
        file.write('\n')
        file.write( 'Min. Vel: '+str(min(slip_lower))+'m/yr')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'nx x ny: '+str(self.grid_size[0])+str('X')+str(self.grid_size[1]))
        file.write('\n')
        file.write( 'rake_inv_ref_deg: '+str(self.rake_referencia))
        #file.write('\n')
        #file.write( 'Angulo convergencia: '+str(math.degrees(math.atan(Uy_v/Ux_v)))+str('°'))
        file.write('\n')
        file.write( 'V_placa: '+str(self.velocidad_placa))
        file.write('\n')
        file.write( 'W :'+str(self.ancho_placa))
        file.write('\n')
        file.write( 'H :'+str(self.plate_thickness))
        file.close()
    ## variables usadas (vertices lon, vertices lat, nx, ny selfs, strike_rad, )
    def subfallas_slabmodel(self):
        strike_rad= None
        if self.sentido_subduccion == 'EW':
            strike_rad = np.radians(self.strike_aux_deg+180)
        if self.sentido_subduccion == 'WE':
            strike_rad = np.radians(self.strike_aux_deg)

        lon_central,lat_central,vertices_lon,vertices_lat=self.coordenadas_subfallas()
        
        strike=self.get_points_from_model(vertices_lon,vertices_lat,key='Strike')
        depth=self.get_points_from_model(vertices_lon,vertices_lat,key='Profundidad')
        #dip=self.get_points_from_model(vertices_lon,vertices_lat,key='Dip')


        dip,lon_vert_model,lat_vert_model,lon_central_model,lat_central_model=self.reescribir_subfallas(strike_rad,
                                                                                                        vertices_lon,
                                                                                                        vertices_lat)
        print('longas',lon_central,lon_central_model)
        print('latgas',lat_central,lat_central_model)
        #print('strike',strike)
        #print('depth',depth)
        ## strike of the convergence plate's vector
        phi_placa_rad = np.radians(np.degrees(strike_rad) + (360-(self.rake_referencia+180)))

        ## rake for inverse fault
        rake_inv_rad  = []
        ## rake for normal fault
        rake_norm_rad = []

        for index, elem in enumerate(strike):
            # Se establece rake en subfallas emplazadas en la
            # interfase superior con un movimiento relativo de tipo inverso
            rake = get_rake(
                            phi_placa_rad,
                            np.radians(elem),
                            np.radians(dip[index])
                            )
            rake_inv_rad.append(rake)
            # Se establece rake en subfallas emplazadas en la interfase
            # inferior con un movimiento relativo de tipo normal
            rake_norm_rad.append(rake_inv_rad[index]+np.pi)
        

        self.subfallas_model_dict.update({'lon_central':lon_central_model,
                                    'lat_central':lat_central_model,
                                    'lon_vertices':lon_vert_model,
                                    'lat_vertices':lat_vert_model,
                                    'dip_model':dip,
                                    'strike_model':strike,
                                    'depth_model':depth,
                                    'strike_rad':strike_rad,
                                    'rake_inv_rad':rake_inv_rad,
                                    'rake_norm_rad':rake_norm_rad,
                                    'phi_placa_rad':phi_placa_rad})

        print('rake_inv',np.degrees(rake_inv_rad))


    def reescribir_subfallas(self,strike,vertices_lon,vertices_lat):
        dip=self.get_points_from_model(vertices_lon,vertices_lat,key='Dip')
        dip_rad=np.radians(dip)
        nx = self.grid_size[0]
        ny = self.grid_size[1]
        #Se evaluaran todas las subfallas.
        FIL=nx*ny
        #Cada subfalla tiene cinco vertices a dibujar (el ultimo coincide con el primero)
        COL=5                  

        #Se declaran variables para almacenar vertices de todas las subfallas.
        vert_lat_new    = []
        vert_lon_new    = []
        lat_central_new = []    
        lon_central_new = []

        #Se crean matrices de ceros para rellenar con los vertices de todas las subfallas.
        for i in range(FIL):                #Numero de filas de la matriz
            vert_lon_new.append([0]*COL)    #Numero de columnas de la matriz, se inicializa con ceros
            vert_lat_new.append([0]*COL)

        alpha = np.degrees(strike) 
        beta = alpha + 270.0


        for subfalla in range(nx*ny):
                w = self.ancho_placa / ny
                #dy = w * np.sin((np.pi/2)-dip_rad[subfalla])
                dy = w*np.cos(dip_rad[subfalla])
                #Vertice 1
                vert_lon_new[subfalla][0] = vertices_lon[subfalla][0]
                vert_lat_new[subfalla][0] = vertices_lat[subfalla][0]
        
                #Vertice 2
                vert_lon_new[subfalla][1] = vertices_lon[subfalla][1]
                vert_lat_new[subfalla][1] = vertices_lat[subfalla][1]

                #Vertice 3.
                vert_lat_new[subfalla][2], vert_lon_new[subfalla][2], aux = vinc_pt( vert_lat_new[subfalla][1],
                                                                                     vert_lon_new[subfalla][1],
                                                                                      beta ,
                                                                                       dy )

                vert_lat_new[subfalla][3], vert_lon_new[subfalla][3], aux = vinc_pt( vert_lat_new[subfalla][0],
                                                                                     vert_lon_new[subfalla][0],
                                                                                     beta ,
                                                                                     dy )
 
                #Vertice 4.
                vert_lon_new[subfalla][4] = vert_lon_new[subfalla][0]
                vert_lat_new[subfalla][4] = vert_lat_new[subfalla][0]
 

                lat1, lon1, aux = vinc_pt( vert_lat_new[subfalla][0], vert_lon_new[subfalla][0], alpha , (self.L/nx)/2 )
                lat2, lon2, aux = vinc_pt( lat1, lon1, beta , dy/2 )
       
                lat_central_new.append(lat2)
                lon_central_new.append(lon2)
        print(vert_lon_new)
        print(vertices_lon)


        return dip,vert_lon_new, vert_lat_new, lon_central_new, lat_central_new,


    def get_points_from_model(self,vertices_lon,vertices_lat,key='Profundidad'):

        ## number of columns for interpolation data
        ## number of rows for interpolation data
        
        if key not in ['Profundidad','Strike','Dip']:
            print('La llave ha sido mal ingresada')
            

        lats=[ elem.get('Latitud') for elem in self.data_falla.get(key.lower()) if str(elem.get(key)) != 'nan' ]
        lons=[ elem.get('Longitud') for elem in self.data_falla.get(key.lower()) if str(elem.get(key)) != 'nan']
        data=[ elem.get(key) for elem in self.data_falla.get(key.lower()) if str(elem.get(key)) != 'nan']

        ## points used for interpolating the studied area
        #(lon,lat) <-> (x,y) in these scripts
        #print('lons',lons,lats,data)
        xi_start=np.linspace(min(lons),max(lons),self.numcols)
        yi_start=np.linspace(min(lats),max(lats),self.numrows)

       
        lon_points=[]
        lat_points=[]
        for subfalla in range(self.grid_size[0]*self.grid_size[1]):
            lon_points.append(vertices_lon[subfalla][0])
            lat_points.append(vertices_lat[subfalla][0])

        xi_new=[]
        yi_new=[]
        
        ## we group all the data sets
        for i in range(len(lon_points)):
            xi_new.append(lon_points[i])
            yi_new.append(lat_points[i])

        for i in range(len(xi_start)):
            xi_new.append(xi_start[i])
            yi_new.append(yi_start[i])


        ## interpolating data
        xi,yi=np.meshgrid(xi_new,yi_new)
        #print(xi,yi)
        zi=griddata((np.array(lons),np.array(lats)),
                                     np.array(data),
                                     (xi,yi),
                                     method='nearest')

        ## we seach for the data of interest
        POS_xi=[]
        POS_yi=[]

        ## coordinates of latitude
        for i in range(len(vertices_lat)):
            flag = True
            for j in range(len(yi)):
                if yi[j][0] == lat_points[i] and flag == True:
                    POS_yi.append(j)
                    flag = False
    
        #coordinates of longitude        
        for i in range(len(vertices_lon)):
            flag = True
            for j in range(len(xi)):
                if xi[0][j] == lon_points[i] and flag == True:
                    POS_xi.append(j)    
                    flag = False
    
        
        ## we extract data from slabmodel

        data_output = []
    
        for i in range(len(POS_yi)):
            if key in ['Profundidad','Dip']:
                data_output.append(-zi[POS_yi[i]][POS_xi[i]])
            if key == 'Strike':
                data_output.append(zi[POS_yi[i]][POS_xi[i]])

        return data_output



def desplaz_okada(xi,yi,dip,d,w,l,rake,strike):
    #La funcion determina la caracterizacion geometrica para una determinada falla.
    # notacion  de Chinnery: f(e,eta)||= f(x,p)-f(x,p-W)-f(x-L,p)+f(x-L,W-p)
    p = yi*np.cos(dip) + d*np.sin(dip)                                       
    q = yi*np.sin(dip) - d*np.cos(dip)                                       
    e = np.array([xi,xi,xi-l,xi-l]).T
    eta = np.array([p,p-w,p,p-w]).T       
    #qq array de q, hay cuatro valores porque se opera con los cuatro valores de eta.               
    qq = np.array([q,q,q,q]).T                    
    ytg = eta*np.cos(dip) + qq*np.sin(dip)           
    dtg = eta*np.sin(dip) - qq*np.cos(dip)           
    R = np.power(e**2 + eta**2 + qq**2, 0.5)     
    X = np.power(e**2 + qq**2, 0.5)               

    I5 = (1/np.cos(dip))*scp.arctan((eta*(X+qq*np.cos(dip))+X*(R+X)*np.sin(dip))/(e*(R+X)*np.cos(dip))) 
    I4 = 0.5/np.cos(dip)*(scp.log(R+dtg)-np.sin(dip)*scp.log(R+eta)) 
    # Solido de Poisson mu=lambda
    I1 = 0.5*((-1./np.cos(dip))*(e/(R+dtg)))-(np.sin(dip)*I5/np.cos(dip)) 
    I3 = 0.5*(1/np.cos(dip)*(ytg/(R+(dtg)))-scp.log(R+eta))+(np.sin(dip)*I4/np.cos(dip))  
    I2 = 0.5*(-scp.log(R+eta))-I3   #ok

    #dip-slip  (en direccion del manteo)
    ux_ds = -np.sin(rake)/(2*np.pi)*(qq/R-I3*np.sin(dip)*np.cos(dip))  #o
    uy_ds = -np.sin(rake)/(2*np.pi)*((ytg*qq/R/(R+e))+(np.cos(dip)*scp.arctan(e*eta/qq/R))
                -(I1*np.sin(dip)*np.cos(dip))) 
    uz_ds = -np.sin(rake)/(2*np.pi)*((dtg*qq/R/(R+e))+(np.sin(dip)*scp.arctan(e*eta/qq/R))
                -(I5*np.sin(dip)*np.cos(dip)))

    # ux_ds = -np.sin(np.radians(90))/(2*np.pi)*(qq/R-I3*np.sin(dip)*np.cos(dip))  #o
    # uy_ds = -np.sin(np.radians(90))/(2*np.pi)*((ytg*qq/R/(R+e))+(np.cos(dip)*scp.arctan(e*eta/qq/R))
    #             -(I1*np.sin(dip)*np.cos(dip))) 
    # uz_ds = -np.sin(np.radians(90))/(2*np.pi)*((dtg*qq/R/(R+e))+(np.sin(dip)*scp.arctan(e*eta/qq/R))
    #             -(I5*np.sin(dip)*np.cos(dip)))  

    # strike-slip  (en direccion del strike)
    ux_ss = -np.cos(rake)/(2*np.pi)*((e*qq/R/(R+eta))+(scp.arctan(e*eta/(qq*R)))+I1*np.sin(dip))  
    uy_ss = -np.cos(rake)/(2*np.pi)*((ytg*qq/R/(R+eta))+qq*np.cos(dip)/(R+eta)+I2*np.sin(dip))       
    uz_ss = -np.cos(rake)/(2*np.pi)*((dtg*qq/R/(R+eta))+qq*np.sin(dip)/(R+eta)+I4*np.sin(dip))
        
    # representacion chinnery dip-slip
    uxd = ux_ds.T[0]-ux_ds.T[1]-ux_ds.T[2]+ux_ds.T[3]   
    uyd = uy_ds.T[0]-uy_ds.T[1]-uy_ds.T[2]+uy_ds.T[3]   
    uzd = uz_ds.T[0]-uz_ds.T[1]-uz_ds.T[2]+uz_ds.T[3]   

    # representacion chinnery strike-slip
    uxs = ux_ss.T[0]-ux_ss.T[1]-ux_ss.T[2]+ux_ss.T[3]  
    uys = uy_ss.T[0]-uy_ss.T[1]-uy_ss.T[2]+uy_ss.T[3]  
    uzs = uz_ss.T[0]-uz_ss.T[1]-uz_ss.T[2]+uz_ss.T[3]  

    # solucion 
    ux = uxd+uxs  
    uy = uyd+uys  
    uz = uzd+uzs

    # # # solucion 
    # ux = uxs  
    # uy = uys  
    # uz = uzs

    # proyeccion a las componentes geograficas
    # rotacion de sistema de coordenadas 
    Ue = ux*np.sin(strike) - uy*np.cos(strike) 
    Un = ux*np.cos(strike) + uy*np.sin(strike) 
    
    #El gran problema es que falta el vector de deslizamiento, pero se podria agregar despues (multiplicando)

    return Ue,Un,uz

def desplaz_okada_dividido(xi,yi,dip,d,w,l,strike,mov='dip_slip'):
    #La funcion determina la caracterizacion geometrica para una determinada falla.
    # notacion  de Chinnery: f(e,eta)||= f(x,p)-f(x,p-W)-f(x-L,p)+f(x-L,W-p)
    p = yi*np.cos(dip) + d*np.sin(dip)                                       
    q = yi*np.sin(dip) - d*np.cos(dip)                                       
    e = np.array([xi,xi,xi-l,xi-l]).T
    eta = np.array([p,p-w,p,p-w]).T       
    #qq array de q, hay cuatro valores porque se opera con los cuatro valores de eta.               
    qq = np.array([q,q,q,q]).T                    
    ytg = eta*np.cos(dip) + qq*np.sin(dip)           
    dtg = eta*np.sin(dip) - qq*np.cos(dip)           
    R = np.power(e**2 + eta**2 + qq**2, 0.5)     
    X = np.power(e**2 + qq**2, 0.5)               

    I5 = (1/np.cos(dip))*scp.arctan((eta*(X+qq*np.cos(dip))+X*(R+X)*np.sin(dip))/(e*(R+X)*np.cos(dip))) 
    I4 = 0.5/np.cos(dip)*(scp.log(R+dtg)-np.sin(dip)*scp.log(R+eta)) 
    # Solido de Poisson mu=lambda
    I1 = 0.5*((-1./np.cos(dip))*(e/(R+dtg)))-(np.sin(dip)*I5/np.cos(dip)) 
    I3 = 0.5*(1/np.cos(dip)*(ytg/(R+(dtg)))-scp.log(R+eta))+(np.sin(dip)*I4/np.cos(dip))  
    I2 = 0.5*(-scp.log(R+eta))-I3   #ok
    if mov =='dip_slip':
        # dip-slip  (en direccion del manteo)
        ux_ds = -np.sin(np.radians(90))/(2*np.pi)*(qq/R-I3*np.sin(dip)*np.cos(dip))  #o
        uy_ds = -np.sin(np.radians(90))/(2*np.pi)*((ytg*qq/R/(R+e))+(np.cos(dip)*scp.arctan(e*eta/qq/R))
                    -(I1*np.sin(dip)*np.cos(dip))) 
        uz_ds = -np.sin(np.radians(90))/(2*np.pi)*((dtg*qq/R/(R+e))+(np.sin(dip)*scp.arctan(e*eta/qq/R))
                    -(I5*np.sin(dip)*np.cos(dip))) 
        #representacion chinnery dip-slip
        uxd = ux_ds.T[0]-ux_ds.T[1]-ux_ds.T[2]+ux_ds.T[3]   
        uyd = uy_ds.T[0]-uy_ds.T[1]-uy_ds.T[2]+uy_ds.T[3]   
        uzd = uz_ds.T[0]-uz_ds.T[1]-uz_ds.T[2]+uz_ds.T[3]   
        # solucion 
        ux = uxd  
        uy = uyd  
        uz = uzd 

    if mov =='strike_slip':
     # strike-slip  (en direccion del strike)
        ux_ss = -np.cos(np.radians(0))/(2*np.pi)*((e*qq/R/(R+eta))+(scp.arctan(e*eta/(qq*R)))+I1*np.sin(dip))  
        uy_ss = -np.cos(np.radians(0))/(2*np.pi)*((ytg*qq/R/(R+eta))+qq*np.cos(dip)/(R+eta)+I2*np.sin(dip))       
        uz_ss = -np.cos(np.radians(0))/(2*np.pi)*((dtg*qq/R/(R+eta))+qq*np.sin(dip)/(R+eta)+I4*np.sin(dip))
        
        # representacion chinnery strike-slip
        uxs = ux_ss.T[0]-ux_ss.T[1]-ux_ss.T[2]+ux_ss.T[3]  
        uys = uy_ss.T[0]-uy_ss.T[1]-uy_ss.T[2]+uy_ss.T[3]  
        uzs = uz_ss.T[0]-uz_ss.T[1]-uz_ss.T[2]+uz_ss.T[3]  
        # solucion 
        ux = uxs  
        uy = uys  
        uz = uzs      

    
    # proyeccion a las componentes geograficas
    # rotacion de sistema de coordenadas 
    Ue = ux*np.sin(strike) - uy*np.cos(strike) 
    Un = ux*np.cos(strike) + uy*np.sin(strike) 
    
    #El gran problema es que falta el vector de deslizamiento, pero se podria agregar despues (multiplicando)

    return Ue,Un,uz

def get_rake(p_placa, p_falla, d_falla):
 #INPUT
 #  p_placa = strike asociado al vector de convergencia de la placa en radianes
 #  p_falla = strike de la subfalla en radianes
 #  d_falla = dip de la subfalla en radianes
 #OUTPUT    
 #  rake    = rake de la subfalla en radianes para la interface superior con un
 #            movimiento relativo de tipo inverso.  
 a = np.sin(p_falla) - np.tan(p_placa) * np.cos(p_falla)
 b = np.tan(p_placa) * np.cos(d_falla) * np.sin(p_falla) + np.cos(d_falla) * np.cos(p_falla)  

 rake = np.arctan2(a,b) + np.pi
 
 return rake 


def vinc_dist(  phi1,  lembda1,  phi2,  lembda2 ) :
        """ 
        Returns the distance between two geographic points on the ellipsoid
        and the forward and reverse azimuths between these points.
        lats, longs and azimuths are in decimal degrees, distance in metres 

        Returns ( s, alpha12,  alpha21 ) as a tuple
        """

        f = 1.0 / 298.257223563     # WGS84
        a = 6378137.0           # metres
        if (abs( phi2 - phi1 ) < 1e-8) and ( abs( lembda2 - lembda1) < 1e-8 ) :
                return 0.0, 0.0, 0.0

        piD4   = math.atan( 1.0 )
        two_pi = piD4 * 8.0

        phi1    = phi1 * piD4 / 45.0
        lembda1 = lembda1 * piD4 / 45.0     # unfortunately lambda is a key word!
        phi2    = phi2 * piD4 / 45.0
        lembda2 = lembda2 * piD4 / 45.0

        b = a * (1.0 - f)

        TanU1 = (1-f) * math.tan( phi1 )
        TanU2 = (1-f) * math.tan( phi2 )

        U1 = math.atan(TanU1)
        U2 = math.atan(TanU2)

        lembda = lembda2 - lembda1
        last_lembda = -4000000.0        # an impossibe value
        omega = lembda

        # Iterate the following equations, 
        #  until there is no significant change in lembda 

        while ( last_lembda < -3000000.0 or lembda != 0 and abs( (last_lembda - lembda)/lembda) > 1.0e-9 ) :

                sqr_sin_sigma = pow( np.cos(U2) * np.sin(lembda), 2) + \
                        pow( (np.cos(U1) * np.sin(U2) - \
                        np.sin(U1) *  np.cos(U2) * np.cos(lembda) ), 2 )

                Sin_sigma = np.sqrt( sqr_sin_sigma )

                Cos_sigma = np.sin(U1) * np.sin(U2) + np.cos(U1) * np.cos(U2) * np.cos(lembda)
        
                sigma = math.atan2( Sin_sigma, Cos_sigma )

                Sin_alpha = np.cos(U1) * np.cos(U2) * np.sin(lembda) / np.sin(sigma)
                alpha = math.asin( Sin_alpha )

                Cos2sigma_m = np.cos(sigma) - (2 * np.sin(U1) * np.sin(U2) / pow(np.cos(alpha), 2) )

                C = (f/16) * pow(np.cos(alpha), 2) * (4 + f * (4 - 3 * pow(np.cos(alpha), 2)))

                last_lembda = lembda

                lembda = omega + (1-C) * f * np.sin(alpha) * (sigma + C * np.sin(sigma) * \
                        (Cos2sigma_m + C * np.cos(sigma) * (-1 + 2 * pow(Cos2sigma_m, 2) )))

        u2 = pow(np.cos(alpha),2) * (a*a-b*b) / (b*b)

        A = 1 + (u2/16384) * (4096 + u2 * (-768 + u2 * (320 - 175 * u2)))

        B = (u2/1024) * (256 + u2 * (-128+ u2 * (74 - 47 * u2)))

        delta_sigma = B * Sin_sigma * (Cos2sigma_m + (B/4) * \
                (Cos_sigma * (-1 + 2 * pow(Cos2sigma_m, 2) ) - \
                (B/6) * Cos2sigma_m * (-3 + 4 * sqr_sin_sigma) * \
                (-3 + 4 * pow(Cos2sigma_m,2 ) )))

        s = b * A * (sigma - delta_sigma)

        alpha12 = math.atan2( (np.cos(U2) * np.sin(lembda)), \
                (np.cos(U1) * np.sin(U2) - np.sin(U1) * np.cos(U2) * np.cos(lembda)))

        alpha21 = math.atan2( (np.cos(U1) * np.sin(lembda)), \
                (-np.sin(U1) * np.cos(U2) + np.cos(U1) * np.sin(U2) * np.cos(lembda)))

        if ( alpha12 < 0.0 ) : 
                alpha12 =  alpha12 + two_pi
        if ( alpha12 > two_pi ) : 
                alpha12 = alpha12 - two_pi

        alpha21 = alpha21 + two_pi / 2.0
        if ( alpha21 < 0.0 ) : 
                alpha21 = alpha21 + two_pi
        if ( alpha21 > two_pi ) : 
                alpha21 = alpha21 - two_pi

        alpha12    = alpha12    * 45.0 / piD4
        alpha21    = alpha21    * 45.0 / piD4
        return s, alpha12,  alpha21 

   # END of Vincenty's Inverse formulae 


#-------------------------------------------------------------------------------
# Vincenty's Direct formulae                            |
# Given: latitude and longitude of a point (phi1, lembda1) and          |
# the geodetic azimuth (alpha12)                        |
# and ellipsoidal distance in metres (s) to a second point,         |
#                                       |
# Calculate: the latitude and longitude of the second point (phi2, lembda2)     |
# and the reverse azimuth (alpha21).                        |
#                                       |
#-------------------------------------------------------------------------------

def  vinc_pt( phi1, lembda1, alpha12, s ) :
        """

        Returns the lat and long of projected point and reverse azimuth
        given a reference point and a distance and azimuth to project.
        lats, longs and azimuths are passed in decimal degrees

        Returns ( phi2,  lambda2,  alpha21 ) as a tuple 

        """
 
        f = 1.0 / 298.257223563     # WGS84
        a = 6378137.0           # metres
        piD4 = math.atan( 1.0 )
        two_pi = piD4 * 8.0

        phi1    = phi1    * piD4 / 45.0
        lembda1 = lembda1 * piD4 / 45.0
        alpha12 = alpha12 * piD4 / 45.0
        if ( alpha12 < 0.0 ) : 
                alpha12 = alpha12 + two_pi
        if ( alpha12 > two_pi ) : 
                alpha12 = alpha12 - two_pi

        b = a * (1.0 - f)

        TanU1 = (1-f) * math.tan(phi1)
        U1 = math.atan( TanU1 )
        sigma1 = math.atan2( TanU1, np.cos(alpha12) )
        Sinalpha = np.cos(U1) * np.sin(alpha12)
        cosalpha_sq = 1.0 - Sinalpha * Sinalpha

        u2 = cosalpha_sq * (a * a - b * b ) / (b * b)
        A = 1.0 + (u2 / 16384) * (4096 + u2 * (-768 + u2 * \
                (320 - 175 * u2) ) )
        B = (u2 / 1024) * (256 + u2 * (-128 + u2 * (74 - 47 * u2) ) )

        # Starting with the approximation
        sigma = (s / (b * A))

        last_sigma = 2.0 * sigma + 2.0  # something impossible

        # Iterate the following three equations 
        #  until there is no significant change in sigma 

        # two_sigma_m , delta_sigma
        two_sigma_m = 0
        while ( abs( (last_sigma - sigma) / sigma) > 1.0e-9 ) :
                two_sigma_m = 2 * sigma1 + sigma

                delta_sigma = B * np.sin(sigma) * ( np.cos(two_sigma_m) \
                        + (B/4) * (np.cos(sigma) * \
                        (-1 + 2 * pow( np.cos(two_sigma_m), 2 ) -  \
                        (B/6) * np.cos(two_sigma_m) * \
                        (-3 + 4 * pow(np.sin(sigma), 2 )) *  \
                        (-3 + 4 * pow( np.cos (two_sigma_m), 2 ))))) \

                last_sigma = sigma
                sigma = (s / (b * A)) + delta_sigma

        phi2 = math.atan2 ( (np.sin(U1) * np.cos(sigma) + np.cos(U1) * np.sin(sigma) * np.cos(alpha12) ), \
                ((1-f) * np.sqrt( pow(Sinalpha, 2) +  \
                pow(np.sin(U1) * np.sin(sigma) - np.cos(U1) * np.cos(sigma) * np.cos(alpha12), 2))))

        lembda = math.atan2( (np.sin(sigma) * np.sin(alpha12 )), (np.cos(U1) * np.cos(sigma) -  \
                np.sin(U1) *  np.sin(sigma) * np.cos(alpha12)))

        C = (f/16) * cosalpha_sq * (4 + f * (4 - 3 * cosalpha_sq ))

        omega = lembda - (1-C) * f * Sinalpha *  \
                (sigma + C * np.sin(sigma) * (np.cos(two_sigma_m) + \
                C * np.cos(sigma) * (-1 + 2 * pow(np.cos(two_sigma_m),2) )))

        lembda2 = lembda1 + omega

        alpha21 = math.atan2 ( Sinalpha, (-np.sin(U1) * np.sin(sigma) +  \
                np.cos(U1) * np.cos(sigma) * np.cos(alpha12)))

        alpha21 = alpha21 + two_pi / 2.0
        if ( alpha21 < 0.0 ) :
                alpha21 = alpha21 + two_pi
        if ( alpha21 > two_pi ) :
                alpha21 = alpha21 - two_pi

        phi2       = phi2       * 45.0 / piD4
        lembda2    = lembda2    * 45.0 / piD4
        alpha21    = alpha21    * 45.0 / piD4

        return phi2,  lembda2,  alpha21 

  
