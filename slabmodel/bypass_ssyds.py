#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 00:03:30 2019

@author: doctor
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 18 21:17:27 2019

@author: doctor
"""


## saltar la creación de las funciones de green para ahorrar tiempo
from calculo_cosismico import Seismic_Cycle
import numpy as np
if __name__ == "__main__":
    bandera = False
#    ids=['/BAYES_TEST_1X1_SINGLE','/BAYES_TEST_1X1_DOUBLE','/BAYES_TEST_1X1_DOUBLE_TRAMPA','/BAYES_TEST_1X1_DOUBLE_TRAMPA_DOUBLE']
#    data=['./Fuentes/bayesian1x1single.txt','./Fuentes/bayesian1x1double.txt','./Fuentes/bayesian1x1doubletrampa.txt','./Fuentes/bayesian1x1doubletrampadouble.txt']
    #ids_tesis=['/BAYES_HP_1x1_SINGLE','/BAYES_HP_1x1_DOUBLE','/BAYES_HP_5X4_SINGLE','/BAYES_HP_5X4_DOUBLE']
    ids_tesis=['/BAYES_HP_1X1_SSYDS',
               '/BAYES_HP_6X4_SSYDS']
    data_tesis=['./Fuentes/bayeshp1x1ssyds.txt',
                './Fuentes/bayeshp6x4ssyds.txt']
#    data_tesis=['./Fuentes/bayeshp1x1single.txt',
#                './Fuentes/bayeshp1x1double.txt',
#                './Fuentes/bayeshp5x4single.txt',
#                './Fuentes/bayeshp5x4double.txt']
    params = {    
        'zona':'Illapel',
        'limites_zona': (-29,-33),
        'stations': [],
        'proceso':'coseismic',
        'interface':'single',
        'output':'inversion',
        'grid_size': (6,4),
        'alto_placa': 12000,
        'ancho_placa':200000,
        'data_observada': data_tesis[1],
        'rake_referencia': 110,
        'plano_CD': False,
        'plano_E': False,
        'ID': ids_tesis[1],
        'bypass': True,
        'split_G': True
         }
    
    print('llamando_clase')
    cosismico = Seismic_Cycle(**params)
    map_params = {
        'latmin': -40,
        'latmax': -10,
        'lonmin': -90,
        'lonmax': -65,
        "paralelos": [-36, -32, -28, -24, -20],
        "meridianos": [-75, -70],
                  }
    cosismico.set_map_params(**map_params)
    bayesian_params = {'how2regularize': 'minimize','add_noise': True}
    Gneww,dobs,dsint=cosismico.bayesian_inversion(**bayesian_params)
    #cosismico.load_slip_sint(plano='upper',data='inversion')
    #cosismico.plot_slips_arrows_new()
    #cosismico.load_greenfunctions()