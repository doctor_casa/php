#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 10:28:22 2019

@author: doctor
"""

import numpy as np
import matplotlib.pyplot as plt
## 
N=1000
v0=np.linspace(3,5,N)
g=np.linspace(8,12,N)
t=np.linspace(0,1,N)
tobs=[0.2,0.4,0.6,0.8]
sigmaz=0.02
sigmat=0.01
#t1=np.linspace(tobs[0]-sigmat,tobs[0]+sigmat,N)
#t2=np.linspace(tobs[1]-sigmat,tobs[1]+sigmat,N)
#t3=np.linspace(tobs[2]-sigmat,tobs[2]+sigmat,N)
#t4=np.linspace(tobs[3]-sigmat,tobs[3]+sigmat,N)
t1=np.linspace(0,1,N)
t2=np.linspace(0,1,N)
t3=np.linspace(0,1,N)
t4=np.linspace(0,1,N)

zobs=[0.62,0.88,0.70,0.15]
def func1(v0,g,t1,t2,t3,t4,zobs,sigmaz):
    print('t1',type(t1))
    S=abs((v0*t1-0.5*g*t1**2)-zobs[0])/sigmaz + \
      abs((v0*t2-0.5*g*t2**2)-zobs[1])/sigmaz + \
      abs((v0*t3-0.5*g*t3**2)-zobs[2])/sigmaz + \
      abs((v0*t4-0.5*g*t4**2)-zobs[3])/sigmaz
     
    return S

def func2(v0,g,t1,t2,t3,t4,sigmat,sigmaz,k=1):
    #t=np.linspace(0,100,1000)
    sigmaM=[]
    t1_lim=[tobs[0]-sigmat,tobs[0]+sigmat]
    t2_lim=[tobs[1]-sigmat,tobs[1]+sigmat]
    t3_lim=[tobs[2]-sigmat,tobs[2]+sigmat]
    t4_lim=[tobs[3]-sigmat,tobs[3]+sigmat]
    #S=func1(v0,g,t1,t2,t3,t4,zobs,sigmaz)
    sigmaM=[]
    t11=[x if x > t1_lim[0] and x < t1_lim[1] else 0 for x in t1 ]
    t22=[x if x > t2_lim[0] and x < t2_lim[1] else 0 for x in t2 ]
    t33=[x if x > t3_lim[0] and x < t3_lim[1] else 0 for x in t3 ]
    t44=[x if x > t4_lim[0] and x < t4_lim[1] else 0 for x in t4 ]
    sigmaM=k*np.exp(-func1(v0,g,t11,t22,t33,t44,zobs,sigmaz))
    # for t1,
#    for ti in t:
#        if ti
#        flag=False
#        for to in [t1,t2,t3,t4]:
#            if ti-sigmat <= to and ti+sigmat >= to:
#                sigmaM.append(k*np.exp(-func1(v0,g,t1,t2,t3,t4,zobs,sigmaz)))
#                flag = True
#                break
#        if not flag:
#            sigmaM.append(0)
#    return sigmaM

sM=func2(v0,g,t1,t2,t3,t4,sigmat,sigmaz,k=1)


 #b=[x for x in t if x > (min(a)) and x < (max(a))   ]
#plt.plot(t,sigma_real)         
#def S(v0,g,t)   
#    t1=tobs
#    t2=0.4
#    t3=0.6
#    t4=0.8
#    tobs=[0.2,0.4,0.6,0.8]
#    sigmat=0.01
#    ## posiciones que se estimaron, incertezas tipo doble exponencial
#    
#    z1=0.62
#    z2=0.88
#    z3=0.70
#    z4=0.15
#    zobs=[z1,z2,z3,z4]
#    sigmaz=0.02
#    
#    k=3
#    t_vector=np.arange(0,1,0.01)
#    ## generar la densidad prior m
#    rho_m=[]
#    for t in t_vector:
#        flag=False
#        for ti in tobs:
#            if t-sigmat <= ti and t+sigmat >= ti:
#                rho_m.append(k)
#                flag = True
#            break
#            if not flag:
#                rho_m.append(0)        
         
#esto esta mal
##plt.plot(t_vector,rho_m,'ro')
#
### generar la densidad prior d
#z=np.linspace(0,5,100)
#zobs=np.array(zobs)
#rho_d=[]
#for zi in z:
#    aux=-sum(abs(zi-zobs)/sigmaz)
#    rho_d.append(k*np.exp(aux))
#    