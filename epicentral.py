#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 31 16:34:31 2019

@author: doctor
"""
import numpy as np
import matplotlib.pyplot as plt
## coordenadas de las estaciones en un SREF cartesiano (X,Y)
e1=[3,15]
e2=[3,16]
e3=[4,15]
e4=[4,16]
e5=[5,15]
e6=[5,16]

posiciones=[e1,e2,e3,e4,e5,e6]
## velocidad en km/s
v=5

## tiempo en segundos (observaciones)
t1=3.12
t2=3.26
t3=2.98
t4=3.12
t5=2.84
t6=2.98
tobs=[t1,t2,t3,t4,t5,t6]

## incertezas independientes y gaussianas
sigma=0.10

## SOLUCION 

kprima=1


def marginal_M(kprima,v,tobs,posiciones,sigma,X,Y):
    sigmaM=[]
    XX,YY=np.meshgrid(X,Y)
    
    tcal=[(1./v)*np.sqrt((pos[0]-XX)**2 + (pos[1]-YY)**2 ) for pos in posiciones]
    factor=(2*sigma**2)**-1
    arg=sum([(-factor*(x-y)**2) for x,y in zip(tcal,tobs)])
    print(np.max(arg))
    sigmaM.append(kprima*np.exp(arg))
    
#    tcal=[1./v**np.sqrt(pos[0])
#    tcal=(1./v)*np.sqrt((pos[0]-x)**2 + (pos[1]-y)**
#        tcal=[(1./v)*np.sqrt((pos[0]-x)**2 + (pos[1]-y)**2 ) for pos in posiciones]
#        ## densidad marginal de M:
#        sigmaM.append(kprima*np.exp(arg))
    return sigmaM

X=np.linspace(0,20,100)
Y=np.linspace(0,20,100)
sigmaXYM=marginal_M(kprima,v,tobs,posiciones,sigma,X,Y)
#fig=plt.figure()
#fig.contourf(X,Y,sigmaXYM[0],cmap=plt.cm.bone)
#fig.colorbar()

vx=[x[0] for x in posiciones]
vy=[x[1] for x in posiciones]
fig,ax1 = plt.subplots(nrows=1)
ctrf=ax1.contourf(X,Y,sigmaXYM[0],cmap=plt.cm.bone)
fig.colorbar(ctrf)
ax1.plot(vx,vy,'^m',linewidth=4)

#x = np.arange(-5,5, 0.1)
#y = np.arange(-5, 5, 0.1)
#xx, yy = np.meshgrid(x, y, sparse=True)
#z = np.sin(xx**2 + yy**2) / (xx**2 + yy**2)
#h = plt.contourf(x,y,z)
