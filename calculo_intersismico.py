# -*- coding: utf-8 -*-

from mpl_toolkits.basemap import Basemap, cm
import matplotlib.pyplot as plt
import numpy as np
import math
import os
import scipy as scp
from datetime import datetime
import time    
from fractions import Fraction
from functools import reduce
"""
Args

zona_subduccion
zona(seccion)
filtros = {
intersmimico: Boolean,
cosismico: Boolean,
postsismico: Boolean
}

fecha_sismo

Boleanos:

datos_moreno
mapa_previo
mapa_vectores
nombre_estacion
sentido_subduccion = ['EW','WE'] --> {0,1}
generar_inversion
condicion_borde_fosa


Files String:

input_file_fosa="./DatosBase/perimetro_fosa_chile.txt" 
limites_zona = (lat_norte, lat_sur)


funcion -> archivo, limites.
-> corte

"""

import csv
def cut_file(filename,
             dict_types,
             limites=(0, -90),
             colname='Latitud'):
    norte = limites[0]
    sur = limites[1]
    data = []
    with open(filename, 'r') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=';')
        # extract headers
        for row in reader:
            final_types = {key:dict_types.get(key, str) for key in row.keys()}
            new_row = {key: final_types.get(key)(value) for key, value in row.items()}
            value = new_row.get(colname, 0)
            if sur <= value and value <= norte :
                data.append(new_row)
    return data

def find_real(lista, value, esp=0.01):
    for index, elem in enumerate(lista):
        if elem-esp<=value and value<=elem+esp:
            return index, elem


class Intersismico:
#class Seismic_Cycle:
    # # velocidad de placa en metros por año
    # v_placa = 0.068
    # # grosor de la placa en metros
    # W = 200000
    # # un rake de referencia
    # rake_inv_ref_deg = 120
    # # alto de la placa
    # H = 11000
    #delta_lat = 0.08
    #dip_plano_inicial = np.radians(14.)
    ## constantes de regularización
    #delta_lat = 0.08
    # lambdas = dict.fromkeys(['A_min',
    #                          'A_suav',
    #                          'B_min',
    #                          'B_suav'], 0.02)

    def __init__(self, *args, **kwargs):
        # self.lambs=kwargs.get({'A_min',0.02,
        #                         'A_suav',0.02,
        #                         'B_min',0.02,
        #                         'B_suav',0.02})
        self.lmbd1=kwargs.get('lambda1',0.02)
        self.lmbd2=kwargs.get('lambda2',0.02)
        self.lmbd3=kwargs.get('lambda3',0.02)
        self.lmbd4=kwargs.get('lambda4',0.02)

        ## una ID del proceso para diferenciarla de las demas.
        self.id_proceso = kwargs.get('ID', '/Output_1')
        self.parte_proceso = kwargs.get('parte_proceso',1)
        if self.parte_proceso == 1:
            self.proceso = '/Directo'
        if self.parte_proceso == 2:
            self.proceso = '/Inversion'

        try:
            os.mkdir('Outputs'+ self.id_proceso)
        except FileExistsError:
                print('la carpeta ya existe')

        try:
            os.mkdir('Outputs'+ self.id_proceso+self.proceso)
        except FileExistsError:
            print('la carpeta ya existe')


        try:
            os.mkdir('Outputs'+ self.id_proceso+'/Figuras')
        except FileExistsError:
            print('la carpeta ya existe')

        try:
            os.mkdir('Outputs'+ self.id_proceso+self.proceso+'/Cosismico')
        except FileExistsError:
            print('la carpeta ya existe')

        try:
            os.mkdir('Outputs'+ self.id_proceso+self.proceso+'/Intersismico')
        except FileExistsError:
            print('la carpeta ya existe')

            
        self.path_slips=kwargs.get('path_slips','Outputs')
        self.makeF = kwargs.get('smooth_matrix',False)
        self.nombre_zona = kwargs.get('zona', 'Chile')
        self.limites_zona = kwargs.get('limites_zona',
                                       (-18, -50))
        self.stations = kwargs.get('stations', [])
        self.filtro = kwargs.get('filtro', 'intersismico')
        self.actions = dict(
            intersismico=self.load_intersismico,
            cosismico=self.load_cosismico,
            postsismico=self.load_postsismico
                            )
        self.action = self.actions.get(self.filtro)
        self.n_dim = kwargs.get('dimensiones', 3)
        self.dip_plano_inicial = kwargs.get('dip_inicial', np.radians(14.))
        self.delta_lat = kwargs.get('delta_lat', 0.08)
        ## velocidad de la placa en metros por año
        self.velocidad_placa = kwargs.get('vel_placa', 0.068)
        ### width of the fault along dip
        self.ancho_placa = kwargs.get('ancho_placa',260000)
        # rake de referencia
        self.rake_referencia = kwargs.get('rake_referencia', 120)
        # alto de la placa en metros (11000 metros de referencia)
        self.alto_placa = kwargs.get('alto_placa', 11000)
        ## length of the'fault along strike
        self.L = None
        self.strike_aux_deg = None
        self.backstrike = None
        ## parameters for creating subfaults
        self.numcols=50
        self.numrows=150
        self.fecha = kwargs.get('fecha', datetime.utcnow().isoformat())
        self.mapa_previo = kwargs.get('mapa_previo', True)
        self.mapa_vectores = kwargs.get('mapa_vectores', True)
        ## para el caso de estudio de chile siempre es WE
        self.sentido_subduccion = kwargs.get('sentido', 'WE')
        # de no requerir inversión dejar Falso
        self.inversion = kwargs.get('inversion', True)
        self.condicion_borde_fosa = kwargs.get('condicion_borde_fosa', False)
        self.file_fosa = kwargs.get(
            'file_fosa',
            "./DatosBase/perimetro_fosa_chile.csv")
        self.file_profundidad = kwargs.get(
            'file_profundidad',
            "./DatosBase/profundidad_xyz_fosa_chile.csv")
        self.file_strike = kwargs.get(
            'file_strike',
            "./DatosBase/strike_fosa_chile.csv")
        self.file_dip = kwargs.get(
            'file_dip',
            "./DatosBase/dip_fosa_chile.csv")
        #print('congrio')
        self.data_falla = self.load_data_falla()
        #print('panes')
        # datos de fuentes de estaciones
        ## cuales fallas construir, por default solo construye plano AB
        self.falla_AB=kwargs.get('plano_AB', True)
        self.falla_CD=kwargs.get('plano_CD', False)
        self.falla_E=kwargs.get('plano_E',  False)

        
        
        ## grillado de las subfallas
        self.grid_size = kwargs.get('grid_size', (75,5))
        ## planos de falla
        self.planos_falla = {}
        self.planos_falla_obj = {}
        ## informacion dado el slabmodel:
        self.subfallas_model_dict={}
        ## distintos inputs y outputs dependiendo de la parte del ciclo que se quiera modelar
        if self.filtro == 'intersismico':
            ## fuente estaciones puede ser usada independientemente si se hace inversion o no
            self.fuente_estaciones = kwargs.get('velocidades_obs.txt','./Fuentes/estaciones_intersismico.txt')
            self.data_estaciones = self.action(stations=self.stations)
            self.velocidades_output=kwargs.get('velocidades_output','Outputs'+ self.id_proceso+'/Intersismico/synthetic_component.txt')
            self.lower_interface_output=kwargs.get('velocidades_output', 'Outputs'+ self.id_proceso+'/Intersismico/interseismic_slip_rate_lower_interface.txt')
            self.upper_interface_output=kwargs.get('velocidades_output', 'Outputs'+ self.id_proceso+'/Intersismico/interseismic_slip_rate_upper_interface.txt')
            self.inversion_params = kwargs.get('inversion_params', 'Outputs' + self.id_proceso+ '/Intersismico/inv_params.txt' )

            # Debo generar esto
            self.velocidades_sinteticas={}
            ## esta modififacion es del 8 de mayo
            self.outputs_inv_intersism={}
            
        if self.filtro == 'cosismico':
            ## fuente estaciones puede ser usada independientemente si se hace inversion o no
            self.fuente_estaciones = kwargs.get('desplazamientos_obs.txt','./Fuentes/estaciones_cosismico.csv')
            self.data_estaciones = self.action(stations=self.stations)
            # nose de donde salio la linea de abajo pero me da miedo borrarla
            #f=open('Outputs/probar_directo/Cosismico/landas.txt','w')
            self.desplaz_output=kwargs.get('slips_output','Outputs' + self.id_proceso + self.proceso + '/Cosismico/synthetic_component.txt')
            self.lower_interface_output=kwargs.get('slips_output', 'Outputs' + self.id_proceso + self.proceso + '/Cosismico/coseismic_slip_lower_interface.txt')
            self.upper_interface_output=kwargs.get('slips_output', 'Outputs'+ self.id_proceso + self.proceso +  '/Cosismico/coseismic_slip_upper_interface.txt')
            self.inversion_params = kwargs.get('inversion_params', 'Outputs' +  self.id_proceso + self.proceso + '/Cosismico/inv_params.txt' )
            ## debo generar esto
            self.desplazamientos_sinteticos={}
            ## esta modificacion es del 8 de mauo
            self.outputs_inv_cosism={}

            
        # PROGRAMAR POSTSÍSMICO
        #if self.filtro = 'cosismico':
        #    self.data_desplaz = self.action(stations=self.stations)
        #    self.velocidades_sinteticos={}
        #    self.slip_sinteticos={}
        
        

        

    def plano_falla_ab(self, **kwargs):
        ### this function gets the data of the lower and upper points fo the AB fault planes. It also 
        ### gets the value f the the initialized variables L, strike_aux_deg and backstrike
        fosa = self.data_falla.get('fosa')
        fosa_lat = [elem.get('Latitud') for elem in fosa]
        fosa_lon = [elem.get('Longitud') for elem in fosa]
        # <-Limite superior de la fosa.
        #if self.filtro=="intersismico":
        latf = min(fosa_lat)
        index, latf = find_real(fosa_lat, latf)
        lonf = fosa_lon[index]
        lat1 = max(fosa_lat)
        index, lat1 = find_real(fosa_lat, lat1)
        lon1 = fosa_lon[index]
        # min(lat) fosa_lat[6]<-Limite inferior de la fosa.
        latf = min(fosa_lat)
        PF_AB = dict(
                superior=(lat1, lon1),
                inferior=(latf, lonf)
                    )
        print('latflati')
        print(latf,lat1)
        print('lonflonfi')
        print(lonf,lon1)
        self.L, self.strike_aux_deg, self.backstrike = vinc_dist(latf,lonf,lat1,lon1)
        self.planos_falla.update({'AB': PF_AB})
        #self.claves_fallo.update({'claves':(latf,lonf),(lat1,lon1)})

    def plano_falla_cd(self, **kwargs):
        """ this function saves the number of faults along dip and their respective width of the
        cd fault planes  """
        ny_cd = kwargs.get('ny_cd', 8)
        w_cd = self.ancho_placa/self.grid_size[1]
        PF_CD = dict(
                ny_cd=ny_cd,
                w_cd=w_cd
                    )
        self.planos_falla.update({'CD': PF_CD})

    def plano_falla_e(self, **kwargs):
        """ this function gets the data of fault E given an length and width for this plane.
            the number of faults along strike is the same of AB, and only one fault is used 
            dip  """
        km = 1000
        ancho = kwargs.get('ancho_e', 300)
        E = dict(
            W_E=ancho*km,
            H_E=self.H,
            nx_E=self.grid_size[0],
            ny_E=1,
            delta_lat_E=-0.19
                )
        self.planos_falla.update({"E": E})

    def load_data_falla(self):
        """ this function cuts the main files of dip, strike, depth and the trench rake
         in the interval given by the zone limits in __init__ """
        data_type = {'Latitud': float,
                     'Longitud': float,
                     'Strike': float,
                     'Dip': float,
                     'Profundidad': float}
        data_profundidad = cut_file(
            self.file_profundidad,
            data_type,
            self.limites_zona,
            'Latitud')
        data_fosa = cut_file(
            self.file_fosa,
            data_type,
            self.limites_zona,
            'Latitud')
        data_strike = cut_file(
            self.file_strike,
            data_type,
            self.limites_zona,
            'Latitud')
        data_dip = cut_file(
            self.file_dip,
            data_type,
            self.limites_zona,
            'Latitud')
        data_falla = dict(
            fosa=data_fosa,
            profundidad=data_profundidad,
            strike=data_strike,
            dip=data_dip)
        #print('xerneas')
        #lool=[elem.get('Dip') for elem in data_falla.get('dip')]
        #lool=data_falla.get('dip')
        #print(lool)
        #print('amalgama')
        #print(data_falla.get('fosa'))
        #print(data_fosa)
        #print(data_profundidad)
        #print('sapa')
        return(data_falla)
        
    #def load_intersismico(self, limites=(0, -90), stations=[]):
    def load_intersismico(self, stations=[]):

        """ this function load the observated data related to the interseismic period. the limits 
            are required only if we have data in a greater space than we want to use. This data
            will be used in the inversion process.
        """
        data_type = {
                     'Latitud': float,
                     'Longitud': float,
                     'UE': lambda x: float(x)/1000,
                     'UN': lambda x: float(x)/1000,
                     'UU': lambda x: float(x)/1000
                    }
        archivo = self.fuente_estaciones
        #data_velocidad = cut_file(archivo, data_type, limites)
        data_velocidad = cut_file(archivo, data_type, self.limites_zona)

        data_velocidad_set = data_velocidad
        if stations:
            data_velocidad_set = [data for data in data_velocidad
                                  if data.get('Station') in stations]

        return data_velocidad_set





    def load_cosismico(self,  stations=[]):
        """ this function load the observated data related to the coseismic period. the limits 
            are required only if we have data in a greater space than we want to use. This data will be
            used in the inversion process
        """
        data_type = { 
                     'Latitud': float,
                     'Longitud': float,
                     'UE': lambda x: float(x)/1000,
                     'UN': lambda x: float(x)/1000,
                     'UU': lambda x: float(x)/1000}
        archivo = self.fuente_estaciones
        data_disp = cut_file(archivo, data_type, self.limites_zona)
        data_disp_set = data_disp
        if stations:
            data_disp_set = [data for data in data_disp
                                  if data.get('Station') in stations]

        return data_disp_set

    def load_postsismico(self):
        """ this function load the observated data related to the poseismic period. the limits 
            are required only if we have data in a greater space than we want to use. This data
            will be used in the inversion process
        """
        pass


    def load_data_estaciones(self,archivo):
        """ this function loads and specific  geodetic file that  wont  be used in the inversion process.
            the purpose of this loading is mostly for comparing data
        """


        data_type = { 
                     'Latitud': float,
                     'Longitud': float,
                     'UE': lambda x: float(x)/1000,
                     'UN': lambda x: float(x)/1000,
                     'UU': lambda x: float(x)/1000}
        
        data_disp = cut_file(archivo, data_type, limites)
        data_disp_set = data_disp
        return data_disp_set



    def load_data_slips(self,archivo,interfaz='upper'):
        """ this function loads and specific  slip  file. the purpose of this loading is mostly for
         comparing data. It must be defined whether the interphase wil be the superior or the inferior
         fault. 
        """

        if interfaz=='upper':
            slip='slip_upper'
        if interfaz=='lower':
            slip='slip_lower'

        data_type = { 
                     'Longitud': float,
                     'Latitud': float,
                     slip: float,
                     }
        
        data_slip = cut_file(archivo, data_type)
        data_slip_set = data_slip
        return data_slip_set



   


    def set_map_params(self, **kwargs):
        """ this function sets the Basemap params of build_map function"""
        self.map_params = dict(
            projection='merc',
            llcrnrlat=kwargs.get('latmin'),
            urcrnrlat=kwargs.get('latmax'),
            llcrnrlon=kwargs.get('lonmin'),
            urcrnrlon=kwargs.get('lonmax'),
            lat_ts=(kwargs.get('latmin')+kwargs.get('latmax'))/2,
            resolution='h',)
            # paralelos=kwargs.get('paralelos'),
            # meridianos=kwargs.get('meridianos'))
        print(self.map_params)

    def build_map(self):
        """  this function builds a map. it is possible deprecated lol"""
        del self.map_params['paralelos']
        del self.map_params['meridianos']
        plt.figure()
        m = Basemap(**self.map_params)
        m.drawcoastlines()
        m.drawparallels(
            np.arange(-90, 90, 5),
            labels=[1, 1, 1, 1])
        m.drawmeridians(
            np.arange(-90, 90, 5),
            labels=[0, 0, 0, 1])
        m.drawcountries()
        m.drawmapscale(-75, -41, 0, 0, 100)
        self.show_arrows(m)
        self.plot_fosa(m)
        plt.show()
        plt.savefig('/Outputs' + self.id_proceso + '/Figuras/mapa_previo.png',format='png',dpi='figure')

    

    
    
    def plot_fosa(self,m):
        """ this function plot the trench data in the m instance of the map building """ 
        lat_fosa=[elem.get('Latitud') for elem in self.data_falla.get('fosa')]
        lon_fosa=[elem.get('Longitud') for elem in self.data_falla.get('fosa')]
        x,y=m(lon_fosa,lat_fosa)
        m.plot(x,y,'-r',linewidth=2)
        #print('aaaa',lat_fosa,lon_fosa)
        
        
    def show_arrows(self, m):
    ###  this function plot the vector data in the m instance. it is probably deprecated lol"""
            lon = [elem.get('Longitud') for elem in self.data_estaciones]
            lat = [elem.get('Latitud') for elem in self.data_estaciones]
            Ue_obs = [elem.get('UE') for elem in self.data_estaciones]
            Un_obs = [elem.get('UN') for elem in self.data_estaciones]
            V = m.quiver(
                         lon,
                         lat,
                         Ue_obs,
                         Un_obs,
                         color='r',
                         scale=0.25,
                         width=0.0050,
                         linewidth=0.5,
                         headwidth=4.,
                         zorder=26
                         )
                
            plt.quiverkey(
                          V,
                          0.75,
                          0.18,
                          0.01,
                          r'GPS obs: 1 cm/yr',
                          labelpos='N',
                          labelcolor= (0,0,0),zorder=26,
                          )
                   

    def contour_maps(self,m,interfaz='upper'):
        ### this function plot the slip data in the m instance """
            # subfallas_methods=self.planos_falla_obj.get('subfallas_methods')
            # prof_subf_inv=subfallas_methods[0]
            # dip_subf_rad=subfallas_methods[1]
            # strike_subf_deg=subfallas_methods[2]
            # lonv_vert_subf_slab=subfallas_methods[3]
            # lat_vert_subf_slab=subfallas_methods[4]
            # lon_cent_subf_slab=subfallas_methods[5]
            # lat_cent_subf_slab=subfallas_methods[6]
            # prof_subf_norm=subfallas_methods[7]
            # phi_placa_rad=subfallas_methods[8]
            # rake_inv_rad=subfallas_methods[9]
            # rake_norm_rad=subfallas_methods[10]

            prof_subf_inv=self.subfallas_model_dict.get('depth_model')
            strike_subf_deg=self.subfallas_model_dict.get('strike_model')
            dip_subf_rad=np.radians(self.subfallas_model_dict.get('dip_model'))
            lonv_vert_subf_slab=self.subfallas_model_dict.get('lon_vertices')
            lat_vert_subf_slab=self.subfallas_model_dict.get('lat_vertices')
            lon_cent_subf_slab=self.subfallas_model_dict.get('lon_central')
            lat_cent_subf_slab=self.subfallas_model_dict.get('lat_central')
            strike_rad=self.subfallas_model_dict.get('strike_rad')
            prof_subf_norm=prof_subf_inv+self.alto_placa/np.cos(dip_subf_rad)
            rake_inv_rad=self.subfallas_model_dict.get('rake_inv_rad')
            rake_norm_rad=self.subfallas_model_dict.get('rake_norm_rad')

            strike_rad = np.radians(self.strike_aux_deg)


            #which_data=kwargs.get('which_data','desplaz')


            
            #if show_vectores:
                #data_disp_set=self.load_data_estaciones(which_data)
                #lat=[elem.get('Latitud') for elem in data_disp_set]
                #lon=[elem.get('Longtitud') for elem in data_disp_set]
                #slip=[elem.get('') for elem in data_disp_set]
            
            if interfaz == 'upper':
                data = self.load_data_slips(self.upper_interface_output,interfaz='upper')
                key='slip_upper'
            if interfaz == 'lower':
                data = self.load_data_slips(self.lower_interface_output,interfaz='lower')
                key='slip_lower'


                
            #lat=[float(elem.get('lat_central')) for elem in data]
            #lon=[float(elem.get('lon_central')) for elem in data]
            #print(data)
            slip=[float(elem.get(key)) for elem in data]
            ## se crea una mascara para evitar que el contourf grafique fuera de las subfallas

            slip_mask         = []
            lat_central_mask  = []
            lon_central_mask  = []


            subfalla=0
            #Mascara para el dibujo. Evita que contourf pinte/grafique fuera de las subfallas.
            for j_ny in range(self.grid_size[1]):
                for j_nx in range(self.grid_size[0]):
                    slip_mask.append(slip[subfalla])      #<----------------
                    lat_central_mask.append(lat_cent_subf_slab[subfalla])
                    lon_central_mask.append(lon_cent_subf_slab[subfalla])
                    if (j_ny==0):  
                        slip_mask.append(0)
                        ## QUEDE EN STRIKE_RAD, probar despues con strike variable 
                        alpha = np.degrees(strike_rad)
                        beta = alpha + 90
                        lataux , lonaux, aux = sub.vinc_pt( lat_cent_subf_slab[subfalla],
                                                            lon_cent_subf_slab[subfalla],
                                                            beta ,
                                                             self.ancho_placa/self.grid_size[1] )
                        lat_central_mask.append(lataux)
                        lon_central_mask.append(lonaux)

                    if (j_ny==(self.grid_size[1]-1)):
                        slip_mask.append(slip[subfalla])
                        alpha = np.degrees(strike_rad)
                        beta = alpha + 270
                        lataux , lonaux, aux = sub.vinc_pt(lat_cent_subf_slab[subfalla],
                                                           lon_cent_subf_slab[subfalla],
                                                           beta , 
                                                           (self.ancho_placa/self.grid_size[1])/2 )
                        lat_central_mask.append(lataux)
                        lon_central_mask.append(lonaux)
                        #borde final
                        slip_mask.append(0)
                        alpha = np.degrees(strike_rad)
                        beta = alpha + 270
                        lataux , lonaux, aux = sub.vinc_pt( lat_cent_subf_slab[subfalla],
                                                            lon_cent_subf_slab[subfalla],
                                                            beta ,
                                                            (self.ancho_placa/self.grid_size[1])/2+(self.ancho_placa/self.grid_size[1])/4 )
                        lat_central_mask.append(lataux)
                        lon_central_mask.append(lonaux)            
                    if (j_nx == 0):
                        slip_mask.append(slip[subfalla])   #<----------------
                        alpha = np.degrees(strike_rad)
                        beta = alpha + 180
                        lataux , lonaux, aux = sub.vinc_pt( lat_cent_subf_slab[subfalla],
                                                            lon_cent_subf_slab[subfalla],
                                                             beta , (self.ancho_placa/self.grid_size[1])/2 )
                        lat_central_mask.append(lataux)
                        lon_central_mask.append(lonaux)   
                    if (j_nx == (self.grid_size[0]-1)):
                        slip_mask.append(slip[subfalla])   #<----------------
                        alpha = np.degrees(strike_rad)
                        beta = alpha 
                        lataux , lonaux, aux = sub.vinc_pt( lat_cent_subf_slab[subfalla],
                                                            lon_cent_subf_slab[subfalla],
                                                            beta , (self.ancho_placa/self.grid_size[1])/2 )
                        lat_central_mask.append(lataux)
                        lon_central_mask.append(lonaux)    
                    subfalla=subfalla+1
            
           
            numcols=500
            numrows=500
            xi = np.linspace(min(lon_central_mask),max(lon_central_mask),numcols)
            yi = np.linspace(min(lat_central_mask),max(lat_central_mask),numrows)
            xi,yi= np.meshgrid(xi,yi)
            zi=scp.interpolate.griddata((np.array(lon_central_mask),
                                        np.array(lat_central_mask)),
                                        np.array(slip_mask),
                                        (xi,yi),
                                        method='nearest')
            ## esto es perfectible          
            tick_colorbar=list(np.linspace(min(slip_mask),max(slip_mask),8))
            #print('t',tick_colorbar)
            #tick_colorbar[float("{0:.1f}".format(x)) for  x in tick_colorbar] 
            # tick_colorbar    = [0,
            #                     float("{0:.1f}".format(max(slip_mask)*(0.25))),
            #                     float("{0:.1f}".format(max(slip_mask)*(0.5))),
            #                     float("{0:.1f}".format(max(slip_mask)*(0.75))),
            #                     float("{0:.1f}".format(max(slip_mask)))]

            paleta=cm.GMT_polar_r
            niveles_contourf=np.linspace(min(slip_mask),max(slip_mask),50)
            xi,yi=m(xi,yi)
            im=m.contourf(xi,yi,zi,
                          vmin=min(slip_mask),
                          vmax=max(slip_mask),
                          levels=niveles_contourf,cmap=paleta,zorder=18,extend='max') #,extend='max'
            cb=m.colorbar(im,ticks=tick_colorbar,
                          location='bottom',size='3%',
                          pad='8%',
                          extend='max')   
    def mapa_geometria(self,**kwargs):
        """  this function builds a map of the studied zone. Several parameters can be plotted
             assigning  different values in the **kwargs """
        # del self.map_params['paralelos']
        # del self.map_params['meridianos']
        plot_fosa=kwargs.get('plot_fosa',True)

        plot_contourf=kwargs.get('plot_contourf',False)
        interfaz=kwargs.get('interfaz','upper')


        plot_datos=kwargs.get('plot_datos',False)
        which_datos=kwargs.get('which_datos','data')
        quiverkey=kwargs.get('quiverkey',False)
        plot_datos_and_inversion=kwargs.get('datos_and_inv',False)

        #show_vectores=kwargs.get('show_vectores',False)


        plt.figure()
        m = Basemap(**self.map_params)               
        m.drawcoastlines()
        m.drawcountries()
        #Se trazan los paralelos.
        m.drawparallels(
            np.arange(-90, 90, 5),
            labels=[1, 1, 1, 1])
        m.drawmeridians(
            np.arange(-90, 90, 5),
            labels=[0, 0, 0, 1])      

        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        x,y = m(lon,lat)
        m.plot(x,y,'go',markersize=10)

        if plot_fosa:
            self.plot_fosa(m)

        if plot_contourf:
            self.contour_maps(m,interfaz=interfaz)

        if plot_datos:
            #print(which_datos)
            self.choose_arrows(m,arrows=which_datos,quiverkey=quiverkey)

        if plot_datos_and_inversion:
                #print('cuqui')
                self.choose_arrows(m,arrows='data',quiverkey=quiverkey)
                self.choose_arrows(m,arrows='invertidas',quiverkey=quiverkey)


        
        if self.falla_AB:
            lon_fallas_ab=self.subfallas_model_dict.get('lon_vertices')
            lat_fallas_ab=self.subfallas_model_dict.get('lat_vertices')
            #print(lat_fallas_ab)
            #print('lon_ab',lon_fallas_ab)
            #print('lat_ab',lat_fallas_ab)
            for lonx,laty in zip(lon_fallas_ab,lat_fallas_ab):
                x,y=m(lonx,laty)
                m.plot(x,y,color='b',  linestyle = '-', linewidth = 0.15,zorder=21)

        # if self.falla_AB:
        #     lon_fallas_ab=self.planos_falla_obj.get('subfallas_methods')[3]
        #     lat_fallas_ab=self.planos_falla_obj.get('subfallas_methods')[4]
        #     #print(lat_fallas_ab)
        #     #print('lon_ab',lon_fallas_ab)
        #     #print('lat_ab',lat_fallas_ab)
        #     for lonx,laty in zip(lon_fallas_ab,lat_fallas_ab):
        #         x,y=m(lonx,laty)
        #         m.plot(x,y,color='b',  linestyle = '-', linewidth = 0.15,zorder=21)



       
        #Distribucion de subfallas para los planos de falla C y D
        if self.falla_CD:
            lon_fallas_cd=self.planos_falla_obj.get('CD')[0].vert_lon_new
            lat_fallas_cd=self.planos_falla_obj.get('CD')[0].vert_lat_new
            #print('lon_cd',lon_fallas_cd)
            #print('lat_cd',lat_fallas_cd)
            for lonx,laty in zip(lon_fallas_cd,lat_fallas_cd):
                x,y=m(lonx,laty)
                m.plot(x,y,color='r',  linestyle = '-', linewidth = 0.15,zorder=21)
           

        

 
            #Distribucion de subfallas para el plano de falla E     
        if self.falla_E:
            lon_fallas_e=self.planos_falla_obj.get('E')[0]
            lat_fallas_e=self.planos_falla_obj.get('E')[1]
            #print('lon_e',lon_fallas_e)
            #print('lat_e',lat_fallas_e)
            for lonx,laty in zip(lon_fallas_e,lat_fallas_e):
                x,y=m(lonx,laty)
                m.plot(x,y,color='r',  linestyle = '-', linewidth = 0.15,zorder=21)


      
        plt.show()
        if not interfaz:
            plt.savefig('Outputs'+self.id_proceso+'/Figuras/geometria_modelo.png',format='png',dpi='figure')
        if interfaz:
            plt.savefig('Outputs'+self.id_proceso+'/Figuras/geometria_slip_'+interfaz+'_.png',format='png',dpi='figure')

        
#####

    def choose_arrows(self,m,arrows='data',quiverkey=False):
        if arrows=='data':
            self.show_arrows_data(m,quiverkey)
        if arrows=='invertidas':
            self.show_arrows_invertidas(m,quiverkey)
        if arrows=='directas':
            self.show_arrows_directas(m,quiverkey)


    def show_arrows_data(self,m,quiverkey):
        """ this function plots te vector data of observations. The data is plotted
        in the m instance"""
        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        Ue_obs = [elem.get('UE') for elem in self.data_estaciones]
        Un_obs = [elem.get('UN') for elem in self.data_estaciones]
        x,y=m(lon,lat)
        V = m.quiver(
                         x,y,
                         Ue_obs,
                         Un_obs,
                         color='m',
                         scale=10,
                         width=0.0050,
                         linewidth=0.5,
                         headwidth=4.,
                         zorder=26,
                         label='V_obs')
        if quiverkey:

            plt.quiverkey(
                                 V,
                                 x1,
                                 y1,
                                 0.01,
                                 label='V_teo: 1cm/yr',
                                 labelpos='N',
                                 coordinates='data',
                                 labelcolor= (0,0,0),
                                 zorder=26, 
                                 fontproperties={'size': 8, 'weight': 'bold'})


    def show_arrows_invertidas(self,m,quiverkey):
        """ this function plots the vector data obtained in a inversion process. The data is plotted
            in the m instance """
        if self.filtro=='intersismico':
            arrows=self.outputs_inv_cosism
        if self.filtro=='cosismico':
            lon = [elem.get('Longitud') for elem in self.data_estaciones]
            lat = [elem.get('Latitud') for elem in self.data_estaciones]
            Ue_inv=self.outputs_inv_cosism.get('UE')
            #Ue_inv=[1000*x for x in Ue_inv]
            Un_inv=self.outputs_inv_cosism.get('UN')
            #Un_inv=[1000*x for x in Ue_inv]

        print('vel',Ue_inv,Un_inv)
        x,y=m(lon,lat)

        #m.plot(x,y,'go',markersize=1)
        
        W =  m.quiver(
                      x,y,
                      Ue_inv,
                      Un_inv,
                      color='k',
                      scale=10,
                      width=0.0050,
                      linewidth=0.5,
                      headwidth=4.,
                      zorder=26,
                      label='V_teo')
        if quiverkey:

            plt.quiverkey(
                          W,
                          x,
                          y,
                          0.01,
                          'V obs: 1 cm/yr',
                           labelpos='N',
                           coordinates='data',
                           labelcolor= (0,0,0),
                           zorder=26, 
                           fontproperties={'size': 8, 'weight': 'bold'}
                            )          


    def show_arrows_directas(self,m,quiverkey):
    ### this function plots the vector data obtained making a direct problem (synthetic data for given a
    ###        slip configuration). The data is plotted in the m instance 
        if self.filtro=='intersismico':
            arrows=None

        if self.filtro=='cosismico':
            lon = [elem.get('Longitud') for elem in self.data_estaciones]
            lat = [elem.get('Latitud') for elem in self.data_estaciones]
            Ue_teo=self.desplazamientos_sinteticos.get('UE')
            Un_teo=self.desplazamientos_sinteticos.get('UN')
            #print(Ue_teo)
            #print(Un_teo)
        x,y=m(lon,lat)
        W =  m.quiver(
                      x,y,
                      np.array(Ue_teo),
                      np.array(Un_teo),
                      color='g',
                      scale=1000,
                      width=0.0050,
                      linewidth=0.5,
                      headwidth=4.,
                      zorder=26,
                      label='V_teo')
        if quiverkey:

            plt.quiverkey(
                          W,
                          x,
                          y,
                          0.01,
                          'V obs: 1 cm/yr',
                          labelpos='N',
                          coordinates='data',
                          labelcolor= (0,0,0),
                          zorder=26, 
                          fontproperties={'size': 8, 'weight': 'bold'}
                           )      


  
    def construye_A(self):
        """ this function builds the A Green Function Matrix """
        #self.generar_subfallas()
        A = self.model_matrix_slab(falla='inversa',
                                   save_subfallas_methods = True,
                                   generar_subfallas = True)

        

        self.planos_falla_obj.update({'A':A})

    def construye_B(self):
        """ this function builds the B Green Function Matrix """
        B = self.model_matrix_slab(falla='normal', save_subfallas_methods = True , generar_subfallas = False)
        self.planos_falla_obj.update({'B':B})
                  
                                   
    def construye_C(self):      
        """ this function builds the C Green Function Matrix """
        pass

    def construye_D(self):
        """ this function builds the D Green Function Matrix """

        pass

    def construye_CD(self):
        pass


    def construye_E(self):
        """ this function builds the E Greens Function Matrix """

        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        AB = self.planos_falla.get('AB')
        latf = AB.get('inferior')[0]
        lonf = AB.get('inferior')[1]
        lat1 = AB.get('superior')[0]
        lon1 = AB.get('superior')[1]
        E = self.data_falla.get('E')
        constructor_E_opt = {
            'EW': sub.constructor_fallaE_EW,
            'WE': sub.constructor_fallaE_WE}
        constructor_E = constructor_E_opt.get(
            self.sentido_subduccion, print)
        fosa_lon = [elem.get('Longitud')
                    for elem in self.data_falla.get('fosa')]
        fosa_lat = [elem.get('Latitud')
                    for elem in self.data_falla.get('fosa')]

        PF_E = self.planos_falla.get('E')
        rake_norm_ref_deg = self.planos_falla_obj.get('AB')[3]
        lon_vertices_E, lat_vertices_E, L_E, strike_E_rad = constructor_E(
            lonf,
            latf,
            lon1,
            lat1,
            self.v_placa,
            PF_E.get("W_E"),
            PF_E.get("H_E"),
            PF_E.get("nx_E"),
            PF_E.get("ny_E"),
            PF_E.get("delta_lat_E"),
            fosa_lon,
            fosa_lat
        )
        # componentes de la velocidad
        n_dim = 2
        E = sub.model_matrix_slab_E(
            n_dim,
            0,
            PF_E.get("H_E"),
            PF_E.get("W_E"),
            L_E,
            np.radians(rake_norm_ref_deg),
            strike_E_rad,
            PF_E.get("nx_E"),
            PF_E.get("ny_E"),
            lon_vertices_E,
            lon_vertices_E,
            lon,
            lat)
        self.planos_falla_obj.update({'E':
                                      (lon_vertices_E,
                                       lat_vertices_E, L_E,
                                       strike_E_rad, E)})
        return lon_vertices_E, lat_vertices_E, L_E, strike_E_rad


 
    # def calcula_inversion(self,**kwargs):
    #     interfases=kwargs.get('interfases':'double')
    #     self.inversion_intersismico_slab(interfases=interfases)


        #L = self.planos_falla_obj.get('AB')[-3]
        # print('L')
        # print(L)

        ## POR AHORA VER SOLO A Y B
        # print(' -----------------------------------------------------------------------')
        # print(' ETAPA  6.2 == Caracterizacion geometrica: C y D                        ')
        # print(' -----------------------------------------------------------------------')    
        # #C = self.planos_falla_obj.get('CD')[-2]
        #D = self.planos_falla_obj.get('CD')[-1]
#        print('Matriz C')
#        print(C)
#        print('Matriz D')
# #        print(D)
#         print(' -----------------------------------------------------------------------')
#         print(' ETAPA  6.3 == Caracterizacion geometrica: E                            ')
#         print(' -----------------------------------------------------------------------')       

#         #1. Caracterizacion geometrica para el plano de falla E
#         print('6.3.1 Calculando matriz E...')    
#         E = self.planos_falla_obj.get('E')[-1]
# #        print('Matriz E')
# #        print(E)

### ESTO SOLO PARA INTERSISMICO 
        # print(' -----------------------------------------------------------------------')
        # print(' ETAPA  7 == Proceso de inversion                                       ')
        # print(' -----------------------------------------------------------------------')

        # #Se establecen velocidades libres en los planos de falla C, D y E.
        # print('7.1 Incluyendo velocidad de placa     ...')
        # l_CD  = self.planos_falla_obj.get('CD')[-3]
        # dip_radianes_CD_final = self.planos_falla_obj.get('CD')[0].dip_slab_rad
        # num_subfallas_CD = len(dip_radianes_CD_final)
        # #Velocidades libres plano de falla C y D.
        # for subfalla in range(num_subfallas_CD):  
        #     if subfalla == 0:
        #       Vp_CD = self.v_placa
        #     else:
        #       Vp_CD = np.hstack((Vp_CD, self.v_placa)) 
        # E = self.planos_falla_obj.get('E')[-1]
        # PF_E = self.planos_falla.get('E')
        # for subfalla in range(PF_E.get("nx_E")*PF_E.get("ny_E")):
        #     if subfalla == 0:
        #       Vp_E = self.v_placa
        #     else:
        #       Vp_E = np.hstack((Vp_E, self.v_placa))  
        '''
        #############################################################################
        SE REALIZA INVERSION
        #############################################################################
        '''

    
    # def proj_mesh2okada(self,lat_falla,lon_falla,strike): 
    #     """ this function obtains the (xi,yi) coordinates in the okada reference frame for given data in the 
    #     geographic reference frame
    #     lat_falla : subfault latitude
    #     lon_falla : subfault longitude
    #     strike: strike of the subfault  """  
    #     # OUTPUT: coordenadas xi,yi proyectadas en los ejes de Okada
    #     #OBS FELIPE: La funcion determina las coordenadas okada (X Y) de un un punto
    #     #de observacion a partir de sus coordenadas geograficas.        
    #     lon_estaciones = [elem.get('Longitud') for elem in self.data_estaciones]
    #     lat_estaciones = [elem.get('Latitud') for elem in self.data_estaciones]
    #     name_estaciones=[elem.get('Station') for elem in self.data_estaciones]
    #     print('namens')
    #     print(self.data_estaciones)
    #     print('cod')
    #     print(self.fuente_estaciones)
    #     dist_lat=[]
    #     dist_lon=[]
    #     for (lat_sta,lon_sta) in zip(lat_estaciones,lon_estaciones):
    #         print('calamardo')
    #         l,az,baz=vinc_dist(lat_falla,lon_falla,lat_sta,lon_sta)
    #         if lon_sta >= lon_falla 
    #         print('distancias,azimutes y backaz sin editar')
    #         print(l,az,baz)
    #         # if az > 300:
    #         #     print('problema con los azimuts... rescribiendo')
    #         #     az = abs(az-360)
    #         #     print(az)
            

    #         dist_lat.append(l*np.cos(np.radians(az)))
    #         dist_lon.append(l*np.sin(np.radians(az)))
    #     ## transformar a array
    #     dist_lat_arr=np.array(dist_lat)
    #     dist_lon_arr=np.array(dist_lon)
    #     # print('tutanca')
    #     # print(len(dist_lat_arr))
    #     # print(len(dist_lon_arr))
    #     # print(strike)
    #     xi = dist_lat_arr*np.cos(strike) + dist_lon_arr*np.sin(strike)  
    #     yi = dist_lat_arr*np.sin(strike) - dist_lon_arr*np.cos(strike)

    #     return xi,yi



    
       
  
    ################################################################################
    
    def proj_mesh2okada(self,lat_falla,lon_falla,strike): 
        """ this function obtains the (xi,yi) coordinates in the okada reference frame for given data in the 
        geographic reference frame
        lat_falla : subfault latitude
        lon_falla : subfault longitude
        strike: strike of the subfault  """  
        # OUTPUT: coordenadas xi,yi proyectadas en los ejes de Okada
        #OBS FELIPE: La funcion determina las coordenadas okada (X Y) de un un punto
        #de observacion a partir de sus coordenadas geograficas.        
        lon_estaciones = [elem.get('Longitud') for elem in self.data_estaciones]
        lat_estaciones = [elem.get('Latitud') for elem in self.data_estaciones]
        name_estaciones=[elem.get('Station') for elem in self.data_estaciones]
        #print('namens')
        #print(self.data_estaciones)
        #print('cod')
        #print(self.fuente_estaciones)
        dist_lat=[]
        dist_lon=[]
        az_m=[]
        l_m=[]
        baz_m=[]
        bandera= False
        for (lat_sta,lon_sta) in zip(lat_estaciones,lon_estaciones):
          
           l,az,baz=vinc_dist(lat_falla,lon_falla,lat_sta,lon_sta)
           
           az_m.append(az)
           l_m.append(l)
           baz_m.append(baz)
           # print('az')  
           # print(l,az,baz)
           if bandera:
               if az <= 180:
                    dist_lat.append(l*np.cos(np.radians(az)))
                    dist_lon.append(l*np.sin(np.radians(az)))
               if az > 180 :
                    dist_lat.append(l*np.cos(np.radians(360-az)))
                    dist_lon.append(l*np.sin(np.radians(360-az)))

               # if az > 355:
               #      print('aca')
               #      dist_lat.append(l*np.cos(np.radians(370-az)))
               #      dist_lon.append(l*np.sin(np.radians(370-az)))
               # if az > 90 and  az :
               #      dist_lat.append(l*np.cos(np.radians(360-az)))
               #      dist_lon.append(l*np.sin(np.radians(360-az)))

               # if az > 90 and az <= 180:
               #      dist_lat.append(l*np.cos(np.radians(90-az)))
               #      dist_lon.append(l*np.sin(np.radians(90-az))) 

               # if az > 180 and az <= 270:
               #      dist_lat.append(l*np.cos(np.radians(290-az)))
               #      dist_lon.append(l*np.sin(np.radians(290-az)))
               
               # if az > 270:
               #      dist_lat.append(l*np.cos(np.radians(380-az)))
               #      dist_lon.append(l*np.sin(np.radians(380-az))) 
       
           if not bandera:
               dist_lat.append(l*np.cos(np.radians(az)))
               dist_lon.append(l*np.sin(np.radians(az)))


       
        dist_lat_arr=np.array(dist_lat)
        dist_lon_arr=np.array(dist_lon)         
        xi = dist_lat_arr*np.cos(strike) + dist_lon_arr*np.sin(strike)
        yi = dist_lat_arr*np.sin(strike) - dist_lon_arr*np.cos(strike)

        #yi = -dist_lat_arr*np.sin(strike) + dist_lon_arr*np.cos(strike)
        #yi = dist_lat_arr*np.sin(strike) - dist_lon_arr*np.cos(strike)
        
        return xi,yi
   

    def subfalla(self,dip,strike):
        """ we get the central coordinates of the subfault assumming a rectangular fault """

        ### loading AB planes data
        AB   = self.planos_falla.get('AB')
        nx   = self.grid_size[0]
        ny   = self.grid_size[1]

        latf = AB.get('inferior')[0]
        lonf = AB.get('inferior')[1]
        lat1 = AB.get('superior')[0]
        lon1 = AB.get('superior')[1]

        alfa = np.degrees(strike)
        beta = alfa  + 270.0
        
        dx = self.L/nx
        ## projected length of the element of line along dip
        dy=self.ancho_placa*np.cos(dip)/ny
        #dy = self.ancho_placa*np.cos(dip)/ny
        
        ## we get the coordinates of the deeper point of the subfault
        lat_prima,lon_prima,alpha21=vinc_pt(latf,
                                            lonf,
                                            np.degrees(strike+np.pi/2),
                                            self.ancho_placa*np.cos(self.dip_plano_inicial)                                            
                                            )
        # lat_prima,lon_prima,alpha21=vinc_pt(latf,
        #                                     lonf,
        #                                     np.degrees(strike+np.pi/2),
        #                                     self.ancho_placa*np.cos(self.dip_plano_inicial)
        #                                     )
        ## we move the vertex to the midpoint of the subfault along strike
        lat_step1,lon_step1,alpha21 = vinc_pt(lat_prima,lon_prima,alfa, dx/2)

        ## we move the vertex to the midpoint of the subfault along dip. Now the 
        ## point is at the midpoint of the subfault. the azimut is beta = strike + 270 
        ## because we start meshing at the bottom-south of the subfault.
        lat_c,lon_c,alpha21 = vinc_pt(lat_step1,lon_step1,beta,dy/2)

       
        xs=[]
        ys=[]
        for j in range(ny):
            for i in range(nx):
                lat_step1 = lat_c
                lon_step1 = lon_c
                ## if i > 0, we move the point along strike
                if i > 0:
                    lat_step1,  lon_step1, alpha21 = vinc_pt( lat_c, lon_c, alfa , i*dx )
                lat_save = lat_step1
                lon_save = lon_step1
                ## if j > 0, we move the point along dip
                if j > 0 :
                    lat_save , lon_save, alpha21 = vinc_pt( lat_step1, lon_step1,  beta , j*dy )
                xs.append(lat_save)
                ys.append(lon_save)
        
        lon_slip =np.array([ys])[0]
        lat_slip=np.array([xs])[0]
       
         ## the outputs are the points where we calculate the slip of the subfault.
        
        return lon_slip,lat_slip







    def coordenadas_subfallas(self):
        """ this function gets the subfault coordinates but now we consider a geometry
        that varies with the trench """
        AB   = self.planos_falla.get('AB')
        latf = AB.get('inferior')[0]
        lonf = AB.get('inferior')[1]
        lat1 = AB.get('superior')[0]
        lon1 = AB.get('superior')[1]
        fosa_lon = [elem.get('Longitud') for elem in self.data_falla.get('fosa')]
        fosa_lat = [elem.get('Latitud') for elem in self.data_falla.get('fosa')] 
        nx=self.grid_size[0]
        ny=self.grid_size[1]
        strike_rad = np.radians(self.strike_aux_deg)
        ## we build the rake of reference for normal faults with the reference rake for
        ## thrust fault
        rake_norm_ref_deg=self.rake_referencia + 180
        # ## we get the coordinates of the bottom-south of the system
        # lat_dep,lon_dep,alpha21 = vinc_pt(latf,
        #                                   lonf,
        #                                   np.degrees(strike_rad+np.pi/2),
        #                                   self.ancho_placa*np.cos
        #                                   )
        
        # lat_dep,lon_dep,alpha21 = vinc_pt(latf,
        #                                   lonf,
        #                                   np.degrees(strike_rad+np.pi/2),
        #                                   self.ancho_placa*np.cos(self.dip_plano_inicial)
        #                                   )
        
       
       
        ## we get the coordinates of the subfaults assuing the same config. for every fault
        lon_cent_pre,lat_cent_pre= self.subfalla(self.dip_plano_inicial,strike_rad)
        
        lon_cent_pre2 = []
        lat_cent_pre2 = []
        
        k = 0
        for i_nx in range(nx):
            for j_ny in range(ny):
                lon_cent_pre2.append(lon_cent_pre[k])
                lat_cent_pre2.append(lat_cent_pre[k])       
                k = k+1
        
        dx = self.L/nx
        dy = self.ancho_placa*np.cos(self.dip_plano_inicial)/ny

        #dy = self.ancho_placa*np.cos(self.dip_plano_inicial)/ny

        ## we will get the points of every subfault hence nx*ny
        FIL=nx*ny
        ## we will consider 5 vertex per fault, the last vertex is the same than the first
        COL= 5


        vertices_lon=[]
        vertices_lat=[]
        ## number of rows of the matrix. COL is the number of colums of the matrix.
        ## for example if we have 90 subfaults the size of the matrix will be (90,5)
        for i in range(FIL):               
            vertices_lon.append([0]*COL)    
            vertices_lat.append([0]*COL)
    
        ## we set up the key vertexes. These are the northern and souther vertexes
        ## along the trench. We need them for searching reference values with slab1.0 model
        vertice_clave_lon_inf=[]
        vertice_clave_lat_inf=[]
        vertice_clave_lon_sup=[]
        vertice_clave_lat_sup=[]
    
        ## the size of this array is the number of faults along strike
        for i in range(nx):                       
            vertice_clave_lon_inf.append([0]*1)    
            vertice_clave_lat_inf.append([0]*1)
            vertice_clave_lon_sup.append([0]*1)    
            vertice_clave_lat_sup.append([0]*1)



      
        k=0
        for i_ny in range(ny):
            fila=0
            for j_nx in range(nx):
                frac = Fraction((ny-i_ny),ny)
                # latitude and longitude of the first vertex  (south-east vertex)
                lat1_step1,lon1_step1,a= vinc_pt(lat_cent_pre2[k],
                                                 lon_cent_pre2[k],
                                                 np.degrees(strike_rad)+90,
                                                 dy/2
                                                 )
                
                lat1_vert,lon1_vert,a = vinc_pt(lat1_step1,
                                                lon1_step1,
                                                np.degrees(strike_rad)+180,
                                                dx/2
                                                )
                
                # latitude and longitude of the second vertex (north-east vertex)
                lat2_step1,lon2_step1,a = vinc_pt(lat_cent_pre2[k],
                                                 lon_cent_pre2[k],
                                                 np.degrees(strike_rad)+90,
                                                 dy/2
                                                 )
                
                lat2_vert,lon2_vert,a = vinc_pt(lat2_step1,
                                                lon2_step1,
                                                np.degrees(strike_rad),
                                                dx/2
                                                )

                # latitude and longitude of the third vertex (north-west vertex)

                lat3_vert,lon3_vert,a = vinc_pt(lat2_vert,
                                                lon2_vert,
                                                np.degrees(strike_rad) + 270,
                                                frac*self.ancho_placa*np.cos(self.dip_plano_inicial)
                                                )

                # lat3_vert,lon3_vert,a = vinc_pt(lat2_vert,
                #                                 lon2_vert,
                #                                 np.degrees(strike_rad) + 270,
                #                                 frac*self.ancho_placa*np.cos(self.dip_plano_inicial)
                #                                 )

                # latitude and longitude of the fourth vertex (south-west vertex)
                lat4_vert,lon4_vert,a = vinc_pt(lat3_vert,
                                                lon3_vert,
                                                np.degrees(strike_rad) + 180,
                                                dx
                                                )
                # latitude and longitud of the fifth vertex (sane first vertex)
                lat5_vert = lat1_vert
                lon5_vert = lon1_vert
       
                ## if we are in the last row (with the trench)
                if (i_ny == ny-1):
                    vertice_clave_lon_inf[j_nx][0]=lon4_vert
                    vertice_clave_lat_inf[j_nx][0]=lat4_vert
                    vertice_clave_lon_sup[j_nx][0]=lon3_vert
                    vertice_clave_lat_sup[j_nx][0]=lat3_vert
                k = k+1
        fila = fila+1


        ## with the key vertexes we obtain the distance of them to the rest of the trench (slab 1.0)
        desplazo=[]
        ## we loop over the subfault and we search the maximum longitude projected to the trench
        for i_nx in range(nx):
            minlon = vertice_clave_lon_inf[i_nx][0]
        ## we loop over the trench points
        ## DUDA EN ESTA PARTE
            for j in range(len(fosa_lat)):
                if (fosa_lat[j] >= vertice_clave_lat_inf[i_nx][0] + self.delta_lat and
                    fosa_lat[j] <= vertice_clave_lat_sup[i_nx][0] + self.delta_lat):
                    if fosa_lon[j] < minlon:
                        minlon=fosa_lon[j]
                    # else:
                    #     print("algo inesperado a ocurrido")
            #Se calcula distancia a desplazar para todos los puntos centrales.
            #Se esta probando delta LAT, TEST
            dl, aux, aux = vinc_dist(vertice_clave_lat_inf[i_nx][0],
                                     vertice_clave_lon_inf[i_nx][0],
                                     vertice_clave_lat_inf[i_nx][0],
                                     minlon
                                    )

            desplazo.append(dl)

        lon_central_final=[]
        lat_central_final=[]

        k = 0
        for i_ny in range(ny):
            ll = 0
            for i_nx in range(nx):
                dist = desplazo[ll]
                lat_central, lon_central, alpha21 = vinc_pt(lat_cent_pre[k],
                                                            lon_cent_pre[k],
                                                            np.degrees(strike_rad)+270,
                                                            dist)
                lat_central_final.append(lat_central)
                lon_central_final.append(lon_central)
                k=k+1
                ll+=1

        ## se reescriben los verices desde una distribucion rectangular a una
        ## adaptada al slab

        k = 0
        for i_ny in range(ny):
            fila = 0
            for j_nx in range(nx):
                frac_W = Fraction(self.ancho_placa,ny)
                 # Latitude and longitude of the first vertex
                lat1_step1,lon1_step1,a1 = vinc_pt(lat_central_final[k],
                                                   lon_central_final[k],
                                                   np.degrees(strike_rad)+90,
                                                   dy/2
                                                   )
                
                lat1_vert,lon1_vert,a1 = vinc_pt(lat1_step1,
                                                 lon1_step1,
                                                 np.degrees(strike_rad)+180,
                                                 dx/2
                                                 )
                
                # Latitude and longitude of the second vertex
                lat2_step1,lon2_step1,a1 = vinc_pt(lat_central_final[k],
                                                   lon_central_final[k],
                                                   np.degrees(strike_rad)+90,
                                                   dy/2
                                                    )
                
                lat2_vert,lon2_vert,a1 = vinc_pt(lat2_step1,
                                                 lon2_step1,
                                                 np.degrees(strike_rad),
                                                 dx/2
                                                 )

                # Latitude and longitude of the third vertex
                lat3_vert,lon3_vert,a1 = vinc_pt(lat2_vert,
                                                 lon2_vert,
                                                 np.degrees(strike_rad) + 270,
                                                 frac_W*np.cos(self.dip_plano_inicial)
                                                 )

                # lat3_vert,lon3_vert,a1 = vinc_pt(lat2_vert,
                #                                  lon2_vert,
                #                                  np.degrees(strike_rad) + 270,
                #                                  frac_W*self.ancho_placa*np.cos(self.dip_plano_inicial)
                #                                  )

                # latitude and longitude of the fourth vertex
                lat4_vert,lon4_vert,a1 = vinc_pt(lat3_vert,
                                                 lon3_vert,
                                                 np.degrees(strike_rad) + 180,
                                                 dx
                                                 )

                # quinto vertice
                lat5_vert = lat1_vert
                lon5_vert = lon1_vert

                vertices_lon[k][0]=lon1_vert
                vertices_lon[k][1]=lon2_vert
                vertices_lon[k][2]=lon3_vert
                vertices_lon[k][3]=lon4_vert
                vertices_lon[k][4]=lon5_vert

                vertices_lat[k][0]=lat1_vert
                vertices_lat[k][1]=lat2_vert
                vertices_lat[k][2]=lat3_vert
                vertices_lat[k][3]=lat4_vert
                vertices_lat[k][4]=lat5_vert

                k = k + 1
            fila = fila + 1

        return lon_central_final,lat_central_final,vertices_lon,vertices_lat
    #return lon_central_final,lat_central_final,vertices_lon,vertices_lat
    
    def generar_subfallas(self):
        print('generar_subfallas')
        pass
        """"we generate the subfaults and call subfallas_Slabmodel class which updates 
            our information about the geomtry of the problem  """
    

    # #   load AB subfault data

    #     AB=self.planos_falla.get('AB')
    #     latf = AB.get('inferior')[0]
    #     lonf = AB.get('inferior')[1]
    #     lat1 = AB.get('superior')[0]
    #     lon1 = AB.get('superior')[1]
    #     ## cargar data de las estaciones
    #     lon = [elem.get('Longitud') for elem in self.data_estaciones]
    #     lat = [elem.get('Latitud') for elem in self.data_estaciones]
       
    #     strike_rad = None
    #     if self.sentido_subduccion == 'EW':
    #         strike_rad = np.radians(self.strike_aux_deg+180)
    #     if self.sentido_subduccion == 'WE':
    #         strike_rad = np.radians(self.strike_aux_deg)
    #     #rake_norm_ref_deg = self.rake_referencia+180      
    #     nx = self.grid_size[0]
    #     ny = self.grid_size[1]
    #     lon_central_final,lat_central_final,vertices_lon,vertices_lat=self.coordenadas_subfallas()
    #     ## we call the subfallas_Slabmodel class
    #     subfallas = sub.subfallas_SlabModel(
    #                                         nx,
    #                                         ny,
    #                                         self.ancho_placa,
    #                                         self.L,
    #                                         strike_rad,
    #                                         vertices_lon,
    #                                         vertices_lat,
    #                                         self.file_profundidad,
    #                                         self.file_dip,
    #                                         self.file_strike,
    #                                         latf,
    #                                         lat1)
    #     ## we update the values of this dict for using it later in other functions
    #     self.planos_falla_obj.update({'subfallas':(subfallas,
    #                                                self.L,
    #                                                nx,
    #                                                ny,
    #                                                strike_rad,
    #                                                vertices_lon,
    #                                                vertices_lat)
    #                                                })


    def model_matrix_slab(self,falla='inversa',save_subfallas_methods=True, generar_subfallas = True):
        """ we generate the subfaults if its needed (it is not needed, for example, if we are building B after A.
         the geometry is the same, we only change the depth and the rake """
        if generar_subfallas:
            self.subfallas_slabmodel()
        
        ## we load the data generated with the prior function

        #load_slab    = self.subfallas_slabmodel
        lon_cent=self.subfallas_model_dict.get('lon_central')
        lat_cent=self.subfallas_model_dict.get('lat_central')
        lon_vert=self.subfallas_model_dict.get('lon_vertices')
        lat_vert=self.subfallas_model_dict.get('lat_vertices')
        dip_model=self.subfallas_model_dict.get('dip_model')
        strike_model=self.subfallas_model_dict.get('strike_model')
        depth_model=self.subfallas_model_dict.get('depth_model')


        # subfallas    = load_slab[0]
        # L            = load_slab[1]
        nx           = self.grid_size[0]
        ny           = self.grid_size[1]
        # strike_rad   = load_slab[4]
        # vertices_lon = load_slab[5]
        # vertices_lat = load_slab[6]

        ### ALL THE DATA GENERATED BELOW IS USING SLAB 1.0 MODEL
        ## profundidad de subfallas correspondientes a los planos de falla A y B
        ## depth of the fault plane 
        profundidad_subfallas_inversa = self.subfallas_model_dict.get('depth_model')
        # print('prof')
        # print(profundidad_subfallas_inversa)

        ## dip of the fault plane (will work for both planes)
        dip_subfallas_radianes  = np.radians(self.subfallas_model_dict.get('dip_model'))
        # print('dip')
        # print(dip_subfallas_radianes)

        ## strike of the fault plane (will work for both planes)
        strike_subfallas_deg = self.subfallas_model_dict.get('strike_model')
        # print('strike')
        # print(strike_subfallas_deg)

        ## longitude of the vertex of fault plane (will work for both)
        lon_vertices_subfallas_slab = self.subfallas_model_dict.get('lon_vertices')
        # print('lon vert')
        # print(lon_vertices_subfallas_slab)

        ## latitude of the vertex of fault plane (will work for both)
        lat_vertices_subfallas_slab = self.subfallas_model_dict.get('lat_vertices')
        # print('lat vert')
        # print(lat_vertices_subfallas_slab)

        ## central longitude of the subfaults  of the fault plane (will work for both)
        lon_central_subfallas_slab = self.subfallas_model_dict.get('lon_cent')
        # print('lon central')
        # print(lon_central_subfallas_slab)

        ## central latitude of the subfaults  of the fault plane (will work for both)

        lat_central_subfallas_slab = self.subfallas_model_dict.get('lat_cent')
        # print('lat_central')
        # print(lat_central_subfallas_slab)


        ## a strike of reference    
        strike_rad=self.subfallas_model_dict.get('strike_rad')

        #we get the depth of the normal fault using a constant thickness of the slab 
        profundidad_subfallas_normal = profundidad_subfallas_inversa + \
                                       (self.alto_placa / np.cos(dip_subfallas_radianes))

        phi_placa_rad = self.subfallas_model_dict.get('phi_placa_rad')


        ## rake for inverse fault
        rake_inv_rad  = self.subfallas_model_dict.get('rake_inv_rad')
        ## rake for normal fault
        rake_norm_rad = self.subfallas_model_dict.get('rake_norm_rad')

       
            
        if falla == 'inversa':
            rake_usado = rake_inv_rad
            prof_usada = profundidad_subfallas_inversa
        if falla == 'normal':
            rake_usado = rake_norm_rad
            prof_usada = profundidad_subfallas_normal
        
        ndim=self.n_dim
        l = self.L/nx
        
        subfalla=0
        for j_ny in range(ny):
            for i_nx in range(nx):
                ## in this case we use real width of the fault since we are using
                ## green functions
                w=self.ancho_placa/ny

                if i_nx == 0  and j_ny == 0:
                    # print(lat_vertices_subfallas_slab[subfalla][0])
                    ddx,ddy= self.proj_mesh2okada(
                                                lat_vertices_subfallas_slab[subfalla][0],
                                                lon_vertices_subfallas_slab[subfalla][0],
                                                np.radians(strike_subfallas_deg[subfalla])

                                                )                      
                    ve,vn,vz = desplaz_okada(
                                             ddx,
                                             ddy,
                                             dip_subfallas_radianes[subfalla],
                                             1000*prof_usada[subfalla],
                                             w,
                                             l,
                                             rake_usado[subfalla],
                                             np.radians(strike_subfallas_deg[subfalla])
                                             )
                                            
                    if ndim == 1:                
                       M = ve
                    elif ndim == 2:
                       M = np.hstack((ve,vn))
                    elif ndim == 3:
                       M = np.hstack((ve,vn,vz))

                else:
                    
                    print('subfalla',subfalla)
                    ddx,ddy = self.proj_mesh2okada(lat_vertices_subfallas_slab[subfalla][0],
                                                   lon_vertices_subfallas_slab[subfalla][0],
                                                   np.radians(strike_subfallas_deg[subfalla])
                                              )

                    ve,vn,vz = desplaz_okada(ddx,
                                             ddy,
                                             dip_subfallas_radianes[subfalla],
                                             1000*prof_usada[subfalla],
                                             w,
                                             l,
                                             rake_usado[subfalla],
                                             np.radians(strike_subfallas_deg[subfalla])
                                             )
                    if self.n_dim == 1 :                
                        tempstack= ve
                    elif self.n_dim == 2 :
                        tempstack = np.hstack((ve,vn))
                    elif self.n_dim == 3 :
                        tempstack=  np.hstack((ve,vn,vz))

                    M = np.vstack((M,tempstack))

                subfalla=subfalla+1
        print('M')
        print(M)
        A= M.T
        
        if save_subfallas_methods:
            self.planos_falla_obj.update({'subfallas_methods':(profundidad_subfallas_inversa,
                                                          dip_subfallas_radianes,
                                                          strike_subfallas_deg,
                                                          lon_vertices_subfallas_slab,
                                                          lat_vertices_subfallas_slab,
                                                          lon_central_subfallas_slab,
                                                          lat_central_subfallas_slab,
                                                          profundidad_subfallas_normal,
                                                          phi_placa_rad,
                                                          rake_inv_rad,
                                                          rake_norm_rad)
                                                                        }) 
        return A

    # def model_matrix_slab_DEPRE(self,falla='inversa',save_subfallas_methods=True, generar_subfallas = True):
    #     """ we generate the subfaults if its needed (it is not needed, for example, if we are building B after A.
    #      the geometry is the same, we only change the depth and the rake """
    #     if generar_subfallas:
    #         self.generar_subfallas()
        
    #     ## we load the data generated with the prior function

    #     load_slab    = self.planos_falla_obj.get('subfallas')
    #     subfallas    = load_slab[0]
    #     L            = load_slab[1]
    #     nx           = self.grid_size[0]
    #     ny           = self.grid_size[1]
    #     strike_rad   = load_slab[4]
    #     vertices_lon = load_slab[5]
    #     vertices_lat = load_slab[6]

    #     ### ALL THE DATA GENERATED BELOW IS USING SLAB 1.0 MODEL
    #     ## profundidad de subfallas correspondientes a los planos de falla A y B
    #     ## depth of the fault plane 
    #     profundidad_subfallas_inversa = subfallas.Z
    #     # print('prof')
    #     # print(profundidad_subfallas_inversa)

    #     ## dip of the fault plane (will work for both planes)
    #     dip_subfallas_radianes  = subfallas.dip_slab_rad
    #     # print('dip')
    #     # print(dip_subfallas_radianes)

    #     ## strike of the fault plane (will work for both planes)
    #     strike_subfallas_deg = subfallas.strike_deg
    #     # print('strike')
    #     # print(strike_subfallas_deg)

    #     ## longitude of the vertex of fault plane (will work for both)
    #     lon_vertices_subfallas_slab = subfallas.vert_lon_new
    #     # print('lon vert')
    #     # print(lon_vertices_subfallas_slab)

    #     ## latitude of the vertex of fault plane (will work for both)
    #     lat_vertices_subfallas_slab = subfallas.vert_lat_new
    #     # print('lat vert')
    #     # print(lat_vertices_subfallas_slab)

    #     ## central longitude of the subfaults  of the fault plane (will work for both)
    #     lon_central_subfallas_slab = subfallas.lon_central_new
    #     # print('lon central')
    #     # print(lon_central_subfallas_slab)

    #     ## central latitude of the subfaults  of the fault plane (will work for both)

    #     lat_central_subfallas_slab = subfallas.lat_central_new
    #     # print('lat_central')
    #     # print(lat_central_subfallas_slab)

    #     #we get the depth of the normal fault using a constant thickness of the slab 
    #     profundidad_subfallas_normal = profundidad_subfallas_inversa + \
    #                                    (self.alto_placa / np.cos(dip_subfallas_radianes))

    #     phi_placa_rad = np.radians(np.degrees(strike_rad) + (360-(self.rake_referencia+180)))

    #     ## rake for inverse fault
    #     rake_inv_rad  = []
    #     ## rake for normal fault
    #     rake_norm_rad = []

    #     for index, elem in enumerate(strike_subfallas_deg):
    #         # Se establece rake en subfallas emplazadas en la
    #         # interfase superior con un movimiento relativo de tipo inverso
    #         rake = get_rake(
    #                         phi_placa_rad,
    #                         np.radians(elem),
    #                         dip_subfallas_radianes[index]
    #                         )
    #         rake_inv_rad.append(rake)
    #         # Se establece rake en subfallas emplazadas en la interfase
    #         # inferior con un movimiento relativo de tipo normal
    #         rake_norm_rad.append(rake_inv_rad[index]+np.pi)
            
    #     if falla == 'inversa':
    #         rake_usado = rake_inv_rad
    #         prof_usada = profundidad_subfallas_inversa
    #     if falla == 'normal':
    #         rake_usado = rake_norm_rad
    #         prof_usada = profundidad_subfallas_normal
        
    #     ndim=self.n_dim
    #     l = self.L/nx
    #     ## PORQUE ESTE ANCHO NO ES EL TIPICO CON COS(DIP) QUE USAMOS SIEMPRE
    #     w=self.ancho_placa/ny
    #     ## construir A
    #     subfalla=0
    #     for j_ny in range(ny):
    #         for i_nx in range(nx):
    #             if i_nx == 0  and j_ny == 0:
    #                 # print(lat_vertices_subfallas_slab[subfalla][0])
    #                 ddx,ddy= self.proj_mesh2okada(
    #                                             lat_vertices_subfallas_slab[subfalla][0],
    #                                             lon_vertices_subfallas_slab[subfalla][0],
    #                                             np.radians(strike_subfallas_deg[subfalla])
    #                                             )
                                           
    #                 ve,vn,vz = desplaz_okada(
    #                                          ddx,
    #                                          ddy,
    #                                          dip_subfallas_radianes[subfalla],
    #                                          prof_usada[subfalla],
    #                                          w,
    #                                          l,
    #                                          rake_usado[subfalla],
    #                                          np.radians(strike_subfallas_deg[subfalla])
    #                                          )
                                            
    #                 if ndim == 1:                
    #                    M = ve
    #                 elif ndim == 2:
    #                    M = np.hstack((ve,vn))
    #                 elif ndim == 3:
    #                    M = np.hstack((ve,vn,vz))

    #             else:
                    
    #                 print('subfalla',subfalla)
    #                 ddx,ddy = self.proj_mesh2okada(lat_vertices_subfallas_slab[subfalla][0],
    #                                                lon_vertices_subfallas_slab[subfalla][0],
    #                                                np.radians(strike_subfallas_deg[subfalla])
    #                                           )

    #                 ve,vn,vz = desplaz_okada(ddx,
    #                                          ddy,
    #                                          dip_subfallas_radianes[subfalla],
    #                                          prof_usada[subfalla],
    #                                          w,
    #                                          l,
    #                                          rake_usado[subfalla],
    #                                          np.radians(strike_subfallas_deg[subfalla])
    #                                          )
    #                 if self.n_dim == 1 :                
    #                     tempstack= ve
    #                 elif self.n_dim == 2 :
    #                     tempstack = np.hstack((ve,vn))
    #                 elif self.n_dim == 3 :
    #                     tempstack=  np.hstack((ve,vn,vz))

    #                 M = np.vstack((M,tempstack))

    #             subfalla=subfalla+1
    #     A= M.T
        
    #     if save_subfallas_methods:
    #         self.planos_falla_obj.update({'subfallas_methods':(profundidad_subfallas_inversa,
    #                                                       dip_subfallas_radianes,
    #                                                       strike_subfallas_deg,
    #                                                       lon_vertices_subfallas_slab,
    #                                                       lat_vertices_subfallas_slab,
    #                                                       lon_central_subfallas_slab,
    #                                                       lat_central_subfallas_slab,
    #                                                       profundidad_subfallas_normal,
    #                                                       phi_placa_rad,
    #                                                       rake_inv_rad,
    #                                                       rake_norm_rad)
    #                                                                     }) 
    #     return A

    
    def make_directo(self,**kwargs):
        interfases=kwargs.get('interfases','double')
        save_slips=kwargs.get('save_slips',False)


        num_sta=len([elem.get('Longitud') for elem in self.data_estaciones])
        nx=self.grid_size[0]
        ny=self.grid_size[1]

        # mu1=0
        # sigma1=1.5
        # mu2=0
        # sigma2=0.75
        # #s1=np.random.normal(mu1,sigma1,self.grid_size[0]*self.grid_size[1])
        # #s2=np.random.normal(mu2,sigma2,self.grid_size[0]*self.grid_size[1])
        # s1=np.zeros(nx*ny)
        # s2=np.zeros(nx*ny)
        # #p1A=np.linspace(0,30,nx*ny/2)
        # p1A=np.ones(nx*ny)
        # #p2A=np.linspace(30,0,nx*ny/2)
        # #ṕ2A=np.ones(30)
        # pA=p1A

        # slipA=[x+y for x,y in zip(pA,s1)]

        # p1B=np.ones(nx*ny)
        # #p2B=np.linspace(15,0,nx*ny/2)
        # pB=p1B
        # slipB=[x+y for x,y in zip(pB,s2)]

        slipA=[]
        for i in range(nx*ny):
            if i%2 == 0:
                slipA.append(1)
            if i%2 == 1:
                slipA.append(0)

        if interfases =='double':
            label='interfaz_doble'
            UeA,UnA,UzA = self.pdirecto(slipA,plano='A',save_slips=save_slips)
            UeB,UnB,UzB = self.pdirecto(slipB,plano='B',save_slips=save_slips)
            Ue_teo=[]
            Un_teo=[]
            Uz_teo=[]


            for i  in range(num_sta):
                Ue_teo.append(1000*(UeA[i] +  UeB[i]))
                Un_teo.append(1000*(UnA[i] +  UnB[i]))
                Uz_teo.append(1000*(UzA[i] +  UzB[i]))


        if interfases =='single':
            print('pan')
            label='interfaz_single'
            print(slipA)
            Ue_teok,Un_teok,Uz_teok = self.pdirecto(slipA,plano='A',save_slips=save_slips)
            
            Ue_teo=[1000*x  for x in Ue_teok]
            Un_teo=[1000*x for x in Un_teok]
            Uz_teo=[1000*x for x in Uz_teok]



        self.desplazamientos_sinteticos.update({'UE': Ue_teo,
                                                'UN': Un_teo,
                                                'UU': Uz_teo})

        print(Ue_teo,Un_teo,Uz_teo)

        lon = [elem.get('Longitud') for elem in self.data_estaciones]
        lat = [elem.get('Latitud') for elem in self.data_estaciones]
        sta = [elem.get('Station') for elem in self.data_estaciones]


        ## archivo de movimientos sinteticos
        name_archivo='Outputs'+ self.id_proceso +self.proceso+ '/Cosismico/directo_components_'+label+'.txt'
        with open(name_archivo,'w') as csvfile:
            writer=csv.writer(csvfile,delimiter=';')
            writer.writerow( ['Station','Longitud','Latitud'] + list(self.desplazamientos_sinteticos.keys()) )
            for row in zip(sta,lon,lat,Ue_teo,Un_teo,Uz_teo):
                writer.writerow(row)


        return Ue_teo,Un_teo,Uz_teo


 # subfalla = 0
        # for iy in range(ny):
        #     for ix in range(nx):
        #         tap = slip[subfalla]
        #         if ix == 0 and iy == 0:
        #             ddx, ddy = self.proj_mesh2okada(lat_vert_subf_slab[subfalla][0],
        #                                             lon_vert_subf_slab[subfalla][0],
        #                                             np.radians(strike_subf_deg[subfalla])
        #                                             )
        #             ue,un,uz= desplaz_okada(ddx,
        #                                     ddy,
        #                                     dip_subf_rad[subfalla],
        #                                     prof_subfallas[subfalla],
        #                                     w,
        #                                     l,
        #                                     rake_subfallas[subfalla],
        #                                     np.radians(strike_subf_deg[subfalla])
        #                                     )
        #             print('caco')
        #             ve = tap*ue
        #             vn = tap*un
        #             vz = tap*uz
        #             print(ddx,ddy)
        #             print('malva')
        #             print(ve,vn,vz)
        #         else:
        #             print('alma') 
        #             ddx, ddy = self.proj_mesh2okada(lat_vert_subf_slab[subfalla][0],
        #                                             lon_vert_subf_slab[subfalla][0],
        #                                             np.radians(strike_subf_deg[subfalla])
        #                                             )
        #             ue,un,uz= desplaz_okada(ddx,
        #                                     ddy,
        #                                     dip_subf_rad[subfalla],
        #                                     prof_subfallas[subfalla],
        #                                     w,
        #                                     l,
        #                                     rake_subfallas[subfalla],
        #                                     np.radians(strike_subf_deg[subfalla])
        #                                     )
        #             ve += tap*ue
        #             vn += tap*un
        #             vz += tap*uz
        #         subfalla+=1



    def pdirecto(self,slip,plano='A',save_slips=False):

        nx = self.grid_size[0]
        ny = self.grid_size[1]
        l = self.L/nx
        #w = self.ancho_placa/ny

        ## UN SLIP FICTICIOS XD
        #slip=[3]*nx*ny

        #subfallas_methods = self.planos_falla_obj.get('subfallas_methods')



        prof_subf_inv=self.subfallas_model_dict.get('depth_model')
        strike_subf_deg=self.subfallas_model_dict.get('strike_model')
        dip_subf_rad=np.radians(self.subfallas_model_dict.get('dip_model'))
        lon_vert_subf_slab=self.subfallas_model_dict.get('lon_vertices')
        lat_vert_subf_slab=self.subfallas_model_dict.get('lat_vertices')
        lon_cent_subf_slab=self.subfallas_model_dict.get('lon_central')
        lat_cent_subf_slab=self.subfallas_model_dict.get('lat_central')
        strike_rad=self.subfallas_model_dict.get('strike_rad')
        prof_subf_norm=prof_subf_inv+self.alto_placa/np.cos(dip_subf_rad)
        rake_inv_rad=self.subfallas_model_dict.get('rake_inv_rad')
        rake_norm_rad=self.subfallas_model_dict.get('rake_norm_rad')
        

        print('lon_cent')
        print(lon_cent_subf_slab)
        print('lat_cent')



        if plano == 'A':
            prof_subfallas = prof_subf_inv
            rake_subfallas = rake_inv_rad
        if plano == 'B':
            prof_subfallas = prof_subf_norm
            rake_subfallas = rake_norm_rad
        #print('MEGIDOLA')
        #print('lat_vert_subf_slab')
        #print(lat_vert_subf_slab)
        #print('lon_vert_subf_slab')
        #print(lon_vert_subf_slab)
        #print('prof_subf_inv')
        #print(prof_subf_inv)
        #print('lon_cent_subf_slab')
        #print(lon_cent_subf_slab)
        #print('lat_cent_subf_slab')
        #print(lat_cent_subf_slab)
        #print('dip_subf_deg')
        #print(np.degrees(dip_subf_rad))
        #print('rake_inv_deg')
        #print(np.degrees(rake_inv_rad))
        #print('strike_deg')
        #print(np.degrees(strike_rad))
        subfalla = 0
        ve=[]
        vn=[]
        vz=[]
        for iy in range(ny):
            for ix in range(nx):
                ## in this case we use real w because its and input for green functions
                ## in following function
                w = self.ancho_placa/ny
                tap = slip[subfalla]
                ddx, ddy = self.proj_mesh2okada(lat_vert_subf_slab[subfalla][0],
                                                lon_vert_subf_slab[subfalla][0],
                                                np.radians(strike_subf_deg[subfalla]))
                

                #print('okada')
                #print(dip_subf_rad[subfalla],
                 #     prof_subfallas[subfalla],
                 #     w,
                 #     l,
                 #     rake_subfallas[subfalla],
                 #     np.radians(strike_subf_deg[subfalla]))
                     
                                                     
                ue,un,uz= desplaz_okada(ddx,
                                        ddy,
                                        dip_subf_rad[subfalla],
                                        1000*prof_subfallas[subfalla],
                                        w,
                                        l,
                                        rake_subfallas[subfalla],
                                        np.radians(strike_subf_deg[subfalla])
                                        )
                #print('caco')
                ve.append(tap*ue)
                vn.append(tap*un)
                vz.append(tap*uz)
                #print(ddx,ddy)
                #print('malva')
                #print(ve,vn,vz)
                subfalla+=1


       

       
        if plano=='A' and save_slips:
            label='upper'
            name_archivo='Outputs'+ self.id_proceso +self.proceso+ '/Cosismico/coseismic_slip_'+label+'_interface.txt'


            with open(name_archivo,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['lon_central','lat_central','slip_'+label]  )
                for row in zip(lon_cent_subf_slab,lat_cent_subf_slab,slip):
                    writer.writerow(row)

        if plano=='B' and save_slips:
            label='lower'
            name_archivo='Outputs'+ self.id_proceso +self.proceso+ '/Cosismico/coseismic_slip_'+label+'_interface.txt'


            with open(name_archivo,'w') as csvfile:
                writer=csv.writer(csvfile,delimiter=';')
                writer.writerow( ['lon_central','lat_central','slip_'+label]  )
                for row in zip(lon_cent_subf_slab,lat_cent_subf_slab,slip):
                    writer.writerow(row)

        return sum(ve),sum(vn),sum(vz)
                        
    def inversion_cosismico_slab(self):
        pass

    def inversion_postsismico_slab(self):
        pass
        
   # def inversion_intersismico_slab(self, interfases='double'):
    def gen_matriz_suavidad(self):
        subfallas_methods=self.planos_falla_obj.get('subfallas_methods')
        dip_subf_rad=subfallas_methods[1]

        nx=self.grid_size[0]
        ny=self.grid_size[1]
        n=nx*ny
        F = np.zeros(n*n).reshape(n,n)
        pos = []
        coef = []
        subfalla = 0

        for j in range(ny):
            ## primera fila
            if j == 0: 
                for i in range(nx):
                    dx=self.L / nx
                    #dy =self.ancho_placa/np.cos(dip_subf_rad[subfalla]) / ny
                    dy =self.ancho_placa/ny
                    dxi = 1.0 / (dx*dx) 
                    dyi = 1.0 / (dy*dy)
                    #print(dx)
                    #print(dy)
                    #print(dyi)
                    #print(dxi)
                    dr = dyi/dxi
                    #print(dr)
                    ## vertice inferior izquierdo
                    if i == 0:
                       print('jauaj')
                       pos.append(np.array([ (i+1,j), (i+2,j), (i,j+1), (i,j+2), (i,j) ]))
                       coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
                    ## vertice inferior derecho
                    elif i == nx-1 :
                       pos.append(np.array([ (i-1,j), (i-2,j), (i,j+1), (i,j+2), (i,j) ]))
                       coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
                    ## arista inferior
                    else :
                       pos.append(np.array([ (i+1,j), (i-1,j), (i,j+1), (i,j+2), (i,j) ]))
                       coef.append(np.array([ 1. , 1. , -2.*dr , dr , -2. + dr ]))
                    subfalla = subfalla + 1
            ## ultima fila
            elif j == ny-1 : 
                for i in range(nx):
                ##################################################################
                    dx = self.L / nx
                    #dy = self.ancho_placa / np.cos(dip_subf_rad[subfalla]) / ny
                    dy = self.ancho_placa/ ny  
                      
                    
                    dxi = 1.0 / (dx*dx)
                    dyi = 1.0 / (dy*dy)
                    dr = dyi / dxi 
                ###################################################################                
                    ## vertice superior izquierdo
                    if i == 0 :
                        pos.append(np.array([ (i+1,j), (i+2,j), (i,j-1), (i,j-2), (i,j) ]))
                        coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))
                    ## vertice superior derecho
                    elif i == nx-1 :
                        pos.append(np.array([ (i-1,j), (i-2,j), (i,j-1), (i,j-2), (i,j) ]))
                        coef.append(np.array([ -2. , 1. , -2.*dr , dr , 1 + dr ]))             
                    ## arista superior
                    else :
                        pos.append(np.array([ (i+1,j), (i-1,j), (i,j-1), (i,j-2), (i,j) ]))
                        coef.append(np.array([ 1. , 1. , -2.*dr , dr , -2. + dr ]))
                    subfalla = subfalla +1
            else :         #<------------ filas restantes        
                for i in range(nx):
                ##################################################################
                    dx = self.L / nx
                    #dy = self.ancho_placa * np.cos(dip_subf_rad[subfalla]) / ny
                    dy = self.ancho_placa / ny  
  
                    dxi = 1.0 / (dx*dx)
                    dyi = 1.0 / (dy*dy)
                    dr = dyi / dxi 
                ###################################################################
                    ## arista izquierda
                    if i == 0:
                        pos.append(np.array([ (i+1,j), (i+2,j), (i,j+1), (i,j-1), (i,j) ]))
                        coef.append(np.array([ -2. , 1. , dr , dr , 1. - 2.*dr ]))
                    ## arista derecha
                    elif i == nx-1 :
                        pos.append(np.array([ (i-1,j), (i-2,j), (i,j+1), (i,j-1), (i,j) ]))
                        coef.append(np.array([ -2. , 1. , dr , dr , 1. - 2.*dr ]))             
                    ## punto central (general )
                    else :
                        pos.append(np.array([ (i+1,j), (i-1,j), (i,j+1), (i,j-1), (i,j) ]))
                        coef.append(np.array([ 1. , 1. , dr , dr , -2. - 2.*dr ]))

                    subfalla = subfalla + 1
        print('naca')
        print(pos,coef)
        for k in range(nx*ny):
            index = pos[k]
            m = 0
            print(index)
            for i,j in index:
                F[k][j*nx+i] = coef[k][m]
                m += 1
            return F

    def inversion_intersismico_slab(self, **kwargs):
        """ we invert the coseismic data  and obtain the slips in every subfault
        , with these slips, we generate synthethic displacementes in surface and compute
        the mean cuadratic error"""

        ## en realidad es inversion cosismico... per no es muy  relevante la diferencia 
        nx=self.grid_size[0]
        ny=self.grid_size[1]
        regul_kind=kwargs.get('how2regularize','min_smooth')

        interfases=kwargs.get('interfases','double')
        A=self.planos_falla_obj.get('A')
        print('A')
        print(np.shape(A))

        if interfases == 'double':
            B=self.planos_falla_obj.get('B')


        #prof_subf_inv=subfallas_methods[0]
        # strike_subf_deg=subfallas_methods[2]
        # lonv_vert_subf_slab=subfallas_methods[3]
        # lat_vert_subf_slab=subfallas_methods[4]
        # lon_cent_subf_slab=subfallas_methods[5]
        # lat_cent_subf_slab=subfallas_methods[6]
        # prof_subf_norm=subfallas_methods[7]
        # phi_placa_rad=subfallas_methods[8]
        # rake_inv_rad=subfallas_methods[9]
        # rake_norm_rad=subfallas_methods[10]
        Ue_obs= [elem.get('UE') for elem in self.data_estaciones]
        Un_obs = [elem.get('UN') for elem in self.data_estaciones]
        Uz_obs = [elem.get('UU') for elem in self.data_estaciones]
       
       
                     

        ## we build the Identity Matrix which is used for constraining
        ## minimum displacements in our solution
    
        if regul_kind ==  'smooth':
           F=self.gen_matriz_suavidad()
           regul_array=F
        if regul_kind == 'minimize':
           I=np.identity(nx*ny)
           regul_array=I
        if regul_kind == 'min_smooth':
            I = np.identity(nx*ny)
            F = self.gen_matriz_suavidad()
            regul_array=[I,F]


        if self.n_dim == 1:
            U_obs = Ue_obs
        if self.n_dim == 2:
            U_obs = np.hstack((Ue_obs,Un_obs))
        if self.n_dim == 3:
            U_obs = np.hstack((Ue_obs,Un_obs,Uz_obs))
        print('hosen')
        print(np.shape(A))
        print(len(np.shape(A)))
        if len(np.shape(A)) == 1:
            A=A.reshape((len(A),1))
        print(np.shape(A))
        if not self.condicion_borde_fosa:
            ceros = np.zeros(nx*ny)
            delta = U_obs
            stack = lambda x,l1,l2: np.vstack((l1*x[0],l2*x[1]))
            if interfases =='single':
                if regul_kind == 'min_smooth':
                    r_matrix = stack(regularray,self.lmbd1,self.lmbd2)
                    A_m= np.vstack((A,r_matrix))
                    Uobs_m = np.hstack((delta,ceros,ceros))
                if regul_kind == 'minimize':
                    print('arpegio')
                    r_matrix = 0.25*regul_array
                    A_m= np.vstack((A,r_matrix))
                    Uobs_m = np.hstack((delta,ceros))
                    print('shape')
                    print(A_m)
                    print(Uobs_m)
                    print(np.shape(A_m))
                    print(np.shape(Uobs_m))
                if regul_kind == 'smooth':
                    r_matrix  = self.lmbd2*regul_array
                    A_m= np.vstack((A,r_matrix))
                    Uobs_m = np.hstack((delta,ceros))
                    print('shape')
                    print(np.shape(A_m))
                    print(np.shape(Uobs_m))
                if regul_kind == 'none':
                    print('wey')
                    A_m=A
                    Uobs_m = U_obs
                    print(np.shape(A_m))
                    print(np.shape(Uobs_m))


            if interfases == 'double':
                if regul_kind == 'min_smooth':
                    print('arpegio')
                    r1 = stack(regul_array,self.lmbd2,self.lmbd4)
                    A_m= np.vstack((A,r_matrix))
                    Uobs_m = np.hstack((delta,ceros))
                    print('shape')
                    print(A_m)
                    print(Uobs_m)
                    print(np.shape(A_m))
                    print(np.shape(Uobs_m))
                if regul_kind == 'minimize':
                    A_m= np.vstack((A,self.lmbd1*regul_array))
                    B_m=np.vstack(B,self.lmbd3*regul_array)
                    G=np.hstack(A_m,B_m)
                    Uobs_m = np.hstack((delta,ceros))
                if regul_kind == 'smooth':
                    r_matrix  = self.lmbd2*regul_array
                    A_m= np.vstack((A,r_matrix))
                    Uobs_m = np.hstack((delta,ceros))
                    print('shape')
                    print(np.shape(A_m))
                    print(np.shape(Uobs_m))
                if regul_kind == 'none':
                    print('wey')
                    A_m=A
                    Uobs_m = U_obs
                    print(np.shape(A_m))
                    print(np.shape(Uobs_m))


               

            # if interfases =='double':
            #     if regul_kind == 'min_smooth':
            #         r_matrix = stack(regularray,self.lmbd1,self.lmbd2)
            #         Uobs_m = np.hstack((delta,ceros,ceros))
            #     if regul_kind == 'minimize':
            #         r_matrix = self.lmbd1*regul_array
            #         Uobs_m = np.hstack((delta,ceros))
            #     if regul_kind == 'smooth':
            #         r_matrix  = self.lmbd2*regul_array
            #         ceros_matrix = np.vstack((ceros))
            #         Uobs_m = np.hstack((delta,ceros))




        # if not self.condicion_borde_fosa :
        #     ## coseismic, only A and B(if apply) contribute to the deformation
        #     ceros = np.zeros(nx*ny)
        #     ## solo contribuye las fallas A y B
        #     #delta = U_obs - (np.dot(C,Vp_CD) + np.dot(D,Vp_CD) + np.dot(E,Vp_E))
        #     delta = U_obs
        #     stackear = lambda x,y: np.vstack((x,y)) 


        #     ## our system is Gm= U, we build G and u
            
         
        #     if interfases == 'double':
        #         ## if smoothing matrix has been built:
        #         if self.makeF:
        #             A_m=np.hstack((A,self.lmbd1*I,self.lmbd2*F,0))
        #             B_m=np.hstack((B,self.lmbd3*I,0,self.lmbd4*F))
        #             Uobs_m=np.hstack((delta,ceros,ceros,ceros))
        #         ## if no smooth matrix
        #         if not self.makeF:
        #             A_m=np.hstack((A,self.lmbd1*I))
        #             B_m=np.hstack((B,self.lmbd3*I))
        #             Uobs_m=np.hstack((delta,ceros))
        #     if interfases == 'single':
        #         if self.makeF:
        #             A_m=np.hstack((A,self.lmbd1*I,self.lmbd2*F))
        #             Uobs_m=np.hstack((delta,ceros,ceros))
        #         if not self.makeF:
        #             print('cangri')
        #             print(np.shape(A),np.shape(I))
        #             try:

        #                 A_m=np.vstack((A,0.02*I))
        #                 Uobs_m=np.hstack((delta,ceros))
        #             except ValueError:
        #                 print('inverting only one subfault, must reshape matrix')
        #                 A=A.reshape((len(A),1))
        #                 A_m=np.vstack((A,0.7*I))
        #                 Uobs_m=np.hstack((delta,ceros))


       


        else:
            #interseismic, will fix this later
            print('Setting the boundary condition in the trench ')
             #Condicion de borde lateral (la de las velocidades intersismicas)
       
            g = []
            for jy in range(ny):
                for ix in range(nx):
                    if jy == (ny-1) or jy==0:
                        g.append(1)
                    else:
                        g.append(0)
                   
            g = np.array(g)
            G = np.zeros(nx*ny*nx*ny).reshape(nx*ny,nx*ny) 
            np.fill_diagonal(G,g) 
       
       
            V = np.ones(nx*ny)
            for i in range(nx*ny):
                 V[i] = self.velocidad_placa

      
       # '''
       # ###########################################################################
       # Se constuye igualdad del sistema de ecuaciones: Termino derecho
       # ###########################################################################
       # '''
            if interfases == 'double':
                ceros   = np.zeros(nx*ny)
                print("C:",C,
                      "Vp:",Vp_CD,
                      "D:",D,
                      "E:",E,
                      "Vp_E:",Vp_E)
                delta   = U_obs - (np.dot(C,Vp_CD) + np.dot(D,Vp_CD) + np.dot(E,Vp_E))  
                #Uobs_m  = hstack((delta,ceros,ceros,ceros,ceros))  
                Uobs_m  = np.hstack((delta,ceros,ceros,ceros,V,V))  
   
       # '''
       # ###########################################################################
       # Se construye termino izquierdo del sistema de ecuaciones
       # ###########################################################################
       # '''
     

                A_m = np.vstack((A, self.lmbd1*I,self.lmbd2*F, F*0,G,G*0))  
                B_m = np.vstack((B, self.lmbd3*I,    F*0, self.lmbd4*F,G*0,G)) 

            if interfases == 'single':
                ceros = np.zeros(nx*ny)
                delta = U_obs
                Uobs_m = np.hstack((delta,ceros))
                A_m=np.vstack((A,self.lmbd1*I))
        #print('B')
        #print(B)

        Ue_teo_total = None
        Un_teo_total = None
        Uz_teo_total = None
        ECM_Ue = None
        ECM_Un = None
        ECM_Uz = None
        horror_total = None
        horror_suave = None

        if interfases == 'double':
            AA=np.hstack((A_m,B_m))
            xx = scp.optimize.nnls(AA,Uobs_m)
            slip = np.array([xx[0]])[0]
            AB=np.hstack((A,B))
            horror = np.linalg.norm( np.dot(AB,slip) - U_obs) 
            horror_slip = np.linalg.norm( slip )
            FF=np.hstack((F,F))
            horror_suave = dxi*np.linalg.norm( np.dot(FF,slip))

            ## se realiza el problema directo

            sl_oka_inv=[]
            sl_oka_norm=[]
            k = self.grid_size[0]*self.grid_size[1]

            for i in range(nx*ny):
                sl_oka_inv.append(slip[i])
                sl_oka_norm.append(slip[i+k])
            
            # para falla inversa
            Ue_teo_inv,Un_teo_inv,Uz_teo_inv=self.pdirecto(sl_oka_inv,plano='A')
            # para falla normal
            Ue_teo_norm,Un_teo_norm, Uz_teo_norm=self.pdirecto(sl_oka_norm,plano='B')

            Ue_teo_total=[x + y for x,y in zip(Ue_teo_inv,Ue_teo_norm) ]
            Un_teo_total=[x + y for x,y in zip(Un_teo_inv,Un_teo_norm) ] 
            Uz_teo_total=[x + y for x,y in zip(Uz_teo_inv,Uz_teo_norm) ]
            print('malilla')
            print(Ue_teo_total)
            print(Un_teo_total)

     
            ## getting the mean squared error (MSE)

            delta_Ue= [ x - y for x,y in zip(Ue_obs,Ue_teo_total) ]
            delta_Un= [ x - y for x,y in zip(Un_obs,Un_teo_total) ]

            ECM_Ue=sum([x**2 for x in delta_Ue])/len(delta_Ue)
            ECM_Un=sum([x**2 for x in delta_Un])/len(delta_Un)

            if self.n_dim == 3:    
                delta_Uz= [ x - y for x,y in zip(Uz_obs,Uz_teo_total) ]
                ECM_Uz=sum([x**2 for x in delta_Uz])/len(delta_Uz)

            ##  total residual
            horror_total=horror + self.lmbd1*horror_slip + self.lmbd2*horror_suave        
 
               

        if interfases == 'single':
            AA=A_m
            print('bode')
            print(np.shape(Uobs_m))
            xx = scp.optimize.nnls(AA,Uobs_m)
            print(xx)
            slip = np.array([xx[0]])[0]
            print(slip)
            horror = np.linalg.norm( np.dot(A,slip) - U_obs) 
            horror_slip = np.linalg.norm( slip )
            if self.makeF:
                FF=F
                horror_suave = dxi*np.linalg.norm( np.dot(FF,slip) )  

            ## making direct problem with the inverted slip 
            Ue_teo_total,Un_teo_total,Uz_teo_total=self.pdirecto(slip,plano='A')

            delta_Ue= [ x - y for x,y in zip(Ue_obs,Ue_teo_total) ]
            delta_Un= [ x - y for x,y in zip(Un_obs,Un_teo_total) ]

            ECM_Ue=sum([x**2 for x in delta_Ue])/len(delta_Ue)
            ECM_Un=sum([x**2 for x in delta_Un])/len(delta_Un)

            if self.n_dim == 3:    
                delta_Uz= [ x - y for x,y in zip(Uz_obs,Uz_teo_total) ]
                ECM_Uz=sum([x**2 for x in delta_Uz])/len(delta_Uz)

            ## total residual
            if self.makeF:
                horror_total=horror + self.lmbd1*horror_slip + self.lmbd2*horror_suave
            if not self.makeF:
                horror_total=horror + self.lmbd1*horror_slip

        if self.filtro == 'cosismico':
            ## desplazamientos invertidos a partir de un problema directo
            self.outputs_inv_cosism.update({'slips_subfallas':slip,
                                            'horror': horror,
                                            'horror_slip': horror_slip,
                                            'horror_suave': horror_suave,
                                            'horror_total': horror_total,
                                            'UE': Ue_teo_total,
                                            'UN': Un_teo_total,
                                            'UU': Uz_teo_total,
                                            'ECM_Ue': ECM_Ue,
                                            'ECM_Un': ECM_Un,
                                            'ECM_Uz': ECM_Uz,
                                            })




    def load_slip_sint(self,interfaz='superior'):
        if interfaz == 'superior':
            name='coseismic_slip_upper_interface.txt'
            slip='slip_upper'
        if interfaz == 'inferior':
            name='coseismic_slip_lower_interface.txt'
            slip='slip_lower'


        filename='Outputs'+self.id_proceso+'/Directo/Cosismico/'+name
        data_type = { 
                     'Longitud': float,
                     'Latitud': float,
                     slip: float,
                     }
        data_slip = cut_file(filename,data_type)
        print(data_slip)
        lon_fallas =[float(x.get('lon_central')) for x in data_slip]
        lat_fallas =[float(x.get('lat_central')) for x in data_slip]
        slips=[float(x.get(slip)) for x in data_slip]
        slip_array=np.array(slips)[::-1].reshape(self.grid_size[0],self.grid_size[1])
        return slip_array
        # print(slip_array)
        # plt.matshow(slip_array)
        # plt.colorbar()





    def plot_slips(self,interfaz='double'):
        if interfaz == 'single':
            slip_inv=self.outputs_inv_cosism.get('slips_subfallas')
            print('sing')
            slip_inv_array=np.array(slip_inv)[::-1].reshape(self.grid_size[0],self.grid_size[1])
            slip_dir_array=self.load_slip_sint(interfaz='superior')
            print(slip_dir_array)
            print(slip_inv_array)


         

            fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(3, 6))
            print('axes')
            print(axs)
            im1=axs[0].matshow(slip_dir_array,cmap='viridis')
            fig.colorbar(im1,ax=axs[0],ticks=[1])
            axs[0].set_title('Directo')
            im2=axs[1].matshow(slip_inv_array,cmap='viridis')
            fig.colorbar(im2,ax=axs[1],ticks=[1])
            axs[1].set_title('Inverso')




            # for ax, matrix,title in zip(axs.flat, (slip_dir_array,slip_inv_array),['directa','inversa']):
            #     im=ax.matshow(matrix,cmap='viridis')
            #     ax.set_title(title)

            # # cax = plt.axes([0.85, 0.1, 0.075, 0.8])
            # # plt.colorbar(fig)
            # fig.colorbar(im,ax=axs)
            # plt.show()



            # plt.subplot(121)
            # plt.matshow(slip_dir_array)
            # plt.colorbar()
            # plt.subplot(122)
            # plt.matshow(slip_inv_array)
            # plt.colorbar()
            # plt.tight_layout()
            # plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
            # plt.show()


    def almacenar_resultados(self,**kwargs):

        interfases=kwargs.get('interfases','double')
        if self.filtro == 'cosismico':
            data_output = self.outputs_inv_cosism

        if self.filtro == 'intersismico':
            data_output = self.outputs_inv_intersism
        lon_sta = [elem.get('Longitud') for elem in self.data_estaciones]
        lat_sta = [elem.get('Latitud') for elem in self.data_estaciones]
        name_sta = [elem.get('Station') for elem in self.data_estaciones]

        Ue_inv=data_output.get('UE')
        Ue_inv=[1000*x for  x in Ue_inv]
        Un_inv=data_output.get('UN')
        Un_inv=[1000*x for  x in Un_inv]
        Uz_inv=data_output.get('UU')
        Uz_inv=[1000*x for  x in Uz_inv]


        data_sta={}
        data_sta.update({'Latitud':lat_sta})
        data_sta.update({'Longitud': lon_sta})
        data_sta.update({'Station': name_sta})

        #print('pata')
        #print(data_output)
        #print('poto')
        #print(data_sta)
        merged_dict={**data_sta,**data_output}
        #print('caldo')
        #print(merged_dict)
        subfallas_methods = self.planos_falla_obj.get('subfallas_methods')

        # prof_subf_inv=subfallas_methods[0]
        # dip_subf_rad=subfallas_methods[1]
        # strike_subf_deg=subfallas_methods[2]
        # lonv_vert_subf_slab=subfallas_methods[3]
        # lat_vert_subf_slab=subfallas_methods[4]
        # lon_cent_subf_slab=subfallas_methods[5]
        # lat_cent_subf_slab=subfallas_methods[6]
        # prof_subf_norm=subfallas_methods[7]
        # phi_placa_rad=subfallas_methods[8]
        # rake_inv_rad=subfallas_methods[9]
        # rake_norm_rad=subfallas_methods[10]

        prof_subf_inv=self.subfallas_model_dict.get('depth_model')
        strike_subf_deg=self.subfallas_model_dict.get('strike_model')
        dip_subf_rad=np.radians(self.subfallas_model_dict.get('dip_model'))
        lonv_vert_subf_slab=self.subfallas_model_dict.get('lon_vertices')
        lat_vert_subf_slab=self.subfallas_model_dict.get('lat_vertices')
        lon_cent_subf_slab=self.subfallas_model_dict.get('lon_central')
        lat_cent_subf_slab=self.subfallas_model_dict.get('lat_central')
        strike_rad=self.subfallas_model_dict.get('strike_rad')
        prof_subf_norm=prof_subf_inv+self.alto_placa/np.cos(dip_subf_rad)
        rake_inv_rad=self.subfallas_model_dict.get('rake_inv_rad')
        rake_norm_rad=self.subfallas_model_dict.get('rake_norm_rad')
        print('lala')
        print(prof_subf_inv)

        print(' -----------------------------------------------------------------------')
        print('Resumen de datos: ')
        print('1. lambda1      : ', self.lmbd1)
        print('2. lambda2      : ', self.lmbd2)
        print('3. lambda3      : ', self.lmbd3)
        print('4. lambda4      : ', self.lmbd4)
        print('5. Inv. ECM_Ue  : ', data_output.get('ECM_Ue'))
        print('6. Inv. ECM_Un  : ', data_output.get('ECM_Un'))
        print('7. Res. ||U-AS||: ', data_output.get('horror'))
        print('8. Res. ||S||   : ', data_output.get('horror_slip'))
        print('9. Res. ||FS||  : ', data_output.get('horror_suave'))
        print('10. Res. Total  : ', data_output.get('horror_total'))
        print(' -----------------------------------------------------------------------')
        print(self.desplaz_output)


        with open(self.desplaz_output,'w') as csvfile:
            writer = csv.writer(csvfile, delimiter= ';')
            writer.writerow(['Station','Longitud','Latitud','UE','UN','UU'])
            for row in zip(name_sta,
                            lon_sta,
                            lat_sta,
                            Ue_inv,
                            Un_inv,
                            Uz_inv):

                writer.writerow(row)

        # with open(self.desplaz_output,'w') as csvfile:
        #     if self.n_dim == 2:
        #         #wanted_keys=['Ue_obs','Un_obs','Ue_teo','Un_teo','Longitud','Latitud']
        #          wanted_keys=['Station','Latitud','Longitud','UE','UN','UU']
        #     if self.n_dim == 3:
        #         #wanted_keys=['Ue_obs','Un_obs','Uz_obs','Ue_teo','Un_teo','Uz_teo','Longitud','Latitud']
        #          wanted_keys=['Station','Latitud','Longitud','UE','UN','UU']
        #     #dict( (k,a[k]) for k in wanted_keys if k in a)
        #     wanted_dict= dict((k,merged_dict[k]) for k in wanted_keys)
        #     column_names=[x for x in  wanted_dict.keys()]
        #     writer = csv.writer(csvfile, delimiter = ';')
        #     writer.writerow(column_names)
        #     for row in wanted_dict.values():
        #          writer.writerow(row)

        if interfases == 'double':
            slip_upper = []
            slip_lower = []
            k = reduce(lambda x, y: x*y, self.grid_size)
            #k = self.grid_size[0]*self.grid_size[1]
            slips_invertidos= data_output.get('slips_subfallas')
            print('slips')
            print('k:', k)

            print(slips_invertidos)
            for i in range(k):
                slip_upper.append(slips_invertidos[i])
                slip_lower.append(slips_invertidos[i+k])


            with open(self.lower_interface_output,'w') as csvfile:
                writer = csv.writer(csvfile, delimiter = ';')
                writer.writerow(['lon_central','lat_central','slip_lower'])
                for row in zip(lon_cent_subf_slab,
                               lat_cent_subf_slab,
                               slip_lower):
                    writer.writerow(row)

            
            with open(self.upper_interface_output,'w') as csvfile:
                writer = csv.writer(csvfile, delimiter = ';')
                writer.writerow(['lon_central','lat_central','slip_upper'])
                for row in zip(lon_cent_subf_slab,
                               lat_cent_subf_slab,
                               slip_upper):
                    writer.writerow(row)

        if interfases == 'single':
            slip_lower= 'placeholder'
            slip_upper = data_output.get('slips_subfallas')
            with open(self.upper_interface_output,'w') as csvfile:
                writer = csv.writer(csvfile, delimiter = ';')
                writer.writerow(['lon_central','lat_central','slip_upper'])
                for row in zip(lon_cent_subf_slab,
                               lat_cent_subf_slab,
                               slip_upper):
                    writer.writerow(row)



 #Se almacenan datos de la generación de la inversión:
        name= self.inversion_params
        file = open(name, "w")
        file.write( ' -----------------------------------------------------------------------')
        file.write('\n')
        file.write( 'Resumen de datos: ')
        file.write('\n')
        file.write( '1. lambda1      : '+str(self.lmbd1))
        file.write('\n')
        file.write( '2. lambda2      : '+str(self.lmbd2))
        file.write('\n')
        file.write( '3. lambda3      : '+str(self.lmbd3))
        file.write('\n')
        file.write( '4. lambda4      : '+str(self.lmbd4))
        file.write('\n')
        file.write( '5. Inv. ECM_Ue  : '+ str(data_output.get('ECM_Ue')))
        file.write('\n')
        file.write( '6. Inv. ECM_Un  : '+str(data_output.get('ECM_Un')))
        file.write('\n')
        file.write( '6. Inv. ECM_Uz  : '+str(data_output.get('ECM_Uz')))
        file.write('\n')
        file.write( '7. Res. ||U-AS||: '+str(data_output.get('horror')))
        file.write('\n')
        file.write( '8. Res. ||S||   : '+str(data_output.get('horror_slip')))
        file.write('\n')
        file.write( '9. Res. ||FS||  : '+str(data_output.get('horror_suave')))
        file.write('\n')
        file.write( '10. Res. Total  : '+str(data_output.get('horror_total')))
        file.write('\n')
        file.write( ' -----------------------------------------------------------------------')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( ' ETAPA  9 == RESUMEN PARAMETROS GEOMETRICOS Y RESULTADOS')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( '                                                        ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE SUPERIOR (A):                                 ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')

       
        file.write('\n')
        file.write( 'Min Depth: '+str(min(prof_subf_inv))+' km')
        file.write('\n')
        file.write( 'Max Depth: '+str(max(prof_subf_inv))+' km')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Dip  : '+str(min(np.degrees(dip_subf_rad)))+' degrees')
        file.write('\n')
        file.write( 'Max Dip  : '+str(max(np.degrees(dip_subf_rad)))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Strike : '+str(min(strike_subf_deg))+' degrees')
        file.write('\n')
        file.write( 'Max Strike : '+str(max(strike_subf_deg))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Rake  : '+str(min(np.degrees(rake_inv_rad)))+' degrees')
        file.write('\n')
        file.write( 'Max Rake  : '+str(max(np.degrees(rake_inv_rad)))+' degrees')
        file.write('\n')
        file.write( '                                                        ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE INFERIOR (B):')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'Min Depth: '+str(min(prof_subf_norm))+' km')
        file.write('\n')
        file.write( 'Max Depth: '+str(max(prof_subf_norm))+' km')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Dip  : '+str(min(np.degrees(dip_subf_rad)))+' degrees')
        file.write('\n')
        file.write( 'Max Dip  : '+str(max(np.degrees(dip_subf_rad)))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Strike : '+str(min(strike_subf_deg))+' degrees')
        file.write('\n')
        file.write( 'Max Strike : '+str(max(strike_subf_deg))+' degrees')
        file.write('\n')
        file.write( '                                                              ')
        file.write('\n')
        file.write( 'Min Rake  : '+str(min(np.degrees(rake_norm_rad)))+' degrees')
        file.write('\n')
        file.write( 'Max Rake  : '+str(max(np.degrees(rake_norm_rad)))+' degrees')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( '          VELOCIDADES INVERTIDAS                         ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE SUPERIOR (A):                                 ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'Max. Vel: '+str(max(slip_upper))+'m/yr')
        file.write('\n')
        file.write( 'Min. Vel: '+str(min(slip_upper))+'m/yr')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'INTERFASE INFERIOR (B):                                 ')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'Max. Vel: '+str(max(slip_lower))+'m/yr')
        file.write('\n')
        file.write( 'Min. Vel: '+str(min(slip_lower))+'m/yr')
        file.write('\n')
        file.write( ' -------------------------------------------------------')
        file.write('\n')
        file.write( 'nx x ny: '+str(self.grid_size[0])+str('X')+str(self.grid_size[1]))
        file.write('\n')
        file.write( 'rake_inv_ref_deg: '+str(self.rake_referencia))
        #file.write('\n')
        #file.write( 'Angulo convergencia: '+str(math.degrees(math.atan(Uy_v/Ux_v)))+str('°'))
        file.write('\n')
        file.write( 'V_placa: '+str(self.velocidad_placa))
        file.write('\n')
        file.write( 'W :'+str(self.ancho_placa))
        file.write('\n')
        file.write( 'H :'+str(self.alto_placa))
        file.close()
    ## variables usadas (vertices lon, vertices lat, nx, ny selfs, strike_rad, )
    def subfallas_slabmodel(self):
        strike_rad= None
        if self.sentido_subduccion == 'EW':
            strike_rad = np.radians(self.strike_aux_deg+180)
        if self.sentido_subduccion == 'WE':
            strike_rad = np.radians(self.strike_aux_deg)

        lon_central,lat_central,vertices_lon,vertices_lat=self.coordenadas_subfallas()
        
        strike=self.get_points_from_model(vertices_lon,vertices_lat,key='Strike')
        depth=self.get_points_from_model(vertices_lon,vertices_lat,key='Profundidad')
        #dip=self.get_points_from_model(vertices_lon,vertices_lat,key='Dip')


        dip,lon_vert_model,lat_vert_model,lon_central_model,lat_central_model=self.reescribir_subfallas(strike_rad,
                                                                                                        vertices_lon,
                                                                                                        vertices_lat)
        #print('strike',strike)
        #print('depth',depth)
        phi_placa_rad = np.radians(np.degrees(strike_rad) + (360-(self.rake_referencia+180)))

        ## rake for inverse fault
        rake_inv_rad  = []
        ## rake for normal fault
        rake_norm_rad = []

        for index, elem in enumerate(strike):
            # Se establece rake en subfallas emplazadas en la
            # interfase superior con un movimiento relativo de tipo inverso
            rake = get_rake(
                            phi_placa_rad,
                            np.radians(elem),
                            np.radians(dip[index])
                            )
            rake_inv_rad.append(rake)
            # Se establece rake en subfallas emplazadas en la interfase
            # inferior con un movimiento relativo de tipo normal
            rake_norm_rad.append(rake_inv_rad[index]+np.pi)
        

        self.subfallas_model_dict.update({'lon_central':lon_central_model,
                                    'lat_central':lat_central_model,
                                    'lon_vertices':lon_vert_model,
                                    'lat_vertices':lat_vert_model,
                                    'dip_model':dip,
                                    'strike_model':strike,
                                    'depth_model':depth,
                                    'strike_rad':strike_rad,
                                    'rake_inv_rad':rake_inv_rad,
                                    'rake_norm_rad':rake_norm_rad,
                                    'phi_placa_rad':phi_placa_rad})

        # self.subfallas_model_dict.update({'lon_central':lon_vert_model,
        #                             'lat_central':lat_central,
        #                             'lon_vertices':vertices_lon,
        #                             'lat_vertices':vertices_lat,
        #                             'dip_model':dip,
        #                             'strike_model':strike,
        #                             'depth_model':depth,
        #                             'strike_rad':strike_rad,
        #                             'rake_inv_rad':rake_inv_rad,
        #                             'rake_norm_rad':rake_norm_rad,
        #                             'phi_placa_rad':phi_placa_rad})




    def reescribir_subfallas(self,strike,vertices_lon,vertices_lat):
        dip=self.get_points_from_model(vertices_lon,vertices_lat,key='Dip')
        dip_rad=np.radians(dip)
        nx = self.grid_size[0]
        ny = self.grid_size[1]
        #Se evaluaran todas las subfallas.
        FIL=nx*ny
        #Cada subfalla tiene cinco vertices a dibujar (el ultimo coincide con el primero)
        COL=5                  

        #Se declaran variables para almacenar vertices de todas las subfallas.
        vert_lat_new    = []
        vert_lon_new    = []
        lat_central_new = []    
        lon_central_new = []

        #Se crean matrices de ceros para rellenar con los vertices de todas las subfallas.
        for i in range(FIL):                #Numero de filas de la matriz
            vert_lon_new.append([0]*COL)    #Numero de columnas de la matriz, se inicializa con ceros
            vert_lat_new.append([0]*COL)

        alpha = np.degrees(strike) 
        beta = alpha + 270.0


        for subfalla in range(nx*ny):
                w = self.ancho_placa / ny
                #dy = w * np.sin((np.pi/2)-dip_rad[subfalla])
                dy = w*np.cos(dip_rad[subfalla])
                #Vertice 1
                vert_lon_new[subfalla][0] = vertices_lon[subfalla][0]
                vert_lat_new[subfalla][0] = vertices_lat[subfalla][0]
        
                #Vertice 2
                vert_lon_new[subfalla][1] = vertices_lon[subfalla][1]
                vert_lat_new[subfalla][1] = vertices_lat[subfalla][1]

                #Vertice 3.
                vert_lat_new[subfalla][2], vert_lon_new[subfalla][2], aux = vinc_pt( vert_lat_new[subfalla][1],
                                                                                     vert_lon_new[subfalla][1],
                                                                                      beta ,
                                                                                       dy )

                vert_lat_new[subfalla][3], vert_lon_new[subfalla][3], aux = vinc_pt( vert_lat_new[subfalla][0],
                                                                                     vert_lon_new[subfalla][0],
                                                                                     beta ,
                                                                                     dy )
 
                #Vertice 4.
                vert_lon_new[subfalla][4] = vert_lon_new[subfalla][0]
                vert_lat_new[subfalla][4] = vert_lat_new[subfalla][0]
 

                lat1, lon1, aux = vinc_pt( vert_lat_new[subfalla][0], vert_lon_new[subfalla][0], alpha , (self.L/nx)/2 )
                lat2, lon2, aux = vinc_pt( lat1, lon1, beta , dy/2 )
       
                lat_central_new.append(lat2)
                lon_central_new.append(lon2)
        print('chamagol')
        print(vert_lon_new)
        print(vertices_lon)


        return dip,vert_lon_new, vert_lat_new, lon_central_new, lat_central_new,


    def get_points_from_model(self,vertices_lon,vertices_lat,key='Profundidad'):

        ## number of columns for interpolation data
        ## number of rows for interpolation data
        
        if key not in ['Profundidad','Strike','Dip']:
            print('La llave ha sido mal ingresada')
            

        lats=[ elem.get('Latitud') for elem in self.data_falla.get(key.lower()) if str(elem.get(key)) != 'nan' ]
        lons=[ elem.get('Longitud') for elem in self.data_falla.get(key.lower()) if str(elem.get(key)) != 'nan']
        data=[ elem.get(key) for elem in self.data_falla.get(key.lower()) if str(elem.get(key)) != 'nan']

        ## points used for interpolating the studied area
        #(lon,lat) <-> (x,y) in these scripts
        #print('lons',lats)
        xi_start=np.linspace(min(lons),max(lons),self.numcols)
        yi_start=np.linspace(min(lats),max(lats),self.numrows)

       
        lon_points=[]
        lat_points=[]
        for subfalla in range(self.grid_size[0]*self.grid_size[1]):
            lon_points.append(vertices_lon[subfalla][0])
            lat_points.append(vertices_lat[subfalla][0])

        xi_new=[]
        yi_new=[]
        
        ## we group all the data sets
        for i in range(len(lon_points)):
            xi_new.append(lon_points[i])
            yi_new.append(lat_points[i])

        for i in range(len(xi_start)):
            xi_new.append(xi_start[i])
            yi_new.append(yi_start[i])


        ## interpolating data
        xi,yi=np.meshgrid(xi_new,yi_new)
        #print('xd')
        #print(xi,yi)
        zi=scp.interpolate.griddata((np.array(lons),np.array(lats)),
                                     np.array(data),
                                     (xi,yi),
                                     method='nearest')

        ## we seach for the data of interest
        POS_xi=[]
        POS_yi=[]

        ## coordinates of latitude
        for i in range(len(vertices_lat)):
            flag = True
            for j in range(len(yi)):
                if yi[j][0] == lat_points[i] and flag == True:
                    POS_yi.append(j)
                    flag = False
    
        #coordinates of longitude        
        for i in range(len(vertices_lon)):
            flag = True
            for j in range(len(xi)):
                if xi[0][j] == lon_points[i] and flag == True:
                    POS_xi.append(j)    
                    flag = False
    
        
        ## we extract data from slabmodel

        data_output = []
    
        for i in range(len(POS_yi)):
            if key in ['Profundidad','Dip']:
                data_output.append(-zi[POS_yi[i]][POS_xi[i]])
            if key == 'Strike':
                data_output.append(zi[POS_yi[i]][POS_xi[i]])

        return data_output



def desplaz_okada(xi,yi,dip,d,w,l,rake,strike):
    #La funcion determina la caracterizacion geometrica para una determinada falla.
    # notacion  de Chinnery: f(e,eta)||= f(x,p)-f(x,p-W)-f(x-L,p)+f(x-L,W-p)
    p = yi*np.cos(dip) + d*np.sin(dip)                                       
    q = yi*np.sin(dip) - d*np.cos(dip)                                       
    e = np.array([xi,xi,xi-l,xi-l]).T
    eta = np.array([p,p-w,p,p-w]).T       
    #qq array de q, hay cuatro valores porque se opera con los cuatro valores de eta.               
    qq = np.array([q,q,q,q]).T                    
    ytg = eta*np.cos(dip) + qq*np.sin(dip)           
    dtg = eta*np.sin(dip) - qq*np.cos(dip)           
    R = np.power(e**2 + eta**2 + qq**2, 0.5)     
    X = np.power(e**2 + qq**2, 0.5)               

    I5 = (1/np.cos(dip))*scp.arctan((eta*(X+qq*np.cos(dip))+X*(R+X)*np.sin(dip))/(e*(R+X)*np.cos(dip))) 
    I4 = 0.5/np.cos(dip)*(scp.log(R+dtg)-np.sin(dip)*scp.log(R+eta)) 
    # Solido de Poisson mu=lambda
    I1 = 0.5*((-1./np.cos(dip))*(e/(R+dtg)))-(np.sin(dip)*I5/np.cos(dip)) 
    I3 = 0.5*(1/np.cos(dip)*(ytg/(R+(dtg)))-scp.log(R+eta))+(np.sin(dip)*I4/np.cos(dip))  
    I2 = 0.5*(-scp.log(R+eta))-I3   #ok

    # dip-slip  (en direccion del manteo)
    ux_ds = -np.sin(rake)/(2*np.pi)*(qq/R-I3*np.sin(dip)*np.cos(dip))  #o
    uy_ds = -np.sin(rake)/(2*np.pi)*((ytg*qq/R/(R+e))+(np.cos(dip)*scp.arctan(e*eta/qq/R))
                -(I1*np.sin(dip)*np.cos(dip))) 
    uz_ds = -np.sin(rake)/(2*np.pi)*((dtg*qq/R/(R+e))+(np.sin(dip)*scp.arctan(e*eta/qq/R))
                -(I5*np.sin(dip)*np.cos(dip))) 

    # strike-slip  (en direccion del strike)
    ux_ss = -np.cos(rake)/(2*np.pi)*((e*qq/R/(R+eta))+(scp.arctan(e*eta/(qq*R)))+I1*np.sin(dip))  
    uy_ss = -np.cos(rake)/(2*np.pi)*((ytg*qq/R/(R+eta))+qq*np.cos(dip)/(R+eta)+I2*np.sin(dip))       
    uz_ss = -np.cos(rake)/(2*np.pi)*((dtg*qq/R/(R+eta))+qq*np.sin(dip)/(R+eta)+I4*np.sin(dip))
        
    # representacion chinnery dip-slip
    uxd = ux_ds.T[0]-ux_ds.T[1]-ux_ds.T[2]+ux_ds.T[3]   
    uyd = uy_ds.T[0]-uy_ds.T[1]-uy_ds.T[2]+uy_ds.T[3]   
    uzd = uz_ds.T[0]-uz_ds.T[1]-uz_ds.T[2]+uz_ds.T[3]   

    # representacion chinnery strike-slip
    uxs = ux_ss.T[0]-ux_ss.T[1]-ux_ss.T[2]+ux_ss.T[3]  
    uys = uy_ss.T[0]-uy_ss.T[1]-uy_ss.T[2]+uy_ss.T[3]  
    uzs = uz_ss.T[0]-uz_ss.T[1]-uz_ss.T[2]+uz_ss.T[3]  

    # solucion 
    ux = uxd+uxs  
    uy = uyd+uys  
    uz = uzd+uzs 
    # proyeccion a las componentes geograficas
    # rotacion de sistema de coordenadas 
    Ue = ux*np.sin(strike) - uy*np.cos(strike) 
    Un = ux*np.cos(strike) + uy*np.sin(strike) 
    
    #El gran problema es que falta el vector de deslizamiento, pero se podria agregar despues (multiplicando)

    return Ue,Un,uz

def get_rake(p_placa, p_falla, d_falla):
 #INPUT
 #  p_placa = strike asociado al vector de convergencia de la placa en radianes
 #  p_falla = strike de la subfalla en radianes
 #  d_falla = dip de la subfalla en radianes
 #OUTPUT    
 #  rake    = rake de la subfalla en radianes para la interface superior con un
 #            movimiento relativo de tipo inverso.  
 a = np.sin(p_falla) - np.tan(p_placa) * np.cos(p_falla)
 b = np.tan(p_placa) * np.cos(d_falla) * np.sin(p_falla) + np.cos(d_falla) * np.cos(p_falla)  

 rake = np.arctan2(a,b) + np.pi
 
 return rake 


def vinc_dist(  phi1,  lembda1,  phi2,  lembda2 ) :
        """ 
        Returns the distance between two geographic points on the ellipsoid
        and the forward and reverse azimuths between these points.
        lats, longs and azimuths are in decimal degrees, distance in metres 

        Returns ( s, alpha12,  alpha21 ) as a tuple
        """

        f = 1.0 / 298.257223563     # WGS84
        a = 6378137.0           # metres
        if (abs( phi2 - phi1 ) < 1e-8) and ( abs( lembda2 - lembda1) < 1e-8 ) :
                return 0.0, 0.0, 0.0

        piD4   = math.atan( 1.0 )
        two_pi = piD4 * 8.0

        phi1    = phi1 * piD4 / 45.0
        lembda1 = lembda1 * piD4 / 45.0     # unfortunately lambda is a key word!
        phi2    = phi2 * piD4 / 45.0
        lembda2 = lembda2 * piD4 / 45.0

        b = a * (1.0 - f)

        TanU1 = (1-f) * math.tan( phi1 )
        TanU2 = (1-f) * math.tan( phi2 )

        U1 = math.atan(TanU1)
        U2 = math.atan(TanU2)

        lembda = lembda2 - lembda1
        last_lembda = -4000000.0        # an impossibe value
        omega = lembda

        # Iterate the following equations, 
        #  until there is no significant change in lembda 

        while ( last_lembda < -3000000.0 or lembda != 0 and abs( (last_lembda - lembda)/lembda) > 1.0e-9 ) :

                sqr_sin_sigma = pow( np.cos(U2) * np.sin(lembda), 2) + \
                        pow( (np.cos(U1) * np.sin(U2) - \
                        np.sin(U1) *  np.cos(U2) * np.cos(lembda) ), 2 )

                Sin_sigma = np.sqrt( sqr_sin_sigma )

                Cos_sigma = np.sin(U1) * np.sin(U2) + np.cos(U1) * np.cos(U2) * np.cos(lembda)
        
                sigma = math.atan2( Sin_sigma, Cos_sigma )

                Sin_alpha = np.cos(U1) * np.cos(U2) * np.sin(lembda) / np.sin(sigma)
                alpha = math.asin( Sin_alpha )

                Cos2sigma_m = np.cos(sigma) - (2 * np.sin(U1) * np.sin(U2) / pow(np.cos(alpha), 2) )

                C = (f/16) * pow(np.cos(alpha), 2) * (4 + f * (4 - 3 * pow(np.cos(alpha), 2)))

                last_lembda = lembda

                lembda = omega + (1-C) * f * np.sin(alpha) * (sigma + C * np.sin(sigma) * \
                        (Cos2sigma_m + C * np.cos(sigma) * (-1 + 2 * pow(Cos2sigma_m, 2) )))

        u2 = pow(np.cos(alpha),2) * (a*a-b*b) / (b*b)

        A = 1 + (u2/16384) * (4096 + u2 * (-768 + u2 * (320 - 175 * u2)))

        B = (u2/1024) * (256 + u2 * (-128+ u2 * (74 - 47 * u2)))

        delta_sigma = B * Sin_sigma * (Cos2sigma_m + (B/4) * \
                (Cos_sigma * (-1 + 2 * pow(Cos2sigma_m, 2) ) - \
                (B/6) * Cos2sigma_m * (-3 + 4 * sqr_sin_sigma) * \
                (-3 + 4 * pow(Cos2sigma_m,2 ) )))

        s = b * A * (sigma - delta_sigma)

        alpha12 = math.atan2( (np.cos(U2) * np.sin(lembda)), \
                (np.cos(U1) * np.sin(U2) - np.sin(U1) * np.cos(U2) * np.cos(lembda)))

        alpha21 = math.atan2( (np.cos(U1) * np.sin(lembda)), \
                (-np.sin(U1) * np.cos(U2) + np.cos(U1) * np.sin(U2) * np.cos(lembda)))

        if ( alpha12 < 0.0 ) : 
                alpha12 =  alpha12 + two_pi
        if ( alpha12 > two_pi ) : 
                alpha12 = alpha12 - two_pi

        alpha21 = alpha21 + two_pi / 2.0
        if ( alpha21 < 0.0 ) : 
                alpha21 = alpha21 + two_pi
        if ( alpha21 > two_pi ) : 
                alpha21 = alpha21 - two_pi

        alpha12    = alpha12    * 45.0 / piD4
        alpha21    = alpha21    * 45.0 / piD4
        return s, alpha12,  alpha21 

   # END of Vincenty's Inverse formulae 


#-------------------------------------------------------------------------------
# Vincenty's Direct formulae                            |
# Given: latitude and longitude of a point (phi1, lembda1) and          |
# the geodetic azimuth (alpha12)                        |
# and ellipsoidal distance in metres (s) to a second point,         |
#                                       |
# Calculate: the latitude and longitude of the second point (phi2, lembda2)     |
# and the reverse azimuth (alpha21).                        |
#                                       |
#-------------------------------------------------------------------------------

def  vinc_pt( phi1, lembda1, alpha12, s ) :
        """

        Returns the lat and long of projected point and reverse azimuth
        given a reference point and a distance and azimuth to project.
        lats, longs and azimuths are passed in decimal degrees

        Returns ( phi2,  lambda2,  alpha21 ) as a tuple 

        """
 
        f = 1.0 / 298.257223563     # WGS84
        a = 6378137.0           # metres
        piD4 = math.atan( 1.0 )
        two_pi = piD4 * 8.0

        phi1    = phi1    * piD4 / 45.0
        lembda1 = lembda1 * piD4 / 45.0
        alpha12 = alpha12 * piD4 / 45.0
        if ( alpha12 < 0.0 ) : 
                alpha12 = alpha12 + two_pi
        if ( alpha12 > two_pi ) : 
                alpha12 = alpha12 - two_pi

        b = a * (1.0 - f)

        TanU1 = (1-f) * math.tan(phi1)
        U1 = math.atan( TanU1 )
        sigma1 = math.atan2( TanU1, np.cos(alpha12) )
        Sinalpha = np.cos(U1) * np.sin(alpha12)
        cosalpha_sq = 1.0 - Sinalpha * Sinalpha

        u2 = cosalpha_sq * (a * a - b * b ) / (b * b)
        A = 1.0 + (u2 / 16384) * (4096 + u2 * (-768 + u2 * \
                (320 - 175 * u2) ) )
        B = (u2 / 1024) * (256 + u2 * (-128 + u2 * (74 - 47 * u2) ) )

        # Starting with the approximation
        sigma = (s / (b * A))

        last_sigma = 2.0 * sigma + 2.0  # something impossible

        # Iterate the following three equations 
        #  until there is no significant change in sigma 

        # two_sigma_m , delta_sigma
        two_sigma_m = 0
        while ( abs( (last_sigma - sigma) / sigma) > 1.0e-9 ) :
                two_sigma_m = 2 * sigma1 + sigma

                delta_sigma = B * np.sin(sigma) * ( np.cos(two_sigma_m) \
                        + (B/4) * (np.cos(sigma) * \
                        (-1 + 2 * pow( np.cos(two_sigma_m), 2 ) -  \
                        (B/6) * np.cos(two_sigma_m) * \
                        (-3 + 4 * pow(np.sin(sigma), 2 )) *  \
                        (-3 + 4 * pow( np.cos (two_sigma_m), 2 ))))) \

                last_sigma = sigma
                sigma = (s / (b * A)) + delta_sigma

        phi2 = math.atan2 ( (np.sin(U1) * np.cos(sigma) + np.cos(U1) * np.sin(sigma) * np.cos(alpha12) ), \
                ((1-f) * np.sqrt( pow(Sinalpha, 2) +  \
                pow(np.sin(U1) * np.sin(sigma) - np.cos(U1) * np.cos(sigma) * np.cos(alpha12), 2))))

        lembda = math.atan2( (np.sin(sigma) * np.sin(alpha12 )), (np.cos(U1) * np.cos(sigma) -  \
                np.sin(U1) *  np.sin(sigma) * np.cos(alpha12)))

        C = (f/16) * cosalpha_sq * (4 + f * (4 - 3 * cosalpha_sq ))

        omega = lembda - (1-C) * f * Sinalpha *  \
                (sigma + C * np.sin(sigma) * (np.cos(two_sigma_m) + \
                C * np.cos(sigma) * (-1 + 2 * pow(np.cos(two_sigma_m),2) )))

        lembda2 = lembda1 + omega

        alpha21 = math.atan2 ( Sinalpha, (-np.sin(U1) * np.sin(sigma) +  \
                np.cos(U1) * np.cos(sigma) * np.cos(alpha12)))

        alpha21 = alpha21 + two_pi / 2.0
        if ( alpha21 < 0.0 ) :
                alpha21 = alpha21 + two_pi
        if ( alpha21 > two_pi ) :
                alpha21 = alpha21 - two_pi

        phi2       = phi2       * 45.0 / piD4
        lembda2    = lembda2    * 45.0 / piD4
        alpha21    = alpha21    * 45.0 / piD4

        return phi2,  lembda2,  alpha21 

  
